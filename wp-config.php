<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kitchen_blog' );

/** MySQL database username */
define( 'DB_USER', 'kitchen' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Cx+Ys8jFqb;CVDE+' );

/** MySQL hostname */
define( 'DB_HOST', 'rds-kitchen-prod-26-02-20.cvfyuvgmpb33.us-east-1.rds.amazonaws.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'UJlg:^D|<HH(F+U-WU}k+FJ|U)vP_hBdnyoeC:JI8t236o62ALmD(_:K.6GM^5#M' );
define( 'SECURE_AUTH_KEY',  'NVOHIiXI-|N+7co^[U6Dnd*A@eOG>CB*Oi6|/D1_0M{EX>&?gFjJQI3 hF&9;v]L' );
define( 'LOGGED_IN_KEY',    ';>R_R)^`<60^[0sV}duI>Af#b4-`m^z9kNQ@%k2/:vPI]uUJLEc&y}k2xIjN|a-!' );
define( 'NONCE_KEY',        'e]#`HsIiO,=PIjc?BYW!N!,0Nbc&lgiy}dEB/C3vAx/>=`%Mji^`9OHX(nD;d@2w' );
define( 'AUTH_SALT',        '!:7/B*o>((u>1QVWz_wZb3cea2wYwZM?BK.GGr(iSo4TX&:wG75re.Sch8hrt6c@' );
define( 'SECURE_AUTH_SALT', 'V7dK`2F|9{&K-@=:2y4+O>&eA<DjI5.ctY,qJNj;+90eB{c::?~A[PPOgJa6EECf' );
define( 'LOGGED_IN_SALT',   '<q@_2X~DT49U0ecOAtv5d4+#|#w!z*qmp*uf$hogX]#q FN@#Rc}*]I^@)RF{X3.' );
define( 'NONCE_SALT',       '9HuD$`O>DBd0y^HLBp[Xh?T3^Y+sUgQsYtxO0vf)Ye[_);h%E){Fj>$t`Z]uyF;k' );
define('FS_METHOD','direct');



/* SSL Settings */
define('FORCE_SSL_ADMIN', true);

/* Turn HTTPS 'on' if HTTP_X_FORWARDED_PROTO matches 'https' */
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false) {
    $_SERVER['HTTPS'] = 'on';
}


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

define( 'UPLOADS', 'wp-content/uploads' );
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
