<form action="<?php echo esc_url( home_url('/') );?>">
    <input name="s" value="<?php echo esc_attr( get_search_query() ); ?>" type="text" class="search-fld" placeholder="<?php esc_attr_e( 'Search Keyword here ...', 'fire-blog' ) ?>">
    <button class="search-btn" type="submit" id="searchbtn">
    	<i class="fa fa-search" aria-hidden="true"></i>
    </button>
</form>