<?php
/**
 * Template part for displaying related post as slider
 *
 */
$nto = new independentThemeOpt;
$author_max = $nto->independentCheckMetaValue( 'independent_post_author_max_posts', 'author-max-posts' );
$post_id = get_the_ID();
$author_id = get_the_author_meta( 'ID' );
$author_related_title = apply_filters( "independent_related_title", esc_html__( "Author Articles", "independent" ) );
?>
<div class="related-articles-wrap">
	<div class="related-title-wrap">
		<h4 class="related-title"><?php echo esc_html( $author_related_title ); ?></h4>
	</div>
	<?php
		$shortcode = '[independent_vc_block_7 animate_type="left" ajax_filter="yes" author="'. esc_attr( $author_id ) .'" post_per_tab="'. esc_attr( $author_max ) .'" grid_title_variation="h6" grid_thumb_size="independent_grid_3" grid_items="{``Enabled``:{``image``:``Image``,``title``:``Title``},``disabled``:{``primary-meta``:``Primary Meta``,``secondary-meta``:``Secondary Meta``,``content``:``Content``}}" grid_primary_meta="{``Left``:{``author``:``Author Icon``,``date``:``Date``},``Right``:{``comments``:``Comments``},``disabled``:{``author-with-image``:``Author Image``,``read-more``:``Read More``,``likes``:``Like``,``views``:``View``,``favourite``:``Favourite``,``category``:``Category``,``share``:``Share``,``rating``:``Rating``}}" grid_secondary_meta="{``Left``:{``likes``:``Like``},``Right``:{``read-more``:``Read More``},``disabled``:{``author``:``Author Icon``,``date``:``Date``,``comments``:``Comments``,``author-with-image``:``Author Image``,``views``:``View``,``favourite``:``Favourite``,``category``:``Category``,``share``:``Share``,``rating``:``Rating``}}"]';
		
		echo do_shortcode( $shortcode );
	?>
</div>