<?php
$zozo_theme = wp_get_theme();
if($zozo_theme->parent_theme) {
    $template_dir =  basename( get_template_directory() );
    $zozo_theme = wp_get_theme($template_dir);
}
$zozo_theme_version = $zozo_theme->get( 'Version' );
$zozo_theme_name = $zozo_theme->get('Name');
$zozothemes_url = 'http://zozothemes.com/';
$ins_demo_stat = get_theme_mod( 'independent_demo_installed' );
$ins_demo_id = get_theme_mod( 'independent_installed_demo_id' );
?>
<div class="wrap about-wrap welcome-wrap zozothemes-wrap">
	<h1 class="hide" style="display:none;"></h1>
	<div class="zozothemes-welcome-inner">
		<div class="welcome-wrap">
			<h1><?php echo esc_html__( "Welcome to", "independent" ) . ' ' . '<span>'. $zozo_theme_name .'</span>'; ?>
			<p class="theme-logo"><span class="theme-version"><?php echo esc_attr( $zozo_theme_version ); ?></span></p></h1>
			
			<div class="zozo-updated zozo-importer-notice importer-notice-success regenerate-thumb"><p><strong><?php echo esc_html__( "Demo data successfully imported. Now, please install and run", "independent" ); ?> <a href="<?php echo esc_url( admin_url() );?>plugin-install.php?tab=plugin-information&amp;plugin=regenerate-thumbnails&amp;TB_iframe=true&amp;width=830&amp;height=472" class="thickbox" title="<?php echo esc_attr__( "Regenerate Thumbnails", "independent" ); ?>"><?php echo esc_html__( "Regenerate Thumbnails", "independent" ); ?></a> <?php echo esc_html__( "plugin once", "independent" ); ?>.</strong></p></div>
			
			<div class="about-text"><?php echo esc_html__( "Nice!", "independent" ) . ' ' . $zozo_theme_name . ' ' . esc_html__( "is now installed and ready to use. Get ready to build your site with more powerful WordPress theme. We hope you enjoy using it.", "independent" ); ?></div>
		</div>
		<h2 class="zozo-nav-tab-wrapper nav-tab-wrapper">
			<?php
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=independent' ),  esc_html__( "Support", "independent" ) );
			printf( '<a href="#" class="nav-tab nav-tab-active">%s</a>', esc_html__( "Install Demos", "independent" ) );
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=zozothemes-plugins' ), esc_html__( "Plugins", "independent" ) );
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=system-status' ), esc_html__( "System Status", "independent" ) );
			?>
		</h2>
	</div>
		
	 <div class="zozothemes-required-notices">
		<p class="notice-description warning-text"><?php echo esc_html__( "Installing a demo provides pages, posts, images, theme options, widgets and more. IMPORTANT: The required plugins need to be installed and activated before you install a demo.", "independent" ); ?></p>
	</div>
	<div class="zozothemes-demo-title">
		<h3 class="one-page"><?php esc_html_e( 'Independent Demos', 'independent'); ?></h3>
	</div>
	<div class="zozothemes-demo-wrapper">
		<div class="features-section theme-demos theme-browser rendered">
			<?php 
				
				//independent Main
				$demo_array = array(
					'demo_id' 	=> 'demo-main',
					'demo_name' => esc_html__( 'Independent', 'independent' ),
					'demo_img'	=> 'demo-1.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/independent/',
					'revslider'	=> '0'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Crypto Currency
				$demo_array = array(
					'demo_id' 	=> 'demo-cryptocurrency',
					'demo_name' => esc_html__( 'Cryptocurrency', 'independent' ),
					'demo_img'	=> 'demo-2.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/cryptocurrency/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Technology
				$demo_array = array(
					'demo_id' 	=> 'demo-technology',
					'demo_name' => esc_html__( 'Technology', 'independent' ),
					'demo_img'	=> 'demo-3.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/technology/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Travel
				$demo_array = array(
					'demo_id' 	=> 'demo-travel',
					'demo_name' => esc_html__( 'Travel', 'independent' ),
					'demo_img'	=> 'demo-4.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/travel/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Food
				$demo_array = array(
					'demo_id' 	=> 'demo-food',
					'demo_name' => esc_html__( 'Food', 'independent' ),
					'demo_img'	=> 'demo-5.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/food/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Daily News
				$demo_array = array(
					'demo_id' 	=> 'demo-daily-news',
					'demo_name' => esc_html__( 'Daily News', 'independent' ),
					'demo_img'	=> 'demo-6.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/daily-news/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Sports
				$demo_array = array(
					'demo_id' 	=> 'demo-sports',
					'demo_name' => esc_html__( 'Sports', 'independent' ),
					'demo_img'	=> 'demo-7.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/sports/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//General News
				$demo_array = array(
					'demo_id' 	=> 'demo-general-news',
					'demo_name' => esc_html__( 'General News', 'independent' ),
					'demo_img'	=> 'demo-8.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/general-news/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Organic
				$demo_array = array(
					'demo_id' 	=> 'demo-organic',
					'demo_name' => esc_html__( 'Organic', 'independent' ),
					'demo_img'	=> 'demo-9.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/organic/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Lifestyle
				$demo_array = array(
					'demo_id' 	=> 'demo-lifestyle',
					'demo_name' => esc_html__( 'Lifestyle', 'independent' ),
					'demo_img'	=> 'demo-10.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/lifestyle/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Business
				$demo_array = array(
					'demo_id' 	=> 'demo-business',
					'demo_name' => esc_html__( 'Business', 'independent' ),
					'demo_img'	=> 'demo-11.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/business/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Fitness
				$demo_array = array(
					'demo_id' 	=> 'demo-fitness',
					'demo_name' => esc_html__( 'Fitness', 'independent' ),
					'demo_img'	=> 'demo-12.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/fitness/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Fruits
				$demo_array = array(
					'demo_id' 	=> 'demo-fruits',
					'demo_name' => esc_html__( 'Fruits', 'independent' ),
					'demo_img'	=> 'demo-13.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/fruits/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Medical
				$demo_array = array(
					'demo_id' 	=> 'demo-medical',
					'demo_name' => esc_html__( 'Medical', 'independent' ),
					'demo_img'	=> 'demo-14.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/medical/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Personal Blog
				$demo_array = array(
					'demo_id' 	=> 'demo-weets',
					'demo_name' => esc_html__( 'Personal Blog', 'independent' ),
					'demo_img'	=> 'demo-15.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/weets/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//City Express Demo
				$demo_array = array(
					'demo_id' 	=> 'demo-citynews',
					'demo_name' => esc_html__( 'City Express', 'independent' ),
					'demo_img'	=> 'demo-16.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/citynews/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Game Demo
				$demo_array = array(
					'demo_id' 	=> 'demo-game3',
					'demo_name' => esc_html__( 'Game Blog', 'independent' ),
					'demo_img'	=> 'demo-17.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/game3/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Independent Times Demo
				$demo_array = array(
					'demo_id' 	=> 'demo-independent-times',
					'demo_name' => esc_html__( 'Independent Times', 'independent' ),
					'demo_img'	=> 'demo-18.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/independent-times/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//Photography Demo
				$demo_array = array(
					'demo_id' 	=> 'demo-photography-blog',
					'demo_name' => esc_html__( 'Photography Blog', 'independent' ),
					'demo_img'	=> 'demo-19.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/photography-blog/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
				//RTL Demo
				$demo_array = array(
					'demo_id' 	=> 'demo-rtl',
					'demo_name' => esc_html__( 'Demo RTL', 'independent' ),
					'demo_img'	=> 'demo-rtl.png',
					'demo_url'	=> 'https://magazine.zozothemes.com/demo-rtl/'
				);
				independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id);
				
			?>
			
		</div>
	</div>
	
	<div class="zozothemes-thanks">
        <hr />
    	<p class="description"><?php echo esc_html__( "Thank you for choosing", "independent" ) . ' ' . $zozo_theme_name . '.'; ?></p>
    </div>
</div>
<?php
function independent_demo_div_generater($demo_array, $ins_demo_stat, $ins_demo_id){
	$demo_class = '';
	if( $ins_demo_stat == 1 ){
		if( $ins_demo_id == $demo_array['demo_id'] ){
			$demo_class .= ' demo-actived';
		}else{
			$demo_class .= ' demo-inactive';
		}
	}else{
		$demo_class .= ' demo-active';
	}
	
	$revslider = isset( $demo_array['revslider'] ) && $demo_array['revslider'] != '' ? $demo_array['revslider'] : '';
	
?>
	<div class="theme zozothemes-demo-item<?php echo esc_attr( $demo_class ); ?>">
		<div class="demo-inner">
			<div class="theme-screenshot zozotheme-screenshot">
				<a href="<?php echo esc_url( $demo_array['demo_url'] ); ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() . '/admin/welcome-page/assets/images/demo/' . $demo_array['demo_img'] ); ?>" /></a>
			</div>
			<h3 class="theme-name" id="<?php echo esc_attr( $demo_array['demo_id'] ); ?>"><?php echo esc_attr( $demo_array['demo_name'] ); ?></h3>
			<div class="theme-actions theme-buttons">
				<a class="button button-primary button-install-demo" data-demo-id="<?php echo esc_attr( $demo_array['demo_id'] ); ?>" data-revslider="<?php echo esc_attr( $revslider ); ?>" href="#">
				<?php esc_html_e( "Install", "independent" ); ?>
				</a>
				<a class="button button-primary button-uninstall-demo" data-demo-id="<?php echo esc_attr( $demo_array['demo_id'] ); ?>" href="#">
				<?php esc_html_e( "Uninstall", "independent" ); ?>
				</a>
				<a class="button button-primary" target="_blank" href="<?php echo esc_url( $demo_array['demo_url'] ); ?>">
				<?php esc_html_e( "Preview", "independent" ); ?>
				</a>
			</div>
			
			<div class="theme-requirements" data-requirements="<?php 
				printf( '<h2>%1$s</h2> <p>%2$s</p> <h3>%3$s</h3> <ol><li>%4$s</li></ol>', 
					esc_html__( 'WARNING:', 'independent' ), 
					esc_html__( 'Importing demo content will give you pages, posts, theme options, sidebars and other settings. This will replicate the live demo. Clicking this option will replace your current theme options and widgets. It can also take a minutes to complete.', 'independent' ),
					esc_html__( 'DEMO REQUIREMENTS:', 'independent' ),
					esc_html__( 'Memory Limit of 128 MB and max execution time (php time limit) of 300 seconds.', 'independent' )
				);
			?>">
			</div>
			<div class="zozo-demo-import-loader zozo-preview-<?php echo esc_attr( $demo_array['demo_id'] ); ?>"><i class="dashicons dashicons-admin-generic"></i></div>
		</div>
		<div class="installation-progress">
			<p></p>
			<div class="progress" style="width:0%">
				<div class="progress-bar progress-bar-success progress-bar-striped active" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
			</div>
		</div>
	</div>
<?php
}