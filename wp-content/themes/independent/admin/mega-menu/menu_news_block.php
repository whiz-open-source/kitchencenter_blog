<?php 
/**
 *		# Menu News Block #
 * 	---- Megamenu News Block ----
 */
function independent_news_block_form_menu( $cat_ids = '', $ppp = '4' ){
	$rand = independent_shortcode_rand_id(); 
	$output = '';
	
	/*Design Option Start*/
	$css_class = $independent_block_styles = $block_styles = $title_bg = '';
	$css_class .= 'animate-news-fade-in-left';
	/*Design Option End*/
	
	/*Dynamic Options*/
	$grid_items = '{"Enabled":{"image":"Image","title":"Title"},"disabled":{"primary-meta":"Primary Meta","content":"Content","secondary-meta":"Secondary Meta"}}';
	$grid_primary_meta = '';	
	$grid_secondary_meta = '';
	
	$all_text = esc_html__( 'All', 'independent' );
	$filter_type = 'default';
	$grid_thumb_size = 'custom';
	$grid_image_custom = '232x154';
	$grid_align = '';
	$grid_title = 'h6';
	
	$css_class .= $filter_type != 'default' ? ' no-slide-verlay' : '';
	$filter = 'recent';
	$orderby = $meta_key = $days = $post_in = $post_not_in = '';
	$order = 'DESC';
		
	$filter_name = 'cat';
	$filter_values = $cat_ids; // cat id's
	$filter_values = str_replace(' ', '', $filter_values);
	$filter_values = rtrim($filter_values, ',');
	$tab_limit = count( explode( ",", $filter_values ) );
	
	
	$css_class .= $filter_values == '' || $tab_limit == 1 ? ' news-tab-none' : '';
	
	$post_per_tab = $ppp;
	$n_cols = $filter_values == '' || $tab_limit == 1 ? '5' : '4';
	
	$dynamic_options = array( 'grid_items' => $grid_items, 'grid_primary_meta' => $grid_primary_meta, 'grid_secondary_meta' => $grid_secondary_meta, 'modal' => 'megamenu', 'all_text' => $all_text, 'filter_type' => $filter_type, 'grid_thumb' => $grid_thumb_size, 'grid_image_custom' => $grid_image_custom, 'grid_align' => $grid_align, 'grid_title' => $grid_title, 'n_cols' => $n_cols, 'tab_hide' => 'no', 'post_icon' => 'no' );
	
	$slide_id = $all_text != '' ? 'all' : preg_replace('/^([^,]*).*$/', '$1', $filter_values);
	
	$independent_ajax_nonce = wp_create_nonce('independent-ajax-nonce');
	$block_options = array(
			'action' => 'independent-ajax-slide',
			'nonce' => $independent_ajax_nonce,
			'filter_name' => $filter_name, 
			'filter_values' => $filter_values, 
			'ppp' => $post_per_tab, 
			'paged' => 1, 
			'meta' => $meta_key,
			'orderby' => $orderby,
			'order' => $order,
			'date' => $days,
			'post_not' => $post_not_in,
			'post_in' => $post_in,
			'block_id' => $rand,
			'slide_id' => $slide_id,
			'tab_limit' => $tab_limit + 1,
			'dynamic_options' => $dynamic_options,
		);	
		
		$block_css_options = array(
			'block_style' => ''
		);			
	
	$nbp = new IndependentBlockParams;
	$nbp->setIndependentBlockParams( "independent_block_id_". esc_attr( $rand ), $block_options );
	
	$output = '<div class="independent-block '. esc_attr( $css_class ) .'" data-id="independent_block_id_'.esc_attr( $rand ).'">';
		$output .= '<input type="hidden" class="news-block-options" id="independent_block_id_'.esc_attr( $rand ).'" />';
		if( $tab_limit == 1 ){
			$output .= '<div class="independent-content">';
				$output .= '<div class="news-slide-loader"><img src="'. esc_url( independent_news_loader() ) .'" alt="'. esc_attr__('Loader..', 'independent') .'" /></div>';
				$output .= independent_news_block_slider($block_options);
			$output .= '</div> <!--independent-content-->';
		}else{
			$output .= independent_news_block_tabs($block_options);
		}
		$output .= '<div class="independent-slider-nav type-default">';
			$output .= '<ul class="slide-nav list-inline">';
				$output .= '<li><a href="#" class="independent-slider-previous disabled"><i class="fa fa-angle-left"></i></a></li>';
				$output .= '<li><a href="#" class="independent-slider-next"><i class="fa fa-angle-right"></i></a></li>';
			$output .= '</ul>';
		$output .= '</div> <!--news-slide-nav-->';	
	$output .= '</div>';
	
	return $output;
}
function independent_news_block_modal_megamenu($loop, $dynamic_options){
	$output = '';
	$i = 1;
	$n_cols = isset( $dynamic_options['n_cols'] ) && $dynamic_options['n_cols'] != '' ? absint( $dynamic_options['n_cols'] ) : '4';
	while ($loop->have_posts()){
		$loop->the_post();
		
			if( $i == 1 )
				$output .= '<div class="row">';
				
					$output .= '<div class="col-sm-2"><!--top col start-->';
						$output .= independent_common_block_grid_generate_megamenu($dynamic_options);
					$output .= '</div>';
			if( $i == $n_cols ){
				$output .= '</div>';
				$i = 0;
			}
			$i++;
	}
	
	if( $i != 1 ) $output .= '</div><!--row-->';
	
	return $output;
}
function independent_common_block_grid_generate_megamenu($dynamic_options){
	$block_class = isset( $dynamic_options['grid_align'] ) ? ' '. esc_attr( $dynamic_options['grid_align'] ) : '';
	$output = '<div class="post post-grid independent-block-post'. $block_class .' clearfix"><!--Menu News Start-->';
	$tit_var = isset( $dynamic_options['grid_title'] ) ? esc_attr( $dynamic_options['grid_title'] ) : '';
	
	$output .= independent_common_block_grid_tumb_megamenu($dynamic_options);
	$output .= independent_common_block_title($tit_var);
	$output .= '</div><!--Single Post End-->';
	
	return $output;	
}
function independent_common_block_grid_tumb_megamenu($dynamic_options){
	
	$output = '';
	
	if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ){
	
		$thumb_size = isset( $dynamic_options['grid_thumb'] ) ? esc_attr( $dynamic_options['grid_thumb'] ) : 'large';
		$custom_opt = '';
		if( $thumb_size == 'custom' ){
			$custom_opt = isset( $dynamic_options['grid_image_custom'] ) && $dynamic_options['grid_image_custom'] != '' ? explode( "x",  $dynamic_options['grid_image_custom'] ) : array();
		} 
		
		$img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt );
		$lazy_opt = independentThemeOpt::independentStaticThemeOpt('news-lazy-load');
		if( $lazy_opt ){
			$lazy_img = independentThemeOpt::independentStaticThemeOpt('news-lazy-load-img');
			if( isset( $lazy_img['id'] ) && $lazy_img['id'] != '' ){
				$pre_img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt, absint( $lazy_img['id'] ) );
				$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="'. esc_url( $pre_img_prop[0] ) .'" data-src="' . esc_url( $img_prop[0] ) . '" />';
			}else{
				$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="' . esc_url( $img_prop[0] ) . '" />';
			}
		}else{
			$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '" />';
		}
		$output .= '<div class="post-thumb-wrapper">';
			$output .= '<a href="'. esc_url( get_permalink() ) .'" rel="bookmark">';  
		
				$output .= $image;
				if( isset( $dynamic_options['post_icon'] ) && $dynamic_options['post_icon'] == 'yes' ){
					$format = get_post_format( get_the_ID() );
					$post_formats_array = array("image"=>"icon-picture", "video"=>"icon-camrecorder", "audio"=>"icon-music-tone-alt", "gallery"=>"icon-wallet", "quote"=>"fa fa-quote-right", "link"=>"icon-link");
					if( $format != '' ){
						if (array_key_exists($format, $post_formats_array)){
							$output .= '<span class="post-format-icon '. $post_formats_array[$format] .'">';
							if( $format == 'gallery' ){
								$img_ids = get_post_meta( get_the_ID(), 'independent_post_gallery', true );
								if( $img_ids != '' ){
									$img_count = count( explode( ",", $img_ids ) );
									$output .= '<span class="img-count">'. esc_html( $img_count ) .'</span>';
								}
							}
							$output .= '</span>';
						}
					}
				}
			$output .= '</a>';
		
			$categories = get_the_category(); 
			if ( ! empty( $categories ) ) $output .= '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" class="cat-tag-'. esc_attr( $categories[0]->term_id ) .' typo-white category-tag" >'. esc_attr( $categories[0]->name ) .'</a>';
	
		$output .= '</div>';
		
	} //has_post_thumbnail
	return $output;
}