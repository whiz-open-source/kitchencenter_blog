<?php
/* independent Page Options */
$prefix = 'independent_post_';
$fields = array(
	array( 
		'label'	=> esc_html__( 'Post General Settings', 'independent' ),
		'desc'	=> esc_html__( 'These all are single post general settings.', 'independent' ), 
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Post Layout', 'independent' ),
		'desc'	=> esc_html__( 'Choose post layout for current post single view.', 'independent' ), 
		'id'	=> $prefix.'layout',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'wide' => esc_html__( 'Wide', 'independent' ),
			'boxed' => esc_html__( 'Boxed', 'independent' )			
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Content Padding Option', 'independent' ),
		'id'	=> $prefix.'content_padding_opt',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Content Padding', 'independent' ), 
		'desc'	=> esc_html__( 'Set the top/right/bottom/left padding of post content.', 'independent' ),
		'id'	=> $prefix.'content_padding',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'space',
		'required'	=> array( $prefix.'content_padding_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Template Option', 'independent' ),
		'id'	=> $prefix.'template_opt',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Template', 'independent' ),
		'id'	=> $prefix.'template',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'image_select',
		'options' => array(
			'no-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/1.png' ), 
			'right-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/2.png' ), 
			'left-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/3.png' ), 
			'both-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/4.png' ), 
		),
		'default'	=> 'right-sidebar',
		'required'	=> array( $prefix.'template_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Left Sidebar', 'independent' ),
		'id'	=> $prefix.'left_sidebar',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'template_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Right Sidebar', 'independent' ),
		'id'	=> $prefix.'right_sidebar',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'template_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Sidebar On Mobile', 'independent' ),
		'id'	=> $prefix.'sidebar_mobile',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Show', 'independent' ),
			'0' => esc_html__( 'Hide', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Featured Slider', 'independent' ),
		'id'	=> $prefix.'featured_slider',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Full Width Wrap', 'independent' ),
		'id'	=> $prefix.'full_wrap',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Items Option', 'independent' ),
		'id'	=> $prefix.'items_opt',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Items', 'independent' ),
		'desc'	=> esc_html__( 'Needed single post items drag from disabled and put enabled part.', 'independent' ),
		'id'	=> $prefix.'items',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'dragdrop',
		'options' => array ( 
			'all' => array( 'title', 'top-meta', 'thumb', 'content', 'bottom-meta' ),
			'items' => array( 
				'title' 	=> esc_html__( 'Title', 'independent' ),
				'top-meta'	=> esc_html__( 'Top Meta', 'independent' ),
				'thumb' 	=> esc_html__( 'Thumbnail', 'independent' ),
				'content' 	=> esc_html__( 'Content', 'independent' ),
				'bottom-meta'		=> esc_html__( 'Bottom Meta', 'independent' )
			)
		),
		'default'	=> 'title,top-meta,thumb,content,bottom-meta',
		'required'	=> array( $prefix.'items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Overlay', 'independent' ),
		'id'	=> $prefix.'overlay_opt',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Overlay Items', 'independent' ),
		'desc'	=> esc_html__( 'Needed overlay post items drag from disabled and put enabled part.', 'independent' ),
		'id'	=> $prefix.'overlay_items',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'dragdrop',
		'options' => array ( 
			'all' => array( 'title', 'top-meta', 'bottom-meta' ),
			'items' => array( 
				'title' 	=> esc_html__( 'Title', 'independent' ),
				'top-meta'	=> esc_html__( 'Top Meta', 'independent' ),
				'bottom-meta'		=> esc_html__( 'Bottom Meta', 'independent' )
			)
		),
		'default'	=> 'title',
		'required'	=> array( $prefix.'overlay_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Page Items Option', 'independent' ),
		'id'	=> $prefix.'page_items_opt',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Page Items', 'independent' ),
		'desc'	=> esc_html__( 'Needed post page items drag from disabled and put enabled part.', 'independent' ),
		'id'	=> $prefix.'page_items',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'dragdrop',
		'options' => array ( 
			'all' => array( 'post-items', 'author-info', 'review', 'related-articles', 'author-articles', 'post-nav', 'comment' ),
			'items' => array( 
				'post-items' 	=> esc_html__( 'Post Items', 'independent' ),
				'author-info'	=> esc_html__( 'Author Info', 'independent' ),
				'review'	=> esc_html__( 'Review Info', 'independent' ),
				'related-articles'	=> esc_html__( 'Artículos relacionados', 'independent' ),
				'author-articles'	=> esc_html__( 'Artículos del autor', 'independent' ),
				'post-nav' 	=> esc_html__( 'Post Nav', 'independent' ),
				'comment' 	=> esc_html__( 'Comment', 'independent' )
			)
		),
		'default'	=> 'post-items,author-info,related-articles,post-nav,comment',
		'required'	=> array( $prefix.'page_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Artículos y columnas relacionadas', 'independent' ),
		'desc'	=> esc_html__( 'Enter related post maximum limit for get from posts query. Example 5.', 'independent' ), 
		'id'	=> $prefix.'related_max_posts',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Author Related Post Columns', 'independent' ),
		'desc'	=> esc_html__( 'Enter author related post maximum limit for get from posts query. Example 5.', 'independent' ), 
		'id'	=> $prefix.'author_max_posts',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Promocionar este artículo', 'independent' ),
		'desc'	=> esc_html__( 'Choose post yes to promote this post. It will show the promotion lable on overlay.', 'independent' ), 
		'id'	=> $prefix.'promotion_opt',
		'tab'	=> esc_html__( 'General', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'no' => esc_html__( 'No', 'independent' ),
			'yes' => esc_html__( 'Yes', 'independent' )
		),
		'default'	=> 'no'
	),
	//Header
	array( 
		'label'	=> esc_html__( 'Header General Settings', 'independent' ),
		'desc'	=> esc_html__( 'These all are header general settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Layout', 'independent' ),
		'desc'	=> esc_html__( 'Choose post layout for current post header layout.', 'independent' ), 
		'id'	=> $prefix.'header_layout',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'wide' => esc_html__( 'Wide', 'independent' ),
			'boxed' => esc_html__( 'Boxed', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Type', 'independent' ),
		'desc'	=> esc_html__( 'Choose post layout for current post header type.', 'independent' ), 
		'id'	=> $prefix.'header_type',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'default' => esc_html__( 'Default', 'independent' ),
			'left-sticky' => esc_html__( 'Left Sticky', 'independent' ),
			'right-sticky' => esc_html__( 'Right Sticky', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Background Image', 'independent' ),
		'desc'	=> esc_html__( 'Choose header background image for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'image',
		'id'	=> $prefix.'header_bg_img',
		'required'	=> array( $prefix.'header_type', 'default' )
	),
	array( 
		'label'	=> esc_html__( 'Header Items Options', 'independent' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
		'id'	=> $prefix.'header_items_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are header general items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_items',
		'dd_fields' => array ( 
			'Normal' => array( 
				'header-topbar' 	=> esc_html__( 'Topbar', 'independent' ),
				'header-logo'	=> esc_html__( 'Logo Bar', 'independent' )
			),
			'Sticky' => array( 
				'header-nav' 	=> esc_html__( 'Navbar', 'independent' )
			),
			'disabled' => array()
		),
		'required'	=> array( $prefix.'header_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Absolute Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose header absolute to change header look transparent.', 'independent' ), 
		'id'	=> $prefix.'header_absolute_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky', 'independent' ),
		'desc'	=> esc_html__( 'Choose header sticky options.', 'independent' ), 
		'id'	=> $prefix.'header_sticky_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'sticky' => esc_html__( 'Header Sticky Part', 'independent' ),
			'sticky-scroll' => esc_html__( 'Sticky Scroll Up', 'independent' ),
			'none' => esc_html__( 'None', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Options', 'independent' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
		'id'	=> $prefix.'header_topbar_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Height', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar height for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_topbar_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Height', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar sticky height for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_topbar_sticky_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header topbar skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header topbar skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_topbar_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_topbar_font',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_topbar_bg',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_topbar_link',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_topbar_border',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_topbar_padding',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header top barsticky skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_topbar_sticky_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header top barsticky font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_topbar_sticky_font',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header top barsticky background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_topbar_sticky_bg',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header top barsticky link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_topbar_sticky_link',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header top barsticky border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_topbar_sticky_border',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header top barsticky padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_topbar_sticky_padding',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose header topbar items enable options.', 'independent' ), 
		'id'	=> $prefix.'header_topbar_items_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are header topbar items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_topbar_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'header-topbar-date' => esc_html__( 'Date', 'independent' ),						
			),
			'Center' => array(),
			'Right' => array(),
			'disabled' => array(
				'header-topbar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
				'header-topbar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
				'header-topbar-menu'    => esc_html__( 'Top Menu', 'independent' ),
				'header-topbar-social'	=> esc_html__( 'Social', 'independent' ),
				'header-topbar-search'	=> esc_html__( 'Search', 'independent' )
			)
		),
		'required'	=> array( $prefix.'header_topbar_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Options', 'independent' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
		'id'	=> $prefix.'header_logo_bar_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Height', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar height for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_logo_bar_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Height', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky height for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_logo_bar_sticky_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header logo bar skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header logo bar skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_logo_bar_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_logo_bar_font',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_logo_bar_bg',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_logo_bar_link',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_logo_bar_border',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_logo_bar_padding',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header logo bar sticky skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_logobar_sticky_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_logobar_sticky_font',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_logobar_sticky_bg',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_logobar_sticky_link',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_logobar_sticky_border',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_logobar_sticky_padding',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose header logo bar items enable options.', 'independent' ), 
		'id'	=> $prefix.'header_logo_bar_items_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are header logo bar items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_logo_bar_items',
		'dd_fields' => array ( 
			'Left'  => array(),
			'Center' => array(
				'header-logobar-logo'	=> esc_html__( 'Logo', 'independent' ),
			),
			'Right' => array(),
			'disabled' => array(
				'header-logobar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
				'header-logobar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
				'header-logobar-menu'    => esc_html__( 'Main Menu', 'independent' ),
				'header-logobar-social'	=> esc_html__( 'Social', 'independent' ),
				'header-logobar-search'	=> esc_html__( 'Search', 'independent' ),
				'header-logobar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'independent' ),
				'header-logobar-search-toggle'	=> esc_html__( 'Search Toggle', 'independent' )
			)
		),
		'required'	=> array( $prefix.'header_logo_bar_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Options', 'independent' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
		'id'	=> $prefix.'header_navbar_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Height', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar height for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_navbar_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Height', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky height for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_navbar_sticky_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header navbar skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header navbar skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_navbar_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_navbar_font',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_navbar_bg',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_navbar_link',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_navbar_border',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_navbar_padding',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header navbar sticky skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_navbar_sticky_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_navbar_sticky_font',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_navbar_sticky_bg',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_navbar_sticky_link',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_navbar_sticky_border',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_navbar_sticky_padding',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose header navbar items enable options.', 'independent' ), 
		'id'	=> $prefix.'header_navbar_items_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are header navbar items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_navbar_items',
		'dd_fields' => array ( 
			'Left'  => array(											
				'header-navbar-menu'    => esc_html__( 'Main Menu', 'independent' ),
			),
			'Center' => array(
			),
			'Right' => array(
				'header-navbar-search'	=> esc_html__( 'Search', 'independent' ),
			),
			'disabled' => array(
				'header-navbar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
				'header-navbar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
				'header-navbar-logo'	=> esc_html__( 'Logo', 'independent' ),
				'header-navbar-social'	=> esc_html__( 'Social', 'independent' ),
				'header-navbar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'independent' ),
				'header-navbar-search-toggle'	=> esc_html__( 'Search Toggle', 'independent' ),
				'header-navbar-sticky-logo'	=> esc_html__( 'Stikcy Logo', 'independent' ),
			)
		),
		'required'	=> array( $prefix.'header_navbar_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Options', 'independent' ),
		'desc'	=> esc_html__( 'Choose header sticky part option.', 'independent' ), 
		'id'	=> $prefix.'header_stikcy_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Width', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy part width for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_stikcy_width',
		'property' => 'width',
		'required'	=> array( $prefix.'header_stikcy_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header stikcy skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose header stikcy skin settings options.', 'independent' ), 
		'id'	=> $prefix.'header_stikcy_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_stikcy_font',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_stikcy_bg',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_stikcy_link',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_stikcy_border',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_stikcy_padding',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose header stikcy items enable options.', 'independent' ), 
		'id'	=> $prefix.'header_stikcy_items_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are header stikcy items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_stikcy_items',
		'dd_fields' => array ( 
			'Top'  => array(
				'header-fixed-logo' => esc_html__( 'Logo', 'independent' )
			),
			'Middle'  => array(
				'header-fixed-menu'	=> esc_html__( 'Menu', 'independent' )					
			),
			'Bottom'  => array(
				'header-fixed-social'	=> esc_html__( 'Social', 'independent' )					
			),
			'disabled' => array(
				'header-fixed-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
				'header-fixed-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
				'header-fixed-search'	=> esc_html__( 'Search Form', 'independent' )
			)
		),
		'required'	=> array( $prefix.'header_stikcy_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Bar', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title bar settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Post Title Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose post title enable or disable.', 'independent' ), 
		'id'	=> $prefix.'header_post_title_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Title Text', 'independent' ),
		'desc'	=> esc_html__( 'If this post title is empty, then showing current post default title.', 'independent' ), 
		'id'	=> $prefix.'header_post_title_text',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'text',
		'default'	=> '',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Description', 'independent' ),
		'desc'	=> esc_html__( 'Enter post title description.', 'independent' ), 
		'id'	=> $prefix.'header_post_title_desc',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'textarea',
		'default'	=> '',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Parallax', 'independent' ),
		'desc'	=> esc_html__( 'Choose post title background parallax.', 'independent' ), 
		'id'	=> $prefix.'header_post_title_parallax',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Video Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose post title background video option.', 'independent' ), 
		'id'	=> $prefix.'header_post_title_video_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Video', 'independent' ),
		'desc'	=> esc_html__( 'Enter youtube video ID. Example: ZSt9tm3RoUU.', 'independent' ), 
		'id'	=> $prefix.'header_post_title_video',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'text',
		'default'	=> '',
		'required'	=> array( $prefix.'header_post_title_video_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Bar Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose post title bar items option.', 'independent' ), 
		'id'	=> $prefix.'post_title_items_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Bar Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title bar items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'post_title_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'title' => esc_html__( 'Post Title Text', 'independent' ),
			),
			'Center'  => array(
				
			),
			'Right'  => array(
				'breadcrumb'	=> esc_html__( 'Breadcrumb', 'independent' )
			),
			'disabled' => array()
		),
		'required'	=> array( $prefix.'post_title_items_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are post title skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'label',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose post title skin settings options.', 'independent' ), 
		'id'	=> $prefix.'post_title_skin_opt',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'post_title_font',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'post_title_bg',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Image', 'independent' ),
		'desc'	=> esc_html__( 'Enter post title background image url.', 'independent' ), 
		'id'	=> $prefix.'post_title_bg_img',
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'url',
		'default'	=> '',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'post_title_link',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'post_title_border',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'post_title_padding',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Overlay', 'independent' ),
		'desc'	=> esc_html__( 'These all are post title overlay color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Header', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'post_title_overlay',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	//Footer
	array( 
		'label'	=> 'Footer General',
		'desc'	=> esc_html__( 'These all are header footer settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer layout for current post.', 'independent' ), 
		'id'	=> $prefix.'footer_layout',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'wide' => esc_html__( 'Wide', 'independent' ),
			'boxed' => esc_html__( 'Boxed', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Hidden Footer', 'independent' ),
		'desc'	=> esc_html__( 'Choose hidden footer option.', 'independent' ), 
		'id'	=> $prefix.'hidden_footer',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are footer skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Skin Settings', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer skin settings options.', 'independent' ), 
		'id'	=> $prefix.'footer_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_font',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Background Image', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer background image for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'image',
		'id'	=> $prefix.'footer_bg_img',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Background Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_bg',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Background Overlay', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer background overlay color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_bg_overlay',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_link',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_border',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_padding',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer items enable options.', 'independent' ), 
		'id'	=> $prefix.'footer_items_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'footer_items',
		'dd_fields' => array ( 
			'Enabled'  => array(
				'footer-bottom'	=> esc_html__( 'Footer Bottom', 'independent' )
			),
			'disabled' => array(
				'footer-top' => esc_html__( 'Footer Top', 'independent' ),
				'footer-middle'	=> esc_html__( 'Footer Middle', 'independent' )
			)
		),
		'required'	=> array( $prefix.'footer_items_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Top',
		'desc'	=> esc_html__( 'These all are footer top settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Skin', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer top skin options.', 'independent' ), 
		'id'	=> $prefix.'footer_top_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer top font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_top_font',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_top_bg',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer top link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_top_link',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer top border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_top_border',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer top padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_top_padding',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Top Columns and Sidebars Settings',
		'desc'	=> esc_html__( 'These all are footer top columns and sidebar settings.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer layout option.', 'independent' ), 
		'id'	=> $prefix.'footer_top_layout_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout', 'independent' ),
		'id'	=> $prefix.'footer_top_layout',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'image_select',
		'options' => array(
			'3-3-3-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-1.png', 
			'4-4-4'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-2.png', 
			'3-6-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-3.png', 
			'6-6'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-4.png', 
			'9-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-5.png', 
			'3-9'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-6.png', 
			'12'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-7.png'
		),
		'default'	=> '4-4-4',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer First Column',
		'desc'	=> esc_html__( 'Select footer first column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_top_sidebar_1',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Second Column',
		'desc'	=> esc_html__( 'Select footer second column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_top_sidebar_2',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Third Column',
		'desc'	=> esc_html__( 'Select footer third column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_top_sidebar_3',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Fourth Column',
		'desc'	=> esc_html__( 'Select footer fourth column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_top_sidebar_4',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Middle',
		'desc'	=> esc_html__( 'These all are footer middle settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Skin', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer middle skin options.', 'independent' ), 
		'id'	=> $prefix.'footer_middle_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer middle font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_middle_font',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_middle_bg',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer middle link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_middle_link',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer middle border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_middle_border',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer middle padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_middle_padding',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Middle Columns and Sidebars Settings',
		'desc'	=> esc_html__( 'These all are footer middle columns and sidebar settings.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer layout option.', 'independent' ), 
		'id'	=> $prefix.'footer_middle_layout_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout', 'independent' ),
		'id'	=> $prefix.'footer_middle_layout',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'image_select',
		'options' => array(
			'3-3-3-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-1.png', 
			'4-4-4'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-2.png', 
			'3-6-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-3.png', 
			'6-6'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-4.png', 
			'9-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-5.png', 
			'3-9'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-6.png', 
			'12'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-7.png'
		),
		'default'	=> '4-4-4',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer First Column',
		'desc'	=> esc_html__( 'Select footer first column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_middle_sidebar_1',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Second Column',
		'desc'	=> esc_html__( 'Select footer second column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_middle_sidebar_2',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Third Column',
		'desc'	=> esc_html__( 'Select footer third column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_middle_sidebar_3',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Fourth Column',
		'desc'	=> esc_html__( 'Select footer fourth column widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_middle_sidebar_4',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Bottom',
		'desc'	=> esc_html__( 'These all are footer bottom settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Fixed', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer bottom fixed option.', 'independent' ), 
		'id'	=> $prefix.'footer_bottom_fixed',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'1' => esc_html__( 'Enable', 'independent' ),
			'0' => esc_html__( 'Disable', 'independent' )			
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are footer bottom skin settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Skin', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer bottom skin options.', 'independent' ), 
		'id'	=> $prefix.'footer_bottom_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Font Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer bottom font color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_bottom_font',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Background', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer bottom background color for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_bottom_bg',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Link Color', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer bottom link color settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_bottom_link',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Border', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer bottom border settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_bottom_border',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Padding', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer bottom padding settings for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_bottom_padding',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Widget Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer bottom widget options.', 'independent' ), 
		'id'	=> $prefix.'footer_bottom_widget_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> 'Footer Bottom Widget',
		'desc'	=> esc_html__( 'Select footer bottom widget.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'id'	=> $prefix.'footer_bottom_widget',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_bottom_widget_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Items Option', 'independent' ),
		'desc'	=> esc_html__( 'Choose footer bottom items options.', 'independent' ), 
		'id'	=> $prefix.'footer_bottom_items_opt',
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'custom' => esc_html__( 'Custom', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Items', 'independent' ),
		'desc'	=> esc_html__( 'These all are footer bottom items for currrent post.', 'independent' ), 
		'tab'	=> esc_html__( 'Footer', 'independent' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'footer_bottom_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'copyright' => esc_html__( 'Copyright Text', 'independent' )
			),
			'Center'  => array(
				'menu'	=> esc_html__( 'Footer Menu', 'independent' )
			),
			'Right'  => array(),
			'disabled' => array(
				'social'	=> esc_html__( 'Footer Social Links', 'independent' ),
				'widget'	=> esc_html__( 'Custom Widget', 'independent' )
			)
		),
		'required'	=> array( $prefix.'footer_bottom_items_opt', 'custom' )
	),
	//Header Slider
	array( 
		'label'	=> esc_html__( 'Slider', 'independent' ),
		'desc'	=> esc_html__( 'This header slider settings.', 'independent' ), 
		'tab'	=> esc_html__( 'Slider', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Slider Option', 'independent' ),
		'id'	=> $prefix.'header_slider_opt',
		'tab'	=> esc_html__( 'Slider', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'bottom' => esc_html__( 'Below Header', 'independent' ),
			'top' => esc_html__( 'Above Header', 'independent' ),
			'none' => esc_html__( 'None', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Slider Shortcode', 'independent' ),
		'desc'	=> esc_html__( 'This is the place for enter slider shortcode. Example revolution slider shortcodes.', 'independent' ), 
		'id'	=> $prefix.'header_slider',
		'tab'	=> esc_html__( 'Slider', 'independent' ),
		'type'	=> 'textarea',
		'default'	=> ''
	),
	//Post Format
	array( 
		'label'	=> esc_html__( 'Video', 'independent' ),
		'desc'	=> esc_html__( 'This part for if you choosed video format, then you must choose video type and give video id.', 'independent' ), 
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Video Modal', 'independent' ),
		'id'	=> $prefix.'video_modal',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'onclick' => esc_html__( 'On Click Run Video', 'independent' ),
			'overlay' => esc_html__( 'Modal Box Video', 'independent' ),
			'direct' => esc_html__( 'Direct Video', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Video Type', 'independent' ),
		'desc'	=> esc_html__( 'Choose video type.', 'independent' ), 
		'id'	=> $prefix.'video_type',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'' => esc_html__( 'None', 'independent' ),
			'youtube' => esc_html__( 'Youtube', 'independent' ),
			'vimeo' => esc_html__( 'Vimeo', 'independent' ),
			'custom' => esc_html__( 'Custom Video', 'independent' )
		),
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Video ID', 'independent' ),
		'desc'	=> esc_html__( 'Enter Video ID Example: ZSt9tm3RoUU. If you choose custom video type then you enter custom video url and video must be mp4 format.', 'independent' ), 
		'id'	=> $prefix.'video_id',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'independent' )
	),
	array( 
		'label'	=> esc_html__( 'Audio', 'independent' ),
		'desc'	=> esc_html__( 'This part for if you choosed audio format, then you must give audio id.', 'independent' ), 
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Audio Type', 'independent' ),
		'desc'	=> esc_html__( 'Choose audio type.', 'independent' ), 
		'id'	=> $prefix.'audio_type',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'' => esc_html__( 'None', 'independent' ),
			'soundcloud' => esc_html__( 'Soundcloud', 'independent' ),
			'custom' => esc_html__( 'Custom Audio', 'independent' )
		),
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Audio ID', 'independent' ),
		'desc'	=> esc_html__( 'Enter soundcloud audio ID. Example: 315307209.', 'independent' ), 
		'id'	=> $prefix.'audio_id',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'independent' )
	),
	array( 
		'label'	=> esc_html__( 'Gallery', 'independent' ),
		'desc'	=> esc_html__( 'This part for if you choosed gallery format, then you must choose gallery images here.', 'independent' ), 
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Gallery Modal', 'independent' ),
		'id'	=> $prefix.'gallery_modal',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'default' => esc_html__( 'Default Gallery', 'independent' ),
			'popup' => esc_html__( 'Popup Gallery', 'independent' ),
			'grid' => esc_html__( 'Grid Popup Gallery', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Choose Gallery Images', 'independent' ),
		'id'	=> $prefix.'gallery',
		'type'	=> 'gallery',
		'tab'	=> esc_html__( 'Format', 'independent' )
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'independent' )
	),
	array( 
		'label'	=> esc_html__( 'Quote', 'independent' ),
		'desc'	=> esc_html__( 'This part for if you choosed quote format, then you must fill the quote text and author name box.', 'independent' ), 
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Quote Modal', 'independent' ),
		'id'	=> $prefix.'quote_modal',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'featured' => esc_html__( 'Dark Overlay', 'independent' ),
			'theme-overlay' => esc_html__( 'Theme Overlay', 'independent' ),
			'theme' => esc_html__( 'Theme Color Background', 'independent' ),
			'none' => esc_html__( 'None', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Quote Text', 'independent' ),
		'desc'	=> esc_html__( 'Enter quote text.', 'independent' ), 
		'id'	=> $prefix.'quote_text',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'textarea',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Quote Author', 'independent' ),
		'desc'	=> esc_html__( 'Enter quote author name.', 'independent' ), 
		'id'	=> $prefix.'quote_author',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'independent' )
	),
	array( 
		'label'	=> esc_html__( 'Link', 'independent' ),
		'desc'	=> esc_html__( 'This part for if you choosed link format, then you must fill the external link box.', 'independent' ), 
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Link Modal', 'independent' ),
		'id'	=> $prefix.'link_modal',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'independent' ),
			'featured' => esc_html__( 'Dark Overlay', 'independent' ),
			'theme-overlay' => esc_html__( 'Theme Overlay', 'independent' ),
			'theme' => esc_html__( 'Theme Color Background', 'independent' ),
			'none' => esc_html__( 'None', 'independent' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Link Text', 'independent' ),
		'desc'	=> esc_html__( 'Enter link text to show.', 'independent' ), 
		'id'	=> $prefix.'link_text',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'External Link', 'independent' ),
		'desc'	=> esc_html__( 'Enter external link.', 'independent' ), 
		'id'	=> $prefix.'extrenal_link',
		'tab'	=> esc_html__( 'Format', 'independent' ),
		'type'	=> 'url',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'independent' )
	),
	//News Review
	array( 
		'label'	=> esc_html__( 'Review', 'independent' ),
		'desc'	=> esc_html__( 'This is news review section.', 'independent' ), 
		'tab'	=> esc_html__( 'Review', 'independent' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Review Text', 'independent' ),
		'desc'	=> esc_html__( 'Enter review summary text here.', 'independent' ), 
		'id'	=> $prefix.'review_text',
		'tab'	=> esc_html__( 'Review', 'independent' ),
		'type'	=> 'textarea',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Review Rating', 'independent' ),
		'desc'	=> esc_html__( 'Enter review rating here within range of 5. Example 4.5', 'independent' ), 
		'id'	=> $prefix.'review_rating',
		'tab'	=> esc_html__( 'Review', 'independent' ),
		'type'	=> 'rating',
		'default'	=> ''
	),
);
/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
 
$post_box = new Custom_Add_Meta_Box( 'independent_post_metabox', esc_html__( 'independent Post Options', 'independent' ), $fields, 'post', true );
/* independent Page Options */
//$prefix = 'independent_page_';
function independentMetaboxFields( $prefix ){
	$independent_menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) ); 
	$independent_nav_menus = array( "none" => esc_html__( "None", "independent" ) );
	foreach( $independent_menus as $menu ){
		if( isset( $menu->slug ) ){
			$independent_nav_menus[$menu->slug] = $menu->name;
		}
	}
		
	$fields = array(
		array( 
			'label'	=> esc_html__( 'Page General Settings', 'independent' ),
			'desc'	=> esc_html__( 'These all are page general settings.', 'independent' ), 
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Page Layout', 'independent' ),
			'desc'	=> esc_html__( 'Choose page layout for current post single view.', 'independent' ), 
			'id'	=> $prefix.'layout',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'wide' => esc_html__( 'Wide', 'independent' ),
				'boxed' => esc_html__( 'Boxed', 'independent' )			
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Page Content Padding Option', 'independent' ),
			'id'	=> $prefix.'content_padding_opt',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'		
		),
		array( 
			'label'	=> esc_html__( 'Page Content Padding', 'independent' ), 
			'desc'	=> esc_html__( 'Set the top/right/bottom/left padding of page content.', 'independent' ),
			'id'	=> $prefix.'content_padding',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'space',
			'required'	=> array( $prefix.'content_padding_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Background Color', 'independent' ),
			'desc'	=> esc_html__( 'Choose color setting for current page background.', 'independent' ),
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'main_bg_color'
		),
		array( 
				'label'	=> esc_html__( 'Page Background Image', 'independent' ),
				'desc'	=> esc_html__( 'Choose custom logo image for current page.', 'independent' ), 
				'tab'	=> esc_html__( 'General', 'independent' ),
				'type'	=> 'image',
				'id'	=> $prefix.'main_bg_image'
			),
		array( 
			'label'	=> esc_html__( 'Page Margin', 'independent' ),
			'desc'	=> esc_html__( 'These all are margin settings for current page.', 'independent' ), 
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'main_margin'
		),
		array( 
			'label'	=> esc_html__( 'Page Template Option', 'independent' ),
			'id'	=> $prefix.'template_opt',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'		
		),
		array( 
			'label'	=> esc_html__( 'Page Template', 'independent' ),
			'id'	=> $prefix.'template',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'image_select',
			'options' => array(
				'no-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/1.png' ), 
				'right-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/2.png' ), 
				'left-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/3.png' ), 
				'both-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/4.png' ), 
			),
			'default'	=> 'right-sidebar',
			'required'	=> array( $prefix.'template_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Left Sidebar', 'independent' ),
			'id'	=> $prefix.'left_sidebar',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'template_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Right Sidebar', 'independent' ),
			'id'	=> $prefix.'right_sidebar',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'template_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Sidebar On Mobile', 'independent' ),
			'id'	=> $prefix.'sidebar_mobile',
			'tab'	=> esc_html__( 'General', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Show', 'independent' ),
				'0' => esc_html__( 'Hide', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header General Settings', 'independent' ),
			'desc'	=> esc_html__( 'These all are header general settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Layout', 'independent' ),
			'desc'	=> esc_html__( 'Choose page layout for current page header layout.', 'independent' ), 
			'id'	=> $prefix.'header_layout',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'wide' => esc_html__( 'Wide', 'independent' ),
				'boxed' => esc_html__( 'Boxed', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Type', 'independent' ),
			'desc'	=> esc_html__( 'Choose page layout for current page header type.', 'independent' ), 
			'id'	=> $prefix.'header_type',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'default' => esc_html__( 'Default', 'independent' ),
				'left-sticky' => esc_html__( 'Left Sticky', 'independent' ),
				'right-sticky' => esc_html__( 'Right Sticky', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Background Image', 'independent' ),
			'desc'	=> esc_html__( 'Choose header background image for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'image',
			'id'	=> $prefix.'header_bg_img',
			'required'	=> array( $prefix.'header_type', 'default' )
		),
		array( 
			'label'	=> esc_html__( 'Header Items Options', 'independent' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
			'id'	=> $prefix.'header_items_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are header general items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_items',
			'dd_fields' => array ( 
				'Normal' => array( 
					'header-topbar' 	=> esc_html__( 'Topbar', 'independent' ),
					'header-logo'	=> esc_html__( 'Logo Bar', 'independent' )
				),
				'Sticky' => array( 
					'header-nav' 	=> esc_html__( 'Navbar', 'independent' )
				),
				'disabled' => array()
			),
			'required'	=> array( $prefix.'header_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Absolute Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose header absolute to change header look transparent.', 'independent' ), 
			'id'	=> $prefix.'header_absolute_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Enable', 'independent' ),
				'0' => esc_html__( 'Disable', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky', 'independent' ),
			'desc'	=> esc_html__( 'Choose header sticky options.', 'independent' ), 
			'id'	=> $prefix.'header_sticky_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'sticky' => esc_html__( 'Header Sticky Part', 'independent' ),
				'sticky-scroll' => esc_html__( 'Sticky Scroll Up', 'independent' ),
				'none' => esc_html__( 'None', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Secondary Space Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose secondary space option for enable secondary menu space for current page.', 'independent' ), 
			'id'	=> $prefix.'header_secondary_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'enable' => esc_html__( 'Enable', 'independent' ),
				'disable' => esc_html__( 'Disable', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Secondary Space Animate Type', 'independent' ),
			'desc'	=> esc_html__( 'Choose secondary space option animate type for current page.', 'independent' ), 
			'id'	=> $prefix.'header_secondary_animate',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array(
				'left-push'		=> esc_html__( 'Left Push', 'independent' ),
				'left-overlay'	=> esc_html__( 'Left Overlay', 'independent' ),
				'right-push'	=> esc_html__( 'Right Push', 'independent' ),
				'right-overlay'	=> esc_html__( 'Right Overlay', 'independent' ),
				'full-overlay'	=> esc_html__( 'Full Page Overlay', 'independent' ),
			),
			'default'	=> 'left-push',
			'required'	=> array( $prefix.'header_secondary_opt', 'enable' )
		),
		array( 
			'label'	=> esc_html__( 'Secondary Space Width', 'independent' ),
			'desc'	=> esc_html__( 'Set secondary space width for current page. Example 300', 'independent' ), 
			'id'	=> $prefix.'header_secondary_width',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'header_secondary_opt', 'enable' )
		),
		array( 
			'label'	=> esc_html__( 'Custom Logo', 'independent' ),
			'desc'	=> esc_html__( 'Choose custom logo image for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'image',
			'id'	=> $prefix.'custom_logo',
		),
		array( 
			'label'	=> esc_html__( 'Custom Sticky Logo', 'independent' ),
			'desc'	=> esc_html__( 'Choose custom sticky logo image for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'image',
			'id'	=> $prefix.'custom_sticky_logo',
		),
		array( 
			'label'	=> esc_html__( 'Select Navigation Menu', 'independent' ),
			'desc'	=> esc_html__( 'Choose navigation menu for current page.', 'independent' ), 
			'id'	=> $prefix.'nav_menu',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => $independent_nav_menus
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Options', 'independent' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
			'id'	=> $prefix.'header_topbar_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Height', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar height for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_topbar_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Height', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar sticky height for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_topbar_sticky_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header topbar skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header topbar skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_topbar_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_topbar_font',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_topbar_bg',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_topbar_link',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_topbar_border',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_topbar_padding',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header top barsticky skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_topbar_sticky_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header top barsticky font color for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_topbar_sticky_font',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header top barsticky background color for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_topbar_sticky_bg',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header top barsticky link color settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_topbar_sticky_link',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header top barsticky border settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_topbar_sticky_border',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header top barsticky padding settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_topbar_sticky_padding',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose header topbar items enable options.', 'independent' ), 
			'id'	=> $prefix.'header_topbar_items_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are header topbar items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_topbar_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'header-topbar-date' => esc_html__( 'Date', 'independent' ),						
				),
				'Center' => array(),
				'Right' => array(),
				'disabled' => array(
					'header-topbar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
					'header-topbar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
					'header-topbar-menu'    => esc_html__( 'Top Menu', 'independent' ),
					'header-topbar-social'	=> esc_html__( 'Social', 'independent' ),
					'header-topbar-search'	=> esc_html__( 'Search', 'independent' )
				)
			),
			'required'	=> array( $prefix.'header_topbar_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Options', 'independent' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
			'id'	=> $prefix.'header_logo_bar_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Height', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar height for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_logo_bar_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Height', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky height for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_logo_bar_sticky_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header logo bar skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header logo bar skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_logo_bar_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_logo_bar_font',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_logo_bar_bg',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_logo_bar_link',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_logo_bar_border',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_logo_bar_padding',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header logo bar sticky skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_logobar_sticky_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky font color for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_logobar_sticky_font',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky background color for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_logobar_sticky_bg',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky link color settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_logobar_sticky_link',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky border settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_logobar_sticky_border',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky padding settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_logobar_sticky_padding',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose header logo bar items enable options.', 'independent' ), 
			'id'	=> $prefix.'header_logo_bar_items_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are header logo bar items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_logo_bar_items',
			'dd_fields' => array ( 
				'Left'  => array(),
				'Center' => array(
					'header-logobar-logo'	=> esc_html__( 'Logo', 'independent' ),
				),
				'Right' => array(),
				'disabled' => array(
					'header-logobar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
					'header-logobar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
					'header-logobar-menu'    => esc_html__( 'Main Menu', 'independent' ),
					'header-logobar-social'	=> esc_html__( 'Social', 'independent' ),
					'header-logobar-search'	=> esc_html__( 'Search', 'independent' ),
					'header-logobar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'independent' ),
					'header-logobar-search-toggle'	=> esc_html__( 'Search Toggle', 'independent' )
				)
			),
			'required'	=> array( $prefix.'header_logo_bar_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Options', 'independent' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'independent' ), 
			'id'	=> $prefix.'header_navbar_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Height', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar height for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_navbar_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Height', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky height for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_navbar_sticky_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header navbar skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header navbar skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_navbar_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_navbar_font',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_navbar_bg',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_navbar_link',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_navbar_border',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_navbar_padding',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header navbar sticky skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_navbar_sticky_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky font color for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_navbar_sticky_font',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky background color for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_navbar_sticky_bg',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky link color settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_navbar_sticky_link',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky border settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_navbar_sticky_border',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky padding settings for currrent post.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_navbar_sticky_padding',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose header navbar items enable options.', 'independent' ), 
			'id'	=> $prefix.'header_navbar_items_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are header navbar items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_navbar_items',
			'dd_fields' => array ( 
				'Left'  => array(											
					'header-navbar-menu'    => esc_html__( 'Main Menu', 'independent' ),
				),
				'Center' => array(
				),
				'Right' => array(
					'header-navbar-search'	=> esc_html__( 'Search', 'independent' ),
				),
				'disabled' => array(
					'header-navbar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
					'header-navbar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
					'header-navbar-logo'	=> esc_html__( 'Logo', 'independent' ),
					'header-navbar-social'	=> esc_html__( 'Social', 'independent' ),
					'header-navbar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'independent' ),
					'header-navbar-search-toggle'	=> esc_html__( 'Search Toggle', 'independent' ),
					'header-navbar-sticky-logo'	=> esc_html__( 'Stikcy Logo', 'independent' ),
				)
			),
			'required'	=> array( $prefix.'header_navbar_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Options', 'independent' ),
			'desc'	=> esc_html__( 'Choose header sticky part option.', 'independent' ), 
			'id'	=> $prefix.'header_stikcy_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Width', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy part width for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_stikcy_width',
			'property' => 'width',
			'required'	=> array( $prefix.'header_stikcy_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header stikcy skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose header stikcy skin settings options.', 'independent' ), 
			'id'	=> $prefix.'header_stikcy_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_stikcy_font',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_stikcy_bg',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_stikcy_link',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_stikcy_border',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_stikcy_padding',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose header stikcy items enable options.', 'independent' ), 
			'id'	=> $prefix.'header_stikcy_items_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are header stikcy items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_stikcy_items',
			'dd_fields' => array ( 
				'Top'  => array(
					'header-fixed-logo' => esc_html__( 'Logo', 'independent' )
				),
				'Middle'  => array(
					'header-fixed-menu'	=> esc_html__( 'Menu', 'independent' )					
				),
				'Bottom'  => array(
					'header-fixed-social'	=> esc_html__( 'Social', 'independent' )					
				),
				'disabled' => array(
					'header-fixed-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
					'header-fixed-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
					'header-fixed-search'	=> esc_html__( 'Search Form', 'independent' )
				)
			),
			'required'	=> array( $prefix.'header_stikcy_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Bar', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title bar settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Page Title Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose page title enable or disable.', 'independent' ), 
			'id'	=> $prefix.'header_page_title_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Enable', 'independent' ),
				'0' => esc_html__( 'Disable', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Page Title Text', 'independent' ),
			'desc'	=> esc_html__( 'If this page title is empty, then showing current page default title.', 'independent' ), 
			'id'	=> $prefix.'header_page_title_text',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Description', 'independent' ),
			'desc'	=> esc_html__( 'Enter page title description.', 'independent' ), 
			'id'	=> $prefix.'header_page_title_desc',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'textarea',
			'default'	=> '',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Parallax', 'independent' ),
			'desc'	=> esc_html__( 'Choose page title background parallax.', 'independent' ), 
			'id'	=> $prefix.'header_page_title_parallax',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Enable', 'independent' ),
				'0' => esc_html__( 'Disable', 'independent' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Video Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose page title background video option.', 'independent' ), 
			'id'	=> $prefix.'header_page_title_video_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Enable', 'independent' ),
				'0' => esc_html__( 'Disable', 'independent' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Video', 'independent' ),
			'desc'	=> esc_html__( 'Enter youtube video ID. Example: ZSt9tm3RoUU.', 'independent' ), 
			'id'	=> $prefix.'header_page_title_video',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'header_page_title_video_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Bar Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose page title bar items option.', 'independent' ), 
			'id'	=> $prefix.'page_title_items_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Bar Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title bar items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'page_title_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'title' => esc_html__( 'Page Title Text', 'independent' ),
				),
				'Center'  => array(
					
				),
				'Right'  => array(
					'breadcrumb'	=> esc_html__( 'Breadcrumb', 'independent' )
				),
				'disabled' => array(
					'description' => esc_html__( 'Page Title Description', 'independent' )
				)
			),
			'required'	=> array( $prefix.'page_title_items_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are page title skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'label',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose page title skin settings options.', 'independent' ), 
			'id'	=> $prefix.'page_title_skin_opt',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'page_title_font',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'page_title_bg',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Image', 'independent' ),
			'desc'	=> esc_html__( 'Enter page title background image url.', 'independent' ), 
			'id'	=> $prefix.'page_title_bg_img',
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'url',
			'default'	=> '',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'page_title_link',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'page_title_border',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'page_title_padding',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Overlay', 'independent' ),
			'desc'	=> esc_html__( 'These all are page title overlay color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Header', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'page_title_overlay',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer General',
			'desc'	=> esc_html__( 'These all are header footer settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer layout for current page.', 'independent' ), 
			'id'	=> $prefix.'footer_layout',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'wide' => esc_html__( 'Wide', 'independent' ),
				'boxed' => esc_html__( 'Boxed', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Hidden Footer', 'independent' ),
			'desc'	=> esc_html__( 'Choose hidden footer option.', 'independent' ), 
			'id'	=> $prefix.'hidden_footer',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Enable', 'independent' ),
				'0' => esc_html__( 'Disable', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are footer skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Skin Settings', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer skin settings options.', 'independent' ), 
			'id'	=> $prefix.'footer_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_font',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Background Image', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer background image for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'image',
			'id'	=> $prefix.'footer_bg_img',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Background Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_bg',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Background Overlay', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer background overlay color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_bg_overlay',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_link',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_border',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_padding',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer items enable options.', 'independent' ), 
			'id'	=> $prefix.'footer_items_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'footer_items',
			'dd_fields' => array ( 
				'Enabled'  => array(
					'footer-bottom'	=> esc_html__( 'Footer Bottom', 'independent' )
				),
				'disabled' => array(
					'footer-top' => esc_html__( 'Footer Top', 'independent' ),
					'footer-middle'	=> esc_html__( 'Footer Middle', 'independent' )
				)
			),
			'required'	=> array( $prefix.'footer_items_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Top',
			'desc'	=> esc_html__( 'These all are footer top settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Skin', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer top skin options.', 'independent' ), 
			'id'	=> $prefix.'footer_top_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer top font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_top_font',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_top_bg',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer top link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_top_link',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer top border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_top_border',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer top padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_top_padding',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Top Columns and Sidebars Settings',
			'desc'	=> esc_html__( 'These all are footer top columns and sidebar settings.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer layout option.', 'independent' ), 
			'id'	=> $prefix.'footer_top_layout_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout', 'independent' ),
			'id'	=> $prefix.'footer_top_layout',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'image_select',
			'options' => array(
				'3-3-3-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-1.png', 
				'4-4-4'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-2.png', 
				'3-6-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-3.png', 
				'6-6'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-4.png', 
				'9-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-5.png', 
				'3-9'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-6.png', 
				'12'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-7.png'
			),
			'default'	=> '4-4-4',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer First Column',
			'desc'	=> esc_html__( 'Select footer first column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_top_sidebar_1',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Second Column',
			'desc'	=> esc_html__( 'Select footer second column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_top_sidebar_2',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Third Column',
			'desc'	=> esc_html__( 'Select footer third column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_top_sidebar_3',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Fourth Column',
			'desc'	=> esc_html__( 'Select footer fourth column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_top_sidebar_4',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Middle',
			'desc'	=> esc_html__( 'These all are footer middle settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Skin', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer middle skin options.', 'independent' ), 
			'id'	=> $prefix.'footer_middle_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer middle font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_middle_font',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_middle_bg',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer middle link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_middle_link',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer middle border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_middle_border',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer middle padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_middle_padding',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Middle Columns and Sidebars Settings',
			'desc'	=> esc_html__( 'These all are footer middle columns and sidebar settings.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer layout option.', 'independent' ), 
			'id'	=> $prefix.'footer_middle_layout_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout', 'independent' ),
			'id'	=> $prefix.'footer_middle_layout',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'image_select',
			'options' => array(
				'3-3-3-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-1.png', 
				'4-4-4'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-2.png', 
				'3-6-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-3.png', 
				'6-6'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-4.png', 
				'9-3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-5.png', 
				'3-9'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-6.png', 
				'12'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/footer-layouts/footer-7.png'
			),
			'default'	=> '4-4-4',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer First Column',
			'desc'	=> esc_html__( 'Select footer first column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_middle_sidebar_1',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Second Column',
			'desc'	=> esc_html__( 'Select footer second column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_middle_sidebar_2',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Third Column',
			'desc'	=> esc_html__( 'Select footer third column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_middle_sidebar_3',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Fourth Column',
			'desc'	=> esc_html__( 'Select footer fourth column widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_middle_sidebar_4',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Bottom',
			'desc'	=> esc_html__( 'These all are footer bottom settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Fixed', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer bottom fixed option.', 'independent' ), 
			'id'	=> $prefix.'footer_bottom_fixed',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'1' => esc_html__( 'Enable', 'independent' ),
				'0' => esc_html__( 'Disable', 'independent' )			
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are footer bottom skin settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Skin', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer bottom skin options.', 'independent' ), 
			'id'	=> $prefix.'footer_bottom_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Font Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer bottom font color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_bottom_font',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Background', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer bottom background color for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_bottom_bg',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Link Color', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer bottom link color settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_bottom_link',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Border', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer bottom border settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_bottom_border',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Padding', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer bottom padding settings for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_bottom_padding',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Widget Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer bottom widget options.', 'independent' ), 
			'id'	=> $prefix.'footer_bottom_widget_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> 'Footer Bottom Widget',
			'desc'	=> esc_html__( 'Select footer bottom widget.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'id'	=> $prefix.'footer_bottom_widget',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_bottom_widget_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Items Option', 'independent' ),
			'desc'	=> esc_html__( 'Choose footer bottom items options.', 'independent' ), 
			'id'	=> $prefix.'footer_bottom_items_opt',
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are footer bottom items for currrent page.', 'independent' ), 
			'tab'	=> esc_html__( 'Footer', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'footer_bottom_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'copyright' => esc_html__( 'Copyright Text', 'independent' )
				),
				'Center'  => array(
					'menu'	=> esc_html__( 'Footer Menu', 'independent' )
				),
				'Right'  => array(),
				'disabled' => array(
					'social'	=> esc_html__( 'Footer Social Links', 'independent' ),
					'widget'	=> esc_html__( 'Custom Widget', 'independent' )
				)
			),
			'required'	=> array( $prefix.'footer_bottom_items_opt', 'custom' )
		),
		//Header Slider
		array( 
			'label'	=> esc_html__( 'Slider', 'independent' ),
			'desc'	=> esc_html__( 'This header slider settings.', 'independent' ), 
			'tab'	=> esc_html__( 'Slider', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Slider Option', 'independent' ),
			'id'	=> $prefix.'header_slider_opt',
			'tab'	=> esc_html__( 'Slider', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'bottom' => esc_html__( 'Below Header', 'independent' ),
				'top' => esc_html__( 'Above Header', 'independent' ),
				'none' => esc_html__( 'None', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Slider Shortcode', 'independent' ),
			'desc'	=> esc_html__( 'This is the place for enter slider shortcode. Example revolution slider shortcodes.', 'independent' ), 
			'id'	=> $prefix.'header_slider',
			'tab'	=> esc_html__( 'Slider', 'independent' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
	);
	return $fields;
}
$page_fields = independentMetaboxFields( 'independent_page_' );
$page_box = new Custom_Add_Meta_Box( 'independent_page_metabox', esc_html__( 'independent Page Options', 'independent' ), $page_fields, 'page', true );
/* Custom Post Type Options */
$independent_option = get_option( 'independent_options' );
// Portfolio Options
if( isset( $independent_option['cpt-opts'] ) && is_array( $independent_option['cpt-opts'] ) && in_array( "portfolio", $independent_option['cpt-opts'] ) ){
	
	// CPT Portfolio Metabox
	$prefix = 'independent_portfolio_';
	$portfolio_fields = array(
		array( 
			'label'	=> esc_html__( 'Portfolio General Settings', 'independent' ),
			'desc'	=> esc_html__( 'These all are single portfolio general settings.', 'independent' ), 
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Layout Option', 'independent' ),
			'id'	=> $prefix.'layout_opt',
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'		
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Layout', 'independent' ),
			'id'	=> $prefix.'layout',
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'image_select',
			'options' => array(
				'1'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/portfolio-layouts/1.png', 
				'2'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/portfolio-layouts/2.png',
				'3'	=> INDEPENDENT_CORE_URL . '/admin/ReduxCore/assets/img/portfolio-layouts/3.png'
	
			),
			'default'	=> '1',
			'required'	=> array( $prefix.'layout_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Sticky Column', 'independent' ),
			'id'	=> $prefix.'sticky',
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'none' => esc_html__( 'None', 'independent' ),
				'right' => esc_html__( 'Right Column', 'independent' ),
				'left' => esc_html__( 'Left Column', 'independent' )
			),
			'default'	=> 'none'		
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Format', 'independent' ),
			'id'	=> $prefix.'format',
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'standard' => esc_html__( 'Standard', 'independent' ),
				'video' => esc_html__( 'Video', 'independent' ),
				'audio' => esc_html__( 'Audio', 'independent' ),
				'gallery' => esc_html__( 'Gallery', 'independent' ),
				'gmap' => esc_html__( 'Google Map', 'independent' )
			),
			'default'	=> 'standard'		
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Meta Items Options', 'independent' ),
			'desc'	=> esc_html__( 'Choose portfolio meta items option.', 'independent' ), 
			'id'	=> $prefix.'items_opt',
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'independent' ),
				'custom' => esc_html__( 'Custom', 'independent' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Meta Items', 'independent' ),
			'desc'	=> esc_html__( 'These all are meta items for portfolio. drag and drop needed items from disabled part to enabled.', 'independent' ), 
			'tab'	=> esc_html__( 'Portfolio', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'items',
			'dd_fields' => array ( 
				'Enabled'  => array(
					'date'		=> esc_html__( 'Date', 'independent' ),
					'client'	=> esc_html__( 'Client', 'independent' ),
					'category'	=> esc_html__( 'Category', 'independent' ),
					'share'		=> esc_html__( 'Share', 'independent' ),
				),
				'disabled' => array(
					'tag'		=> esc_html__( 'Tags', 'independent' ),
					'duration'	=> esc_html__( 'Duration', 'independent' ),
					'url'		=> esc_html__( 'URL', 'independent' ),
					'place'		=> esc_html__( 'Place', 'independent' ),
					'estimation'=> esc_html__( 'Estimation', 'independent' ),
				)
			),
			'required'	=> array( $prefix.'items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Date', 'independent' ),
			'desc'	=> esc_html__( 'Choose/Enter portfolio date.', 'independent' ), 
			'id'	=> $prefix.'date',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'date',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Date Format', 'independent' ),
			'desc'	=> esc_html__( 'Enter date format to show selcted portfolio date. Example: F j, Y', 'independent' ), 
			'id'	=> $prefix.'date_format',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'text',
			'default'	=> 'F j, Y'
		),
		array( 
			'label'	=> esc_html__( 'Client Name', 'independent' ),
			'desc'	=> esc_html__( 'Enter client name.', 'independent' ), 
			'id'	=> $prefix.'client_name',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Duration', 'independent' ),
			'desc'	=> esc_html__( 'Enter duration years or months.', 'independent' ), 
			'id'	=> $prefix.'duration',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Estimation', 'independent' ),
			'desc'	=> esc_html__( 'Enter project estimation.', 'independent' ), 
			'id'	=> $prefix.'estimation',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Place', 'independent' ),
			'desc'	=> esc_html__( 'Enter project place.', 'independent' ), 
			'id'	=> $prefix.'place',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'URL', 'independent' ),
			'desc'	=> esc_html__( 'Enter project URL.', 'independent' ), 
			'id'	=> $prefix.'url',
			'tab'	=> esc_html__( 'Info', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		//Portfolio Format
		array( 
			'label'	=> esc_html__( 'Video', 'independent' ),
			'desc'	=> esc_html__( 'This part for if you choosed video format, then you must choose video type and give video id.', 'independent' ), 
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Video Modal', 'independent' ),
			'id'	=> $prefix.'video_modal',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'onclick' => esc_html__( 'On Click Run Video', 'independent' ),
				'overlay' => esc_html__( 'Modal Box Video', 'independent' ),
				'direct' => esc_html__( 'Direct Video', 'independent' )
			),
			'default'	=> 'direct'
		),
		array( 
			'label'	=> esc_html__( 'Video Type', 'independent' ),
			'desc'	=> esc_html__( 'Choose video type.', 'independent' ), 
			'id'	=> $prefix.'video_type',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'' => esc_html__( 'None', 'independent' ),
				'youtube' => esc_html__( 'Youtube', 'independent' ),
				'vimeo' => esc_html__( 'Vimeo', 'independent' ),
				'custom' => esc_html__( 'Custom Video', 'independent' )
			),
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Video ID', 'independent' ),
			'desc'	=> esc_html__( 'Enter Video ID Example: ZSt9tm3RoUU. If you choose custom video type then you enter custom video url and video must be mp4 format.', 'independent' ), 
			'id'	=> $prefix.'video_id',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'independent' )
		),
		array( 
			'label'	=> esc_html__( 'Audio', 'independent' ),
			'desc'	=> esc_html__( 'This part for if you choosed audio format, then you must give audio id.', 'independent' ), 
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Audio Type', 'independent' ),
			'desc'	=> esc_html__( 'Choose audio type.', 'independent' ), 
			'id'	=> $prefix.'audio_type',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'' => esc_html__( 'None', 'independent' ),
				'soundcloud' => esc_html__( 'Soundcloud', 'independent' ),
				'custom' => esc_html__( 'Custom Audio', 'independent' )
			),
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Audio ID', 'independent' ),
			'desc'	=> esc_html__( 'Enter soundcloud audio ID. Example: 315307209.', 'independent' ), 
			'id'	=> $prefix.'audio_id',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'independent' )
		),
		array( 
			'label'	=> esc_html__( 'Gallery', 'independent' ),
			'desc'	=> esc_html__( 'This part for if you choosed gallery format, then you must choose gallery images here.', 'independent' ), 
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Gallery Modal', 'independent' ),
			'id'	=> $prefix.'gallery_modal',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'default' => esc_html__( 'Default Gallery', 'independent' ),
				'normal' => esc_html__( 'Normal Gallery', 'independent' ),
				'grid' => esc_html__( 'Grid/Masonry Gallery', 'independent' )
			),
			'default'	=> 'default'
		),
		array( 
			'label'	=> esc_html__( 'Grid Gutter Size', 'independent' ),
			'desc'	=> esc_html__( 'Enter gallery grid gutter size. Example 20', 'independent' ), 
			'id'	=> $prefix.'grid_gutter',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'gallery_modal', 'grid' )
		),
		array( 
			'label'	=> esc_html__( 'Grid Columns', 'independent' ),
			'desc'	=> esc_html__( 'Enter gallery grid columns count. Example 2', 'independent' ), 
			'id'	=> $prefix.'grid_cols',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'gallery_modal', 'grid' )
		),
		array( 
			'label'	=> esc_html__( 'Choose Gallery Images', 'independent' ),
			'id'	=> $prefix.'gallery',
			'type'	=> 'gallery',
			'tab'	=> esc_html__( 'Format', 'independent' )
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'independent' )
		),
		array( 
			'label'	=> esc_html__( 'Google Map', 'independent' ),
			'desc'	=> esc_html__( 'This part for if you choosed google map format, then you must give google map lat, lang and map style.', 'independent' ), 
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Google Map Latitude', 'independent' ),
			'desc'	=> esc_html__( 'Enter google latitude.', 'independent' ), 
			'id'	=> $prefix.'gmap_latitude',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Longitude', 'independent' ),
			'desc'	=> esc_html__( 'Enter google longitude.', 'independent' ), 
			'id'	=> $prefix.'gmap_longitude',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Marker URL', 'independent' ),
			'desc'	=> esc_html__( 'Enter google map custom marker url.', 'independent' ), 
			'id'	=> $prefix.'gmap_marker',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Style', 'independent' ),
			'id'	=> $prefix.'gmap_style',
			'tab'	=> esc_html__( 'Format', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'standard' => esc_html__( 'Standard', 'independent' ),
				'silver' => esc_html__( 'Silver', 'independent' ),
				'retro' => esc_html__( 'Retro', 'independent' ),
				'dark' => esc_html__( 'Dark', 'independent' ),
				'night' => esc_html__( 'Night', 'independent' ),
				'aubergine' => esc_html__( 'Aubergine', 'independent' )
			),
			'default'	=> 'standard'
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'independent' )
		),
	);
	// CPT Portfolio Options
	$portfolio_box = new Custom_Add_Meta_Box( 'independent_portfolio_metabox', esc_html__( 'independent Portfolio Options', 'independent' ), $portfolio_fields, 'independent-portfolio', true );
	
	// CPT Portfolio Page Options
	$portfolio_page_box = new Custom_Add_Meta_Box( 'independent_portfolio_page_metabox', esc_html__( 'independent Page Options', 'independent' ), $page_fields, 'independent-portfolio', true );
} // In theme option CPT option if portfolio exists
// Testimonial Options
if( isset( $independent_option['cpt-opts'] ) && is_array( $independent_option['cpt-opts'] ) && in_array( "testimonial", $independent_option['cpt-opts'] ) ){
	
	$prefix = 'independent_testimonial_';
	$testimonial_fields = array(	
		array( 
			'label'	=> esc_html__( 'Author Designation', 'independent' ),
			'desc'	=> esc_html__( 'Enter author designation.', 'independent' ), 
			'id'	=> $prefix.'designation',
			'tab'	=> esc_html__( 'Testimonial', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Company Name', 'independent' ),
			'desc'	=> esc_html__( 'Enter company name.', 'independent' ), 
			'id'	=> $prefix.'company_name',
			'tab'	=> esc_html__( 'Testimonial', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Company URL', 'independent' ),
			'desc'	=> esc_html__( 'Enter company URL.', 'independent' ), 
			'id'	=> $prefix.'company_url',
			'tab'	=> esc_html__( 'Testimonial', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Rating', 'independent' ),
			'desc'	=> esc_html__( 'Set user rating.', 'independent' ), 
			'id'	=> $prefix.'rating',
			'tab'	=> esc_html__( 'Testimonial', 'independent' ),
			'type'	=> 'rating',
			'default'	=> ''
		)
	);
	
	// CPT Testimonial Options
	$testimonial_box = new Custom_Add_Meta_Box( 'independent_testimonial_metabox', esc_html__( 'independent Testimonial Options', 'independent' ), $testimonial_fields, 'independent-testimonial', true );
	
	// CPT Testimonial Page Options
	$testimonial_page_box = new Custom_Add_Meta_Box( 'independent_testimonial_page_metabox', esc_html__( 'independent Page Options', 'independent' ), $page_fields, 'independent-testimonial', true );
	
} // In theme option CPT option if testimonial exists
// Team Options
if( isset( $independent_option['cpt-opts'] ) && is_array( $independent_option['cpt-opts'] ) && in_array( "team", $independent_option['cpt-opts'] ) ){
	
	$prefix = 'independent_team_';
	$team_fields = array(	
		array( 
			'label'	=> esc_html__( 'Member Designation', 'independent' ),
			'desc'	=> esc_html__( 'Enter member designation.', 'independent' ), 
			'id'	=> $prefix.'designation',
			'tab'	=> esc_html__( 'Team', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Member Email', 'independent' ),
			'desc'	=> esc_html__( 'Enter member email.', 'independent' ), 
			'id'	=> $prefix.'email',
			'tab'	=> esc_html__( 'Team', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Link Target', 'independent' ),
			'id'	=> $prefix.'link_target',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'_blank' => esc_html__( 'New Window', 'independent' ),
				'_self' => esc_html__( 'Self Window', 'independent' )
			),
			'default'	=> '_blank'
		),
		array( 
			'label'	=> esc_html__( 'Facebook', 'independent' ),
			'desc'	=> esc_html__( 'Facebook profile link.', 'independent' ), 
			'id'	=> $prefix.'facebook',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Twitter', 'independent' ),
			'desc'	=> esc_html__( 'Twitter profile link.', 'independent' ), 
			'id'	=> $prefix.'twitter',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Instagram', 'independent' ),
			'desc'	=> esc_html__( 'Instagram profile link.', 'independent' ), 
			'id'	=> $prefix.'instagram',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Plus', 'independent' ),
			'desc'	=> esc_html__( 'Google Plus profile link.', 'independent' ), 
			'id'	=> $prefix.'gplus',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Linkedin', 'independent' ),
			'desc'	=> esc_html__( 'Linkedin profile link.', 'independent' ), 
			'id'	=> $prefix.'linkedin',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Pinterest', 'independent' ),
			'desc'	=> esc_html__( 'Pinterest profile link.', 'independent' ), 
			'id'	=> $prefix.'pinterest',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Dribbble', 'independent' ),
			'desc'	=> esc_html__( 'Dribbble profile link.', 'independent' ), 
			'id'	=> $prefix.'dribbble',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Flickr', 'independent' ),
			'desc'	=> esc_html__( 'Flickr profile link.', 'independent' ), 
			'id'	=> $prefix.'flickr',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Youtube', 'independent' ),
			'desc'	=> esc_html__( 'Youtube profile link.', 'independent' ), 
			'id'	=> $prefix.'youtube',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Vimeo', 'independent' ),
			'desc'	=> esc_html__( 'Vimeo profile link.', 'independent' ), 
			'id'	=> $prefix.'vimeo',
			'tab'	=> esc_html__( 'Social', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		)
	);
	
	// CPT Team Options
	$team_box = new Custom_Add_Meta_Box( 'independent_team_metabox', esc_html__( 'independent Team Options', 'independent' ), $team_fields, 'independent-team', true );
	
	// CPT Team Page Options
	$team_page_box = new Custom_Add_Meta_Box( 'independent_team_page_metabox', esc_html__( 'independent Page Options', 'independent' ), $page_fields, 'independent-team', true );
	
} // In theme option CPT option if team exists
// Event Options
if( isset( $independent_option['cpt-opts'] ) && is_array( $independent_option['cpt-opts'] ) && in_array( "event", $independent_option['cpt-opts'] ) ){
	
	$prefix = 'independent_event_';
	$event_fields = array(	
		array( 
			'label'	=> esc_html__( 'Event Organiser Name', 'independent' ),
			'desc'	=> esc_html__( 'Enter event organiser name.', 'independent' ), 
			'id'	=> $prefix.'organiser_name',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Organiser Designation', 'independent' ),
			'desc'	=> esc_html__( 'Enter event organiser designation.', 'independent' ), 
			'id'	=> $prefix.'organiser_designation',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Start Date', 'independent' ),
			'desc'	=> esc_html__( 'Enter event start date.', 'independent' ), 
			'id'	=> $prefix.'start_date',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'date',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event End Date', 'independent' ),
			'desc'	=> esc_html__( 'Enter event end date.', 'independent' ), 
			'id'	=> $prefix.'end_date',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'date',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Date Format', 'independent' ),
			'desc'	=> esc_html__( 'Enter date format to show selcted event date. Example: F j, Y', 'independent' ), 
			'id'	=> $prefix.'date_format',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> 'F j, Y'
		),
		array( 
			'label'	=> esc_html__( 'Event Start Time', 'independent' ),
			'desc'	=> esc_html__( 'Enter event start time.', 'independent' ), 
			'id'	=> $prefix.'time',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Cost', 'independent' ),
			'desc'	=> esc_html__( 'Enter event cost.', 'independent' ), 
			'id'	=> $prefix.'cost',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Custom Link for Event Item', 'independent' ),
			'desc'	=> esc_html__( 'Enter custom link to redirect custom event page.', 'independent' ), 
			'id'	=> $prefix.'link',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Custom Link Target', 'independent' ),
			'desc'	=> esc_html__( 'Choose custom link target to new window or self window.', 'independent' ), 
			'id'	=> $prefix.'link_target',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'_blank' => esc_html__( 'New Window', 'independent' ),
				'_self' => esc_html__( 'Self Window', 'independent' )
			),
			'default'	=> '_blank'
		),
		array( 
			'label'	=> esc_html__( 'Custom Link Button Text', 'independent' ),
			'desc'	=> esc_html__( 'Enter custom link buttom text: Example More About Event.', 'independent' ), 
			'id'	=> $prefix.'link_text',
			'tab'	=> esc_html__( 'Events', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Venue Name', 'independent' ),
			'desc'	=> esc_html__( 'Enter event venue name.', 'independent' ), 
			'id'	=> $prefix.'venue_name',
			'tab'	=> esc_html__( 'Address', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Venue Address', 'independent' ),
			'desc'	=> esc_html__( 'Enter event venue address.', 'independent' ), 
			'id'	=> $prefix.'venue_address',
			'tab'	=> esc_html__( 'Address', 'independent' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'E-mail', 'independent' ),
			'desc'	=> esc_html__( 'Enter email id for clarification about event.', 'independent' ), 
			'id'	=> $prefix.'email',
			'tab'	=> esc_html__( 'Address', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Phone', 'independent' ),
			'desc'	=> esc_html__( 'Enter phone number for contact about event.', 'independent' ), 
			'id'	=> $prefix.'phone',
			'tab'	=> esc_html__( 'Address', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Website', 'independent' ),
			'desc'	=> esc_html__( 'Enter event website.', 'independent' ), 
			'id'	=> $prefix.'website',
			'tab'	=> esc_html__( 'Address', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Latitude', 'independent' ),
			'desc'	=> esc_html__( 'Enter map latitude.', 'independent' ), 
			'id'	=> $prefix.'gmap_latitude',
			'tab'	=> esc_html__( 'GMap', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Longitude', 'independent' ),
			'desc'	=> esc_html__( 'Enter map longitude.', 'independent' ), 
			'id'	=> $prefix.'gmap_longitude',
			'tab'	=> esc_html__( 'GMap', 'independent' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Marker URL', 'independent' ),
			'desc'	=> esc_html__( 'Enter google map custom marker url.', 'independent' ), 
			'id'	=> $prefix.'gmap_marker',
			'tab'	=> esc_html__( 'GMap', 'independent' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Style', 'independent' ),
			'id'	=> $prefix.'gmap_style',
			'tab'	=> esc_html__( 'GMap', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'standard' => esc_html__( 'Standard', 'independent' ),
				'silver' => esc_html__( 'Silver', 'independent' ),
				'retro' => esc_html__( 'Retro', 'independent' ),
				'dark' => esc_html__( 'Dark', 'independent' ),
				'night' => esc_html__( 'Night', 'independent' ),
				'aubergine' => esc_html__( 'Aubergine', 'independent' )
			),
			'default'	=> 'standard'
		),
		array( 
			'label'	=> esc_html__( 'Google Map Height', 'independent' ),
			'desc'	=> esc_html__( 'Enter map height in values. Example 400', 'independent' ), 
			'id'	=> $prefix.'gmap_height',
			'tab'	=> esc_html__( 'GMap', 'independent' ),
			'type'	=> 'text',
			'default'	=> '400'
		),
		array( 
			'label'	=> esc_html__( 'Contact Form', 'independent' ),
			'desc'	=> esc_html__( 'Contact form shortcode here.', 'independent' ), 
			'id'	=> $prefix.'contact_form',
			'tab'	=> esc_html__( 'Contact', 'independent' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Info Columns', 'independent' ),
			'desc'	=> esc_html__( 'Enter column division values like given format. Example 3-3-6', 'independent' ), 
			'id'	=> $prefix.'col_layout',
			'tab'	=> esc_html__( 'Layout', 'independent' ),
			'type'	=> 'text',
			'default'	=> '3-3-6'
		),
		array( 
			'label'	=> esc_html__( 'Event Detail Items', 'independent' ),
			'desc'	=> esc_html__( 'This is layout settings for event.', 'independent' ), 
			'tab'	=> esc_html__( 'Layout', 'independent' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'event_info_items',
			'dd_fields' => array ( 
				'Enable'  => array(
					'event-details' => esc_html__( 'Event Details', 'independent' ),
					'event-venue' => esc_html__( 'Event Venue', 'independent' ),
					'event-map' => esc_html__( 'Event Map', 'independent' )
				),
				'disabled' => array(
					'event-form'	=> esc_html__( 'Event Form', 'independent' ),
				)
			),
		),
		array( 
			'label'	=> esc_html__( 'Navigation', 'independent' ),
			'id'	=> $prefix.'nav_position',
			'tab'	=> esc_html__( 'Layout', 'independent' ),
			'type'	=> 'select',
			'options' => array ( 
				'top' => esc_html__( 'Top', 'independent' ),
				'bottom' => esc_html__( 'Bottom', 'independent' )
			),
			'default'	=> 'top'
		),
	);
	
	// CPT Events Options
	$event_box = new Custom_Add_Meta_Box( 'independent_event_metabox', esc_html__( 'independent Event Options', 'independent' ), $event_fields, 'independent-event', true );
	
	// CPT Events Page Options
	$event_page_box = new Custom_Add_Meta_Box( 'independent_event_page_metabox', esc_html__( 'independent Page Options', 'independent' ), $page_fields, 'independent-event', true );
	
} // In theme option CPT option if event exists
// Service Options
if( isset( $independent_option['cpt-opts'] ) && is_array( $independent_option['cpt-opts'] ) && in_array( "service", $independent_option['cpt-opts'] ) ){
	
	$prefix = 'independent_service_';
	
	// CPT Events Page Options
	$service_page_box = new Custom_Add_Meta_Box( 'independent_service_page_metabox', esc_html__( 'independent Page Options', 'independent' ), $page_fields, 'independent-service', true );
	
}