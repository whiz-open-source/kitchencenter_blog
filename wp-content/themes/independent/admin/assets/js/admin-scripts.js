(function( $ ) {
	"use strict";

	jQuery( document ).ready(function() {

		$(".independent-post-featured-status").change(function(){
												 
			var postid = $(this).attr('data-post');
			var stat;
			if( $(this).is( ":checked" ) ) {
				stat = 1;
			}else{
				stat = 0;
			}
			$( "#post-featured-stat-msg-" + postid ).text( independent_admin_ajax_var.process + "..." );
			if( postid ){
				// Ajax call
				$.ajax({
					type: "post",
					url: independent_admin_ajax_var.admin_ajax_url,
					data: "action=independent-post-featured-active&nonce="+independent_admin_ajax_var.featured_nonce+"&featured-stat="+ stat +"&postid="+ postid,
					success: function(data){
						$( "#post-featured-stat-msg-"+ postid ).text( "" );
					}
				});
			}
		});

		$( ".export-custom-sidebar" ).on( "click", function() {
			// Ajax call
			$.ajax({
				type: "post",
				url: independent_admin_ajax_var.admin_ajax_url,
				data: "action=independent-custom-sidebar-export&nonce="+independent_admin_ajax_var.sidebar_nounce,
				success: function( data ){
					
					$("<a />", {
						"download": "custom-sidebars.json",
						"href" : "data:application/json," + encodeURIComponent( data )
					}).appendTo("body").on( "click", function() {
						$(this).remove();
					})[0].click();
					
				}
			});
			return false;
		});
		
		if( $( '#import-code-value' ).length ){
			$( "#redux-import" ).on( "click", 
				function( e ) {
					$( '#redux-import' ).attr( "disabled", "disabled" );
					if ( $( '#import-code-value' ).val() === "" && $( '#import-link-value' ).val() === "" ) {
						e.preventDefault();
						return false;
					}else{
						var json_data = '';
						var stat = '';
						if( $( '#import-code-value' ).val() != "" ){
							json_data = $( '#import-code-value' ).val();
							stat = 'data';
						}else if( $( '#import-link-value' ).val() != "" ){
							json_data = $( '#import-link-value' ).val()
							stat = 'url';
						}
						var post_data = { action: "independent-redux-themeopt-import", nonce: independent_admin_ajax_var.redux_themeopt_import, json_data : json_data, stat: stat };

						jQuery.post(independent_admin_ajax_var.admin_ajax_url, post_data, function( response ) {
							location.reload(true);
							$( '#redux-import' ).removeAttr( "disabled" );
						});
						
						return false;
					}
				}
			);
		}
		
		/*Meta Drag and Drop Multi Field*/
		$( ".meta-drag-drop-multi-field .meta-items" ).each(function( index ) {
			var cur_items = this;
			var auth = $( cur_items ).parent( ".meta-drag-drop-multi-field" ).children( ".meta-items" );
			var part = $( cur_items ).data( "part" );
			var final_val = '';
			var t_json = '';
			final_val = $( cur_items ).parent('.meta-drag-drop-multi-field').children( ".meta-drag-drop-multi-value" );
			final_val.val( JSON.stringify( final_val.data( "params" ) ) );
			$( cur_items ).sortable({
			  connectWith: auth,
			  update: function () {
	
				t_json = jQuery.parseJSON( final_val.val() );
				t_json[part] = '';
				var t = {};
				$( this ).children( "li" ).each(function( index ) {
					var data_id = $(this).attr('data-id');
					var data_val = $(this).attr('data-val');
					t[data_id] = data_val;
				});
				t_json[part] = t;
				final_val.val( JSON.stringify( t_json ) );
	
			  }
			});
		});
		
		/* independent VC Custom Widget Script */
		if( $(".widgets-holder-wrap").length ){
			$(document).ajaxSuccess(function(e, xhr, settings) {
				var widget_id_base = 'independent_widget_block_9';

				if(settings.data.search('action=save-widget') != -1 && settings.data.search('id_base=' + widget_id_base) != -1) {
					
					$('.custom-vc-widget-select').each( function() {
						var org = $(this).attr("data-org");
						var val = this.value;
						var parent = $(this).parents(".widget-content");
						 
						$(parent).find( "div.custom-vc-widget-field[data-depend="+ org +"]" ).each( function() {
							if( $(this).attr("data-depend-val") == val ){
								$(this).css('display', 'block');
							}else{
								$(this).css('display', 'none');
							}
						});
					});
					// do other stuff
				}
			});
		}
		
		$('.custom-vc-widget-select').each( function() {
			var org = $(this).attr("data-org");
			var val = this.value;
			var parent = $(this).parents(".widget-content");
			 
			$(parent).find( "div.custom-vc-widget-field[data-depend="+ org +"]" ).each( function() {
				if( $(this).attr("data-depend-val") == val ){
					$(this).css('display', 'block');
				}else{
					$(this).css('display', 'none');
				}
			});
		});
		
		$(document).on('change', '.custom-vc-widget-select', function() {
			var org = $(this).attr("data-org");
			var val = this.value;
			var parent = $(this).parents(".widget-content");
			 
			$(parent).find( "div.custom-vc-widget-field[data-depend="+ org +"]" ).each( function() {
				if( $(this).attr("data-depend-val") == val ){
					$(this).css('display', 'block');
				}else{
					$(this).css('display', 'none');
				}
			});
		});
		
		/*Color Picker*/
		var parent = $('body');
		if ($('body').hasClass('widgets-php')){
			parent = $('.widget-liquid-right');
		}
		$(document).ready(function() {
			if( parent.find('.independent-color-picker').length ){
				parent.find('.independent-color-picker').wpColorPicker();
			}
		});
		
		$(document).on('widget-added', function(e, widget){
			if( widget.find('.independent-color-picker').length ){
				widget.find('.independent-color-picker').wpColorPicker();
			}
		});
		
		$(document).on('widget-updated', function(e, widget){
			if( widget.find('.independent-color-picker').length ){
				widget.find('.independent-color-picker').wpColorPicker();
			}
		});
		
		/*Social Counters Widget*/
		//Facebook
		$( ".fb_stat" ).change(function() {
			var option = $(this).find('option:selected').val();
			if(option == '1'){
				$('.fb-fields').removeClass('fb-inactive');
			}else{
				$('.fb-fields').addClass('fb-inactive');
			}
		});
		
		//Twitter
		$( ".twitter_stat" ).change(function() {
			var option = $(this).find('option:selected').val();
			if(option == '1'){
				$('.twitter-fields').removeClass('twitter-inactive');
			}else{
				$('.twitter-fields').addClass('twitter-inactive');
			}
		});
		
		//Google Plus
		$( ".gplus_stat" ).change(function() {
			var option = $(this).find('option:selected').val();
			if(option == '1'){
				$('.gplus-fields').removeClass('gplus-inactive');
			}else{
				$('.gplus-fields').addClass('gplus-inactive');
			}
		});
		
		//Youtube
		$( ".yt_stat" ).change(function() {
			var option = $(this).find('option:selected').val();
			if(option == '1'){
				$('.yt-fields').removeClass('yt-inactive');
			}else{
				$('.yt-fields').addClass('yt-inactive');
			}
		});
		
		//Pinterest
		$( ".pin_stat" ).change(function() {
			var option = $(this).find('option:selected').val();
			if(option == '1'){
				$('.pin-fields').removeClass('pin-inactive');
			}else{
				$('.pin-fields').addClass('pin-inactive');
			}
		});
		
		if( $("textarea.template-block-shortcode-hook").length ){
			
			$("textarea.template-block-shortcode-hook").each( function() {			
				var parent_tr = $(this).parents("tr.template-block-shortcode-hook");
				var prev_tr = $(parent_tr).prev("tr");
				var block_id = $(prev_tr).find('.redux-image-select input[checked="checked"]').val();
			
				var btn_txt = '<div class="custom-modal-popup-wrap"><div class="custom-modal-popup"><span class="custom-modal-popup-title">'+ independent_admin_ajax_var.block +" "+ block_id +" "+ independent_admin_ajax_var.settings +'</span><a href="#" class="custom-btn-primary redux-block-update-btn">'+ independent_admin_ajax_var.update +'</a><a href="#" class="close custom-modal-close"><span class="fa fa-close"></span></a><div class="custom-modal-inner"><form class="independent-custom-news-block"></form></div></div><a href="#" class="custom-btn-primary redux-block-edit-btn" data-block="'+ block_id +'" data-block-old="'+ block_id +'"><span class="fa fa-pencil-square-o"></span> '+ independent_admin_ajax_var.edit +' <img src="'+ independent_admin_ajax_var.assets_path +'/images/admin-block-loader.gif" />'+'</a>';
				$(this).parent("fieldset").prepend( btn_txt );
			});
			
			$( document ).on( "click", "input.template-block-shortcode-select", function() {
				var value = $(this).val();//to get content of "value" attrib
				$(this).parents("tr.template-block-shortcode-select").next("tr.template-block-shortcode-hook").find("a.redux-block-edit-btn").attr("data-block", value);
				
				var block_name = independent_admin_ajax_var.block +" "+ value +" "+ independent_admin_ajax_var.settings;
				$(this).parents("tr.template-block-shortcode-select").next("tr.template-block-shortcode-hook").find(".custom-modal-popup-title").text(block_name);
				//
				
			});
			
			$( document ).on( "click", ".custom-modal-close", function() {
				$(this).parent(".custom-modal-popup").fadeOut(100);
				var replace_val = $(this).parents(".custom-modal-popup-wrap").find("a.redux-block-edit-btn").attr("data-block");
				$(this).parents(".custom-modal-popup-wrap").find("a.redux-block-edit-btn").attr("data-block-old", replace_val);
				return false;
			});
			
			$( document ).on( "click", ".redux-block-update-btn", function() {
				var form_data = $( this ).parent(".custom-modal-popup").find("form.independent-custom-news-block").serialize();
				$(this).parents("fieldset").find("textarea.template-block-shortcode-hook").val(form_data);
				$(this).parent(".custom-modal-popup").fadeOut(100);
				var replace_val = $(this).parents(".custom-modal-popup-wrap").find("a.redux-block-edit-btn").attr("data-block");
				$(this).parents(".custom-modal-popup-wrap").find("a.redux-block-edit-btn").attr("data-block-old", replace_val);
				return false;
			});
			
			$( document ).on( "click", ".custom-modal-inner .independent-admin-tabs-menu > li a", function(event ) {
				var target = $(this).attr("href");
				$(this).parents(".independent-admin-tabs-menu").find("li.current").removeClass("current");
				$(this).parent("li").addClass("current");

				$(this).parents(".independent-admin-tabs-container").find(".independent-admin-tab .independent-admin-tab-content.current").removeClass("current");
				$(this).parents(".independent-admin-tabs-container").find(".independent-admin-tab .independent-admin-tab-content" + target).addClass("current");
				return false;
			});
			
			$( document ).on( "click", ".redux-block-edit-btn", function() {
			
				var cur = this;
				var block_id = $(this).attr("data-block");
				var block_id_old = $(this).attr("data-block-old");
				var saved_values = '';
				
				//Loader Activate
				$(cur).find('img').fadeIn(200);
				
				var saved_stat = false;
				if( block_id == block_id_old ){
					saved_stat = saved_values != '' ? true : false;
					saved_values = $(this).parents(".custom-modal-popup-wrap").next("textarea.template-block-shortcode-hook").val();
				}				

				if( block_id ){
					// Ajax call
					$.ajax({
						type: "post",
						dataType: 'JSON',
						url: independent_admin_ajax_var.admin_ajax_url,
						data: "action=independent-redux-block&nonce="+independent_admin_ajax_var.redux_block_nonce+"&block_id="+block_id+"&saved_stat="+saved_stat+'&'+saved_values,
						success: function( data ){							
							
							$(cur).prev(".custom-modal-popup").fadeIn(300);
							
							var pop_parent = $(cur).prev(".custom-modal-popup").find(".custom-modal-inner form.independent-custom-news-block");
							var block_form = data['block_form'];
							$(pop_parent).html(block_form);
							
							$(pop_parent).find('.independent-color-picker').wpColorPicker();
							
							//Meta Drag and Drop Multi Field
							$(pop_parent).find( ".meta-drag-drop-multi-field .meta-items" ).each(function( index ) {
								var cur_items = this;
								var auth = $( cur_items ).parent( ".meta-drag-drop-multi-field" ).children( ".meta-items" );
								var part = $( cur_items ).data( "part" );
								var final_val = '';
								var t_json = '';
								final_val = $( cur_items ).parent('.meta-drag-drop-multi-field').children( ".meta-drag-drop-multi-value" );
								final_val.val( JSON.stringify( final_val.data( "params" ) ) );
								$( cur_items ).sortable({
								  connectWith: auth,
								  update: function () {

									t_json = jQuery.parseJSON( final_val.val() );
									t_json[part] = '';
									var t = {};
									$( this ).children( "li" ).each(function( index ) {
										var data_id = $(this).attr('data-id');
										var data_val = $(this).attr('data-val');
										t[data_id] = data_val;
									});
									t_json[part] = t;
									final_val.val( JSON.stringify( t_json ) );

								  }
								});
							});
							
							//Dependency Field Check
							$(pop_parent).find(".custom-vc-widget-select").each( function() {
								var org = $(this).attr("data-org");
								var val = this.value;
								var parent = $(this).parents(".independent-admin-tab");

								$(parent).find( "div.custom-vc-widget-field[data-depend="+ org +"]" ).each( function() {
									if( $(this).attr("data-depend-val") != val ){
										$(this).css('display', 'none');
									}else{
										$(this).css('display', 'block');
									}
								});
							});
							//Dependency Field Checck on Change				
							$(pop_parent).on('change', '.custom-vc-widget-select', function() {
								var org = $(this).attr("data-org");
								var val = this.value;
								var parent = $(this).parents(".independent-admin-tab");
								 
								$(parent).find( "div.custom-vc-widget-field[data-depend="+ org +"]" ).each( function() {
									if( $(this).attr("data-depend-val") == val ){
										$(this).css('display', 'block');
									}else{
										$(this).css('display', 'none');
									}
								});
							});
							
							$(cur).find('img').fadeOut(200);
							
						}
					});
				}
				return false;
			});
			
		}
	
	});
	
})(jQuery);

