<?php
/**
 * Enqueue Google Web Fonts.
 */
function independent_theme_default_fonts_url() {
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'independent' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Poppins:100,300,400,500,600,700|Poppins:400,500,600,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}
function independent_enqueue_google_web_fonts() {
	wp_enqueue_style( 'google-fonts', independent_theme_default_fonts_url(), array(), null, 'all' );
}
function independent_default_theme_options(){
	$cur_theme = wp_get_theme();	
	if ( $cur_theme->get( 'Name' ) == 'Independent' || $cur_theme->get( 'Name' ) == 'Independent Child' ){
		
		$independent_options = get_option( 'independent_options' );
		$input_val = independent_default_theme_values();
		$independent_options = json_decode( $input_val, true );
		update_option( 'independent_options', $independent_options );
		independent_save_theme_options();
	}
}
if ( ! class_exists( 'independentRedux' ) ) {
	add_action( 'wp_enqueue_scripts', 'independent_enqueue_google_web_fonts' );
	add_action("after_switch_theme", "independent_default_theme_options", 10);
}
function independent_default_theme_values(){
	$theme_opt_def =  '{"last_tab":"","page-layout":"wide","site-width":{"width":"1240px","units":"px"},"page-content-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"page-loader":"no","page-loader-img":{"url":"","id":"","height":"","width":"","thumbnail":""},"infinite-loader-img":{"url":"","id":"","height":"","width":"","thumbnail":""},"news-loader-img":{"url":"","id":"","height":"","width":"","thumbnail":""},"logo":{"url":"","id":"","height":"","width":"","thumbnail":""},"logo-height":{"height":"","units":"px"},"sticky-logo":{"url":"","id":"","height":"","width":"","thumbnail":""},"sticky-logo-height":{"height":"","units":"px"},"mobile-logo":{"url":"","id":"","height":"","width":"","thumbnail":""},"mobile-logo-height":{"height":"","units":"px"},"mailchimp-api":"","google-api":"","open-weather-api":"","comments-type":"wp","comments-like":"0","comments-share":"0","fb-developer-key":"","fb-comments-number":"","fb-comments-width":{"width":"500px","units":"px"},"smooth-opt":"0","scroll-time":"600","scroll-distance":"30","independent_grid_large":{"width":"440px","height":"260px","units":"px"},"independent_grid_medium":{"width":"390px","height":"231px","units":"px"},"independent_grid_small":{"width":"220px","height":"130px","units":"px"},"independent_team_medium":{"width":"300px","height":"300px","units":"px"},"independent_grid_1":{"width":"544px","height":"316px","units":"px"},"independent_grid_2":{"width":"356px","height":"240px","units":"px"},"independent_grid_3":{"width":"356px","height":"206px","units":"px"},"independent_grid_4":{"width":"130px","height":"80px","units":"px"},"independent_banner_67x100":{"width":"827px","height":"460px","units":"px"},"independent_banner_50x50":{"width":"570px","height":"200px","units":"px"},"independent_banner_33x50":{"width":"414px","height":"230px","units":"px"},"independent_banner_25x50":{"width":"285px","height":"200px","units":"px"},"independent_banner_50x100":{"width":"570px","height":"400px","units":"px"},"independent_banner_25x100":{"width":"285px","height":"400px","units":"px"},"independent_banner_33x100":{"width":"380px","height":"400px","units":"px"},"independent_banner_100x100":{"width":"1140px","height":"400px","units":"px"},"independent_banner_50x33":{"width":"570px","height":"132px","units":"px"},"independent_banner_25x66":{"width":"285px","height":"264px","units":"px"},"independent_banner_25x33":{"width":"285px","height":"132px","units":"px"},"duplicate-news":"0","news-lazy-load":"0","news-promotion-text":"","custom-css":"","demo-id-style":"none","rtl":"0","header-ads-text":"","header-ads-md":"no","header-ads-sm":"no","header-ads-xs":"no","footer-ads-text":"","footer-ads-md":"no","footer-ads-sm":"no","footer-ads-xs":"no","sidebar-ads-text":"","sidebar-ads-md":"no","sidebar-ads-sm":"no","sidebar-ads-xs":"no","artical-top-ads-text":"","artical-top-ads-md":"no","artical-top-ads-sm":"no","artical-top-ads-xs":"no","artical-inline-ads-text":"","artical-inline-ads-md":"no","artical-inline-ads-sm":"no","artical-inline-ads-xs":"no","artical-bottom-ads-text":"","artical-bottom-ads-md":"no","artical-bottom-ads-sm":"no","artical-bottom-ads-xs":"no","custom1-ads-text":"","custom1-ads-md":"no","custom1-ads-sm":"no","custom1-ads-xs":"no","custom2-ads-text":"","custom2-ads-md":"no","custom2-ads-sm":"no","custom2-ads-xs":"no","custom3-ads-text":"","custom3-ads-md":"no","custom3-ads-sm":"no","custom3-ads-xs":"no","custom4-ads-text":"","custom4-ads-md":"no","custom4-ads-sm":"no","custom4-ads-xs":"no","custom5-ads-text":"","custom5-ads-md":"no","custom5-ads-sm":"no","custom5-ads-xs":"no","theme-color":"#0172ff","theme-link-color":{"regular":"#282828","hover":"#0172ff","active":"#0172ff"},"body-background":{"background-color":"#f6f6f6","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"body-content-background":{"background-color":"#ffffff","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"body-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"400","font-style":"","subsets":"latin","text-align":"","font-size":"14px","line-height":"28px","letter-spacing":"","color":"#818181"},"h1-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"latin","text-align":"","font-size":"34px","line-height":"42px","letter-spacing":"","color":"#272727"},"h2-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"latin","text-align":"","font-size":"28px","line-height":"36px","letter-spacing":"","color":"#272727"},"h3-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"latin","text-align":"","font-size":"24px","line-height":"32px","letter-spacing":"","color":"#272727"},"h4-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"latin","text-align":"","font-size":"20px","line-height":"29px","letter-spacing":"","color":"#272727"},"h5-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"latin","text-align":"","font-size":"18px","line-height":"26px","letter-spacing":"","color":"#272727"},"h6-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"latin","text-align":"","font-size":"16px","line-height":"25px","letter-spacing":"","color":"#272727"},"widgets-title":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"600","font-style":"","subsets":"latin","text-align":"","font-size":"18px","line-height":"28px","letter-spacing":"","color":"#272727"},"widgets-content":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"400","font-style":"","subsets":"","text-align":"","font-size":"14px","line-height":"28px","letter-spacing":"","color":"#6b6b6b"},"header-layout":"wide","header-type":"default","header-background":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"header-items":{"Normal":{"placebo":"placebo","header-logo":"Logo Section"},"Sticky":{"placebo":"placebo","header-nav":"Nav Bar"},"disabled":{"placebo":"placebo","header-topbar":"Top Bar"}},"header-phone-text":"","header-address-text":"","header-email-text":"","header-slider-position":"none","header-absolute":"0","sticky-part":"0","sticky-part-scrollup":"0","mainmenu-menutype":"advanced","menu-tag":"0","menu-tag-hot-text":"Hot","menu-tag-hot-bg":"#dd2525","menu-tag-new-text":"New","menu-tag-new-bg":"#7100e2","menu-tag-trend-text":"Trend","menu-tag-trend-bg":"#0172ff","main-menu-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"500","font-style":"","subsets":"","text-align":"","text-transform":"uppercase","font-size":"14px","letter-spacing":"","color":""},"header-mainmenu-bg-color":"","header-mainmenu-bg-hcolor":"#0172ff","secondary-menu":"0","secondary-menu-type":"right-overlay","secondary-menu-space-width":{"width":"350px","units":"px"},"secondary-space-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","line-height":"","letter-spacing":"","color":""},"secondary-space-link-color":{"regular":"","hover":"","active":""},"secondary-space-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"secondary-space-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"secondary-space-background":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"dropdown-menu-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"600","font-style":"","subsets":"latin","text-align":"","text-transform":"","font-size":"13px","line-height":"14px","letter-spacing":"","color":"#282828"},"dropdown-menu-background":{"color":"","alpha":"","rgba":""},"dropdown-menu-link-color":{"regular":"#282828","hover":"#0172ff","active":"#0172ff"},"dropdown-menu-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"header-top-sliding-switch":"0","header-top-sliding-device":["desktop","tab"],"header-top-sliding-cols":"3","header-top-sliding-sidebar-1":"","header-top-sliding-sidebar-2":"","header-top-sliding-sidebar-3":"","header-top-sliding-sidebar-4":"","top-sliding-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","line-height":"","letter-spacing":"","color":""},"top-sliding-background":{"color":"","alpha":"","rgba":""},"top-sliding-link-color":{"regular":"","hover":"","active":""},"top-sliding-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"top-sliding-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"search-toggle-form":"1","header-topbar-height":{"height":"40px","units":"px"},"header-topbar-sticky-height":{"height":"","units":"px"},"header-topbar-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"500","font-style":"","subsets":"latin","text-align":"","text-transform":"","font-size":"12px","letter-spacing":"","color":"#545454"},"header-topbar-background":{"color":"#ffffff","alpha":"1","rgba":"rgba(255,255,255,1)"},"header-topbar-link-color":{"regular":"#545454","hover":"#0172ff","active":"#0172ff"},"header-topbar-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"header-topbar-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"header-topbar-text-1":"Welcome to Independent!","header-topbar-text-2":"","header-topbar-date":"l, F j, Y","header-topbar-ads-list":"","header-topbar-items":{"disabled":{"placebo":"placebo","header-topbar-text-2":"Custom Text 2","header-topbar-date":"Date","header-topbar-menu":"Top Menu","header-topbar-social":"Social","header-topbar-search":"Search","header-topbar-ads-list":"Ads","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email"},"Left":{"placebo":"placebo","header-topbar-text-1":"Custom Text 1"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"header-logobar-height":{"height":"130px","units":"px"},"header-logobar-sticky-height":{"height":"px","units":"px"},"header-logobar-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","letter-spacing":"","color":""},"header-logobar-background":{"color":"","alpha":"","rgba":""},"header-logobar-link-color":{"regular":"","hover":"","active":""},"header-logobar-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"header-logobar-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"sticky-header-logobar-color":"","sticky-header-logobar-background":{"color":"","alpha":"","rgba":""},"sticky-header-logobar-link-color":{"regular":"","hover":"","active":""},"sticky-header-logobar-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"sticky-header-logobar-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"header-logobar-text-1":"","header-logobar-text-2":"","header-logobar-ads-list":"","header-logobar-items":{"disabled":{"placebo":"placebo","header-logobar-text-1":"Custom Text 1","header-logobar-text-2":"Custom Text 2","header-logobar-menu":"Main Menu","header-logobar-ads-list":"Ads","header-logobar-social":"Social","header-logobar-search":"Search","header-logobar-secondary-toggle":"Secondary Toggle","header-logobar-search-toggle":"Search Toggle","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email","header-cart":"Cart"},"Left":{"placebo":"placebo","header-logobar-logo":"Logo"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"header-navbar-height":{"height":"50px","units":"px"},"header-navbar-sticky-height":{"height":"50px","units":"px"},"header-navbar-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"600","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"14px","letter-spacing":"0.5px","color":"#ffffff"},"header-navbar-background":{"color":"#000000","alpha":"1","rgba":"rgba(0,0,0,1)"},"header-navbar-link-color":{"regular":"#ffffff","hover":"#ffffff","active":"#ffffff"},"header-navbar-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"header-navbar-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"sticky-header-navbar-color":"#ffffff","sticky-header-navbar-background":{"color":"#000000","alpha":"1","rgba":"rgba(0,0,0,1)"},"sticky-header-navbar-link-color":{"regular":"#ffffff","hover":"#ffffff","active":"#ffffff"},"sticky-header-navbar-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"sticky-header-navbar-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"header-navbar-text-1":"","header-navbar-text-2":"","header-navbar-ads-list":"","header-navbar-items":{"disabled":{"placebo":"placebo","header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-logo":"Logo","header-navbar-social":"Social","header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-sticky-logo":"Stikcy Logo","header-navbar-search":"Search","header-navbar-ads-list":"Ads","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email","header-cart":"Cart"},"Left":{"placebo":"placebo","header-navbar-menu":"Main Menu"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo","header-navbar-search-toggle":"Search Toggle"}},"header-fixed-width":{"width":"350px","units":"px"},"header-fixed-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","letter-spacing":"","color":""},"header-fixed-link-color":{"regular":"","hover":"","active":""},"header-fixed-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"header-fixed-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"header-fixed-background":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"header-fixed-text-1":"","header-fixed-text-2":"","header-fixed-items":{"disabled":{"placebo":"placebo","header-fixed-text-1":"Custom Text 1","header-fixed-social":"Social","header-fixed-text-2":"Custom Text 2"},"Top":{"placebo":"placebo","header-fixed-logo":"Logo"},"Middle":{"placebo":"placebo","header-fixed-menu":"Menu"},"Bottom":{"placebo":"placebo","header-fixed-search":"Search Form"}},"mobile-header-from":"tab-land","mobile-header-height":{"height":"80px","units":"px"},"mobile-header-background":{"color":"#ffffff","alpha":"1","rgba":"rgba(255,255,255,1)"},"mobile-header-link-color":{"regular":"","hover":"","active":""},"mobile-header-sticky":"0","mobile-header-sticky-scrollup":"0","mobile-header-sticky-height":{"height":"75px","units":"px"},"mobile-header-sticky-background":{"color":"","alpha":"","rgba":""},"mobile-header-sticky-link-color":{"regular":"","hover":"","active":""},"mobile-header-items":{"disabled":{"placebo":"placebo","mobile-header-cart":"Cart Icon"},"Left":{"placebo":"placebo","mobile-header-menu":"Menu Icon"},"Center":{"placebo":"placebo","mobile-header-logo":"Logo"},"Right":{"placebo":"placebo","mobile-header-search":"Search Icon"}},"mobile-menu-max-width":{"width":"","units":"px"},"mobile-menu-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","letter-spacing":"","color":""},"mobile-menu-link-color":{"regular":"","hover":"","active":""},"mobile-menu-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"mobile-menu-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"mobile-menu-background":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"mobile-menu-animate-from":"left","mobile-menu-text-1":"","mobile-menu-text-2":"","mobile-menu-items":{"disabled":{"placebo":"placebo","mobile-menu-text-1":"Custom Text 1","mobile-menu-text-2":"Custom Text 2","mobile-menu-social":"Social"},"Top":{"placebo":"placebo","mobile-menu-logo":"Logo"},"Middle":{"placebo":"placebo","mobile-menu-mainmenu":"Menu"},"Bottom":{"placebo":"placebo","mobile-menu-search":"Search Form"}},"footer-layout":"wide","back-to-top":"1","hidden-footer":"0","footer-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"400","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"15px","letter-spacing":"","color":"#c6c6c6"},"footer-link-color":{"regular":"#ffffff","hover":"#0172ff","active":"#0172ff"},"footer-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"footer-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"footer-background":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"footer-background-overlay":{"color":"","alpha":"","rgba":""},"footer-ads-list":"","footer-items":{"Enabled":{"placebo":"placebo","footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{"placebo":"placebo","footer-top":"Footer Top"}},"footer-top-container":"boxed","footer-top-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","letter-spacing":"","color":""},"footer-top-background":{"color":"","alpha":"","rgba":""},"footer-top-link-color":{"regular":"","hover":"","active":""},"footer-top-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"footer-top-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"footer-top-margin":{"margin-top":"","margin-right":"","margin-bottom":"","margin-left":"","units":"px"},"footer-top-title-color":"","footer-top-layout":"3-3-3-3","footer-top-sidebar-1":"","footer-top-sidebar-2":"","footer-top-sidebar-3":"","footer-top-sidebar-4":"","footer-middle-container":"boxed","footer-middle-typography":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"","letter-spacing":"","color":""},"footer-middle-background":{"color":"#111111","alpha":"1","rgba":"rgba(17,17,17,1)"},"footer-middle-link-color":{"regular":"","hover":"","active":""},"footer-middle-border":{"border-top":"","border-right":"","border-bottom":"1px","border-left":"","border-style":"solid","border-color":"#2e2e2e"},"footer-middle-padding":{"padding-top":"110px","padding-right":"60px","padding-bottom":"50px","padding-left":"60px","units":"px"},"footer-middle-margin":{"margin-top":"","margin-right":"","margin-bottom":"","margin-left":"","units":"px"},"footer-middle-title-color":"#ffffff","footer-middle-layout":"4-4-4","footer-middle-sidebar-1":"sidebar-2","footer-middle-sidebar-2":"sidebar-3","footer-middle-sidebar-3":"sidebar-4","footer-middle-sidebar-4":"","footer-bottom-container":"boxed","copyright-text":"&copy; Copyrights 2019. Todos los Derechos Reservados. Kitchen Center 2019 ","footer-bottom-fixed":"0","footer-bottom-typography":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"400","font-style":"","subsets":"","text-align":"","text-transform":"","font-size":"14px","letter-spacing":"","color":"#949494"},"footer-bottom-background":{"color":"#000000","alpha":"1","rgba":"rgba(0,0,0,1)"},"footer-bottom-link-color":{"regular":"#999999","hover":"#0172ff","active":"#0172ff"},"footer-bottom-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"footer-bottom-padding":{"padding-top":"33px","padding-right":"60px","padding-bottom":"33px","padding-left":"60px","units":"px"},"footer-bottom-margin":{"margin-top":"","margin-right":"","margin-bottom":"","margin-left":"","units":"px"},"footer-bottom-title-color":"","footer-bottom-widget":"","footer-bottom-items":{"disabled":{"placebo":"placebo","social":"Footer Social Links","widget":"Custom Widget","menu":"Footer Menu"},"Left":{"placebo":"placebo"},"Center":{"placebo":"placebo","copyright":"Copyright Text"},"Right":{"placebo":"placebo"}},"widget-title-style":"","page-page-title-opt":"1","template-page-color":"","template-page-link-color":{"regular":"","hover":"","active":""},"template-page-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"template-page-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"template-page-background-all":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"page-page-title-parallax":"0","page-page-title-bg":"0","page-page-title-video":"","page-page-title-overlay":{"color":"","alpha":"","rgba":""},"template-page-pagetitle-items":{"disabled":{"placebo":"placebo"},"Left":{"placebo":"placebo","title":"Page Title Text","breadcrumb":"Breadcrumb"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"page-page-template":"no-sidebar","page-left-sidebar":"","page-right-sidebar":"sidebar-1","page-sidebar-sticky":"1","page-page-hide-sidebar":"1","theme-templates":["archive","search"],"theme-categories":"","search-content":"post","single-post-page-title-opt":"0","template-single-post-color":"","template-single-post-link-color":{"regular":"","hover":"","active":""},"template-single-post-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"template-single-post-padding":{"padding-top":"","padding-right":"","padding-bottom":"0","padding-left":"","units":"px"},"template-single-post-background-all":{"background-color":"#ffffff","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"single-post-page-title-parallax":"0","single-post-page-title-bg":"0","single-post-page-title-video":"","single-post-page-title-overlay":{"color":"","alpha":"","rgba":""},"template-single-post-pagetitle-items":{"disabled":{"placebo":"placebo","title":"Page Title Text"},"Left":{"placebo":"placebo","breadcrumb":"Breadcrumb"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"single-post-featured-slider":"0","single-post-article-color":"#808080","single-post-article-link-color":{"regular":"#808080","hover":"#0172ff","active":"#0172ff"},"single-post-article-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"solid","border-color":"#eeeeee"},"single-post-article-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"single-post-article-background":{"color":"","alpha":"","rgba":""},"single-post-article-overlay-color":"","single-post-article-overlay-link-color":{"regular":"","hover":"","active":""},"single-post-article-overlay-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"single-post-article-overlay-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"single-post-article-overlay-margin":{"margin-top":"","margin-right":"","margin-bottom":"","margin-left":"","units":"px"},"single-post-article-overlay-background":{"color":"","alpha":"","rgba":""},"single-post-video-format":"onclick","single-post-quote-format":"featured","single-post-link-format":"featured","single-post-gallery-format":"default","single-post-page-template":"right-sidebar","single-post-left-sidebar":"","single-post-right-sidebar":"sidebar-1","single-post-sidebar-sticky":"1","single-post-page-hide-sidebar":"1","single-post-full-wrap":"0","article-top-ads-list":"","article-inline-ads-list":"","article-bottom-ads-list":"","single-post-topmeta-items":{"Left":{"placebo":"placebo","author":"Author"},"Right":{"placebo":"placebo","date":"Date"},"disabled":{"placebo":"placebo","category":"Category","social":"Social Share","comments":"Comments","likes":"Likes","views":"Views","tag":"Tags","favourite":"Favourite","author-name":"Author Name"}},"single-post-bottommeta-items":{"Left":{"placebo":"placebo","category":"Category"},"Right":{"placebo":"placebo","tag":"Tags"},"disabled":{"placebo":"placebo","date":"Date","social":"Social Share","comments":"Comments","likes":"Likes","author":"Author","views":"Views","tag":"Tags","favourite":"Favourite","author-name":"Author Name"}},"single-post-items":{"Enabled":{"placebo":"placebo","breadcrumb":"Breadcrumbs","thumb":"Thumbnail","top-meta":"Top Meta","title":"Title","content":"Content","bottom-meta":"Bottom Meta"},"disabled":{"placebo":"placebo"}},"single-post-overlay-opt":"0","single-post-overlay-items":{"Enabled":{"placebo":"placebo","title":"Title"},"disabled":{"placebo":"placebo","top-meta":"Top Meta","bottom-meta":"Bottom Meta"}},"single-post-page-items":{"Enabled":{"placebo":"placebo","post-items":"Post Items","author-info":"Author Info","review":"Review Info","post-nav":"Post Navigation","comment":"Comment"},"disabled":{"placebo":"placebo","author-articles":"Author Articles","related-articles":"Related Articles","article-inline-ads-list":"Article Inline Ads"}},"related-posts-filter":"category","related-max-posts":"3","author-max-posts":"3","blog-tag-color":"","blog-page-title-opt":"1","template-blog-color":"#818181","template-blog-link-color":{"regular":"#818181","hover":"#0172ff","active":"#0172ff"},"template-blog-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"template-blog-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"template-blog-background-all":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"blog-page-title-parallax":"0","blog-page-title-bg":"0","blog-page-title-video":"","blog-page-title-overlay":{"color":"","alpha":"","rgba":""},"blog-page-title":"Latest Blog","blog-page-desc":"","template-blog-pagetitle-items":{"disabled":{"placebo":"placebo","breadcrumb":"Breadcrumb","description":"Page Title Description"},"Left":{"placebo":"placebo","title":"Page Title Text"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"blog-featured-slider":"0","blog-page-template":"right-sidebar","blog-left-sidebar":"","blog-right-sidebar":"sidebar-1","blog-sidebar-sticky":"1","blog-page-hide-sidebar":"1","blog-template-shortcode-model":"13","blog-template-shortcode":"","archive-tag-color":"","archive-page-title-opt":"1","template-archive-color":"#818181","template-archive-link-color":{"regular":"#818181","hover":"#0172ff","active":"#0172ff"},"template-archive-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"template-archive-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"template-archive-background-all":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"archive-page-title-parallax":"0","archive-page-title-bg":"0","archive-page-title-video":"","archive-page-title-overlay":{"color":"","alpha":"","rgba":""},"template-archive-pagetitle-items":{"disabled":{"placebo":"placebo"},"Left":{"placebo":"placebo","title":"Page Title Text","breadcrumb":"Breadcrumb"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"archive-featured-slider":"0","archive-page-template":"right-sidebar","archive-left-sidebar":"","archive-right-sidebar":"sidebar-1","archive-sidebar-sticky":"1","archive-page-hide-sidebar":"1","archive-template-shortcode-model":"6","archive-template-shortcode":"","search-tag-color":"","search-page-title-opt":"1","template-search-color":"#818181","template-search-link-color":{"regular":"#818181","hover":"#0172ff","active":"#0172ff"},"template-search-border":{"border-top":"","border-right":"","border-bottom":"","border-left":"","border-style":"none","border-color":""},"template-search-padding":{"padding-top":"","padding-right":"","padding-bottom":"","padding-left":"","units":"px"},"template-search-background-all":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"search-page-title-parallax":"0","search-page-title-bg":"0","search-page-title-video":"","search-page-title-overlay":{"color":"","alpha":"","rgba":""},"template-search-pagetitle-items":{"disabled":{"placebo":"placebo"},"Left":{"placebo":"placebo","title":"Page Title Text","breadcrumb":"Breadcrumb"},"Center":{"placebo":"placebo"},"Right":{"placebo":"placebo"}},"search-featured-slider":"0","search-page-template":"right-sidebar","search-left-sidebar":"","search-right-sidebar":"sidebar-1","search-sidebar-sticky":"1","search-page-hide-sidebar":"1","search-template-shortcode-model":"6","search-template-shortcode":"","featured-slide-items":"3","featured-slide-tab":"1","featured-slide-mobile":"1","featured-slide-scrollby":"1","featured-slide-autoplay":"true","featured-slide-center":"false","featured-slide-duration":"5000","featured-slide-smartspeed":"250","featured-slide-infinite":"false","featured-slide-margin":"0","featured-slide-pagination":"false","featured-slide-navigation":"false","featured-slide-autoheight":"false","related-slide-items":"3","related-slide-tab":"1","related-slide-mobile":"1","related-slide-scrollby":"1","related-slide-autoplay":"true","related-slide-center":"false","related-slide-duration":"5000","related-slide-smartspeed":"250","related-slide-infinite":"false","related-slide-margin":"10","related-slide-pagination":"false","related-slide-navigation":"false","related-slide-autoheight":"false","blog-slide-items":"1","blog-slide-tab":"1","blog-slide-mobile":"1","blog-slide-scrollby":"1","blog-slide-autoplay":"true","blog-slide-center":"false","blog-slide-duration":"5000","blog-slide-smartspeed":"250","blog-slide-infinite":"false","blog-slide-margin":"0","blog-slide-pagination":"false","blog-slide-navigation":"false","blog-slide-autoheight":"false","single-slide-items":"3","single-slide-tab":"1","single-slide-mobile":"1","single-slide-scrollby":"1","single-slide-autoplay":"true","single-slide-center":"false","single-slide-duration":"5000","single-slide-smartspeed":"250","single-slide-infinite":"false","single-slide-margin":"0","single-slide-pagination":"false","single-slide-navigation":"false","single-slide-autoheight":"false","social-icons-type":"transparent","social-icons-type-footer":"transparent","social-icons-fore":"white","social-icons-hfore":"h-white","social-icons-bg":"bg-own","social-icons-hbg":"hbg-dark","social-fb":"","social-twitter":"","social-instagram":"","social-pinterest":"","social-gplus":"","social-youtube":"","social-vimeo":"","social-soundcloud":"","social-yahoo":"","social-tumblr":"","social-paypal":"","social-mailto":"","social-flickr":"","social-dribbble":"","social-linkedin":"","social-rss":"","post-social-shares":["fb","twitter","linkedin","gplus","pinterest"],"comments-social-shares":["fb","twitter","linkedin","gplus","pinterest"],"woo-page-template":"no-sidebar","woo-left-sidebar":"","woo-right-sidebar":"sidebar-1","woo-shop-columns":"4","woo-shop-ppp":"12","woo-related-ppp":"3","wooarchive-page-template":"right-sidebar","wooarchive-left-sidebar":"","wooarchive-right-sidebar":"sidebar-1","woo-related-slide-items":"3","woo-related-slide-tab":"1","woo-related-slide-mobile":"1","woo-related-slide-scrollby":"1","woo-related-slide-autoplay":"true","woo-related-slide-center":"false","woo-related-slide-duration":"5000","woo-related-slide-smartspeed":"250","woo-related-slide-infinite":"false","woo-related-slide-margin":"0","woo-related-slide-pagination":"false","woo-related-slide-navigation":"false","woo-related-slide-autoheight":"false","js-minify":"0","css-minify":"0","maintenance-mode":"0","maintenance-type":"","maintenance-custom":"","maintenance-phone":"","maintenance-email":"","maintenance-address":"","redux-backup":"1"}'; //Here Theme Default Values
	return $theme_opt_def;
}
function independent_default_block_values(){
	return 'extra_class=block-default&title=&title_position=left&title_style=&title_transform=capitalize&title_size=&nav_align=left&excerpt_len=20&more_text=More&readmore_text=Read+More&loadmore_text=Load+More..&block_color=&cat_tag=no&cat_tag_style=&cat_tag_bg_color=&cat_tag_color=&post_icon=no&tab_hide=no&animate_type=up&post_per_tab=10&pagination=yes&block_grid_align=text-center&grid_items=%7B%22Enabled%22%3A%7B%22image%22%3A%22Image%22%2C%22primary-meta%22%3A%22Primary+Meta%22%2C%22title%22%3A%22Title%22%2C%22secondary-meta%22%3A%22Secondary+Meta%22%2C%22content%22%3A%22Content%22%2C%22more%22%3A%22Read+More%22%7D%2C%22disabled%22%3A%7B%7D%7D&grid_primary_meta=%7B%22Left%22%3A%7B%22category%22%3A%22Category%22%7D%2C%22Right%22%3A%7B%7D%2C%22disabled%22%3A%7B%22author-with-image%22%3A%22Author+Image%22%2C%22read-more%22%3A%22Read+More%22%2C%22likes%22%3A%22Like%22%2C%22date%22%3A%22Date%22%2C%22comments%22%3A%22Comments%22%2C%22author%22%3A%22Author+Icon%22%2C%22views%22%3A%22View%22%2C%22favourite%22%3A%22Favourite%22%2C%22share%22%3A%22Share%22%2C%22rating%22%3A%22Rating%22%7D%7D&grid_secondary_meta=%7B%22Left%22%3A%7B%22author%22%3A%22Author+Icon%22%2C%22date%22%3A%22Date%22%7D%2C%22Right%22%3A%7B%7D%2C%22disabled%22%3A%7B%22comments%22%3A%22Comments%22%2C%22likes%22%3A%22Like%22%2C%22read-more%22%3A%22Read+More%22%2C%22views%22%3A%22View%22%2C%22favourite%22%3A%22Favourite%22%2C%22category%22%3A%22Category%22%2C%22share%22%3A%22Share%22%2C%22author-with-image%22%3A%22Author+Image%22%2C%22rating%22%3A%22Rating%22%7D%7D&grid_title_variation=h4&grid_thumb_size=large&custom_image_size=&overlay_options=no&block_grid_ovelay_items=%7B%22Enabled%22%3A%7B%22title%22%3A%22Title%22%7D%2C%22disabled%22%3A%7B%22primary-meta%22%3A%22Primary+Meta%22%2C%22secondary-meta%22%3A%22Secondary+Meta%22%7D%7D&sc_grid_spacing=';
}
function independent_vc_map_block_default(){
	
	$cats_show = $tags_show = $authors = esc_html__( 'Example: 2, 3, 4', 'independent' ); 
			
	$map_fields = array(
		"name"					=> esc_html__( "Block Default", "independent" ),
		"description"			=> esc_html__( "Single News.", 'independent' ),
		"base"					=> "independent_vc_block_default",
		"category"				=> esc_html__( "Blocks", "independent" ),
		"icon"					=> "zozo-vc-icon",
		"params"				=> array(
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Extra Classes', "independent" ),
				'param_name'	=> 'extra_class',
				'value' 		=> '',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Title', "independent" ),
				'param_name'	=> 'title',
				'value' 		=> '',
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Position", "independent" ),
				"param_name"	=> "title_position",
				"value"			=> array(
					esc_html__( "Left", "independent" )			=> "left",
					esc_html__( "Center", "independent" )	=> "center",
					esc_html__( "Right", "independent" )	=> "right",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Style", "independent" ),
				"param_name"	=> "title_style",
				"value"			=> array(
					esc_html__( "Default", "independent" )	=> "",
					esc_html__( "Style 1", "independent" )	=> "1",
					esc_html__( "Style 2", "independent" )	=> "2",
					esc_html__( "Style 3", "independent" )	=> "3",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Style", "independent" ),
				'description'	=> esc_html__( 'Choose block title style either capitalize, uppercase, etc..', 'independent' ),
				"param_name"	=> "title_transform",
				"value"			=> array(
					esc_html__( "Capitalize", "independent" )	=> "capitalize",
					esc_html__( "Upper Case", "independent" )	=> "uppercase",
					esc_html__( "Lower Case", "independent" )	=> "lowercase",
					esc_html__( "None", "independent" )	=> "none"
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Title Size', "independent" ),
				'param_name'	=> 'title_size',
				'description'	=> esc_html__( 'This is settings for title font size. Example 12px', 'independent' ),
				'value' 		=> '',
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Navigation Alignment", "independent" ),
				"param_name"	=> "nav_align",
				"value"			=> array(
					esc_html__( "Left", "independent" )	=> "left",
					esc_html__( "Center", "independent" )	=> "center",
					esc_html__( "Right", "independent" )	=> "right",
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Excerpt Length', "independent" ),
				'param_name'	=> 'excerpt_len',
				'description'	=> esc_html__( 'Enter integer value for current block content length. eg: 20', 'independent' ),
				'value' 		=> '20',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Tab Dropdown Text', "independent" ),
				'param_name'	=> 'more_text',
				'description'	=> esc_html__( 'This is text show on more dropdown label', 'independent' ),
				'value' 		=> esc_html__( 'More', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Read More Text', "independent" ),
				'param_name'	=> 'readmore_text',
				'description'	=> esc_html__( 'This is for read more button label', 'independent' ),
				'value' 		=> esc_html__( 'Read More', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Load More Text', "independent" ),
				'param_name'	=> 'loadmore_text',
				'description'	=> esc_html__( 'This is for load more button label. this is only working when ajax filter on and filter type will be load more.', 'independent' ),
				'value' 		=> esc_html__( 'Load More..', 'independent' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__( 'Block Color', "independent" ),
				'param_name'	=> 'block_color',
				'description'	=> esc_html__( 'Choose this color to change current block title, link and some hover colors.', 'independent' ),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Enable Category Tag", "independent" ),
				'description'	=> esc_html__( 'Enable this option to show tag the category with specified color.', 'independent' ),
				"param_name"	=> "cat_tag",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Category Tag Style", "independent" ),
				'description'	=> esc_html__( 'Choose style of category tag.', 'independent' ),
				"param_name"	=> "cat_tag_style",
				"value"			=> array(
					esc_html__( "Default", "independent" )	=> "",
					esc_html__( "Classic", "independent" )	=> "classic"
				)
			),
			array(
				"type"			=> 'colorpicker',
				"heading"		=> esc_html__( "Category Tag Background Color", "independent" ),
				'description'	=> esc_html__( 'Choose category tag background color. This color overwrite which color you gave for the category tag.', 'independent' ),
				"param_name"	=> "cat_tag_bg_color",
				"value"			=> ""
			),
			array(
				"type"			=> 'colorpicker',
				"heading"		=> esc_html__( "Category Tag Color", "independent" ),
				'description'	=> esc_html__( 'Choose category tag fore color.', 'independent' ),
				"param_name"	=> "cat_tag_color",
				"value"			=> ""
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Enable Post Format Icon", "independent" ),
				'description'	=> esc_html__( 'Enable to show post format icons on thumbnail.', 'independent' ),
				"param_name"	=> "post_icon",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Disable Block Tab", "independent" ),
				'description'	=> esc_html__( 'Choose yes to disable news block tab.', 'independent' ),
				"param_name"	=> "tab_hide",
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Animation Type", "independent" ),
				'description'	=> esc_html__( 'Choose animation type for load post with animation.', 'independent' ),
				"param_name"	=> "animate_type",
				"value"			=> array(
					esc_html__( "Animate From Down", "independent" )	=> "up",
					esc_html__( "Animate From Up", "independent" )	=> "down",
					esc_html__( "Animate From Right", "independent" )	=> "right",
					esc_html__( "Animate From Left", "independent" )	=> "left",
					esc_html__( "Animate None", "independent" )	=> "none"
				),
			),
			//Filters
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Ajax Filter", "independent" ),
				"param_name"	=> "ajax_filter",
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Ajax Filter Type", "independent" ),
				"param_name"	=> "ajax_filter_type",
				"value"			=> array(
					esc_html__( "Next/Prev", "independent" )	=> "default",
					esc_html__( "Load More", "independent" )	=> "load-more",
					esc_html__( "Infinite Scroll", "independent" )	=> "infinite",
				),
				"dependency"	=> array(
						"element"	=> "ajax_filter",
						"value"		=> "yes"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Select Tag/Category", "independent" ),
				"param_name"	=> "filter_name",
				"value"			=> array(
					esc_html__( "Category", "independent" )	=> "cat",
					esc_html__( "Tag", "independent" )		=> "tag",
					esc_html__( "Author", "independent" )	=> "author",
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Category ID\'s', "independent" ),
				'param_name'	=> 'cat',
				'value' 		=> '',
				'description'	=> $cats_show,
				"dependency"	=> array(
						"element"	=> "filter_name",
						"value"		=> "cat"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Tag ID\'s', "independent" ),
				'param_name'	=> 'tag',
				'value' 		=> '',
				'description'	=> $tags_show,
				"dependency"	=> array(
						"element"	=> "filter_name",
						"value"		=> "tag"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Authors ID\'s', "independent" ),
				'param_name'	=> 'author',
				'value' 		=> '',
				'description'	=> $authors,
				"dependency"	=> array(
						"element"	=> "filter_name",
						"value"		=> "author"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Minimum Tab to Show', "independent" ),
				'param_name'	=> 'tab_limit',
				'description'	=> esc_html__( 'Limited number of selected tags/categories show in tab. remaining shift to more dropdown. ', "independent" ),
				'value' 		=> '2',
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Post to Show', "independent" ),
				'param_name'	=> 'post_per_tab',
				'description'	=> esc_html__( 'Number of post to show on first load.', "independent" ),
				'value' 		=> '5',
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Filter", "independent" ),
				"param_name"	=> "post_filter",
				"value"			=> array(
					esc_html__( "Recent News(Descending)", "independent" )	=> "recent",
					esc_html__( "Older News(Ascending)", "independent" )		=> "asc",
					esc_html__( "Random", "independent" )					=> "random"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Filter By", "independent" ),
				"param_name"	=> "post_filter_by",
				"value"			=> array(
					esc_html__( "None", "independent" )				=> "none",
					esc_html__( "Most Likes", "independent" )		=> "likes",
					esc_html__( "Most Views", "independent" )		=> "views",
					esc_html__( "High Rated", "independent" )		=> "rated",
					esc_html__( "Most Commented", "independent" )	=> "comment",
					esc_html__( "From Custom Days", "independent" )	=> "days",
					esc_html__( "Custom Posts IDs", "independent" )	=> "custom"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Enter Days', "independent" ),
				'param_name'	=> 'days_count',
				'description'	=> esc_html__( 'if enter 10 means, it\'s showing last 10 days posts.', "independent" ),
				'group'			=> esc_html__( 'Filter', 'independent' ),
				"dependency" => array( "element" => "post_filter_by", "value" => 'days' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Include Post ID\'s', "independent" ),
				'param_name'	=> 'include_post_ids',
				'description'	=> esc_html__( 'Manually enter post id\'s for include. These post ordered not based on Ascending, Descending or Random. eg: 21, 15, 30', "independent" ),
				'group'			=> esc_html__( 'Filter', 'independent' ),
				"dependency" => array( "element" => "post_filter_by", "value" => 'custom' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Exclude Post ID\'s', "independent" ),
				'param_name'	=> 'exclude_post_ids',
				'description'	=> esc_html__( 'Manually enter post id\'s for exclude. eg: 21, 15, 30', "independent" ),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'First Tab Text', "independent" ),
				'param_name'	=> 'first_tab_text',
				'description'	=> esc_html__( 'This is first tab text. default shown All.(if don\'t want all tab means just leave it blank.)', 'independent' ),
				'value' 		=> 'All',
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Infinite to Load More', "independent" ),
				'param_name'	=> 'infinite_loadmore',
				'description'	=> esc_html__( 'Enter value here when infinite stop and show loadmore after some loads. eg: 3', 'independent' ),
				'value' 		=> '',
				"dependency" => array( "element" => "ajax_filter_type", "value" => 'infinite' ),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Pagination", "independent" ),
				"param_name"	=> "pagination",
				'description'	=> esc_html__( 'If you choose pagination yes, ajax filter must be select no.', 'independent' ),
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
				"dependency"	=> array(
						"element"	=> "ajax_filter",
						"value"		=> "no"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			//grid items
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Grid Item Alignment", "independent" ),
				"param_name"	=> "block_grid_align",
				"value"			=> array(
					esc_html__( "Left", "independent" )	=> "",
					esc_html__( "Center", "independent" )=> "text-center",
					esc_html__( "Right", "independent" )	=> "text-right",
				),
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Grid Items', 'independent' ),
				"description"	=> esc_html__( "This is settings for block grid items. Drag and drop needed meta items to enabled part.", "independent" ),
				'param_name'	=> 'grid_items',
				'dd_fields' => array ( 
					'Enabled' => array( 
						'image'=> esc_html__( 'Image', 'independent' ),
						'title'	=> esc_html__( 'Title', 'independent' ),
						'content'	=> esc_html__( 'Content', 'independent' )
					),
					'disabled' => array(
						'primary-meta'	=> esc_html__( 'Primary Meta', 'independent' ),
						'secondary-meta'	=> esc_html__( 'Secondary Meta', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Top Meta Items', 'independent' ),
				"description"	=> esc_html__( "This is settings for block 1 top meta items. Drag and drop needed meta items to Enabled part(Left or Right).", "independent" ),
				'param_name'	=> 'grid_primary_meta',
				'dd_fields' => array ( 
					'Left' => array( 
						'author'=> esc_html__( 'Author Icon', 'independent' ),
						'date'	=> esc_html__( 'Date', 'independent' )				
					),
					'Right' => array( 
						'comments'	=> esc_html__( 'Comments', 'independent' )				
					),
					'disabled' => array(
						'author-with-image'	=> esc_html__( 'Author Image', 'independent' ),
						'read-more'	=> esc_html__( 'Read More', 'independent' ),
						'likes'	=> esc_html__( 'Like', 'independent' ),
						'views'	=> esc_html__( 'View', 'independent' ),
						'favourite'	=> esc_html__( 'Favourite', 'independent' ),
						'category'	=> esc_html__( 'Category', 'independent' ),
						'share'	=> esc_html__( 'Share', 'independent' ),
						'rating'=> esc_html__( 'Rating', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Bottom Meta Items', 'independent' ),
				"description"	=> esc_html__( "This is settings for block 1 bottom meta items. Drag and drop needed meta items to Enabled part(Left or Right).", "independent" ),
				'param_name'	=> 'grid_secondary_meta',
				'dd_fields' => array ( 
					'Left' => array( 
						'likes'	=> esc_html__( 'Like', 'independent' )			
					),
					'Right' => array( 
						'read-more'	=> esc_html__( 'Read More', 'independent' )		
					),
					'disabled' => array(
						'author'=> esc_html__( 'Author Icon', 'independent' ),
						'date'	=> esc_html__( 'Date', 'independent' ),
						'comments'	=> esc_html__( 'Comments', 'independent' ),
						'author-with-image'	=> esc_html__( 'Author Image', 'independent' ),
						'views'	=> esc_html__( 'View', 'independent' ),
						'favourite'	=> esc_html__( 'Favourite', 'independent' ),
						'category'	=> esc_html__( 'Category', 'independent' ),
						'share'	=> esc_html__( 'Share', 'independent' ),
						'rating'=> esc_html__( 'Rating', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Variation", "independent" ),
				"param_name"	=> "grid_title_variation",
				'description'	=> esc_html__( 'Choose title tag for grid title.', 'independent' ),
				"value"			=> array(
					esc_html__( "H2", "independent" ) => "h2",
					esc_html__( "H3", "independent" ) => "h3",
					esc_html__( "H4", "independent" ) => "h4",
					esc_html__( "H5", "independent" ) => "h5",
					esc_html__( "H6", "independent" ) => "h6",
				),
				'std'			=> 'h4',
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Image Size", "independent" ),
				"param_name"	=> "grid_thumb_size",
				'description'	=> esc_html__( 'Choose thumbnail size for display different size image.', 'independent' ),
				"value"			=> array(
					esc_html__( "Large", "independent" )=> "large",
					esc_html__( "Medium", "independent" )=> "medium",
					esc_html__( "independent Grid 1", "independent" )=> "independent_grid_1",
					esc_html__( "independent Grid 2", "independent" )=> "independent_grid_2",
					esc_html__( "independent Grid 3", "independent" )=> "independent_grid_3",
					esc_html__( "independent Grid 4", "independent" )=> "independent_grid_4",
					esc_html__( "Custom", "independent" )=> "custom",
				),
				'std'			=> 'large',
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Custom Image Size', "independent" ),
				'param_name'	=> 'custom_image_size',
				'description'	=> esc_html__( 'Enter custom image size. eg: 200x200', 'independent' ),
				'value' 		=> '',
				"dependency"	=> array(
						"element"	=> "grid_thumb_size",
						"value"		=> "custom"
				),
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Overlay Options", "independent" ),
				"param_name"	=> "overlay_options",
				'description'	=> esc_html__( 'Enable image overlay options.', 'independent' ),
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Grid Overlay Items', 'independent' ),
				"description"	=> esc_html__( "Enable needed items overlay on image. Drag and drop needed meta items to enabled part.", "independent" ),
				'param_name'	=> 'block_grid_ovelay_items',
				'dd_fields' => array ( 
					'Enabled' => array( 
						'title'	=> esc_html__( 'Title', 'independent' )						
					),
					'disabled' => array(
						'primary-meta'	=> esc_html__( 'Primary Meta', 'independent' ),
						'secondary-meta'	=> esc_html__( 'Secondary Meta', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" ),
				"dependency"	=> array(
						"element"	=> "overlay_options",
						"value"		=> "yes"
				)
			),
			array(
				"type"			=> "textarea",
				"heading"		=> esc_html__( "Grid Items Spacing", "independent" ),
				"description"	=> esc_html__( "Enter custom bottom space for each grid item on main wrapper. Your space values will apply like nth child method. If you leave this empty, default theme space apply for each child. If you want default value for any child, just type \"-\". It will take default value for that child. Example 10 12 8", "independent" ),
				"param_name"	=> "sc_grid_spacing",
				"value" 		=> "",
				"group"			=> esc_html__( "Spacing", "independent" ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__( 'Css', 'independent' ),
				'param_name' => 'css',
				'group' => esc_html__( 'Design options', 'independent' ),
			),
		)
	);
	return $map_fields;
}
function independent_news_block_modal_default($loop, $dynamic_options){
	$output = '';
	if( is_page() ){
		while ($loop->have_posts()){
			$loop->the_post();
			$unique_ids = independent_get_unique_news_ids( get_the_ID() );
			$output .= '<div class="row">';
				$output .= '<div class="col-md-12"><!--top col start-->';
					$output .= independent_common_block_grid_generate($dynamic_options);
				$output .= '</div>';
			$output .= '</div>';
		}
	}else{
		while ( have_posts() ) : the_post();
			$unique_ids = independent_get_unique_news_ids( get_the_ID() );
			$output .= '<div class="row">';
				$output .= '<div class="col-md-12"><!--top col start-->';
					if ( is_sticky() && is_home() ) :
						$output .= '<span class="sticky-post-icon"><i class="icon-pin icons"></i></span>';
					endif;
					$output .= independent_common_block_grid_generate($dynamic_options);
				$output .= '</div>';
			$output .= '</div>';
		endwhile;
	}
	return $output;
}
function independent_news_block_form_default($atts, $custom_loop = ''){
	extract( $atts );
	$rand = independent_shortcode_rand_id(); 
	$output = '';
	/*Design Option Start*/
	$css = '';
	extract(shortcode_atts(array( 
		'css' => '',
	), $atts));
	$css_class = $independent_block_styles = $block_styles = $title_bg = '';
	$independent_block_class = 'independent-block-css-'.$rand ;
	
	$css_class = isset( $extra_class ) ? $extra_class : '';
	
	$title_transform = isset( $title_transform ) ? esc_attr( $title_transform ) : 'capitalize';	
	$title_size = isset( $title_size ) && $title_size != '' ? $title_size : '17px';
	$block_color = isset( $block_color ) && $block_color != '' ? $block_color : '';
	$tab_limit = isset( $tab_limit ) && $tab_limit != '' ? $tab_limit : '2';
	$title_position = isset( $title_position ) && $title_position != '' ? $title_position : '';
	$title = isset( $title ) && $title != '' ? $title : '';
	
	$style_arr = array(
		'title_transform' => $title_transform,
		'block_color' => $block_color,
		'title_size' => $title_size
	);
	if( $title_transform || $block_color != '' ){
		$css_class .= ' '.$independent_block_class;
		$independent_block_styles = independent_block_dynamic_css($independent_block_class, $style_arr);
	}
	
	//Grid Spacing
	if( isset( $sc_grid_spacing ) && !empty( $sc_grid_spacing ) ){
		$sc_grid_spacing = preg_replace( '!\s+!', ' ', $sc_grid_spacing );
		$space_arr = explode( " ", $sc_grid_spacing );
		$i = 1;
		$space_class_name = '.' . esc_attr( $independent_block_class ) . ' .independent-news .post-grid >';
		foreach( $space_arr as $space ){
			$independent_block_styles .= $space != '-' ? $space_class_name .' div:nth-child('. esc_attr( $i ) .') { margin-bottom: '. esc_attr( $space ) .'px; }' : '';
			$i++;
		}
	}
	
	//Category Tag Color
	if( isset( $cat_tag_bg_color ) && !empty( $cat_tag_bg_color ) ){
		$independent_block_styles .= '.' . esc_attr( $independent_block_class ) . ' .independent-news .independent-block-post .category-tag { background-color: '. esc_attr( $cat_tag_bg_color ) .' !important; }';
		$independent_block_styles .= '.' . esc_attr( $independent_block_class ) . ' .independent-news .independent-block-post .category-tag:after { border-top-color: '. esc_attr( $cat_tag_bg_color ) .' !important; }';
	}
	if( isset( $cat_tag_color ) && !empty( $cat_tag_color ) ){
		$independent_block_styles .= '.' . esc_attr( $independent_block_class ) . ' .independent-news .independent-block-post .category-tag { color: '. esc_attr( $cat_tag_color ) .' !important; }';
	}
	
	//$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'independent_vc_block_new', $atts );
	$css_class .= isset( $animate_type ) ? ' animate-news-fade-in-' . $animate_type : 'animate-news-fade-in-down';
	/*Design Option End*/
	
	/*Dynamic Options*/
	$grid_items = isset( $grid_items ) ? independent_drag_and_drop_trim( $grid_items ) : '';
	$grid_primary_meta = isset( $grid_primary_meta ) ? independent_drag_and_drop_trim( $grid_primary_meta ) : '';	
	$grid_secondary_meta = isset( $grid_secondary_meta ) ? independent_drag_and_drop_trim( $grid_secondary_meta ) : '';
	
	$ovelay_items = '';
	if( isset( $overlay_options ) && $overlay_options != 'no' ){
		$ovelay_items = isset( $block_grid_ovelay_items ) ? independent_drag_and_drop_trim( $block_grid_ovelay_items ) : '';
	}
	
	$content_len = isset( $excerpt_len ) && absint( $excerpt_len ) ? absint( $excerpt_len ) : esc_html__( 'More', 'independent' );
	$all_text = isset( $first_tab_text ) && $first_tab_text != '' ? esc_attr( $first_tab_text ) : '';
	$more_text = isset( $more_text ) && $more_text != '' ? esc_attr( $more_text ) : 20;
	$readmore_text = isset( $readmore_text ) && $readmore_text != ''  ? esc_attr( $readmore_text ) : esc_html__( 'Read More', 'independent' );
	$filter_type = isset( $ajax_filter_type ) && $ajax_filter_type != ''  ? esc_attr( $ajax_filter_type ) : 'default';
	$loadmore_after = isset( $infinite_loadmore ) && $infinite_loadmore != ''  ? absint( $infinite_loadmore ) : '';
	$pagination = isset( $pagination ) ? esc_attr( $pagination ) : 'no';
	$grid_thumb_size = isset( $grid_thumb_size ) ? esc_attr( $grid_thumb_size ) : 'independent_grid_1';
	$grid_image_custom = isset( $custom_image_size ) ? esc_attr( $custom_image_size ) : '';
	$grid_align = isset( $block_grid_align ) ? esc_attr( $block_grid_align ) : '';
	$cat_tag = isset( $cat_tag ) ? esc_attr( $cat_tag ) : '';
	$cat_tag_style = isset( $cat_tag_style ) ? esc_attr( $cat_tag_style ) : '';
	$post_icon = isset( $post_icon ) ? esc_attr( $post_icon ) : '';
	$grid_title = isset( $grid_title_variation ) ? esc_attr( $grid_title_variation ) : 'h4';
	$tab_hide = isset( $tab_hide ) ? esc_attr( $tab_hide ) : 'no';
	
	$css_class .= $filter_type != 'default' ? ' no-slide-verlay' : '';
	
	$dynamic_options = array('grid_items' => $grid_items, 'grid_primary_meta' => $grid_primary_meta, 'grid_secondary_meta' => $grid_secondary_meta, 'modal' => 'default', 'excerpt_len' => $content_len, 'all_text' => $all_text, 'more_text' => $more_text, 'read_more' => $readmore_text, 'filter_type' => $filter_type, 'loadmore_after' => $loadmore_after, 'pagination' => $pagination, 'ovelay_items' => $ovelay_items, 'grid_thumb' => $grid_thumb_size, 'grid_image_custom' => $grid_image_custom, 'grid_align' => $grid_align, 'cat_tag' => $cat_tag, 'cat_tag_style' => $cat_tag_style, 'post_icon' => $post_icon, 'grid_title' => $grid_title, 'tab_hide' => $tab_hide );
	$filter = isset( $post_filter ) ? $post_filter : 'recent';
	$filter_by = isset( $post_filter_by ) ? $post_filter_by : 'recent';
	$orderby = $meta_key = $days = $post_in = '';
	$order = 'DESC';
	
	if( $filter == 'recent' ){ // ascending order
		$order = 'DESC';
	}elseif( $filter == 'asc' ){ // ascending order
		$order = 'ASC';
	}elseif( $filter == 'random' ){
		$orderby = 'rand';
	}
	
	if( $filter_by == 'likes' ){ // likes 
		$meta_key = 'likes';
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
	}elseif( $filter_by == 'views' ){ //views
		$meta_key = 'views';
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
	}elseif( $filter_by == 'rated' ){ //rated
		$meta_key = 'rated';
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
	}elseif( $filter_by == 'comment' ){ // comment
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'comment_count';			
	}elseif( $filter_by == 'days' ){ // days
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'date';
		$days = isset( $days_count ) && $days_count != '' ? absint( $days_count ) : '10';
	}elseif( $filter_by == 'custom' ){ // comment
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'post__in';
		$post_in = isset( $include_post_ids ) && $include_post_ids != '' ? $include_post_ids : '';
	}
	
	$post_not_in = isset( $exclude_post_ids ) && $exclude_post_ids != '' ? $exclude_post_ids : '';
		
	$filter_name = isset( $filter_name ) ? esc_attr( $filter_name ) : '';
	$filter_values = isset( $$filter_name ) ? esc_attr( $$filter_name ) : '';
	$filter_values = str_replace(' ', '', $filter_values);
	$filter_values = rtrim($filter_values, ',');
	
	$post_per_tab = isset( $post_per_tab ) ? esc_attr( $post_per_tab ) : '5';
	
	$slide_id = $all_text != '' ? 'all' : preg_replace('/^([^,]*).*$/', '$1', $filter_values);
	
	$independent_ajax_nonce = wp_create_nonce('independent-ajax-nonce');
	$block_options = array(
			'action' => 'independent-ajax-slide',
			'nonce' => $independent_ajax_nonce,
			'filter_name' => $filter_name, 
			'filter_values' => $filter_values, 
			'ppp' => $post_per_tab, 
			'paged' => 1, 
			'meta' => $meta_key,
			'orderby' => $orderby,
			'order' => $order,
			'date' => $days,
			'post_not' => $post_not_in,
			'post_in' => $post_in,
			'block_id' => $rand,
			'slide_id' => $slide_id,
			'tab_limit' => $tab_limit,
			'dynamic_options' => $dynamic_options,
		);	
		
		$block_css_options = array(
			'block_style' => $independent_block_styles
		);			
	
	$nbp = new independentBlockParams;
	$nbp->setindependentBlockParams( "independent_block_id_". esc_attr( $rand ), $block_options );
	
	$output = '<div class="independent-block independent-block-13 '. esc_attr( $css_class ) .'" data-id="independent_block_id_'.esc_attr( $rand ).'">';
		$output .= '<input type="hidden" class="news-block-options" id="independent_block_id_'.esc_attr( $rand ).'" />';
		if( $independent_block_styles != '' )
		$output .= '<input type="hidden" class="news-block-css-options" data-options="'. htmlspecialchars(json_encode($block_css_options), ENT_QUOTES, 'UTF-8') .'" />';
		
		$title_class = independent_blocksTitleAlign($title_position);
		$title_class .= isset( $title_style ) && $title_style != '' ? ' title-style-' . $title_style : '';
		if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_html( $title ) .'</span></h4>';
		
		$t_count = count( explode(',', $filter_values ) );
		if( ( isset( $ajax_filter ) && $ajax_filter == 'yes' ) || $t_count > 1 ){
			
			if( $t_count == 1 ){
				$output .= '<div class="independent-content">';
					$output .= '<div class="news-slide-loader"><img src="'.esc_url( independent_news_loader() ).'" alt="'. esc_attr__('Loader..', 'independent') .'" /></div>';
					$output .= independent_news_block_slider($block_options);
				$output .= '</div> <!--independent-content-->';
			}else{
				$output .= independent_news_block_tabs($block_options);
			}
			
			if( isset( $ajax_filter ) && $ajax_filter == 'yes' ){
				if( $pagination != 'yes' ){
					$output .= '<div class="independent-slider-nav type-'. esc_attr( $filter_type ) .'">';
						$output .= '<ul class="slide-nav list-inline '. esc_attr( independent_blocksTitleAlign($nav_align) ) .'">';
							if( $filter_type == 'default' ){
								$output .= '<li><a href="#" class="independent-slider-previous disabled"><i class="fa fa-angle-left"></i></a></li>';
								$output .= '<li><a href="#" class="independent-slider-next"><i class="fa fa-angle-right"></i></a></li>';
							}else{
								$output .= '<li>
												<span class="independent-load-more btn">'. esc_attr( $loadmore_text ) .'</span>
												<img src="'.esc_url( independent_news_loader() ).'" alt="'. esc_attr__('Loader..', 'independent') .'" />
											</li>';
							}
						$output .= '</ul>';
					$output .= '</div> <!--news-slide-nav-->';	
				}
			}
		}else{
			$output .= '<div class="independent-content">';
				$output .= independent_news_block($block_options, true);
			$output .= '</div> <!--independent-content-->';
		}
	$output .= '</div>';
	
	return $output;
}