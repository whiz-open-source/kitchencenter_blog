<?php
if( !class_exists( "independentThemeStyles" ) ){
	require_once INDEPENDENT_INC . '/theme-class/theme-style-class.php';
}
$ats = new independentThemeStyles;
echo "
/*
 * independent theme custom style
 */\n\n";
$independent_options = get_option( 'independent_options' );
echo "\n/* General Styles */\n";
$ats->independent_custom_font_check( 'body-typography' );
echo 'body{';
	$ats->independent_typo_generate( 'body-typography' );
	$ats->independent_bg_settings( 'body-background' );
echo '
}';
echo 'body{';
	$ats->independent_margin_settings( 'page-content-margin' );
echo '
}';
echo 'body .independent-content-inner > .container {';
	$ats->independent_bg_settings( 'body-content-background' );
echo '
}';
$ats->independent_custom_font_check( 'h1-typography' );
echo 'h1{';
	$ats->independent_typo_generate( 'h1-typography' );
echo '
}';
$ats->independent_custom_font_check( 'h2-typography' );
echo 'h2{';
	$ats->independent_typo_generate( 'h2-typography' );
echo '
}';
$ats->independent_custom_font_check( 'h3-typography' );
echo 'h3{';
	$ats->independent_typo_generate( 'h3-typography' );
echo '
}';
$ats->independent_custom_font_check( 'h4-typography' );
echo 'h4{';
	$ats->independent_typo_generate( 'h4-typography' );
echo '
}';
$ats->independent_custom_font_check( 'h5-typography' );
echo 'h5{';
	$ats->independent_typo_generate( 'h5-typography' );
echo '
}';
$ats->independent_custom_font_check( 'h6-typography' );
echo 'h6{';
	$ats->independent_typo_generate( 'h6-typography' );
echo '
}';
$gen_link = $ats->independent_theme_opt('theme-link-color');
if( $gen_link ):
echo 'a{';
	$ats->independent_link_color( 'theme-link-color', 'regular' );
echo '
}';
echo 'a:hover{';
	$ats->independent_link_color( 'theme-link-color', 'hover' );
echo '
}';
echo 'a:active{';
	$ats->independent_link_color( 'theme-link-color', 'active' );
echo '
}';
endif;
echo "\n/* Widget Typography Styles */\n";
$ats->independent_custom_font_check( 'widgets-content' );
echo '.widget{';
	$ats->independent_typo_generate( 'widgets-content' );
echo '
}';
$ats->independent_custom_font_check( 'widgets-title' );
echo '.widget .widget-title, .widget .independent-block-title span {';
	$ats->independent_typo_generate( 'widgets-title' );
echo '
}';
$page_loader = $ats->independent_theme_opt('page-loader') && $ats->independent_theme_opt('page-loader-img') != '' ? $independent_options['page-loader-img']['url'] : '';
if( $page_loader ):
	echo ".page-loader {background: url('". esc_url( $page_loader ). "') 50% 50% no-repeat rgb(249,249,249);}";
endif;
echo '.container, .boxed-container, .boxed-container .site-footer.footer-fixed, .custom-container  {
	width: '. $ats->independent_container_width() .';
}';
echo '.independent-content > .independent-content-inner > div {';
	$ats->independent_padding_settings( 'page-content-padding' );
echo '
}';
echo "\n/* Header Styles */\n";
echo 'header.independent-header {';
	$ats->independent_bg_settings('header-background');
echo '}';
echo "\n/* Topbar Styles */\n";
$ats->independent_custom_font_check( 'header-topbar-typography' );
echo '.topbar{';
	$ats->independent_typo_generate( 'header-topbar-typography' );
	$ats->independent_bg_rgba( 'header-topbar-background' );
	$ats->independent_border_settings( 'header-topbar-border' );
	$ats->independent_padding_settings( 'header-topbar-padding' );
echo '
}';
echo '.topbar a{';
	$ats->independent_link_color( 'header-topbar-link-color', 'regular' );
echo '
}';
echo '.topbar a:hover{';
	$ats->independent_link_color( 'header-topbar-link-color', 'hover' );
echo '
}';
echo '.topbar a:active,.topbar a:focus {';
	$ats->independent_link_color( 'header-topbar-link-color', 'active' );
echo '
}';
echo '
.topbar-items > li{
    height: '. esc_attr( $ats->independent_dimension_height('header-topbar-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('header-topbar-height') ) .' ;
}
.header-sticky .topbar-items > li,
.sticky-scroll.show-menu .topbar-items > li{
	height: '. esc_attr( $ats->independent_dimension_height('header-topbar-sticky-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('header-topbar-sticky-height') ) .' ;
}';
echo '
.topbar-items > li img{
	max-height: '. esc_attr(  $ats->independent_dimension_height('header-topbar-height') ) .' ;
}';
echo "\n/* Logobar Styles */\n";
$ats->independent_custom_font_check( 'header-logobar-typography' );
echo '.logobar{';
	$ats->independent_typo_generate( 'header-logobar-typography' );
	$ats->independent_bg_rgba( 'header-logobar-background' );
	$ats->independent_border_settings( 'header-logobar-border' );
	$ats->independent_padding_settings( 'header-logobar-padding' );
echo '
}';
echo '.logobar a{';
	$ats->independent_link_color( 'header-logobar-link-color', 'regular' );
echo '
}';
echo '.logobar a:hover{';
	$ats->independent_link_color( 'header-logobar-link-color', 'hover' );
echo '
}';
echo '.logobar a:active,
.logobar a:focus, .logobar .independent-main-menu > li.current-menu-item > a, .logobar .independent-main-menu > li.current-menu-ancestor > a, .logobar a.active {';
	$ats->independent_link_color( 'header-logobar-link-color', 'active' );
echo '
}';
echo '
.logobar-items > li{
    height: '. esc_attr( $ats->independent_dimension_height('header-logobar-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('header-logobar-height') ) .' ;
}
.header-sticky .logobar-items > li,
.sticky-scroll.show-menu .logobar-items > li{
	height: '. esc_attr( $ats->independent_dimension_height('header-logobar-sticky-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('header-logobar-sticky-height') ) .' ;
}';
echo '
.logobar-items > li img{
	max-height: '. esc_attr( $ats->independent_dimension_height('header-logobar-height') ) .' ;
}';
echo "\n/* Logobar Sticky Styles */\n";
$color = $ats->independent_theme_opt('sticky-header-logobar-color');
echo '.header-sticky .logobar, .sticky-scroll.show-menu .logobar{
	'. ( $color != '' ? 'color: '. $color .';' : '' );
	$ats->independent_bg_rgba( 'sticky-header-logobar-background' );
	$ats->independent_border_settings( 'sticky-header-logobar-border' );
	$ats->independent_padding_settings( 'sticky-header-logobar-padding' );
echo '
}';
echo '.header-sticky .logobar a, .sticky-scroll.show-menu .logobar a{';
	$ats->independent_link_color( 'sticky-header-logobar-link-color', 'regular' );
echo '
}';
echo '.header-sticky .logobar a:hover, .sticky-scroll.show-menu .logobar a:hover{';
	$ats->independent_link_color( 'sticky-header-logobar-link-color', 'hover' );
echo '
}';
echo '.header-sticky .logobar a:active, .sticky-scroll.show-menu .logobar a:active,
.header-sticky .logobar .independent-main-menu .current-menu-item > a, .header-sticky .logobar .independent-main-menu .current-menu-ancestor > a,
.sticky-scroll.show-menu .logobar .independent-main-menu .current-menu-item > a, .sticky-scroll.show-menu .logobar .independent-main-menu .current-menu-ancestor > a ,
.header-sticky .logobar a.active, .sticky-scroll.show-menu .logobar a.active{';
	$ats->independent_link_color( 'sticky-header-logobar-link-color', 'active' );
echo '
}';
echo '
.header-sticky .logobar img.custom-logo, .sticky-scroll.show-menu .logobar img.custom-logo{
	max-height: '. esc_attr( $ats->independent_dimension_height('header-logobar-sticky-height') ) .' ;
}';
echo "\n/* Navbar Styles */\n";
$ats->independent_custom_font_check( 'header-navbar-typography' );
echo '.navbar{';
	$ats->independent_typo_generate( 'header-navbar-typography' );
	$ats->independent_bg_rgba( 'header-navbar-background' );
	$ats->independent_border_settings( 'header-navbar-border' );
	$ats->independent_padding_settings( 'header-navbar-padding' );
echo '
}';
echo '.navbar a{';
	$ats->independent_link_color( 'header-navbar-link-color', 'regular' );
echo '
}';
echo '.navbar a:hover{';
	$ats->independent_link_color( 'header-navbar-link-color', 'hover' );
echo '
}';
echo '.navbar a:active,.navbar a:focus, .navbar .independent-main-menu > li.current-menu-item > a, .navbar .independent-main-menu > li.current-menu-ancestor > a, .navbar a.active {';
	$ats->independent_link_color( 'header-navbar-link-color', 'active' );
echo '
}';
$color = $ats->independent_theme_opt( 'header-navbar-typography' );
$color = isset( $color['color'] ) && $color['color'] != '' ? $color['color'] : '';
$scolor = $ats->independent_theme_opt( 'sticky-header-navbar-color' );
if( $color ):
echo '.navbar .secondary-space-toggle > span {
	background-color: '. esc_attr( $color ) .';
}';
endif;
if( $scolor ):
echo '.header-sticky .navbar .secondary-space-toggle > span,
.sticky-scroll.show-menu .navbar .secondary-space-toggle > span{
	background-color: '. esc_attr( $scolor ) .';
}';
endif;
echo '
.navbar-items > li {
    height: '. esc_attr( $ats->independent_dimension_height('header-navbar-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('header-navbar-height') ) .' ;
}
.header-sticky .navbar-items > li,
.sticky-scroll.show-menu .navbar-items > li{
	height: '. esc_attr( $ats->independent_dimension_height('header-navbar-sticky-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('header-navbar-sticky-height') ) .' ;
}';
echo '
.navbar-items > li img{
	max-height: '. esc_attr( $ats->independent_dimension_height('header-navbar-height') ) .' ;
}';
echo "\n/* Navbar Sticky Styles */\n";
$color = $ats->independent_theme_opt('sticky-header-navbar-color');
echo '.header-sticky .navbar, .sticky-scroll.show-menu .navbar{
	'. ( $color != '' ? 'color: '. $color .';' : '' );
	$ats->independent_bg_rgba( 'sticky-header-navbar-background' );
	$ats->independent_border_settings( 'sticky-header-navbar-border' );
	$ats->independent_padding_settings( 'sticky-header-navbar-padding' );
echo '
}';
echo '.header-sticky .navbar a, .sticky-scroll.show-menu .navbar a {';
	$ats->independent_link_color( 'sticky-header-navbar-link-color', 'regular' );
echo '
}';
echo '.header-sticky .navbar a:hover, .sticky-scroll.show-menu .navbar a:hover {';
	$ats->independent_link_color( 'sticky-header-navbar-link-color', 'hover' );
echo '
}';
echo '.header-sticky .navbar a:active, .sticky-scroll.show-menu .navbar a:active,
.header-sticky .navbar .independent-main-menu .current-menu-item > a, .header-sticky .navbar .independent-main-menu .current-menu-ancestor > a,
.sticky-scroll.show-menu .navbar .independent-main-menu .current-menu-item > a, .sticky-scroll.show-menu .navbar .independent-main-menu .current-menu-ancestor > a,
.header-sticky .navbar a.active, .sticky-scroll.show-menu .navbar a.active {';
	$ats->independent_link_color( 'sticky-header-navbar-link-color', 'active' );
echo '
}';
echo '
.header-sticky .navbar img.custom-logo, .sticky-scroll.show-menu .navbar img.custom-logo{
	max-height: '. esc_attr( $ats->independent_dimension_height('header-navbar-sticky-height') ) .' ;
}';
echo "\n/* Secondary Menu Space Styles */\n";
$sec_menu_type = $ats->independent_theme_opt('secondary-menu-type');
$ats->independent_custom_font_check( 'secondary-space-typography' );
echo '.secondary-menu-area{';
	echo 'width: '. esc_attr( $ats->independent_dimension_width('secondary-menu-space-width') ) .' ;';
echo '}';
echo '.secondary-menu-area,.secondary-menu-area .widget{';
	$ats->independent_border_settings( 'secondary-space-border' );
	$ats->independent_typo_generate( 'secondary-space-typography' );
	$ats->independent_bg_settings('secondary-space-background');
	if( $sec_menu_type == 'left-overlay' || $sec_menu_type == 'left-push' ){
		echo 'left: -' . esc_attr( $ats->independent_dimension_width('secondary-menu-space-width') ) . ';';
	}elseif( $sec_menu_type == 'right-overlay' || $sec_menu_type == 'right-push' ){
		echo 'right: -' . esc_attr( $ats->independent_dimension_width('secondary-menu-space-width') ) . ';';
	}
echo '
}';
echo '.secondary-menu-area.left-overlay, .secondary-menu-area.left-push{';
	if( $sec_menu_type == 'left-overlay' || $sec_menu_type == 'left-push' ){
		echo 'left: -' . esc_attr( $ats->independent_dimension_width('secondary-menu-space-width') ) . ';';
	}
echo '
}';
echo '.secondary-menu-area.right-overlay, .secondary-menu-area.right-push{';
	if( $sec_menu_type == 'right-overlay' || $sec_menu_type == 'right-push' ){
		echo 'right: -' . esc_attr( $ats->independent_dimension_width('secondary-menu-space-width') ) . ';';
	}
echo '
}';
echo '.secondary-menu-area .secondary-menu-area-inner{';
	$ats->independent_padding_settings( 'secondary-space-padding' );
echo '
}';
echo '.secondary-menu-area a{';
	$ats->independent_link_color( 'secondary-space-link-color', 'regular' );
echo '
}';
echo '.secondary-menu-area a:hover{';
	$ats->independent_link_color( 'secondary-space-link-color', 'hover' );
echo '
}';
echo '.secondary-menu-area a:active{';
	$ats->independent_link_color( 'secondary-space-link-color', 'active' );
echo '
}';
echo "\n/* Sticky Header Styles */\n";
if( $ats->independent_theme_opt('header-type') != 'default' ):
$sticky_width = $ats->independent_dimension_width('header-fixed-width');
echo '.sticky-header-space{
	width: '. esc_attr( $sticky_width ) .';
}';
	if( $ats->independent_theme_opt('header-type') == 'left-sticky' ):
	echo 'body, .top-sliding-bar{
		padding-left: '. esc_attr( $sticky_width ) .';
	}';
	else:
	echo 'body, .top-sliding-bar{
		padding-right: '. esc_attr( $sticky_width ) .';
	}';
	endif;
endif;
$ats->independent_custom_font_check( 'header-fixed-typography' );
echo '.sticky-header-space{';
	$ats->independent_typo_generate( 'header-fixed-typography' );
	$ats->independent_bg_settings( 'header-fixed-background' );
	$ats->independent_border_settings( 'header-fixed-border' );
	$ats->independent_padding_settings( 'header-fixed-padding' );
echo '
}';
echo '.sticky-header-space li a{';
	$ats->independent_link_color( 'header-fixed-link-color', 'regular' );
echo '
}';
echo '.sticky-header-space li a:hover{';
	$ats->independent_link_color( 'header-fixed-link-color', 'hover' );
echo '
}';
echo '.sticky-header-space li a:active{';
	$ats->independent_link_color( 'header-fixed-link-color', 'active' );
echo '
}';
echo "\n/* Mobile Header Styles */\n";
echo '
.mobile-header-items > li{
    height: '. esc_attr( $ats->independent_dimension_height('mobile-header-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('mobile-header-height') ) .' ;
}
.mobile-header .mobile-header-inner ul > li img {
	max-height: '. esc_attr( $ats->independent_dimension_height('mobile-header-height') ) .' ;
}
.mobile-header{';
	$ats->independent_bg_rgba('mobile-header-background');
echo '
}';
echo '.mobile-header-items li a{';
	$ats->independent_link_color( 'mobile-header-link-color', 'regular' );
echo '
}';
echo '.mobile-header-items li a:hover{';
	$ats->independent_link_color( 'mobile-header-link-color', 'hover' );
echo '
}';
echo '.mobile-header-items li a:active{';
	$ats->independent_link_color( 'mobile-header-link-color', 'active' );
echo '
}';
echo '
.header-sticky .mobile-header-items > li, .show-menu .mobile-header-items > li{
    height: '. esc_attr( $ats->independent_dimension_height('mobile-header-sticky-height') ) .' ;
    line-height: '. esc_attr( $ats->independent_dimension_height('mobile-header-sticky-height') ) .' ;
}
.header-sticky .mobile-header-items > li .mobile-logo img, .show-menu .mobile-header-items > li .mobile-logo img{
	max-height: '. esc_attr( $ats->independent_dimension_height('mobile-header-sticky-height') ) .' ;
}
.mobile-header .header-sticky, .mobile-header .show-menu{';
	$ats->independent_bg_rgba('mobile-header-sticky-background');
echo '}';
echo '.header-sticky .mobile-header-items li a, .show-menu .mobile-header-items li a{';
	$ats->independent_link_color( 'mobile-header-sticky-link-color', 'regular' );
echo '
}';
echo '.header-sticky .mobile-header-items li a:hover, .show-menu .mobile-header-items li a:hover{';
	$ats->independent_link_color( 'mobile-header-sticky-link-color', 'hover' );
echo '
}';
echo '.header-sticky .mobile-header-items li a:hover, .show-menu .mobile-header-items li a:hover{';
	$ats->independent_link_color( 'mobile-header-sticky-link-color', 'active' );
echo '
}';
$mm_max = $ats->independent_dimension_width( 'mobile-menu-max-width' );
if( $mm_max && $mm_max != '0px' ):
echo '.mobile-bar, .mobile-bar .container{
	max-width: '. $mm_max .';
}';
endif;
echo "\n/* Mobile Bar Styles */\n";
$ats->independent_custom_font_check( 'mobile-menu-typography' );
echo '.mobile-bar{';
	$ats->independent_typo_generate( 'mobile-menu-typography' );
	$ats->independent_bg_settings( 'mobile-menu-background' );
	$ats->independent_border_settings( 'mobile-menu-border' );
	$ats->independent_padding_settings( 'mobile-menu-padding' );
echo '
}';
echo '.mobile-bar li a{';
	$ats->independent_link_color( 'mobile-menu-link-color', 'regular' );
echo '
}';
echo '.mobile-bar li a:hover{';
	$ats->independent_link_color( 'mobile-menu-link-color', 'hover' );
echo '
}';
echo '.mobile-bar li a:active, .mobile-bar li.current-menu-item > a, 
.mobile-bar ul>li.current-menu-parent>a, .mobile-bar li.current-menu-ancestor > a{';
	$ats->independent_link_color( 'mobile-menu-link-color', 'active' );
echo '
}';
echo "\n/* Top Sliding Bar Styles */\n";
$ats->independent_custom_font_check( 'top-sliding-typography' );
if( $ats->independent_theme_opt( 'header-top-sliding-switch' ) ):
echo '.top-sliding-bar-inner{';
	$ats->independent_typo_generate( 'top-sliding-typography' );
	$ats->independent_bg_rgba( 'top-sliding-background' );
	$ats->independent_border_settings( 'top-sliding-border' );
	$ats->independent_padding_settings( 'top-sliding-padding' );
echo '
}';
$ts_bg = $ats->independent_theme_opt( 'top-sliding-background' );
echo '.top-sliding-toggle{
	'. ( $ts_bg != '' ? 'border-top-color: '. $ts_bg['rgba'] . ';' : '' ) .'
}';
echo '.top-sliding-bar-inner a{';
	$ats->independent_link_color( 'top-sliding-link-color', 'regular' );
echo '
}';
echo '.top-sliding-bar-inner a:hover{';
	$ats->independent_link_color( 'top-sliding-link-color', 'hover' );
echo '
}';
echo '.top-sliding-bar-inner a:active{';
	$ats->independent_link_color( 'top-sliding-link-color', 'active' );
echo '
}';
endif;

echo '
.header-inner .main-logo img{
    max-height: '. esc_attr( $ats->independent_dimension_height('logo-height') ) .' ;
}';

echo '
.header-inner .sticky-logo img{
    max-height: '. esc_attr( $ats->independent_dimension_height('sticky-logo-height') ) .' !important;
}';

echo '
.mobile-header .mobile-header-inner ul > li img ,
.mobile-bar-items .mobile-logo img {
    max-height: '. esc_attr( $ats->independent_dimension_height('mobile-logo-height') ) .' !important;
}';


echo "\n/* General Menu Styles */\n";
echo '.menu-tag-hot{
	background-color: '. $ats->independent_theme_opt( 'menu-tag-hot-bg' ) .';
}';
echo '.menu-tag-new{
	background-color: '. $ats->independent_theme_opt( 'menu-tag-new-bg' ) .';
}';
echo '.menu-tag-trend{
	background-color: '. $ats->independent_theme_opt( 'menu-tag-trend-bg' ) .';
}';
echo "\n/* Main Menu Styles */\n";
$ats->independent_custom_font_check( 'main-menu-typography' );
echo 'ul.independent-main-menu > li > a,
ul.independent-main-menu > li > .main-logo{';
	$ats->independent_typo_generate( 'main-menu-typography' );
echo '
}';
$mainmenu_bg = $ats->independent_theme_opt( 'header-mainmenu-bg-color' );
if( $mainmenu_bg ){
	echo 'ul.independent-main-menu > li > a {
		background-color: '. esc_attr( $mainmenu_bg ) .';
	}';
}
$mainmenu_hbg = $ats->independent_theme_opt( 'header-mainmenu-bg-hcolor' );
if( $mainmenu_hbg ){
	echo 'ul.independent-main-menu > li > a:hover, ul.independent-main-menu > li > a:active, ul.independent-main-menu > li > a:focus,
	.navbar .independent-main-menu > li.current-menu-item > a, .navbar .independent-main-menu > li.current-menu-item > a:active, .navbar .independent-main-menu > li.current-menu-item > a:focus, .navbar .independent-main-menu > li.current-menu-ancestor > a, .navbar ul>li.current-menu-parent>a  {
		background-color: '. esc_attr( $mainmenu_hbg ) .';
	}';
}
echo "\n/* Dropdown Menu Styles */\n";
echo 'ul.dropdown-menu{';
	$ats->independent_bg_rgba( 'dropdown-menu-background' );
	$ats->independent_border_settings( 'dropdown-menu-border' );
echo '
}';
$ats->independent_custom_font_check( 'dropdown-menu-typography' );
echo 'ul.dropdown-menu > li{';
	$ats->independent_typo_generate( 'dropdown-menu-typography' );
echo '
}';
echo 'ul.dropdown-menu > li a,
ul.mega-child-dropdown-menu > li a,
.header-sticky ul.dropdown-menu > li a, .sticky-scroll.show-menu ul.dropdown-menu > li a,
.header-sticky ul.mega-child-dropdown-menu > li a, .sticky-scroll.show-menu ul.mega-child-dropdown-menu > li a {';
	$ats->independent_link_color( 'dropdown-menu-link-color', 'regular' );
echo '
}';
echo 'ul.dropdown-menu > li a:hover,
ul.mega-child-dropdown-menu > li a:hover,
.header-sticky ul.dropdown-menu > li a:hover, .sticky-scroll.show-menu ul.dropdown-menu > li a:hover,
.header-sticky ul.mega-child-dropdown-menu > li a:hover, .sticky-scroll.show-menu ul.mega-child-dropdown-menu > li a:hover {';
	$ats->independent_link_color( 'dropdown-menu-link-color', 'hover' );
echo '
}';
echo 'ul.dropdown-menu > li a:active,
ul.mega-child-dropdown-menu > li a:active,
.header-sticky ul.dropdown-menu > li a:active, .sticky-scroll.show-menu ul.dropdown-menu > li a:active,
.header-sticky ul.mega-child-dropdown-menu > li a:active, .sticky-scroll.show-menu ul.mega-child-dropdown-menu > li a:active,
ul.dropdown-menu > li.current-menu-item > a, ul.dropdown-menu > li.current-menu-parent > a, ul.mega-child-dropdown-menu > li.current_page_item a,
.header-sticky .navbar .independent-main-menu .dropdown-menu .current-menu-item > a,
.header-sticky .navbar .independent-main-menu .dropdown .current-menu-ancestor>a {';
	$ats->independent_link_color( 'dropdown-menu-link-color', 'active' );
echo '
}';
/* Template Page Title Styles */
echo "\n/* Template Page Title Styles */\n";
independentPostTitileStyle( 'single-post' );
independentPostTitileStyle( 'blog' );
independentPostTitileStyle( 'page' );
$actived_tmplt = $ats->independent_theme_opt('theme-templates');
if( !empty( $actived_tmplt ) && is_array( $actived_tmplt ) ){
	foreach( $actived_tmplt as $template ){
		independentPostTitileStyle( $template );
	}
}
$actived_cat_tmplt = $ats->independent_theme_opt('theme-categories');
if( !empty( $actived_cat_tmplt ) && is_array( $actived_cat_tmplt ) ){
	foreach( $actived_cat_tmplt as $template ){
		independentPostTitileStyle( $template );
	}
}
function independentPostTitileStyle( $field ){
	$ats = new independentThemeStyles; 
	echo '.independent-'. $field .' .page-title-wrap-inner{
		color: '. $ats->independent_theme_opt( 'template-'. $field .'-color' ) .';';
		$ats->independent_bg_settings( 'template-'. $field .'-background-all' );
		$ats->independent_border_settings( 'template-'. $field .'-border' );
		$ats->independent_padding_settings( 'template-'. $field .'-padding' );
	echo '
	}';
	echo '.independent-'. $field .' .page-title-wrap a{';
		$ats->independent_link_color( 'template-'. $field .'-link-color', 'regular' );
	echo '
	}';
	echo '.independent-'. $field .' .page-title-wrap a:hover{';
		$ats->independent_link_color( 'template-'. $field .'-link-color', 'hover' );
	echo '
	}';
	echo '.independent-'. $field .' .page-title-wrap a:active{';
		$ats->independent_link_color( 'template-'. $field .'-link-color', 'active' );
	echo '
	}';
	echo '.independent-'. $field .' .page-title-wrap-inner > .page-title-overlay{';
		$ats->independent_bg_rgba( $field .'-page-title-overlay' );
	echo '
	}';
}
/* Template Article Styles */
echo "\n/* Template Article Styles */\n";
independentPostArticleStyle( 'single-post' );
independentPostArticleStyle( 'blog' );
$actived_tmplt = $ats->independent_theme_opt('theme-templates');
if( !empty( $actived_tmplt ) && is_array( $actived_tmplt ) ){
	foreach( $actived_tmplt as $template ){
		independentPostArticleStyle( $template );
	}
}
$actived_cat_tmplt = $ats->independent_theme_opt('theme-categories');
if( !empty( $actived_cat_tmplt ) && is_array( $actived_cat_tmplt ) ){
	foreach( $actived_cat_tmplt as $template ){
		independentPostArticleStyle( $template );
	}
}
function independentPostArticleStyle( $field ){
	$ats = new independentThemeStyles; 
	echo '.'. $field .'-template article.post{
		color: '. $ats->independent_theme_opt( $field .'-article-color' ) .';';
		$ats->independent_bg_rgba( $field .'-article-background' );
		$ats->independent_border_settings( $field .'-article-border' );
		$ats->independent_padding_settings( $field .'-article-padding' );
	echo '
	}';
	echo '.'. $field .'-template article.post a{';
		$ats->independent_link_color( $field .'-article-link-color', 'regular' );
	echo '
	}';
	echo '.'. $field .'-template article.post a:hover{';
		$ats->independent_link_color( $field .'-article-link-color', 'hover' );
	echo '
	}';
	echo '.'. $field .'-template article.post a:active{';
		$ats->independent_link_color( $field .'-article-link-color', 'active' );
	echo '
	}';
	$post_thumb_margin = $ats->independent_theme_opt( $field .'-article-padding' );
	if( $post_thumb_margin ):
		echo '.'. $field .'-template .post-format-wrap{
			'. ( isset( $post_thumb_margin['padding-left'] ) && $post_thumb_margin['padding-left'] != '' ? 'margin-left: -' . $post_thumb_margin['padding-left'] .';' : '' ) .'
			'. ( isset( $post_thumb_margin['padding-right'] ) && $post_thumb_margin['padding-right'] != '' ? 'margin-right: -' . $post_thumb_margin['padding-right'] .';' : '' ) .'
		}';
		echo '.'. $field .'-template .post-quote-wrap > .blockquote, .'. $field .'-template .post-link-inner, .'. $field .'-template .post-format-wrap .post-audio-wrap{
			'. ( isset( $post_thumb_margin['padding-left'] ) && $post_thumb_margin['padding-left'] != '' ? 'padding-left: ' . $post_thumb_margin['padding-left'] .';' : '' ) .'
			'. ( isset( $post_thumb_margin['padding-right'] ) && $post_thumb_margin['padding-right'] != '' ? 'padding-right: ' . $post_thumb_margin['padding-right'] .';' : '' ) .'
		}';
	endif;
}
$theme_color = $ats->independentThemeColor();
echo "\n/* Blockquote / Audio / Link Styles */\n";
echo '.post-quote-wrap > .blockquote{
	border-left-color: '. esc_attr( $theme_color ) .';
}';
echo '.post-audio-wrap{
	background-color: '. esc_attr( $theme_color ) .';
}';
$rgba_08 = $ats->independent_hex2rgba( $theme_color, '0.8' );
// Single Post Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'single-post-quote-format' );
independentQuoteDynamicStyle( 'single-post', $blockquote_bg_opt, $theme_color, $rgba_08 );
// Blog Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'blog-quote-format' );
independentQuoteDynamicStyle( 'blog', $blockquote_bg_opt, $theme_color, $rgba_08 );
// Archive Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'archive-quote-format' );
independentQuoteDynamicStyle( 'archive', $blockquote_bg_opt, $theme_color, $rgba_08 );
// Tag Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'tag-quote-format' );
independentQuoteDynamicStyle( 'tag', $blockquote_bg_opt, $theme_color, $rgba_08 );
// Search Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'search-quote-format' );
independentQuoteDynamicStyle( 'search', $blockquote_bg_opt, $theme_color, $rgba_08 );
// Author Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'author-quote-format' );
independentQuoteDynamicStyle( 'author', $blockquote_bg_opt, $theme_color, $rgba_08 );
// Category Blockquote
$blockquote_bg_opt = $ats->independent_theme_opt( 'category-quote-format' );
independentQuoteDynamicStyle( 'category', $blockquote_bg_opt, $theme_color, $rgba_08 );
// All Category Blockquote
$actived_cat_tmplt = $ats->independent_theme_opt('theme-categories');
if( !empty( $actived_cat_tmplt ) && is_array( $actived_cat_tmplt ) ){
	foreach( $actived_cat_tmplt as $template ){
		$blockquote_bg_opt = $ats->independent_theme_opt( $template.'-quote-format' );
		independentQuoteDynamicStyle( $template, $blockquote_bg_opt, $theme_color, $rgba_08 );
	}
}
function independentQuoteDynamicStyle( $field, $value, $theme_color, $rgba_08 ){
	if( $value == 'none' ):
		echo '.'. $field .'-template .post-quote-wrap > .blockquote{
			background-color: #333;
		}';
	elseif( $value == 'theme' ):
		echo '.'. $field .'-template .post-quote-wrap > .blockquote{
			background-color: '. $theme_color .';
			border-left-color: #333;
		}';
	elseif( $value == 'theme-overlay' ):
		echo '.'. $field .'-template .post-quote-wrap > .blockquote{
			background-color: '. $rgba_08 .';
		}';
	elseif( $value == 'featured' ):
		echo '.'. $field .'-template .post-quote-wrap > .blockquote{
			background-color: rgba(0, 0, 0, 0.7);
		}';
	endif;
}
/* Single Post Link */
$link_bg_opt = $ats->independent_theme_opt( 'single-post-link-format' );
independentLinkDynamicStyle( 'single-post', $link_bg_opt, $theme_color, $rgba_08 );
/* Blog Link */
$link_bg_opt = $ats->independent_theme_opt( 'blog-link-format' );
independentLinkDynamicStyle( 'blog', $link_bg_opt, $theme_color, $rgba_08 );
/* Archive Link */
$link_bg_opt = $ats->independent_theme_opt( 'archive-link-format' );
independentLinkDynamicStyle( 'archive', $link_bg_opt, $theme_color, $rgba_08 );
/* Tag Link */
$link_bg_opt = $ats->independent_theme_opt( 'tag-link-format' );
independentLinkDynamicStyle( 'tag', $link_bg_opt, $theme_color, $rgba_08 );
/* Author Link */
$link_bg_opt = $ats->independent_theme_opt( 'author-link-format' );
independentLinkDynamicStyle( 'author', $link_bg_opt, $theme_color, $rgba_08 );
/* Search Link */
$link_bg_opt = $ats->independent_theme_opt( 'search-link-format' );
independentLinkDynamicStyle( 'search', $link_bg_opt, $theme_color, $rgba_08 );
/* Catgeory Link */
$link_bg_opt = $ats->independent_theme_opt( 'category-link-format' );
independentLinkDynamicStyle( 'category', $link_bg_opt, $theme_color, $rgba_08 );
// All Category Link
$actived_cat_tmplt = $ats->independent_theme_opt('theme-categories');
if( !empty( $actived_cat_tmplt ) && is_array( $actived_cat_tmplt ) ){
	foreach( $actived_cat_tmplt as $template ){
		$link_bg_opt = $ats->independent_theme_opt( $template.'-link-format' );
		independentLinkDynamicStyle( $template, $link_bg_opt, $theme_color, $rgba_08 );
	}
}
function independentLinkDynamicStyle( $field, $value, $theme_color, $rgba_08 ){
	if( $value == 'none' ):
		echo '.'. $field .'-template .post-link-inner{
			background-color: #333;
		}';
	elseif( $value == 'theme' ):
		echo '.'. $field .'-template .post-link-inner{
			background-color: '. $theme_color .';
		}';
	elseif( $value == 'theme-overlay' ):
		echo '.'. $field .'-template .post-link-inner{
			background-color: '. $rgba_08 .';
		}';
	elseif( $value == 'featured' ):
		echo '.'. $field .'-template .post-link-inner{
			background-color: rgba(0, 0, 0, 0.7);
		}';
	endif;
}
echo "\n/* Post Item Overlay Styles */\n";
echo '.post-overlay-items{
	color: '. $ats->independent_theme_opt( 'single-post-article-overlay-color' ) .';';
	$ats->independent_bg_rgba( 'single-post-article-overlay-background' );
	$ats->independent_border_settings( 'single-post-article-overlay-border' );
	$ats->independent_padding_settings( 'single-post-article-overlay-padding' );
	$ats->independent_margin_settings( 'single-post-article-overlay-margin' );
	
echo '
}';
echo '.post-overlay-items a{';
	$ats->independent_link_color( 'single-post-article-overlay-link-color', 'regular' );
echo '
}';
echo '.post-overlay-items a:hover{';
	$ats->independent_link_color( 'single-post-article-overlay-link-color', 'hover' );
echo '
}';
echo '.post-overlay-items a:hover{';
	$ats->independent_link_color( 'single-post-article-overlay-link-color', 'active' );
echo '
}';
/* Extra Styles */
echo "\n/* Footer Styles */\n";
echo '.site-footer{';
	$ats->independent_typo_generate( 'footer-typography' );
	$ats->independent_bg_settings( 'footer-background' );
	$ats->independent_border_settings( 'footer-border' );
	$ats->independent_padding_settings( 'footer-padding' );
echo '
}';
echo '.site-footer .widget{';
	$ats->independent_typo_generate( 'footer-typography' );
echo '
}';
$bg_overlay = $ats->independent_theme_opt( 'footer-background-overlay' );
if( !empty( $bg_overlay ) && isset( $bg_overlay['rgba'] ) ):
echo '
footer.site-footer:before {
	position: absolute;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	content: "";
	background-color: '. esc_attr( $bg_overlay['rgba'] ) .';
}';
endif;
echo '.site-footer a{';
	$ats->independent_link_color( 'footer-link-color', 'regular' );
echo '
}';
echo '.site-footer a:hover{';
	$ats->independent_link_color( 'footer-link-color', 'hover' );
echo '
}';
echo '.site-footer a:hover{';
	$ats->independent_link_color( 'footer-link-color', 'active' );
echo '
}';
echo "\n/* Footer Top Styles */\n";
$ats->independent_custom_font_check( 'footer-top-typography' );
echo '.footer-top-wrap{';
	$ats->independent_typo_generate( 'footer-top-typography' );
	$ats->independent_bg_rgba( 'footer-top-background' );
	$ats->independent_border_settings( 'footer-top-border' );
	$ats->independent_padding_settings( 'footer-top-padding' );
	$ats->independent_margin_settings( 'footer-top-margin' );
echo '
}';
echo '.footer-top-wrap .widget{';
	$ats->independent_typo_generate( 'footer-top-typography' );
echo '
}';
echo '.footer-top-wrap a{';
	$ats->independent_link_color( 'footer-top-link-color', 'regular' );
echo '
}';
echo '.footer-top-wrap a:hover{';
	$ats->independent_link_color( 'footer-top-link-color', 'hover' );
echo '
}';
echo '.footer-top-wrap a:hover{';
	$ats->independent_link_color( 'footer-top-link-color', 'active' );
echo '
}';
echo '.footer-top-wrap .widget .widget-title {
	color: '. esc_attr( $ats->independent_theme_opt( 'footer-top-title-color' ) ) .';
}';
echo "\n/* Footer Middle Styles */\n";
$ats->independent_custom_font_check( 'footer-middle-typography' );
echo '.footer-middle-wrap{';
	$ats->independent_typo_generate( 'footer-middle-typography' );
	$ats->independent_bg_rgba( 'footer-middle-background' );
	$ats->independent_border_settings( 'footer-middle-border' );
	$ats->independent_padding_settings( 'footer-middle-padding' );
	$ats->independent_margin_settings( 'footer-middle-margin' );
echo '
}';
echo '.footer-middle-wrap .widget{';
	$ats->independent_typo_generate( 'footer-middle-typography' );
echo '
}';
echo '.footer-middle-wrap a{';
	$ats->independent_link_color( 'footer-middle-link-color', 'regular' );
echo '
}';
echo '.footer-middle-wrap a:hover{';
	$ats->independent_link_color( 'footer-middle-link-color', 'hover' );
echo '
}';
echo '.footer-middle-wrap a:active{';
	$ats->independent_link_color( 'footer-middle-link-color', 'active' );
echo '
}';
echo '.footer-middle-wrap .widget .widget-title {
	color: '. esc_attr( $ats->independent_theme_opt( 'footer-middle-title-color' ) ) .';
}';
echo "\n/* Footer Bottom Styles */\n";
$ats->independent_custom_font_check( 'footer-bottom-typography' );
echo '.footer-bottom{';
	$ats->independent_typo_generate( 'footer-bottom-typography' );
	$ats->independent_bg_rgba( 'footer-bottom-background' );
	$ats->independent_border_settings( 'footer-bottom-border' );
	$ats->independent_padding_settings( 'footer-bottom-padding' );
	$ats->independent_margin_settings( 'footer-bottom-margin' );
echo '
}';
echo '.footer-bottom .widget{';
	$ats->independent_typo_generate( 'footer-bottom-typography' );
echo '
}';
echo '.footer-bottom a{';
	$ats->independent_link_color( 'footer-bottom-link-color', 'regular' );
echo '
}';
echo '.footer-bottom a:hover{';
	$ats->independent_link_color( 'footer-bottom-link-color', 'hover' );
echo '
}';
echo '.footer-bottom a:active{';
	$ats->independent_link_color( 'footer-bottom-link-color', 'active' );
echo '
}';
echo '.footer-bottom-wrap .widget .widget-title {
	color: '. esc_attr( $ats->independent_theme_opt( 'footer-bottom-title-color' ) ) .';
}';
echo "\n/* Theme Extra Styles */\n";
//Here your code
$theme_link_color = $ats->independent_get_link_color( 'theme-link-color', 'regular' );
$theme_link_hover = $ats->independent_get_link_color( 'theme-link-color', 'hover' );
$theme_link_active = $ats->independent_get_link_color( 'theme-link-color', 'active' );
$rgb = $ats->independent_hex2rgba( $theme_color, 'none' );
/*
 * Theme Color -> $theme_color
 * Theme RGBA -> $rgb example -> echo 'body{ background: rgba('. esc_attr( $rgb ) .', 0.5); }';
 * Link Colors -> $theme_link_color, $theme_link_hover, $theme_link_active
 */
echo '.theme-color {
	color: '. esc_attr( $theme_color ) .';
}';
echo '.theme-color-bg {
	background-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- News Blocks Style----------- */\n";
echo '.category-tag {
	background-color: '. esc_attr( $theme_color ) .';
}';
$cat_templates = $ats->independent_theme_opt( 'theme-categories' );
if( !empty( $cat_templates ) ){
	foreach( $cat_templates as $cat_name ){
		$cat_key = str_replace( "category-", "", $cat_name );
		$cat_id = $cat_key;
		$cat_key = "category-" . $cat_key;
		$cat_color = $ats->independent_theme_opt( esc_attr( $cat_key ) . '-tag-color' );
		echo '.category-tag.cat-tag-'. esc_attr( $cat_id ) .'{background-color:'. esc_attr( $cat_color ) .';}';
	}
}
echo "\n/*----------- General Style----------- */\n";
echo '::selection {
	background : '. esc_attr( $theme_color ) .';
}';
echo '.top-sliding-toggle.fa-minus {
	border-top-color : '. esc_attr( $theme_color ) .';
}';
echo '.typo-white .entry-title:hover {
	color : '. esc_attr( $theme_color ) .';
}'; 
echo "\n/*----------- Menu----------- */\n";
echo '.dropdown:hover > .dropdown-menu {
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Search Style----------- */\n";
echo '.search-form .input-group .btn,
.full-search-wrapper .input-group-btn {
	background: '. esc_attr( $theme_color ) .';
}';
echo '.full-search-wrapper .search-form .input-group .btn {
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.form-control:focus ,input.search-field:focus{
	border-color: '. esc_attr( $theme_color ) .' !important;
}';
echo "\n/*----------- Button Style----------- */\n";
echo '.btn, button , .btn.bordered:hover, .post-grid a.read-more,
.post-password-form input[type="submit"] {
	background: '. esc_attr( $theme_color ) .';
}';
echo '.btn.classic:hover {
	background: '. esc_attr( $theme_color ) .';
}';
echo '.btn.bordered {
	border-color: '. esc_attr( $theme_color ) .';
	color: '. esc_attr( $theme_color ) .';
}';
echo '.btn:hover, button:hover, .search-form .input-group .btn:hover {
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/* -----------Pagination Style----------- */\n";
echo '.nav.pagination > li.nav-item.active span, 
.nav.pagination > li.nav-item a:hover, .post-comments .page-numbers.current, .post-comments .page-numbers:hover { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Select Style ----------- */\n";
echo 'select:focus, .wpcf7 textarea:focus, .wpcf7 input:focus, .wpcf7 select:focus {
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Header Styles---------------- */\n";
echo '.close:before, .close:after, .secondary-space-toggle.active { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.nav-link:focus, .nav-link:hover { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.zmm-dropdown-toggle { 
	color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Post Style----------- */\n";
echo '.post-more .read-more:hover, .post-meta .read-more:hover {
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Post Meta----------- */\n";
echo '.post-meta li a:hover, .post-meta .post-date a:hover, .post-meta .post-category a:hover,
article.post .post-format-wrap .post-overlay-items .post-meta ul li a:hover {
	color: '. esc_attr( $theme_color ) .';
}';
/*echo '.independent-single-post .author-url a:hover,
.post-navigation .nav-links .nav-next, .post-navigation .nav-links .nav-previous {
	background: '. esc_attr( $theme_color ) .';
}';*/
echo "\n/*----------- Post Navigation ---------*/\n";
/*echo '.post-navigation .nav-links .nav-next a, .post-navigation .nav-links .nav-previous a {
	background: '. esc_attr( $theme_color ) .';
}';
*/
echo "\n/*----------- Calender---------------- */\n";
echo '.calendar_wrap th ,tfoot td { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Calender---------------- */\n";
//echo '.comments-wrap > * i, .post-comments-wrapper * a {
	//color: '. esc_attr( $theme_color ) .';
//}';
echo "\n/*----------- Archive---------------- */\n";
echo '.widget_archive li:before { 
	color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Instagram widget---------------- */\n";
echo '.null-instagram-feed p a { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Tag Cloud---------------- */\n";
/*echo '.widget.widget_tag_cloud a.tag-cloud-link { 
	background: '. esc_attr( $theme_color ) .';
}';*/
echo "\n/*----------- Service Menu---------------- */\n";
echo '.widget-area .widget .menu-item-object-independent-service a:hover,
.widget-area .widget .menu-item-object-independent-service.current-menu-item a { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Service Menu---------------- */\n";
echo '.widget-area .widget .menu-item-object-independent-service a { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Post Nav---------------- */\n";
/*echo '.zozo_advance_tab_post_widget .nav-tabs .nav-item.show .nav-link, .widget .nav-tabs .nav-link.active { 
	background: '. esc_attr( $theme_color ) .';
}';*/
echo '.widget.independent_latest_post_widget a:hover,
.zozo_advance_tab_post_widget a:hover { 
	color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Back to top---------------- */\n";
echo '.back-to-top > i { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Owl Carousel---------------- */\n";
echo '.owl-dot span , .owl-prev, .owl-next  { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Shortcodes---------------- */\n";
echo '.typo-white .client-name:hover,.team-wrapper .team-overlay a.client-name:hover,.team-dark .client-name:hover,
.entry-title a:hover,.blog-dark .entry-title a:hover,
.portfolio-title a:hover, .portfolio-overlay a:hover,
.services-dark .entry-title:hover,.entry-title:hover { 
	color: '. esc_attr( $theme_color ) .' !important;
}';
echo '.title-separator.separator-border { 
	background-color: '. esc_attr( $theme_color ) .';
}';
echo '.twitter-3 .tweet-info { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.twitter-dark a:hover { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.header-inner .media i { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Blog---------------- */\n";
echo '.blog-dark .blog-inner a:hover { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.blog-style-3 .post-thumb { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Blog List---------------- */\n";
/*echo '.list-layout .post-more .read-more { 
	color: '. esc_attr( $theme_color ) .';
}';
*/
echo "\n/*----------- Pricing table---------------- */\n";
echo '.price-text { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.pricing-style-3 .pricing-inner-wrapper,.pricing-table-wrapper .btn:hover { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.pricing-style-2 .price-text p { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.pricing-style-3 ul.pricing-features-list > li {
	box-shadow: 0 7px 10px -9px rgba('. esc_attr( $rgb ) .', 0.8);
}';
echo "\n/*-----------Compare Pricing table---------------- */\n";
echo '.compare-pricing-wrapper .pricing-table-head, .compare-features-wrap { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Counter Style---------------- */\n";
echo '.counter-wrapper.counter-style-2 { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.font-big .counter-value h3 span { 
	color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Testimonials---------------- */\n";
echo '.testimonial-wrapper.testimonial-3 .testimonial-inner, .testimonial-wrapper.testimonial-3 .testimonial-thumb img { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.testimonial-3 .testimonial-name a { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Events---------------- */\n";
echo '.events-date { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.events-date { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.events-inner .read-more:hover { 
	color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Team---------------- */\n";
echo '.team-wrapper.team-3 .team-inner > .team-thumb { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.team-1 .team-designation > p { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.team-2 .team-overlay-actived .team-thumb:before, .team-2 .team-overlay-actived .team-thumb:after { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.single-independent-team .team-social-wrap ul.social-icons > li > a:hover { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Portfolio---------------- */\n";
echo '.portfolio-masonry-layout .portfolio-angle .portfolio-title h4:after {
	background-color: '. esc_attr( $theme_color ) .';
}';
/*Meta Icon*/
echo 'span.portfolio-meta-icon , ul.portfolio-share.social-icons > li > a:hover{
	color: '. esc_attr( $theme_color ) .';
}';
/*CPT Filter Styles*/
echo '.portfolio-filter.filter-1 ul > li.active > a, .portfolio-filter.filter-1 ul > li > a:hover {
	background-color: '. esc_attr( $theme_color ) .';
}';
echo '.portfolio-filter.filter-1 ul > li > a, .portfolio-filter.filter-1 ul > li > a:hover {
	border: solid 1px '. esc_attr( $theme_color ) .';
}';
echo '.portfolio-filter.filter-1 ul > li > a {
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.portfolio-masonry-layout .portfolio-classic .portfolio-content-wrap {
	background: '. esc_attr( $theme_color ) .';
}';
echo '.portfolio-filter.filter-2 .active a.portfolio-filter-item {
	color: '. esc_attr( $theme_color ) .';
}';
echo '.portfolio-slide .portfolio-content-wrap > .portfolio-title {
	background: '. esc_attr( $theme_color ) .';
}'; 
echo '.portfolio-minimal .portfolio-overlay-wrap:before,
.portfolio-minimal .portfolio-overlay-wrap:after { 
 border-color: '. esc_attr( $theme_color ) .';
}';
echo '.portfolio-angle .portfolio-overlay-wrap:before { 
 background: linear-gradient(-45deg, rgba(0, 0, 0, 0.75) 0%, rgba('. esc_attr( $rgb ) .', 0.86) 100%);
 
}';
echo '.portfolio-overlay .portfolio-icons a:hover i { 
	color: '. esc_attr( $theme_color ) .';
}';
 
echo "\n/*-----------Feature Box---------------- */\n";
echo 'span.feature-box-ribbon { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Flipbox---------------- */\n";
echo "[class^='imghvr-shutter-out-']:before, [class*=' imghvr-shutter-out-']:before,
[class^='imghvr-shutter-in-']:after, [class^='imghvr-shutter-in-']:before, [class*=' imghvr-shutter-in-']:after, [class*=' imghvr-shutter-in-']:before,
[class^='imghvr-reveal-']:before, [class*=' imghvr-reveal-']:before { 
	background: ". esc_attr( $theme_color ) .";
}";
echo "\n/*-----------Progress Bar---------------- */\n";
echo '.vc_progress_bar .vc_single_bar .vc_bar { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Accordion---------------- */\n";
echo '.wpb-js-composer .transparent-accordion.vc_tta-color-grey.vc_tta-style-outline .vc_tta-panel .vc_tta-panel-heading { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Services---------------- */\n";
echo '.services-2 .services-title a { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.services-wrapper.services-1 .services-inner > .services-thumb , .services-3 .services-inner > .services-thumb { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Contact form 7---------------- */\n";
echo '.wpcf7 input[type="submit"] { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.contact-form-classic .wpcf7 textarea, .contact-form-classic .wpcf7 input, .contact-form-classic .wpcf7 select,
.wpcf7 textarea:focus, .wpcf7 input:focus, .wpcf7 select:focus, .invalid div.wpcf7-validation-errors { 
 	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.contact-form-grey .wpcf7 textarea:focus, .contact-form-grey .wpcf7 input:focus, .contact-form-grey .wpcf7 select:focus { 
 	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.mptt-shortcode-wrapper ul.mptt-menu.mptt-navigation-tabs li.active a, .mptt-shortcode-wrapper ul.mptt-menu.mptt-navigation-tabs li:hover a { 
 	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.mptt-shortcode-wrapper ul.mptt-menu.mptt-navigation-tabs li.active a, .mptt-shortcode-wrapper ul.mptt-menu.mptt-navigation-tabs li:hover a { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Tab---------------- */\n";
echo '.wpb_tabs .wpb_tabs_nav li { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.wpb-js-composer .vc_tta-container .vc_tta-style-classic.theme-tab .vc_tta-tab> a { 
	background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Flipbox---------------- */\n";
echo "\n/*-----------Charity---------------- */\n";
echo '.donate-button.button { 
	background: '. esc_attr( $theme_color ) .';
}';
echo '.campaign-raised .amount, .campaign-figures .amount, .donors-count, .time-left, .charitable-form-field a:not(.button), .charitable-form-fields .charitable-fieldset a:not(.button), .charitable-notice, .charitable-notice .errors a {
	color: '. esc_attr( $theme_color ) .' !important;
}';
echo '.campaign-progress-bar .bar, .donate-button, .charitable-donation-form .donation-amount.selected, .charitable-donation-amount-form .donation-amount.selected {
	background: '. esc_attr( $theme_color ) .' !important;
}';
echo "\n/*-----------Woocommerce---------------- */\n";
echo '.woocommerce ul.products li.product .price,
.woocommerce .product .price,
.woocommerce.single  .product .price,
.woocommerce p.stars a, .woocommerce ul.products li.product .woocommerce-loop-product__title:hover, .woocommerce ul.products li.product:hover .woocommerce-loop-product__title { 
 color: '. esc_attr( $theme_color ) .';
}';
echo '.woocommerce .product .onsale,.woocommerce nav.woocommerce-pagination ul li span.current,
.woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce #respond input#submit, 
.woocommerce a.button, .woocommerce button.button, .woocommerce input.button { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo '.woocommerce .woocommerce-error .button, 
.woocommerce .woocommerce-info .button, 
.woocommerce .woocommerce-message .button, 
.woocommerce-page .woocommerce-error .button, 
.woocommerce-page .woocommerce-info .button, 
.woocommerce-page .woocommerce-message .button,
.woocommerce ul.products li.product .button, 
.woocommerce ul.products li.product .added_to_cart { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo '.woocommerce .product .button, 
.woocommerce.single .product .button,
.woocommerce #review_form #respond .form-submit input,
.woocommerce button.button,
.woocommerce ul.products li.product .woo-thumb-wrap .button:hover,
.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo '.woocommerce .widget_price_filter .ui-slider .ui-slider-range { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo '.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,.dropdown-menu.cart-dropdown-menu .mini-view-cart a { 
 background: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Widgets---------------- */\n";
echo '.widget .widget-title:before,
.related-articles-wrap .related-title:before { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo '.widget .nav.nav-tabs { 
	border-color: '. esc_attr( $theme_color ) .';
}';
echo '.widget-title { 
	border-top-color: '. esc_attr( $theme_color ) .';
}';
echo '.widget.independent_latest_post_widget li .side-item:hover .post-serial { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- Scrollbar ---------------- */\n";
echo '.player-type-3 .video-playlist-wrapper::-webkit-scrollbar-thumb,
.video-playlist-nav > i { 
 background-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*----------- News Related Styles ---------------- */\n";
echo "\n/*----------- Banner ---------------- */\n";
echo '.category-tag { 
 	color: '. esc_attr( $theme_color ) .';
}';
echo '.category-tag:after, .independent-block-overlay .post-category > a:before, .category-tag:hover:after {
 	border-top-color: '. esc_attr( $theme_color ) .';
}';
echo '.independent-block-title.title-style-1:before, .independent-block-overlay .post-category > a { 
 	background-color: '. esc_attr( $theme_color ) .';
}';
echo '.gradient-theme-overlay { 
 background: linear-gradient(-45deg, rgba(0, 0, 0, 0) 0%, rgba('. esc_attr( $rgb ) .', 0.86) 100%);
}';
echo '.hvr-overlay-wrapper:hover .gradient-theme-overlay {
 background: linear-gradient(-45deg, rgba('. esc_attr( $rgb ) .', 0.86), rgba(0, 0, 0, 0.99) 100%);
}';
echo '.independent-block .news-tabs ul.more-tab-dropdown > li > a:hover,
.independent-news-slider .independent-block-overlay .post-meta li a span.before-icon,
.independent-block.independent-block-3.custom-list-dark .independent-block-list.big-list .post-title a:hover,
.nav.news-tabs > li > a:hover, .nav.news-tabs > li.active > a { 
 color: '. esc_attr( $theme_color ) .';
}';
echo '.news-ticker-title, .news-ticker-navs > i,
.mega-dropdown-menu .independent-block .news-tabs > li.active { 
 	background-color: '. esc_attr( $theme_color ) .';
}';
echo '.independent-block.independent-block-3.custom-list-dark .independent-block-list.big-list .post-list-content a.read-more { 
 	background-color: '. esc_attr( $theme_color ) .';
}';
echo '.independent-slider-nav .slide-nav > li > a:hover, .page-links > a:hover, .page-links > span { 
 	background-color: '. esc_attr( $theme_color ) .';
	border-color: '. esc_attr( $theme_color ) .';
}';
echo "\n/*-----------Social Widget---------------- */\n";
echo 'ul.social-icons.social-theme > li a, 
ul.social-icons.social-hbg-theme > li a:hover,
ul.social-icons.social-bg-light > li a:hover {
	background-color: '. esc_attr( $theme_color ) .';
}';
echo 'ul.social-icons.social-bg-light > li a:hover,
.single-post-template article ul.social-icons > li > a:hover {
	background-color: '. esc_attr( $theme_color ) .';
}';
echo 'ul.social-icons.social-theme > li a, 
ul.social-icons.social-h-theme > li a:hover,
.custom-post-nav ul.social-icons > li > a:hover,
.topbar-items ul.social-icons > li > a:hover { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.most-viewed-post-image .most-viewed-index,
.topbar span.site-date, .mega-dropdown-menu .independent-block .post-grid .post-thumb-wrapper:after {
	background-color: '. esc_attr( $theme_color ) .';
}';
echo '.typo-dark .independent-block-slide .independent-block-overlay.typo-white .post-title:hover, 
.typo-dark .independent-block-slide .independent-block-overlay.typo-white .post-title > a:hover,
.crypto-short-info .crypto-usd { 
	color: '. esc_attr( $theme_color ) .';
}';
echo '.vc_row div.btcwdgt-chart .stats, 
.vc_row div.btcwdgt-chart .btcwdgt-header h2, 
.vc_row div.btcwdgt-text-ticker .btcwdgt-edge, .theme-bg { 
	background-color: '. esc_attr( $theme_color ) .'!important;
}';

echo '.wp-block-button.is-style-outline .wp-block-button__link:not(.has-text-color) {
color: '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}
.wp-block-quote:not(.is-large):not(.is-style-large),
.wp-block-freeform blockquote,
.single-post-template blockquote {
border-left: 2px solid '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}
blockquote.wp-block-quote.is-large p, 
blockquote.wp-block-quote.is-style-large p {
	border-left-color: '. esc_attr( $theme_color ) .';
}
.entry-content .wp-block-quote[style*="text-align:right"]:not(.is-large):not(.is-style-large) {
	border-right: 2px solid '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}
.wp-block-file .wp-block-file__button,
.wp-block-button:not(.is-style-outline) .wp-block-button__link:not(.has-background) {
background-color: '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}

/* Hover colors */
.wp-block-file .wp-block-file__textlink:hover {
color: '. esc_attr( $theme_color ) .'; /* base: #005177; */
}

/* Do not overwrite solid color pullquote or cover links */
.wp-block-pullquote.is-style-solid-color a,
.wp-block-cover a {
color: inherit;
}';

/* Theme Demo Styles */
//Demo Style
$demo_id = '';
$custom_demo_type = $ats->independent_theme_opt('demo-id-style');
if( $custom_demo_type != 'none' ){
	if( $custom_demo_type != 'demo-main' ){
		$demo_id = $custom_demo_type;
	}
}else{
	$demo_type = get_theme_mod('independent_installed_demo_id');
	if( !empty( $demo_type ) && $demo_type != 'demo-main' ){
		$demo_id = $demo_type;
	}
}

echo '/* Demo id: '. $demo_id .' */';

if( $demo_id ){
	switch( $demo_id ){
		//Cryptocurrency Demo Theme Styles
		case "demo-cryptocurrency":
			echo '.independent-block-title.title-style-1:before, .widget .widget-title:before{border-top-color: '. esc_attr( $theme_color ) .';}.independent-block-list.big-list .post-category > a{background: '. esc_attr( $theme_color ) .';}.nav.pagination > li.nav-item a, .nav.pagination > li.nav-item span, .independent-slider-nav .slide-nav > li > a, .independent-main-menu .independent-slider-nav .slide-nav > li > a{color: '. esc_attr( $theme_color ) .';}.nav.pagination > li.nav-item a, .nav.pagination > li.nav-item span, .independent-slider-nav .slide-nav > li > a, .independent-main-menu .independent-slider-nav .slide-nav > li > a{border-color: '. esc_attr( $theme_color ) .';}';
		break;
		//Technology Demo Theme Styles
		case "demo-technology":
			echo '.vc_row .widget .widget-title:before, .widget-area .widget .widget-title:before, .secondary-menu-area .widget .widget-title:before, .secondary-menu-area .vc_row .widget .widget-title:after, .independent-block .independent-block-title.title-style-3:before{border-top-color: '. esc_attr( $theme_color ) .';}.blog-template .post-thumb-wrapper .category-tag, .widget-area .widget .widget-title, .vc_row .widget .widget-title, .secondary-menu-area .widget .widget-title, .secondary-menu-area .vc_row .widget .widget-title, .independent-block .independent-block-title.title-style-3, .independent-block-list.big-list .post-category > a{background: '. esc_attr( $theme_color ) .';}.post-meta .post-date span.before-icon, .post-meta .post-author .author-name:before, .independent-content-inner span.before-icon, .post-comments span.date:before, .post-navigation .nav-links .nav-previous a:hover, .post-navigation .nav-links .nav-next a:hover {color: '. esc_attr( $theme_color ) .';}';
		break;
		//Travel Demo Theme Styles
		case "demo-travel":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .' !important;}.post-meta .post-category a, .owl-dot.active span{background: '. esc_attr( $theme_color ) .' !important;}.post-meta .post-category a:before, .independent-block-list.big-list .post-category > a:after, .post-meta .post-category a:before{border-top-color: '. esc_attr( $theme_color ) .' !important;}';
		break;
		//Food Demo Theme Styles
		case "demo-food":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .' !important;}.owl-dot.active span, .blog-template .post-thumb-wrapper .category-tag, .independent-block-post .nav.post-meta > li.post-category > a{background: '. esc_attr( $theme_color ) .' !important;}';
		break;
		//Daily News Demo Theme Styles
		case "demo-daily-news":
			echo '.independent-slider-nav .slide-nav > li > a, .nav.pagination > li.nav-item a, .nav.pagination > li.nav-item span{color: '. esc_attr( $theme_color ) .';}.post .post-category a, .independent-block-list.big-list .post-category > a{background: '. esc_attr( $theme_color ) .';}.independent-block-title span, .widget-area .widget .widget-title, .vc_row .widget .widget-title, .site-footer .widget-title, .footer-middle-wrap .widget .widget-title{border-left-color: '. esc_attr( $theme_color ) .';}.independent-slider-nav .slide-nav > li > a{border-color: '. esc_attr( $theme_color ) .';}';
		break;
		//Sports Demo Theme Styles
		case "demo-sports":
			echo '.topbar span.site-date:before{color: '. esc_attr( $theme_color ) .';}.independent-block-overlay .post-category > a, .post-thumb-wrapper .category-tag, .category-tag, .independent-block-list.big-list .post-category > a, .post .post-category a, .independent-block-title span, .wpb_widgetised_column .independent-block-title, .widget-area .widget .widget-title, .vc_row .widget .widget-title, .wpb_widgetised_column .independent-block-title, .widget-area .independent-block-title {background: '. esc_attr( $theme_color ) .';}.page-title-wrap-inner, .independent-block-title, .independent-block-title span:after{border-bottom-color: '. esc_attr( $theme_color ) .';}';
		break;
		//General News Demo Theme Styles
		case "demo-general-news":
			echo 'span.before-icon{color: '. esc_attr( $theme_color ) .';}.independent-block-title.title-style-1:before, .widget .widget-title:before{border-top-color: '. esc_attr( $theme_color ) .';}';
		break;
		//Organic Demo Theme Styles
		case "demo-organic":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .'!important;}.independent-block-list.big-list .post-category > a {color: '. esc_attr( $theme_color ) .';}.widget-area, .vc_row .wpb_widgetised_column{border-top-color: '. esc_attr( $theme_color ) .';}.organic-tabs .independent-block-title{background: '. esc_attr( $theme_color ) .';}.owl-dot.active span{background: '. esc_attr( $theme_color ) .'!important;}';
		break;
		//Lifestyle Demo Theme Styles
		case "demo-lifestyle":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .'!important;}.owl-nav > div:hover, .post-read-more a.read-more{color: '. esc_attr( $theme_color ) .';}.owl-nav > div:hover{border-color: '. esc_attr( $theme_color ) .';}.organic-tabs .independent-block-title{background: '. esc_attr( $theme_color ) .';}.post-meta .post-category a, .owl-dot.active span {background: '. esc_attr( $theme_color ) .'!important;}';
		break;
		//Business Demo Theme Styles
		case "demo-business":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .'!important;}.independent-slider-nav .slide-nav > li > a, .post-meta .post-author .author-name{color: '. esc_attr( $theme_color ) .';}.independent-block-title span{border-left-color: '. esc_attr( $theme_color ) .';}.independent-slider-nav .slide-nav > li > a{border-color: '. esc_attr( $theme_color ) .';}.navbar .independent-main-menu > li.current-menu-item > a:before, .navbar .independent-main-menu > li.current-menu-ancestor > a:before, .widget .widget-title:after, .widget .independent-block-title:after, .independent-block.independent-block-8 .post-thumb-wrapper .category-tag, .independent-block.independent-block-2 .post-thumb-wrapper .category-tag, .site-footer .independent-block-title:after, .independent-block.independent-block-1 .post-grid > .post-thumb-wrapper .category-tag, .independent-block-title span:before{background: '. esc_attr( $theme_color ) .';}';
		break;
		//Fitness Demo Theme Styles
		case "demo-fitness":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .'!important;} .independent-slider-nav .slide-nav > li > a{color: '. esc_attr( $theme_color ) .';} .independent-slider-nav .slide-nav > li > a{border-color: '. esc_attr( $theme_color ) .';} .independent-block.independent-block-8 .post-thumb-wrapper .category-tag, .independent-block.independent-block-2 .post-thumb-wrapper .category-tag, .widget .widget-title:after, .widget .independent-block-title:after, .independent-block.independent-block-1 .post-grid > .post-thumb-wrapper .category-tag, .post .post-category a, .independent-block-list.big-list .post-category > a{background: '. esc_attr( $theme_color ) .';}';
		break;
		//Fruits Demo Theme Styles
		case "demo-fruits":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .'!important;}.post-meta .post-category a, .owl-dot.active span{background: '. esc_attr( $theme_color ) .'!important;}post-read-more a.read-more{color: '. esc_attr( $theme_color ) .';}.independent-block-title:before, .independent-block-title:after{border-color: '. esc_attr( $theme_color ) .';}.post .post-category a, .post-thumb-wrapper .category-tag, .category-tag, .blog-zz-inner .post-category a, .blog-zz-inner .post .post-tags a {background: '. esc_attr( $theme_color ) .';}';
		break;
		//Medical Demo Theme Styles
		case "demo-medical":
			echo '.navbar .independent-main-menu > li.current-menu-item > a{color: '. esc_attr( $theme_color ) .'!important;}.independent-load-more.btn, .independent-slider-nav .slide-nav > li > a{color: '. esc_attr( $theme_color ) .';}.independent-block-title span{border-left-color: '. esc_attr( $theme_color ) .';}.independent-block.independent-block-8 .post-thumb-wrapper .category-tag, .independent-block.independent-block-2 .post-thumb-wrapper .category-tag,.widget .widget-title:after, .widget .independent-block-title:after, .widget .widget-title:before, .widget .independent-block-title:before, .site-footer .independent-block-title:after, .independent-block.independent-block-1 .post-grid > .post-thumb-wrapper .category-tag {background: '. esc_attr( $theme_color ) .';}';
		break;
		//Weets Personal Blog Demo Theme Styles
		case "demo-weets":
			echo '.navbar .independent-main-menu > li.current-menu-item > a, .post .post-category a, .post-thumb-wrapper .category-tag, .category-tag{color: '. esc_attr( $theme_color ) .'!important;}.owl-nav > div:hover, .post-read-more a.read-more{color: '. esc_attr( $theme_color ) .';}.owl-nav > div:hover{border-color: '. esc_attr( $theme_color ) .';}.organic-tabs .independent-block-title{background: '. esc_attr( $theme_color ) .';} .owl-dot.active span {background: '. esc_attr( $theme_color ) .'!important;}';
		break;
		//City News Demo Theme Styles
		case "demo-citynews":
			echo '.vc_row .widget .widget-title:before, .widget-area .widget .widget-title:before, .secondary-menu-area .widget .widget-title:before, .secondary-menu-area .vc_row .widget .widget-title:after, .independent-block .independent-block-title.title-style-3:before{border-top-color: '. esc_attr( $theme_color ) .';}.blog-template .post-thumb-wrapper .category-tag, .widget-area .widget .widget-title, .vc_row .widget .widget-title, .secondary-menu-area .widget .widget-title, .secondary-menu-area .vc_row .widget .widget-title, .independent-block .independent-block-title.title-style-3, .independent-block-list.big-list .post-category > a {background: '. esc_attr( $theme_color ) .';}';
		break;
		//Game Demo Theme Styles
		case "demo-game3":
			echo '.navbar .independent-main-menu > li.current-menu-item > a, .post:hover .post-title > a, .search-form .input-group .btn:hover{color: '. esc_attr( $theme_color ) .' !important;}.post-category > a, .independent-block-overlay .post-category > a, .independent-block .post-meta .read-more:hover, .widget.independent_mailchimp_widget .btn:hover, .post-navigation .nav-links .nav-previous a:hover, .post-navigation .nav-links .nav-next a:hover, .single-post-template article.post .post-tags > a {background-color: '. esc_attr( $theme_color ) .' !important;}.post:hover .post-thumb-wrapper {border-color: '. esc_attr( $theme_color ) .';}.independent-block-title, .widget-title {border-left-color: '. esc_attr( $theme_color ) .';}'; 
		break;
		//Independent Times Theme Styles
		case "demo-independent-times":
			echo '.navbar .independent-main-menu > li.current-menu-item > a, .single-post-template article ul.nav.social-icons > li > a:hover{color: '. esc_attr( $theme_color ) .'!important;}.independent-slider-nav .slide-nav > li > a{color: '. esc_attr( $theme_color ) .';}.independent-block-title span{border-left-color: '. esc_attr( $theme_color ) .';}.independent-slider-nav .slide-nav > li > a{border-color: '. esc_attr( $theme_color ) .';}.navbar .independent-main-menu > li.current-menu-item > a:before, .navbar .independent-main-menu > li.current-menu-ancestor > a:before, .widget .widget-title:after, .widget .independent-block-title:after, .independent-block.independent-block-8 .post-thumb-wrapper .category-tag, .independent-block.independent-block-2 .post-thumb-wrapper .category-tag, .site-footer .independent-block-title:after, .independent-block.independent-block-1 .post-grid > .post-thumb-wrapper .category-tag, .independent-block-title span:before{background: '. esc_attr( $theme_color ) .';}';
		break;
		
		//Photography Blog Styles
		case "demo-photography-blog":
			echo '.navbar .independent-main-menu > li.current-menu-item > a, .post:hover .post-title > a, .search-form .input-group .btn:hover, .independent-block-overlay .post-category > a:hover, .single-post-template blockquote.bq-style1:before, .single-post-template article.post .post-tags > a:hover, .post-navigation .nav-links .nav-previous a:hover, .post-navigation .nav-links .nav-next a:hover, .independent-block-slide.single-slide .independent-block-overlay .post-category > a:before {color: '. esc_attr( $theme_color ) .' !important;} .independent-block .post-meta .read-more:hover, .widget.independent_mailchimp_widget .btn:hover {background-color: '. esc_attr( $theme_color ) .' !important;}.independent-block-slide.single-slide .post-title-wrapper .post-title {border-left-color: '. esc_attr( $theme_color ) .';}.nav.pagination > li.nav-item a:hover, .nav.pagination > li.nav-item span:hover, .nav.pagination > li.nav-item.active span {border-color: '. esc_attr( $theme_color ) .';}'; 
		break;
	}
}