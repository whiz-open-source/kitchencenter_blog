<?php
/**
 * independent functions and definitions
 *
 */
/**
 * independent predifined vars
 */
define('INDEPENDENT_ADMIN', get_template_directory().'/admin');
define('INDEPENDENT_INC', get_template_directory().'/inc');
define('INDEPENDENT_THEME_ELEMENTS', get_template_directory().'/admin/theme-elements');
define('INDEPENDENT_ADMIN_URL', get_template_directory_uri().'/admin');
define('INDEPENDENT_INC_URL', get_template_directory_uri().'/inc');
define('INDEPENDENT_ASSETS', get_template_directory_uri().'/assets');

$independent_option = get_option( 'independent_options' );

//Theme Default
require_once INDEPENDENT_ADMIN . '/theme-default/theme-default.php';

//Theme Class
require_once INDEPENDENT_INC . '/theme-class/theme-class.php';

//Theme Options
require_once INDEPENDENT_THEME_ELEMENTS . '/theme-options.php';
//Custom Sidebar
require_once INDEPENDENT_ADMIN . '/custom-sidebar/sidebar-generator.php';
//TGM
require_once INDEPENDENT_ADMIN . '/tgm/tgm-init.php'; 
require_once INDEPENDENT_ADMIN . '/welcome-page/welcome.php';
//Custom Menu
require_once INDEPENDENT_ADMIN . '/mega-menu/custom_menu.php';
//Zozo Importer
if( class_exists( 'independent_Zozo_Admin_Page' ) ){
	require_once INDEPENDENT_ADMIN . '/welcome-page/importer/zozo-importer.php'; 	
}
//Meta Box
if( class_exists( 'independentRedux' ) ) {
	require_once INDEPENDENT_ADMIN . '/metabox/inc/independent-metabox.php';
}

//Botstrap Nav Walker
require_once INDEPENDENT_INC . '/walker/wp_bootstrap_navwalker.php';

//News Menu Block
require_once INDEPENDENT_ADMIN . "/mega-menu/menu_news_block.php";

//News Functions
require_once INDEPENDENT_INC . '/news-functions.php';

// Independent AQ Resizer
require_once INDEPENDENT_ADMIN . '/resizer/aq_resizer.php';

//VC Shortcodes
if ( class_exists( 'Vc_Manager' ) ) {
	require_once INDEPENDENT_INC . '/visual-composer/visual-composer.php'; 
}

//Woo
if ( class_exists( 'WooCommerce' ) ) {
	require_once INDEPENDENT_INC . "/woo-functions.php";
}
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function independent_setup() {

	/* independent Text Domain */
	load_theme_textdomain( 'independent', get_template_directory() . '/languages' );
	
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	
	/* Custom background */
	$defaults = array(
		'default-color'          => '',
		'default-image'          => '',
		'wp-head-callback'       => '_custom_background_cb',
		'admin-head-callback'    => '',
		'admin-preview-callback' => ''
	);
	add_theme_support( 'custom-background', $defaults );
	
	/* Custom header */
	$defaults = array(
		'default-image'          => '',
		'random-default'         => false,
		'width'                  => 0,
		'height'                 => 0,
		'flex-height'            => false,
		'flex-width'             => false,
		'default-text-color'     => '',
		'header-text'            => true,
		'uploads'                => true,
		'wp-head-callback'       => '',
		'admin-head-callback'    => '',
		'admin-preview-callback' => '',
	);
	add_theme_support( 'custom-header', $defaults );
	
	/* Content width */
	if ( ! isset( $content_width ) ) $content_width = 640;
	
	$ao = new independentThemeOpt;
	$grid_large = $ao->independentThemeOpt('independent_grid_large');
	$grid_medium = $ao->independentThemeOpt('independent_grid_medium');
	$grid_small = $ao->independentThemeOpt('independent_grid_small');
	$port_masonry = $ao->independentThemeOpt('independent_portfolio_masonry');
	
	if( !empty( $grid_large ) && is_array( $grid_large ) ) add_image_size( 'independent-grid-large', $grid_large['width'], $grid_large['height'], true );
	if( !empty( $grid_medium ) && is_array( $grid_medium ) ) add_image_size( 'independent-grid-medium', $grid_medium['width'], $grid_medium['height'], true );
	if( !empty( $grid_small ) && is_array( $grid_small ) ) add_image_size( 'independent-grid-small', $grid_small['width'], $grid_small['height'], true );
	
	//Team
	$team_medium = $ao->independentThemeOpt('independent_team_medium');
	if( !empty( $team_medium ) && is_array( $team_medium ) ) add_image_size( 'independent-team-medium', $team_medium['width'], $team_medium['height'], true );
	update_option( 'large_size_w', 1170 );
	update_option( 'large_size_h', 694 );
	update_option( 'large_crop', 1 );
	update_option( 'medium_size_w', 768 );
	update_option( 'medium_size_h', 456 );
	update_option( 'medium_crop', 1 );
	update_option( 'thumbnail_size_w', 90 );
	update_option( 'thumbnail_size_h', 90 );
	update_option( 'thumbnail_crop', 1 );
	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top-menu'		=> esc_html__( 'Top Menu', 'independent' ),
		'primary-menu'	=> esc_html__( 'Primary Menu', 'independent' ),
		'footer-menu'	=> esc_html__( 'Footer Menu', 'independent' ),
	) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	/*
	 * Enable support for Post Formats.
	 *
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );
	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo' );
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	
	// Add support for Block Styles.
	add_theme_support( 'wp-block-styles' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	// Enqueue editor styles.
	add_editor_style( 'style-editor.css' );

	// Editor color palette.
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'Dark Gray', 'independent' ),
				'slug'  => 'dark-gray',
				'color' => '#111',
			),
			array(
				'name'  => __( 'Light Gray', 'independent' ),
				'slug'  => 'light-gray',
				'color' => '#767676',
			),
			array(
				'name'  => __( 'White', 'independent' ),
				'slug'  => 'white',
				'color' => '#FFF',
			),
		)
	);

	// Add support for responsive embedded content.
	add_theme_support( 'responsive-embeds' );
}
add_action( 'after_setup_theme', 'independent_setup' );
/**
 * Register widget area.
 *
 */
function independent_widgets_init() {
	$independent_option = get_option( 'independent_options' );
	$title_class = '';
	//Widget Title Style
	if( isset( $independent_option['widget-title-style'] ) && $independent_option['widget-title-style'] != '' ){
		$title_class = ' title-style-' . $independent_option['widget-title-style'];
	}
	
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'independent' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'independent' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title'. esc_attr( $title_class ) .'">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Secondary Menu Sidebar', 'independent' ),
		'id'            => 'secondary-menu-sidebar',
		'description'   => esc_html__( 'Add widgets here to appear in your secondary menu area.', 'independent' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title'. esc_attr( $title_class ) .'">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'independent' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'independent' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title'. esc_attr( $title_class ) .'">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'independent' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'independent' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title'. esc_attr( $title_class ) .'">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'independent' ),
		'id'            => 'sidebar-4',
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'independent' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title'. esc_attr( $title_class ) .'">',
		'after_title'   => '</h3>',
	) );
	
}
add_action( 'widgets_init', 'independent_widgets_init' );
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since independent 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function independent_excerpt_more( $link ) {
	return '';
}
add_filter( 'excerpt_more', 'independent_excerpt_more' );
/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function independent_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'independent_pingback_header' );
/**
 * Admin Enqueue scripts and styles.
 */
function independent_enqueue_admin_script() { 
	wp_enqueue_style( 'independent-admin-style', get_theme_file_uri( '/admin/assets/css/admin-style.css' ), array(), '1.0' );
	
	// Meta Drag and Drop Script
	wp_enqueue_script( 'independent-admin-scripts', get_theme_file_uri( '/admin/assets/js/admin-scripts.js' ), array( 'jquery' ), '1.0', true ); 
	
	//Admin Localize Script
	wp_localize_script('independent-admin-scripts', 'independent_admin_ajax_var', array(
		'admin_ajax_url' => esc_url( admin_url('admin-ajax.php') ),
		'featured_nonce' => wp_create_nonce('independent-post-featured'), 
		'sidebar_nounce' => wp_create_nonce('independent-sidebar-featured'), 
		'redux_themeopt_import' => wp_create_nonce('independent-redux-import'),
		'unins_confirm' => esc_html__('Please backup your files and database before uninstall. Are you sure want to uninstall current demo?.', 'independent'),
		'yes' => esc_html__('Yes', 'independent'),
		'no' => esc_html__('No', 'independent'),
		'proceed' => esc_html__('Proceed', 'independent'),
		'cancel' => esc_html__('Cancel', 'independent'),
		'block' => esc_html__('Block', 'independent'),
		'edit' => esc_html__('Edit', 'independent'),
		'update' => esc_html__('Update', 'independent'),
		'settings' => esc_html__('Settings', 'independent'),
		'process' => esc_html__( 'Processing', 'independent' ),
		'uninstalling' => esc_html__('Uninstalling...', 'independent'),
		'uninstalled' => esc_html__('Uninstalled.', 'independent'),
		'unins_pbm' => esc_html__('Uninstall Problem!.', 'independent'),
		'downloading' => esc_html__('Downloading Demo Files...', 'independent'), 
		'import_xml' => esc_html__('Importing Xml...', 'independent'),
		'import_theme_opt' => esc_html__('Importing Theme Option...', 'independent'),
		'import_widg' => esc_html__('Importing Widgets...', 'independent'),
		'import_sidebars' => esc_html__('Importing Sidebars...', 'independent'),
		'import_revslider' => esc_html__('Importing Revolution Sliders...', 'independent'),
		'imported' => esc_html__('Successfully Imported, Check Above Message.', 'independent'),
		'import_pbm' => esc_html__('Import Problem.', 'independent'),
		'access_pbm' => esc_html__('File access permission problem.', 'independent'),
		'redux_block_nonce' => wp_create_nonce('independent-redux-news-block'),
		'assets_path' => INDEPENDENT_ASSETS
	));
	
}
add_action( 'admin_enqueue_scripts', 'independent_enqueue_admin_script' );
/**
 * Enqueue scripts and styles.
 */
function independent_scripts() { 
	/*Visual Composer CSS*/
	if ( class_exists( 'Vc_Manager' ) ) {
		wp_enqueue_script( 'wpb_composer_front_js' );
		wp_enqueue_style( 'js_composer_front' );
		wp_enqueue_style( 'js_composer_custom_css' );
	}
	/* independent Theme Styles */
	
	// independent Style Libraries
	
	$nto = new independentThemeOpt;
	$minify_js = $nto->independentThemeOpt('js-minify');
	$minify_css = $nto->independentThemeOpt('css-minify');
	
	if( $minify_js ){
		wp_enqueue_style( 'independent-min', get_theme_file_uri( '/assets/css/theme.min.css' ), array(), '1.0' );
	}else{
		wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), array(), '4.1.1' );
		wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), array(), '4.7.0' );
		wp_enqueue_style( 'simple-line-icons', get_theme_file_uri( '/assets/css/simple-line-icons.min.css' ), array(), '1.0' );
		wp_enqueue_style( 'themify-icons', get_theme_file_uri( '/assets/css/themify-icons.css' ), array(), '1.0' );
		wp_enqueue_style( 'owl-carousel', get_theme_file_uri( '/assets/css/owl-carousel.min.css' ), array(), '2.2.1' );
		wp_enqueue_style( 'magnific-popup', get_theme_file_uri( '/assets/css/magnific-popup.min.css' ), array(), '1.0' );
		wp_enqueue_style( 'image-hover', get_theme_file_uri( '/assets/css/image-hover.min.css' ), array(), '1.0' );
		wp_enqueue_style( 'ytplayer', get_theme_file_uri( '/assets/css/ytplayer.min.css' ), array(), '1.0' );
		wp_enqueue_style( 'animate', get_theme_file_uri( '/assets/css/animate.min.css' ), array(), '3.5.1' );
	}
	
	// Theme stylesheet.
	wp_enqueue_style( 'independent-style', get_template_directory_uri() . '/style.css', array(), '1.0' );
	
	// Shortcode Styles
	wp_enqueue_style( 'independent-shortcode', get_theme_file_uri( '/assets/css/shortcode.css' ), array(), '1.0' );

	// News Styles
	wp_enqueue_style( 'independent-news-styles', get_theme_file_uri( '/assets/css/news-styles.css' ), array(), '1.0' );
	
	//Demo Style
	$custom_demo_type = $nto->independentThemeOpt('demo-id-style');
	if( $custom_demo_type != 'none' ){
		if( $custom_demo_type != 'demo-main' ){
			wp_enqueue_style( 'independent-demo-'.$custom_demo_type, get_theme_file_uri( '/assets/css/'. $custom_demo_type .'.css' ), array(), '1.0' );
		}
	}else{
		$demo_type = get_theme_mod('independent_installed_demo_id');
		if( !empty( $demo_type ) && $demo_type != 'demo-main' ){
			wp_enqueue_style( 'independent-demo-'.$demo_type, get_theme_file_uri( '/assets/css/'. $demo_type .'.css' ), array(), '1.0' );
		}
	}

	/* independent theme script files */
	
	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );
	
	// independent JS Libraries
	if( $minify_css ){
		wp_enqueue_script( 'independent-theme-min', get_theme_file_uri( '/assets/js/theme.min.js' ), array( 'jquery' ), '1.0', true );
	}else{
		wp_enqueue_script( 'popper', get_theme_file_uri( '/assets/js/popper.min.js' ), array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'bootstrap', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array( 'jquery' ), '4.1.1', true );
		wp_enqueue_script( 'owl-carousel', get_theme_file_uri( '/assets/js/owl.carousel.min.js' ), array( 'jquery' ), '2.2.1', true );
		wp_enqueue_script( 'isotope', get_theme_file_uri( '/assets/js/isotope.pkgd.min.js' ), array( 'jquery' ), '3.0.3', true );
		wp_enqueue_script( 'infinite-scroll', get_theme_file_uri( '/assets/js/infinite-scroll.pkgd.min.js' ), array( 'jquery' ), '2.0', true );
		wp_enqueue_script( 'imagesloaded' );
		wp_enqueue_script( 'jquery-stellar', get_theme_file_uri( '/assets/js/jquery.stellar.min.js' ), array( 'jquery' ), '0.6.2', true );
		wp_enqueue_script( 'sticky-kit', get_theme_file_uri( '/assets/js/sticky-kit.min.js' ), array( 'jquery' ), '1.1.3', true );
		wp_enqueue_script( 'jquery-mb-YTPlayer', get_theme_file_uri( '/assets/js/jquery.mb.YTPlayer.min.js' ), array( 'jquery' ), '1.0', true );	
		wp_enqueue_script( 'jquery-magnific', get_theme_file_uri( '/assets/js/jquery.magnific.popup.min.js' ), array( 'jquery' ), '1.1.0', true );
		wp_enqueue_script( 'jquery-easy-ticker', get_theme_file_uri( '/assets/js/jquery.easy.ticker.min.js' ), array( 'jquery' ), '2.0', true );
		wp_enqueue_script( 'jquery-easing', get_theme_file_uri( '/assets/js/jquery.easing.min.js' ), array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'jquery-countdown', get_theme_file_uri( '/assets/js/jquery.countdown.min.js' ), array( 'jquery' ), '2.2.0', true );
		wp_enqueue_script( 'jquery-circle-progress', get_theme_file_uri( '/assets/js/jquery.circle.progress.min.js' ), array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'jquery-appear', get_theme_file_uri( '/assets/js/jquery.appear.min.js' ), array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'smoothscroll', get_theme_file_uri( '/assets/js/smoothscroll.min.js' ), array( 'jquery' ), '1.20.2', true );
	}
	
	// Theme Js
	wp_enqueue_script( 'independent-theme', get_theme_file_uri( '/assets/js/theme.js' ), array( 'jquery' ), '1.0', true );
	
	// News Block Js
	wp_enqueue_script( 'independent-block', get_theme_file_uri( '/assets/js/news-block.js' ), array( 'jquery' ), '1.0', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	// Theme option stylesheet.
	$upload_dir = independent_fn_get_upload_dir_var('baseurl');
	$independent = wp_get_theme();
	$theme_style = $upload_dir . '/independent/theme_'.get_current_blog_id().'.css';
	wp_enqueue_style( 'independent-theme-style', esc_url( $theme_style ), array(), $independent->get( 'Version' ) );
	
	$independent_option = get_option( 'independent_options' );
	
	//Google Map Script
	if( isset( $independent_option['google-api'] ) && $independent_option['google-api'] != '' ){
		wp_register_script( 'independent-gmaps', '//maps.googleapis.com/maps/api/js?key='. esc_attr( $independent_option['google-api'] ) , array('jquery'), null, true );
	}
		
	$infinite_image = isset( $independent_option['infinite-loader-img']['url'] ) && $independent_option['infinite-loader-img']['url'] != '' ? $independent_option['infinite-loader-img']['url'] : get_theme_file_uri( '/assets/images/infinite-loder.gif' );
	
	$lazy_opt = independentThemeOpt::independentStaticThemeOpt('news-lazy-load');
	
	//Localize Script
	wp_localize_script('independent-theme', 'independent_ajax_var', array(
		'admin_ajax_url' => esc_url( admin_url('admin-ajax.php') ),
		'like_nonce' => wp_create_nonce('independent-post-like'), 
		'fav_nonce' => wp_create_nonce('independent-post-fav'),
		'infinite_loader' => $infinite_image,
		'load_posts' => apply_filters( 'infinite_load_msg', esc_html__( 'Caragando el siguiente artículo.', 'independent' ) ),
		'no_posts' => apply_filters( 'infinite_finished_msg', esc_html__( 'No hay más artículo por cargar.', 'independent' ) ),
		'cmt_nonce' => wp_create_nonce('independent-comment-like'),
		'mc_nounce' => wp_create_nonce('independent-mailchimp'), 
		'wait' => esc_html__('Espere..', 'independent'),
		'must_fill' => esc_html__('Debe ingresar el campo obligatorio.', 'independent'),
		'valid_email' => esc_html__('Ingrese un correo válido.', 'independent'),
		'cart_update_pbm' => esc_html__('Problema de actualzación de carrito de compra.', 'independent'),
		'news_problem' => esc_html__('News Retrieve Problem.', 'independent'),
		'redirecturl' => home_url( '/' ),
        'loadingmessage' => esc_html__( 'Enviando información de usuario, por favor espere...', 'independent' ),
		'valid_email' => esc_html__( 'Por favor ingrese un correo válido!', 'independent' ),
		'valid_login' => esc_html__( 'Por favor ingrese un usuario/contrañseda valida!', 'independent' ),
		'req_reg' => esc_html__( 'Please enter required fields values for registration!', 'independent' ),
		'lazy_opt' => esc_attr( $lazy_opt )
	));
	
}
add_action( 'wp_enqueue_scripts', 'independent_scripts' );

/**
 * Enqueue supplemental block editor styles.
 */
function independent_editor_customizer_styles() {
	wp_enqueue_style( 'google-fonts', independent_redux_fonts_url(), array(), null, 'all' );
	wp_enqueue_style( 'independent-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.0', 'all' );

	if( function_exists('independent_get_custom_styles') ) {
		wp_add_inline_style( 'independent-editor-customizer-styles', independent_get_custom_styles() );
	}
}
add_action( 'enqueue_block_editor_assets', 'independent_editor_customizer_styles' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );
/*Theme Code*/
/*Search Form Filter*/
if( ! function_exists('independent_zozo_search_form') ) {
	function independent_zozo_search_form( $form ) {
		
		$search_out = '
		<form method="get" class="search-form" action="'. esc_url( home_url( '/' ) ) .'">
			<div class="input-group">
				<input type="text" class="form-control" name="s" value="'. esc_attr( get_search_query() ) .'" placeholder="'. esc_attr__('Buscar...', 'independent') .'">
				<span class="input-group-btn">
					<button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>';
		return $search_out;
	}
	add_filter( 'get_search_form', 'independent_zozo_search_form' );
}
$aps = new independentPostSettings;
add_action( 'wp_ajax_post_like_act', array( &$aps, 'independentMetaLikeCheck' ) );
add_action( 'wp_ajax_nopriv_post_like_act', array( &$aps, 'independentMetaLikeCheck' ) ); 
add_action( 'wp_ajax_post_fav_act', array( &$aps, 'independentMetaFavouriteCheck' ) );
add_action( 'wp_ajax_nopriv_post_fav_act', array( &$aps, 'independentMetaFavouriteCheck' ) );
if( $aps->independentGetThemeOpt( 'comments-like' ) ){
	add_action('wp_ajax_nopriv_comment_like', array( &$aps, 'independentCommentsLike' ) );
	add_action('wp_ajax_comment_like', array( &$aps, 'independentCommentsLike' ) );
}
if( ! function_exists('independentPostComments') ) {
	function independentPostComments( $comment, $args, $depth ) {
	
		$GLOBALS['comment'] = $comment;
		
		$aps = new independentPostSettings;		
		
		$allowed_html = array(
			'a' => array(
				'href' => array(),
				'title' => array()
			)
		);
		
		?>
		<li <?php comment_class('clearfix'); ?> id="comment-<?php comment_ID() ?>">
			
			<div class="media thecomment">
						
				<div class="media-left author-img">
					<?php echo get_avatar($comment,$args['avatar_size']); ?>
				</div>
				
				<div class="media-body comment-text">
					<div class="comment-meta">
					<?php if( $depth < $args['max_depth'] ) : ?>
					
					<?php endif; ?>
					<h6 class="author"><?php echo get_comment_author_link(); ?></h6>
					<span class="date"><?php printf( wp_kses( __( '%1$s at %2$s', 'independent' ), $allowed_html ), get_comment_date(),  get_comment_time()) ?></span>
					<?php if ( $comment->comment_approved == '0' ) : ?>
						<em><i class="icon-info-sign"></i> <?php esc_html_e( 'Comment awaiting approval', 'independent' ); ?></em>
						<br />
					<?php endif; ?>
					<?php comment_text(); ?>
					<span class="reply">
						<?php comment_reply_link( array_merge( $args, array('reply_text' => '<i class="fa fa-reply"></i> ', 'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID ); ?>
					</span>
					<!-- Custom Comments Meta -->
					<?php if( $aps->independentGetThemeOpt( 'comments-like' ) || $aps->independentGetThemeOpt( 'comments-share' ) ) : ?>
						<div class="comment-meta-wrapper clearfix">
							<ul class="list-inline">
								<?php if( $aps->independentGetThemeOpt( 'comments-like' ) ) : ?>
								<li class="comment-like-wrapper"><?php echo do_shortcode( $aps->independentCommentLikeOut( $comment->comment_ID ) ); ?></li>
								<?php endif; ?>
								<?php if( $aps->independentGetThemeOpt( 'comments-social-shares' )) : ?>
								<li class="comment-share-wrapper pull-right"><?php echo do_shortcode( $aps->independentCommentShare( $comment->comment_ID ) ); ?></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<?php endif; // if comment meta need ?>
				</div>
						
			</div>
			
			
		</li>
		<?php
		
	} 
}