/*
 * independent Theme Js 
 */ 

(function( $ ) {

	"use strict";
		  
	$( document ).ready(function() {
	
		/* Page Loader */
		$( window ).load(function() {
			$(".page-loader").fadeOut("slow");
		});
		
		/* Shortcode CSS Append */
		var css_out = '';
		$( ".independent-inline-css" ).each(function() {
			var shortcode = $( this );
			var shortcode_css = shortcode.attr("data-css");		
			css_out += ($).parseJSON( shortcode_css );
			shortcode.removeAttr("data-css");
		});
		
		/* VC Row Custom Style */
		$( ".independent-vc-row" ).each(function() {
			var shortcode = $( this );
			var row_class = shortcode.attr("data-class");
			if( shortcode.attr("data-color") ){
				var row_color = shortcode.attr("data-color");		
				css_out += "." + row_class + "{ color:" + row_color + ";}"; 
				shortcode.removeAttr("data-color");
			}
			if( shortcode.attr("data-bg-overlay") ){
				var row_overlay = shortcode.attr("data-bg-overlay");
				css_out += "." + row_class + "{ position: relative; }";
				css_out += "." + row_class + " > span.row-overlay { background-color:" + row_overlay + ";}";
				shortcode.removeAttr("data-bg-overlay");				
			}
			shortcode.removeAttr("data-class");
		});
		if( css_out != '' ){
			$('head').append( '<style id="independent-shortcode-styles">'+ css_out +'</style>' );
		}

		/* Secondary Toggle */
		$( ".secondary-space-toggle" ).on( "click", function() {
			$('.secondary-space-toggle').toggleClass('active');
			$('body').toggleClass('secondary-active');
			var sec_width = $( ".secondary-menu-area" ).width();
			var sec_pos = $( ".secondary-menu-area" ).data('pos') ? $( ".secondary-menu-area" ).data('pos') : 'left';
			
			if( sec_pos == 'overlay' ){
				$( ".secondary-menu-area" ).fadeToggle(500);
			}else if( $('body').hasClass('secondary-active') ){
				if( sec_pos == 'left' ){
					if( $( ".secondary-menu-area" ).hasClass('left-overlay') ){
						setTimeout(function(){
							$( ".secondary-menu-area" ).animate( { left : "0" }, { duration: 500, specialEasing: { left: "easeInOutExpo" } } );
							inedependentLazyLoad(200, ".secondary-menu-area");
						}, 600);
					}else{
						$('body').toggleClass('secondary-push-actived');
						$( ".secondary-menu-area" ).animate( { left : "0" }, { duration: 500 } );
						$( "body" ).css('overflow','hidden');
						$( "body .independent-wrapper" ).animate( { left : sec_width +"px" }, 500 );
						if( $( ".sticky-outer" ).length ) {
							$( ".sticky-outer .header-sticky, .sticky-outer .show-menu" ).animate( { left : sec_width +"px", right: "-" + sec_width +"px" }, 500 );
							inedependentLazyLoad(200, ".secondary-menu-area");
						}
					}
				}else{
					if( $( ".secondary-menu-area" ).hasClass('right-overlay') ){
						setTimeout(function(){
							$( ".secondary-menu-area" ).animate( { right : "0" }, { duration: 500, specialEasing: { right: "easeInOutExpo" } } );
							inedependentLazyLoad(200, ".secondary-menu-area");
						}, 600);						
					}else{
						$('body').toggleClass('secondary-push-actived');
						$( ".secondary-menu-area" ).animate( { right : "0" }, { duration: 500 } );
						$( "body" ).css('overflow','hidden');
						$( "body .independent-wrapper" ).animate( { right : sec_width +"px" }, 500 );
						if( $( ".sticky-outer" ).length ){
							$( ".sticky-outer .header-sticky, .sticky-outer .show-menu" ).animate( { right : sec_width +"px", left : "-" + sec_width +"px" }, 500 );
							inedependentLazyLoad(200, ".secondary-menu-area");
						}
					}
				}
			}else{
				if( sec_pos == 'left' ){
					if( $( ".secondary-menu-area" ).hasClass('left-overlay') ){
						$( ".secondary-menu-area" ).animate( { left : "-"+ sec_width +"px" }, { duration: 500, specialEasing: { left: "easeInOutExpo" } } );
						inedependentLazyLoad(200, ".secondary-menu-area");
					}else{
						$('body').toggleClass('secondary-push-actived');
						$( ".secondary-menu-area" ).animate( { left : "-"+ sec_width +"px" }, { duration: 500 } );
						$( "body .independent-wrapper" ).animate( { left : 0 }, 500, function(){ $( "body" ).css('overflow-y','scroll'); } );
						if( $( ".sticky-outer" ).length ){
							$( ".sticky-outer .header-sticky, .sticky-outer .show-menu" ).animate( { left : 0, right: 0 }, 500 );
						}
					}
				}else{
					if( $( ".secondary-menu-area" ).hasClass('right-overlay') ){
						$( ".secondary-menu-area" ).animate( { right : "-"+ sec_width +"px" }, { duration: 500, specialEasing: { right: "easeInOutExpo" } } );
						inedependentLazyLoad(200, ".secondary-menu-area");
					}else{
						$('body').toggleClass('secondary-push-actived');
						$( ".secondary-menu-area" ).animate( { right : "-"+ sec_width +"px" }, { duration: 500 } );
						$( "body .independent-wrapper" ).animate( { right : 0 }, 500, function(){ $( "body" ).css('overflow-y','scroll'); } );
						if( $( ".sticky-outer" ).length ){
							$( ".sticky-outer .header-sticky, .sticky-outer .show-menu" ).animate( { right: 0, left : 0 }, 500 );
						}
						inedependentLazyLoad(200, ".secondary-menu-area");
					}
				}
			}
			
			/* Slider Revolution Issue Fixed */
			if( $(".rev_slider_wrapper").length ){
				$(".rev_slider_wrapper").css("left" , "inherit");    
			}
			
			return false;
		});		
		
		/* Header Bar Center Item Margin Fun */
		setTimeout( independentCenterMenuMargin, 300 );
		
		/* Set Sticky Height for Menu Bars */
		independentSetStickyOuterHeight();
		
		/* Sticky Menu */
		if($('.header-inner .sticky-head').length){
			independentStickyPart( '.header-inner' );
		}
		
		/* Scroll Sticky */
		if($('.header-inner .sticky-scroll').length){
			independentStickyScrollUpPart( '.header-inner', 'header' );
		}
		
		/* Mobile Header Sticky Menu */
		if($('.mobile-header-inner .sticky-head').length){
			independentStickyPart( '.mobile-header-inner' );
		}
		
		/* Mobile Header Scroll Sticky */
		if($('.mobile-header-inner .sticky-scroll').length){
			independentStickyScrollUpPart( '.mobile-header-inner', '.mobile-header' );
		}
		
		/* Sticky Header Space Menu to Modern Toggle Menu Convert */
		if( $('.sticky-header-space').length ){
			
			//Add toggle dropdown icon
			$( ".sticky-header-space .independent-main-menu" ).find('.menu-item-has-children').append( '<span class="zmm-dropdown-toggle fa fa-plus"></span>' );
			$( ".sticky-header-space .independent-main-menu" ).find('.sub-menu').slideToggle();
			
			//zmm dropdown toggle
			$( ".sticky-header-space .zmm-dropdown-toggle" ).on( "click", function() {
				var parent = $( this ).parent('li').children('.sub-menu');
				$( this ).parent('li').children('.sub-menu').slideToggle();
				$( this ).toggleClass('fa-minus');
				if( $( parent ).find('.sub-menu').length ){
					$( parent ).find('.sub-menu').slideUp();
					$( parent ).find('.zmm-dropdown-toggle').removeClass('fa-minus');
				}
			});
			
		}
		
		/* Full Search Toggle */
		$( ".full-search-toggle" ).on( "click", function() {
			$('.full-search-wrapper').toggleClass("search-wrapper-opened");
			$('.full-search-wrapper').fadeToggle(500);
			setTimeout(function(){
				$('.full-search-wrapper').toggleClass("search-form-animate");
				var search_in = $('.search-wrapper-opened').find("input.form-control");
				search_in.focus();
			}, 700);
			
			return false;
		});	
		
		/* Mobile Bar Animate Toggle */
		$( ".mobile-bar-toggle" ).on( "click", function() {
			$( ".mobile-bar" ).toggleClass('active');
			$( "body" ).toggleClass('mobile-bar-active');
			if( $( ".mobile-bar" ).hasClass('animate-from-left') ){
				if( $( ".mobile-bar" ).hasClass('active') )
					$( ".mobile-bar" ).animate( { left : 0 }, { duration: 500, specialEasing: { left: "easeInOutExpo" } } );
				else
					$( ".mobile-bar" ).animate( { left : "-100%" }, { duration: 500, specialEasing: { left: "easeInOutExpo" } } );
			}
			if( $( ".mobile-bar" ).hasClass('animate-from-right') ){
				if( $( ".mobile-bar" ).hasClass('active') )
					$( ".mobile-bar" ).animate( { right : 0 }, { duration: 500, specialEasing: { right: "easeInOutExpo" } } );
				else
					$( ".mobile-bar" ).animate( { right : "-100%" }, { duration: 500, specialEasing: { right: "easeInOutExpo" } } );
			}
			if( $( ".mobile-bar" ).hasClass('animate-from-top') ){
				if( $( ".mobile-bar" ).hasClass('active') )
					$( ".mobile-bar" ).animate( { top : 0 }, { duration: 500, specialEasing: { top: "easeInOutExpo" } } );
				else
					$( ".mobile-bar" ).animate( { top : "-100%" }, { duration: 500, specialEasing: { top: "easeInOutExpo" } } );
			}
			if( $( ".mobile-bar" ).hasClass('animate-from-bottom') ){
				if( $( ".mobile-bar" ).hasClass('active') )
					$( ".mobile-bar" ).animate( { bottom : 0 }, { duration: 500, specialEasing: { bottom: "easeInOutExpo" } } );
				else
					$( ".mobile-bar" ).animate( { bottom : "-100%" }, { duration: 500, specialEasing: { bottom: "easeInOutExpo" } } );
			}
			return false;
		});
		
		/* Mobile Bar Menu to Modern Toggle Menu Convert */
		if( $('.mobile-bar').length ){
			
			if( $( ".independent-main-menu" ).length || $( ".secondary-menu-area-inner ul.menu" ).length ){
			
				var main_menu = ".independent-main-menu";
				if( !$( ".independent-main-menu" ).length ){
					$( ".secondary-menu-area-inner ul.menu" ).addClass( "independent-main-menu" );
				}
				
				var mobile_menu = ".mobile-bar .independent-mobile-main-menu";
				var find_classes = ".dropdown, .mega-dropdown, .dropdown-toggle, .dropdown-menu, .mega-dropdown-menu, .mega-child-heading, .mega-child-dropdown, .mega-child-dropdown-menu, .hidden-xs-up, .row, .mega-sub-dropdown, .mega-sub-dropdown-menu, .mega-sub-child, .mega-sub-child-inner, .left-side";
				var removable_classes = "dropdown mega-dropdown dropdown-toggle dropdown-menu mega-dropdown-menu mega-child-heading mega-child-dropdown mega-child-dropdown-menu hidden-xs-up row mega-sub-dropdown mega-sub-dropdown-menu mega-sub-child mega-sub-child-inner left-side";
				
				//Mobile menu copy from main menu
				$(main_menu).clone().appendTo( mobile_menu );
				
				//Add main class name
				$( mobile_menu + " " + main_menu ).addClass( "flex-column" );
				
				//Remove unwanted item from mobile menu
				$( mobile_menu + " .mega-child-widget" ).parent( "li.menu-item" ).remove();
				$( mobile_menu + " .mega-child-divider" ).remove();
				$( mobile_menu + " .menu-item-logo" ).remove();
				$( mobile_menu + " li.menu-item" ).removeClass (function (index, css) {
					return ( css.match (/\bcol-\S+/g) || [] ).join(' ');

				});
				$( mobile_menu + " li.menu-item" ).removeClass (function (index, css) {
					return ( css.match (/\bmax-col-\S+/g) || [] ).join(' ');
				});
				
				//Change class name
				$( mobile_menu ).find( ".dropdown-menu, .mega-child-dropdown-menu, .mega-sub-child-inner" ).toggleClass( "sub-menu" );
				
				//Content reform
				$( mobile_menu + " .mega-child-item-disabled" ).replaceWith( "<a class='nav-link' href='#'>" + $( mobile_menu + " .mega-child-item-disabled" ).html() + "</a>" );
				
				//Remove unwanted classes
				$( mobile_menu ).find( find_classes ).removeClass( removable_classes );
				//Remove News Blocks
				$( mobile_menu ).find( ".independent-block" ).remove();
				
				//menu-item-has-children
				if( $( mobile_menu ).find( 'li[data-no-children="1"]' ).length ){
					$( mobile_menu ).find( 'li[data-no-children="1"]' ).each( function() {
						var no_child_parent = $(this).parents("li.menu-item-has-children");
						no_child_parent.removeClass("menu-item-has-children");
						no_child_parent.children(".sub-menu").remove();
					});
				}
				
				//Remove Background
				$( mobile_menu + " .sub-menu" ).css('background','none');
				
				//Add toggle dropdown icon
				$( ".mobile-bar " + main_menu ).find('.menu-item-has-children').append( '<span class="zmm-dropdown-toggle fa fa-plus"></span>' );
				$( ".mobile-bar " + main_menu ).find('.sub-menu').slideToggle();
				
				$( ".mobile-bar " + main_menu ).removeClass('independent-main-menu').addClass('independent-mobile-menu');
				
				//dropdown toggle
				$( ".mobile-bar .zmm-dropdown-toggle" ).on( "click", function() {
					var parent = $( this ).parent('li').children('.sub-menu');
					$( this ).parent('li').children('.sub-menu').slideToggle();
					$( this ).toggleClass('fa-minus');
					if( $( parent ).find('.sub-menu').length ){
						$( parent ).find('.sub-menu').slideUp();
						$( parent ).find('.zmm-dropdown-toggle').removeClass('fa-minus');
					}
				});
			}// check page have main menu or not
			
		}
		
		/* Mobile Bar Menu to Modern Toggle Menu Convert */
		if( $('.secondary-menu-area-inner ul.menu').length ){
				
				var sec_menu = ".secondary-menu-area-inner ul.menu";
				//Add main class name
				$( sec_menu ).addClass( "flex-column" );
				
				//Add toggle dropdown icon
				$( sec_menu ).find('.menu-item-has-children').append( '<span class="zmm-dropdown-toggle fa fa-plus"></span>' );
				$( sec_menu ).find('.sub-menu').slideToggle();
				
				//dropdown toggle
				$( sec_menu + " .zmm-dropdown-toggle" ).on( "click", function() {
					var parent = $( this ).parent('li').children('.sub-menu');
					$( this ).parent('li').children('.sub-menu').slideToggle();
					$( this ).toggleClass('fa-minus');
					if( $( parent ).find('.sub-menu').length ){
						$( parent ).find('.sub-menu').slideUp();
						$( parent ).find('.zmm-dropdown-toggle').removeClass('fa-minus');
					}
				});
			
		}
		
		/* Twitter Widget Slider(newsticker) */
		if( $( ".twitter-slider" ).length ){
			$( ".twitter-slider" ).each(function() {
				var twit_slider = $(this);	
				var slide = twit_slider.attr( "data-show" );
				twit_slider.easyTicker({
					direction: 'up',
					visible: parseInt(slide),
					easing: 'swing',
					interval: 4000
				});
			});
		}


		
		/* Menu Scroll */
		var cur_offset = 0;
		
		var o_stat = 0; // One Page Menu Status
		$( '.independent-main-menu li.menu-item' ).each(function( index ) {
			var cur_item = this;
			var target = $(cur_item).children("a").attr("href");
			if( target && target.indexOf("#section-") != -1 ){
				o_stat = 1;
				var res = target.split("#");
				if( res.length == 2 ){
					$(cur_item).children("a").attr("data-target", res[0]);
					$(cur_item).children("a").attr("href", "#"+res[1]);
				}	
			}
		});
		
		if( o_stat ){
		
			if( $('.independent-main-menu .menu-item').find('a[href="#section-top"]').length ){
				$("body").attr("id","section-top");
			}
			
			$( '.independent-main-menu li.menu-item' ).removeClass("current-menu-item");
			
			$( window ).on( "scroll", function() {
				var minus_height = $("#wpadminbar").length ? $("#wpadminbar").outerHeight() : 0;
				minus_height += $(".independent-header .sticky-outer").length ? $(".independent-header .sticky-outer").outerHeight() : 0;
				minus_height += 10;
				$('.vc_row[id*="section-"], body').each(function () {
					var anchored = $(this).attr("id"),
						targetOffset = $(this).offset().top - minus_height;
						
					if ($(window).scrollTop() > targetOffset) {
						$('.independent-main-menu .menu-item').find("a").removeClass("active");
						$('.independent-main-menu .menu-item').find('a[href="#'+ anchored +'"]').addClass("active");
					}
				});
			});
			
			//$( '.independent-main-menu .menu-item > a[href^="#section-"], .independent-mobile-main-menu .menu-item > a[href^="#section-"], .menu .menu-item > a[href^="#section-"]' ).on('click',function (e) {

		}
		
		$( 'a[href^="#section-"]' ).on('click',function (e) {
			
			var cur_item = this;
			var target = $(cur_item).attr("href");
			
			if( $(cur_item).parents(".independent-mobile-main-menu").length ) {
				$(".mobile-bar-toggle.close").trigger( "click" );
			}
			if( $( ".secondary-menu-area" ) ){
				$( ".secondary-menu-area .secondary-space-toggle.active" ).trigger( "click" );
			}
			
			var target_id = target.slice( target.indexOf("#"), ( target.length ) );

			if( $( target_id ).length ){

				var offs = $(target_id).offset().top;
				
				var hght_ele;
				if( $(".mobile-header").height() ){
					hght_ele = $(".mobile-header .sticky-head");
				}else {
					hght_ele = $(".independent-header .sticky-head");
				}
				
				var sticky_head_hgt = hght_ele.outerHeight();
				if( hght_ele.length ){
					offs = offs - parseInt( sticky_head_hgt );
				}
				if( $( "#wpadminbar" ).length ) offs = offs - parseInt( $( "#wpadminbar" ).outerHeight() );
				
				var sec_ani_call = 1;
				if( target_id == '#section-top' ){
					sec_ani_call = 1;
					offs = 0;
				}
				
				$('html,body').animate({ 'scrollTop': offs }, 1000, 'easeInOutExpo', function() {
					if( sticky_head_hgt != hght_ele.outerHeight() && sec_ani_call ){
						sec_ani_call = 0;
						var n_hgth = sticky_head_hgt - hght_ele.outerHeight();
						offs += n_hgth;
						$('html,body').animate({ 'scrollTop': offs }, 100, 'easeInOutExpo' );
					}
				 });

				return false;
			}else{
				
				if( target_id == '#section-top' ){
					$('html,body').animate({ 'scrollTop': 0 }, 1000, 'easeInOutExpo' );
					return false;
				}else{
					var cur_url = window.location.href;
					var data_targ = $(cur_item).attr("data-target");
					if( cur_url != data_targ && target_id != '#' ){
						window.location.href = data_targ + target;
					}else{
						window.location.href = target;
					}
				}

			}
		
		});	
		
		/*Back to top*/
		if( $( ".back-to-top" ).length ){
			$( '#back-to-top' ).on('click',function (e) {
				$('html,body').animate({ 'scrollTop': 0 }, 1000, 'easeInOutExpo' );
				return false;
			});
			$( document ).scroll(function() {
				var y = $( this ).scrollTop();
				if ( y > 300 )
					$( '#back-to-top' ).fadeIn();
				else
					$( '#back-to-top' ).fadeOut();
			});
		}
		
		/*Woo Cart Item Remove Through Ajax*/
		if( $('.mini-cart-items').length ){
			$( document ).on('click', '.remove-cart-item', function(){
				var product_id = $(this).attr("data-product_id");
				var loader_url = $(this).attr("data-url");
				var main_parent = $(this).parents('li.menu-item.dropdown');
				var parent_li = $(this).parents('li.cart-item');
				parent_li.find('.product-thumbnail > .remove-item-overlay').css({'display':'block'});
				$.ajax({
					type: 'post',
					dataType: 'json',
					url: independent_ajax_var.admin_ajax_url,
					data: { action: "independent_product_remove", 
							product_id: product_id
					},success: function(data){
						main_parent.html( data["mini_cart"] );
						$( document.body ).trigger( 'wc_fragment_refresh' );
					},error: function(xhr, status, error) {
						$('.mini-cart-items').children('ul.cart-dropdown-menu').html('<li class="cart-item"><p class="cart-update-pbm text-center">'+ independent_ajax_var.cart_update_pbm +'</p></li>');
					}
				});
				return false;
			});	
		}
		
		/* Top Sliding Bar */
		if( $( ".top-sliding-bar" ).length ){
			$( ".top-sliding-toggle" ).on( "click", function() {
				$( ".top-sliding-bar-inner" ).slideToggle();
				inedependentLazyLoad(200, ".top-sliding-bar-inner");
				$( ".top-sliding-toggle" ).toggleClass( "fa-minus" );
				return false;
			});
		}
		
		/* Sticky Header Space */
		if( $('.sticky-header-space').length ){
			var elem_pos = $('.sticky-header-space').hasClass('left-sticky') ? 'left' : 'right';
			var elem_width = $('.sticky-header-space').outerWidth();
			
			independentStickyHeaderAdjust(elem_pos, elem_width);
			$( window ).resize(function() {
				independentStickyHeaderAdjust(elem_pos, elem_width);
			});
		}
		
		/* Toggle Search Modal Triggers */
		if( $( ".textbox-search-toggle" ).length ){
			$( ".textbox-search-toggle" ).on( "click", function() {
				$(this).parents('.search-toggle-wrap').toggleClass('active');
				setTimeout(function(){
					var search_in = $('.search-toggle-wrap').find("input.form-control");
					search_in.focus();
				}, 500);
				return false;
			});
		}else if( $( ".full-bar-search-toggle" ).length ){
			$( ".full-bar-search-toggle" ).on( "click", function() {
				$('.full-bar-search-wrap').toggleClass('active');
				setTimeout(function(){
					var search_in = $('.full-bar-search-wrap').find("input.form-control");
					search_in.focus();
				}, 500);				
				return false;
			});
		}else if( $( ".bottom-search-toggle" ).length ){
			$( ".bottom-search-toggle" ).on( "click", function() {
				$(this).parents('.search-toggle-wrap').toggleClass('active');
				setTimeout(function(){
					var search_in = $('.search-toggle-wrap').find("input.form-control");
					search_in.focus();
				}, 500);
				return false;
			});
		}
		
		/* Sticky Footer */
		if( $( ".footer-fixed" ).length ){
			if( $( window ).width() > 767 ){
				$( ".independent-wrapper" ).css({ 'margin-bottom' : $( ".footer-fixed" ).outerHeight() + 'px' });
			}else{
				$( ".independent-wrapper" ).css({ 'margin-bottom' : '0' });
			}
		}else if( $( ".footer-bottom-fixed" ).length ){
			if( $( window ).width() > 767 ){
				$( ".independent-wrapper" ).css({ 'margin-bottom' : $( ".footer-bottom-fixed" ).outerHeight() + 'px' });
			}else{
				$( ".independent-wrapper" ).css({ 'margin-bottom' : '0' });
			}
		}
		$( window ).resize(function() {
			if( $( ".footer-fixed" ).length ){
				if( $( window ).width() > 767 ){
					$( ".independent-wrapper" ).css({ 'margin-bottom' : $( ".footer-fixed" ).outerHeight() + 'px' });
				}else{
					$( ".independent-wrapper" ).css({ 'margin-bottom' : '0' });
				}
			}else if( $( ".footer-bottom-fixed" ).length ){
				if( $( window ).width() > 767 ){
					$( ".independent-wrapper" ).css({ 'margin-bottom' : $( ".footer-bottom-fixed" ).outerHeight() + 'px' });
				}else{
					$( ".independent-wrapper" ).css({ 'margin-bottom' : '0' });
				}
			}
		});							
		
		/* Stellar Parallax */
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 40
		});
		
		/* Bootstrap Tooltip */
		if( $('[data-toggle="tooltip"]').length ){
			$('[data-toggle="tooltip"]').tooltip();
		}
		
		/* Post Like */
		$( document ).on( 'click', ".post-like, .post-dislike", function( event) {
	
			var current = $(this);
			var like_stat = current.data("stat");
			var post_id = current.data("id");
			var parent = current.parents('.post-like-wrap');

			//return false;
			if( like_stat != '' ){
				
				if( like_stat == '1' ){
					parent.find('.post-disliked').removeClass('fa-thumbs-down post-disliked').addClass('fa-thumbs-o-down post-dislike');
					current.removeClass('fa-thumbs-o-up post-like').addClass('fa-thumbs-up post-liked');
				}else{
					parent.find('.post-liked').removeClass('fa-thumbs-up post-liked').addClass('fa-thumbs-o-up post-like');
					current.removeClass('fa-thumbs-o-down post-dislike').addClass('fa-thumbs-down post-disliked');
				}
				
				// Ajax call
				$.ajax({
					type: "post",
					url: independent_ajax_var.admin_ajax_url,
					data: "action=post_like_act&nonce="+independent_ajax_var.like_nonce+"&like_stat="+like_stat+"&post_id="+post_id,
					success: function(res){
						$( parent ).html(res);
						$('body').tooltip({
							container: 'body',
							trigger: 'hover',
							html: true,
							animation: false,
							selector: '[data-toggle="tooltip"]'
						});
					},
					error: function (jqXHR, exception) {
						console.log(jqXHR);
					}
				});
			}
			return false;
		});
		$( document ).on( 'click', ".post-liked, .post-disliked, .post-fav-done", function( event) {
			return false;
		});															 
																		 
		
		/* Post Favourite */
		$( document ).on( 'click', ".post-favourite", function( event) {
	
			var current = $(this);
			var post_id = current.data("id");
			var parent = current.parents('.post-fav-wrap');

			if( post_id != '' ){
				parent.find('.post-favourite').removeClass('fa-heart-o post-favourite').addClass('fa-heart');
				// Ajax call
				$.ajax({
					type: "post",
					url: independent_ajax_var.admin_ajax_url,
					data: "action=post_fav_act&nonce="+independent_ajax_var.fav_nonce+"&post_id="+post_id,
					success: function(res){
						$( parent ).html(res);
						$('body').tooltip({
							container: 'body',
							trigger: 'hover',
							html: true,
							animation: false,
							selector: '[data-toggle="tooltip"]'
						});
					},
					error: function (jqXHR, exception) {
						console.log(jqXHR);
					}
				});
			}
			return false;
		});

		/* Magnific Zoom Gallery Code */
		$('.zoom-gallery').magnificPopup({
          delegate: 'a',
          type: 'image',
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: 'mfp-with-zoom mfp-img-mobile',
          gallery: {
            enabled: true
          },
          zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
              return element.find('img');
            }
          }
        });
		
		$('.image-gallery').magnificPopup({
			delegate: '.image-gallery-link',
			type: 'image',
			closeOnContentClick: false,
			closeBtnInside: false,
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			gallery: {
				enabled: true
			},
		});
		
			
		/* Magnific Popup Code */
		$('.popup-video-post').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
        });
		
		$('.popup-with-zoom-anim').magnificPopup({
			disableOn: 700,
			type: 'inline',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false,
			callbacks: {
			open: function() {
				// Play video on open:
				if( !$( this.content ).find('video').length ){
					var parent = $( this.content ).parent( "post-video-wrap" );
					var url = $( this.content ).find('span').data( "url" );
					var video = '<video width="100%" height="450" preload="true" style="max-width:100%;" autoplay="true"><source src="'+ url +'" type="video/mp4"></video>';
					$( this.content ).find('span').replaceWith( video );
					var video = $( this.content ).find('video');
				}else{
					$(this.content).find('video')[0].load();
				}
			},
			close: function() {
				// Reset video on close:
				$(this.content).find('video')[0].pause();
	
			}
		}
		});
		
		/* Set Blockquote Background */
		$( ".post-quote-wrap, .post-link-wrap" ).each(function() {
			var img_url = $(this).data('url');
			if( img_url ){
				$(this).css( 'background-image','url('+ img_url +')' );	
			}
		});
		
		/* Set Background Image */
		$( ".set-bg-img" ).each(function() {
			var img_url = $(this).data('src');
			if( img_url ){
				$(this).css( 'background-image','url('+ img_url +')' );	
			}
		});
		
		$("a.onclick-video-post").on( "click", function(){

			var parent = $(this).parent('.post-video-wrap');
			var frame = '<iframe src="'+ $(this).attr("href") +'?autoplay=1" width="100%" height="'+ parent.height() +'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
			$(this).fadeOut(300);
			$(this).replaceWith( frame );
			return false;
		});
		
		$(".onclick-custom-video").on( "click", function(){
	
			var parent = $(this).parent('.post-video-wrap');
			var video = '<video width="100%" height="'+ parent.height() +'" preload="true" style="max-width:100%;" autoplay="true"><source src="'+ $(this).data("url") +'" type="video/mp4"></video>';
			$(this).fadeOut(300);
			$(this).replaceWith( video );
			return false;
			
		});
		
		/* Page Title Background Video */
		if( $( "#page-title-bg .page-title-wrap-inner" ).length ){
			$( "#page-title-bg .page-title-wrap-inner" ).YTPlayer();
		}
		
		/* Comments Like/Dislike */
		$( document ).on( 'click', ".fa-thumbs-o-up.comment-like, .fa-thumbs-o-down.comment-like", function( event) {
	
			var cmt_cur = $(this);
			var cmt_meta = cmt_cur.data('id');
			var cmt_id = cmt_cur.data('cmt-id');
			var parent = cmt_cur.parents('.comment-like-wrapper');
			if( cmt_meta == '1' ){
				cmt_cur.parents('.list-inline').find('.comment-liked').removeClass('fa-thumbs-down comment-liked').addClass('fa-thumbs-o-down comment-like');
				cmt_cur.removeClass('fa-thumbs-o-up comment-like').addClass('fa-thumbs-up comment-liked');
			}else{
				cmt_cur.parents('.list-inline').find('.comment-liked').removeClass('fa-thumbs-up comment-liked').addClass('fa-thumbs-o-up comment-like');
				cmt_cur.removeClass('fa-thumbs-o-down comment-like').addClass('fa-thumbs-down comment-liked');	
			}
			
			if( cmt_id != '' && cmt_meta != '' ){
				// Ajax call
				(jQuery).ajax({
					type: "post",
					url: independent_ajax_var.admin_ajax_url,
					data: "action=comment_like&nonce="+independent_ajax_var.cmt_nonce+"&cmt_id="+cmt_id+"&cmt_meta="+cmt_meta,
					success: function(res){
						$( parent ).html(res);
					},
					error: function (jqXHR, exception) {
						console.log(jqXHR);
					}
				});
			}
			return false;
		});
		
		/*Mailchimp Code*/
		if( $('.zozo-mc').length ){
			$('.zozo-mc').live( "click", function () {
				
				var c_btn = $(this);
				var mc_wrap = $( this ).parents('.mailchimp-wrapper');
				var mc_form = $( this ).parents('.zozo-mc-form');
				
				if( mc_form.find('input[name="zozo_mc_email"]').val() == '' ){
					mc_wrap.find('.mc-notice-msg').text( independent_ajax_var.must_fill );
				}else{
					c_btn.attr( "disabled", "disabled" );
					$.ajax({
						type: "POST",
						url: independent_ajax_var.admin_ajax_url,
						data: 'action=zozo-mc&nonce='+independent_ajax_var.mc_nounce+'&'+mc_form.serialize(),
						success: function (data) {
							//Success
							c_btn.removeAttr( "disabled" );
							if( data == 'success' || data == 'already' ){
								mc_wrap.find('.mc-notice-msg').text( mc_wrap.find('.mc-notice-group').attr('data-success') );
							}else{
								mc_wrap.find('.mc-notice-msg').text( mc_wrap.find('.mc-notice-group').attr('data-fail') );
							}
						},error: function(xhr, status, error) {
							c_btn.removeAttr( "disabled" );
							mc_wrap.find('.mc-notice-msg').text( mc_wrap.find('.mc-notice-group').attr('data-fail') );
						}
					});
				}
			});
		} // if shortcode mailchimp exists
		
		/* Facbook Comment Width Resize */
		if( $( '.fb-comments-wrapper' ).length ){
			$( window ).resize(function() {
				setTimeout(function(){
					if($( window ).width() <= 768 ){
						$( ".fb-comments-wrapper iframe" ).width( $( ".content-area" ).width() );
					}else{
						$( ".fb-comments-wrapper iframe" ).width( $( ".content-area .fb-comments" ).data('width') );
					}
				}, 200);
			});
		}
		
		/* Weather Custom Script */
		if( $( '.weather-widget' ).length ){
			$('.weather-widget .weather-single-wrap .weather-temp, .weather-short-wrap .weather-temp').on( "click", function () {
				var weather_parent = $(this).parents('.weather-main');
				if( $(weather_parent).hasClass("weather-calc-default") ){
					$(weather_parent).toggleClass("weather-calc-default");
					$( weather_parent ).find("*[data-walter]").each(function() {
						$(this).text($(this).attr("data-walter"));
					});
				}else{
					$(weather_parent).toggleClass("weather-calc-default");
					$( weather_parent ).find("*[data-wdefault]").each(function() {
						$(this).text($(this).attr("data-wdefault"));
					});
				}
			});
		}
		
		/* Crypto Slider Script */
		if( $( ".crypto-slider" ).length ){
			$( ".crypto-slider" ).each(function( index ) {
				$(this).endlessRiver({
					buttons: false,
					speed: 50
				});
			});
		}
		
		/* Single Post Nav Bg Image */
		/*if( $( 'body.single-post' ).length ){
			$( ".element-bg-image" ).each(function() {
				var img_url = $(this).attr("data-img");
				if( img_url ){
					$(this).css({ 'background-image' : 'url('+ img_url +')' });
				}
			});
		}*/

	}); // doc ready end
	
	
	$( window ).load(function() {
		
		/* Grid Layout Set Width for Owl and Isotope */
		if( $( ".grid-layout.grid-normal" ).length ){
			$( ".grid-layout.grid-normal" ).each(function() {
			
				var c_elem = $( this );
				var parent_width = c_elem.width();
				var gutter_size = c_elem.data( "gutter" );
				var grid_cols = c_elem.data( "cols" );
				
				var net_width = Math.floor( ( parent_width - ( gutter_size * ( grid_cols - 1 ) ) ) / grid_cols );
				c_elem.find( "article" ).css({'width':net_width+'px', 'margin-bottom':gutter_size+'px'});
				
				c_elem.find( ".top-standard-post article" ).css({'width':'auto'});
			
			});	// each end		
		} // .grid-layout
		
		/* Theme Owl Carousel Code */
		$( ".owl-carousel" ).each(function() {
			if( !$( this ).parents( ".isotope" ).length ){
				independentOwlSettings( $( this ) );
			}
		});
		
		/* Masonry post */
		if( $( ".indy-masonry-wrap" ).length ){
			
			var win_wdth = $(window).width();

			$( ".indy-masonry-wrap" ).each(function() {
				var gutter = $(this).data("gutter");
				var cols = $(this).data("cols");
				
				if( win_wdth < 768 ){
					cols = 1;
				}else if( win_wdth < 992 ){
					cols = 2;	
				}
				
				$(this).zozoMasonry({
					'clName'    : '.blog-masonry-items',
					'gap'       : parseInt(gutter),
					'column'    : parseInt(cols),
				});
			});
			
			
			$( window ).resize(function() {

				setTimeout(function(){ 
					
					var win_wdth = $(window).width();
					
					$( ".indy-masonry-wrap" ).each(function() {
						var gutter = $(this).data("gutter");
						var cols = $(this).data("cols");
						
						if( win_wdth < 768 ){
							cols = 1;
						}else if( win_wdth < 992 ){
							cols = 2;	
						}
						
						$(this).zozoMasonry({
							'clName'    : '.blog-masonry-items',
							'gap'       : parseInt(gutter),
							'column'    : parseInt(cols),
						});
					});
					
				}, 200);
				
			}); //resize close
		}
		
		/* Normal Grid Layout */
		if( $( ".grid-layout.grid-normal" ).length ){
			$( ".grid-layout.grid-normal" ).each(function() {
			
				var c_elem = $( this );
				var parent_width = c_elem.width();
				var gutter_size = c_elem.data( "gutter" );
				var grid_cols = c_elem.data( "cols" );
				
				if( $(window).width() < 768 ) grid_cols = 1;
				
				var net_width = Math.floor( ( parent_width - ( gutter_size * ( grid_cols - 1 ) ) ) / grid_cols );
				c_elem.find( "article" ).css({'width':net_width+'px', 'margin-right':gutter_size+'px', 'margin-bottom':gutter_size+'px'});
				c_elem.find(".grid-parent").css({ 'margin-right' : '-' + gutter_size + 'px' });
				
				c_elem.find( ".top-standard-post article" ).css({'width':'auto'});
				
				$( window ).resize(function() {

					setTimeout(function(){ 
			
						parent_width = c_elem.width();
						grid_cols = c_elem.data( "cols" );
						
						if( $(window).width() < 768 ) grid_cols = 1;
						
						net_width = Math.floor( ( parent_width - ( gutter_size * ( grid_cols - 1 ) ) ) / grid_cols );
						c_elem.find( "article" ).css({'width':net_width+'px', 'margin-right':gutter_size+'px', 'margin-bottom':gutter_size+'px'});
						
						c_elem.find( "audio, video" ).each(function( index ) {
							$( this )[0].play();
							$( this )[0].pause();
						});
								
					}, 200);
					
				});	
			});	// each end
		}
		
		/* Isotope Grid Layout */
		if( $( ".grid-layout > .isotope" ).length ){
			$( ".grid-layout > .isotope" ).each(function() {
			
				var c_elem = $( this );
				var parent_width = c_elem.width();
				var gutter_size = c_elem.data( "gutter" );
				var grid_cols = c_elem.data( "cols" );

				var layoutmode = c_elem.is('[data-layout]') ? c_elem.data( "layout" ) : '';
				
				layoutmode = layoutmode ? layoutmode : 'masonry';
				
				if( $(window).width() < 768 ) grid_cols = 1;
				
				var net_width = Math.floor( ( parent_width - ( gutter_size * ( grid_cols - 1 ) ) ) / grid_cols );
				c_elem.find( "article" ).css({'width':net_width+'px', 'margin-bottom':gutter_size+'px'});
				if( $( ".top-standard-post" ).length ){
					$( ".top-standard-post article" ).css({'margin-bottom':gutter_size+'px'});	
				}
				
				c_elem.find( ".owl-carousel" ).each(function() {
					independentOwlSettings( $( this ) );
				});
				
				c_elem.find( "video" ).each(function( index ) {
					$( this ).attr( "src", $( this ).find( "source" ).attr( "src" ) );
					$( this ).css({ 'height':'200px' });
				});
				
				var filter = "*";
				var isot_parent = c_elem.parent(".grid-layout");
				if( $( isot_parent ).attr("data-filter-stat") == 0 ){
					filter = $( isot_parent ).attr("data-first-cat") ? "." + $( isot_parent ).attr("data-first-cat") : '*';
				}
				
				c_elem.imagesLoaded( function(){
					c_elem.isotope({
						itemSelector: 'article',
						layoutMode: layoutmode,
						filter: filter,
						masonry: {
							gutter: gutter_size
						},
						fitRows: {
						  gutter: gutter_size
						}
					});
				});
				
				/* Portfolio Filter Item */
				if( $(".portfolio-filter").length ){
					$( ".portfolio-filter-item" ).on( 'click', function() {
						$( this ).parents("ul.nav").find("li").removeClass("active");
						$( this ).parent("li").addClass("active");
						var filterValue = $( this ).attr( "data-filter" );
						c_elem = $( this ).parents( ".portfolio-wrapper" ).find( ".grid-layout .isotope" );
						c_elem.isotope({ 
							filter: filterValue
						});
						return false;
					});
				}

				$( window ).resize(function() {

					setTimeout(function(){ 
						grid_cols = c_elem.data( "cols" );
						if( $(window).width() < 768 ) grid_cols = 1;
						
						var parent_width = c_elem.width();
						net_width = Math.floor( ( parent_width - ( gutter_size * ( grid_cols - 1 ) ) ) / grid_cols );
						c_elem.find( "article" ).css({'width':net_width+'px'});
						c_elem.imagesLoaded( function(){
							var $isot = c_elem.isotope({
								itemSelector: 'article',
								masonry: {
									gutter: gutter_size
								}
							});
							$isot.on( 'arrangeComplete', isotopeArrange );
						});
						
					}, 200);
					
				});	
				
				// Isotope Grid Infinite
				if( c_elem.data( "infinite" ) == 1 && $(".post-pagination").length ){

					c_elem.infinitescroll({
						navSelector  : '.post-pagination',//'#page_nav',    // selector for the paged navigation 
						nextSelector : 'a.next-page',//'#page_nav a',  // selector for the NEXT link (to page 2)
						itemSelector : 'article',     // selector for all items you'll retrieve
						loading: {
							msgText : independent_ajax_var.load_posts,
							finishedMsg: independent_ajax_var.no_posts,
							img: independent_ajax_var.infinite_loader
						}
					},
					// call Isotope as a callback
					function( newElements ) {
						
						var elems = $(newElements);
						
						var net_width = Math.floor( ( parent_width - ( gutter_size * ( grid_cols - 1 ) ) ) / grid_cols );
						c_elem.find( "article" ).css({'width':net_width+'px', 'margin-bottom':gutter_size+'px'});
						if( $( ".top-standard-post" ).length ){
							$( ".top-standard-post article" ).css({'margin-bottom':gutter_size+'px'});	
						}
						
						c_elem.find( ".owl-carousel" ).each(function() {
							independentOwlSettings( $( this ) );
						});
						
						elems.find( "video" ).each(function( index ) {
							$( this ).attr( "src", $( this ).find( "source" ).attr( "src" ) );
							$( this ).css({ 'height':'200px' });
						});
						
						elems.imagesLoaded( function(){
							c_elem.isotope( 'appended', elems );
						});
						
					});
				}
			}); // each end
		}

		/* Related Slider Empty Post Image Height Set */
		if( $( ".related-slider .empty-post-image" ).length ){
			if( $( ".related-slider .item .wp-post-image" ).length ){
				$( ".related-slider .item .empty-post-image" ).height( $( ".related-slider .item .wp-post-image" ).height() );	
			}
		}
		
		/* Featured Slider Empty Post Image Height Set */
		if( $( ".featured-slider .empty-post-image" ).length ){
			if( $( ".featured-slider .item .wp-post-image" ).length ){
				$( ".featured-slider .item .empty-post-image" ).height( $( ".featured-slider .item .wp-post-image" ).height() );	
			}
		}
		
		/* Sticky Sidebar */
		var $sticky_sidebars = $( ".independent-sticky-obj" );
		if( $( window ).width() > 767 ) {
			$sticky_sidebars.stick_in_parent();
		}
		$( window ).resize(function() {
			if( $( window ).width() > 767 ) {
				$sticky_sidebars.trigger( "sticky_kit:detach" );	
				$sticky_sidebars.stick_in_parent();
				$sticky_sidebars.trigger( "sticky_kit:recalc" );
			}else{
				$sticky_sidebars.trigger( "sticky_kit:detach" );	
			}
		});
		
		/* VC Stciky Sidebar Code Start */
		if( $(".zozo-vcsticky").length ){
			
			var full_w;
			full_w = $('.independent-content-inner .content-area').width();
	
			if($( window ).width() >= 768 ){
				$(".zozo-vcsticky").trigger("sticky_kit:detach");
				$(".zozo-vcsticky").stick_in_parent().on("sticky_kit:stick", function(e) {
					var wdth = ( 100 / Math.round( full_w / parseInt($(this).width()) ) ).toFixed(4); 
					$(this).parent('div').css({'width':wdth+'%', 'position':'static'});
				});
			}else{
				$(".zozo-vcsticky").trigger("sticky_kit:detach");
			}
		
			$( window ).resize(function() {
				if($( window ).width() >= 768 ){
					full_w = $('.independent-content-inner .content-area').width();
					$(".zozo-vcsticky").trigger("sticky_kit:detach");
					$(".zozo-vcsticky").stick_in_parent().on("sticky_kit:stick", function(e) {
						var wdth = ( 100 / Math.round( full_w / parseInt($(this).width()) ) ).toFixed(4); 
						$(this).parent('div').css({'width':wdth+'%', 'position':'static'});
					});
				}else{
					$(".zozo-vcsticky").trigger("sticky_kit:detach");
				}
			});
		}
		/* VC Stciky Sidebar Code End */
		
		/* Counter Script */
		var counterUp = $( ".counter-up" );
		counterUp.appear(function() {
			var $this = $(this),
			countTo = $this.attr( "data-count" );
			$({ countNum: $this.text()}).animate({
					countNum: countTo
				},
				{
				duration: 1000,
				easing: 'linear',
				step: function() {
					$this.text( Math.floor( this.countNum ) );
				},
				complete: function() {
					$this.text( this.countNum );
				}
			});  
		});
		

		/* Circle Counter Shortcode Script */
		if( $( '.circle-progress-circle' ).length ){
			var circle = $( '.circle-progress-circle' );
			circle.appear(function() {
							  
				var c_circle = $( this );
				var c_value = c_circle.attr( "data-value" );
				var c_size = c_circle.attr( "data-size" );
				var c_thickness = c_circle.attr( "data-thickness" );
				var c_duration = c_circle.attr( "data-duration" );
				var c_empty = c_circle.attr( "data-empty" ) != '' ? c_circle.attr( "data-empty" ) : 'transparent';
				var c_scolor = c_circle.attr( "data-scolor" );
				var c_ecolor = c_circle.attr( "data-ecolor" ) != '' ? c_circle.attr( "data-ecolor" ) : c_scolor;
									
				c_circle.circleProgress({
					value: Math.floor( c_value ) / 100,
					size: Math.floor( c_size ),
					thickness: Math.floor( c_thickness ),
					emptyFill: c_empty,
					animation: {
						duration: Math.floor( c_duration )
					},
					lineCap: 'round',
					fill: {
						gradient: [c_scolor, c_ecolor]
					}
				}).on( 'circle-animation-progress', function( event, progress ) {
					$( this ).find( '.progress-value' ).html( Math.round( c_value * progress ) + '%' );
				});
			});
		}
		
		/* Day Counter Shortcode Script */
		if( $( '.day-counter' ).length ){
			$( '.day-counter' ).each(function() {
				var day_counter = $( this );
				var c_date = day_counter.attr('data-date');
				day_counter.countdown( c_date, function(event) {
					if( day_counter.find('.counter-day').length ){
						day_counter.find('.counter-day h3').text( event.strftime('%D') );
					}
					if( day_counter.find('.counter-hour').length ){
						day_counter.find('.counter-hour h3').text( event.strftime('%H') );
					}
					if( day_counter.find('.counter-min').length ){
						day_counter.find('.counter-min h3').text( event.strftime('%M') );
					}
					if( day_counter.find('.counter-sec').length ){
						day_counter.find('.counter-sec h3').text( event.strftime('%S') );
					}
					if( day_counter.find('.counter-week').length ){
						day_counter.find('.counter-week h3').text( event.strftime('%w') );
					}
				});
			});
		}
		
		/* Page Load Modal Script */
		if( $('.modal-popup-wrapper.page-load-modal').length ){
			var modal_id = $('.modal-popup-wrapper.page-load-modal .modal').attr("id");
			$('#'+modal_id).modal('show');
		}
		
		/* Canvas Shapes */
		if( $(".canvas_agon").length ){
			$( '.canvas_agon' ).each(function() {
				independentAgon( $(this) );
			});
		}
		
		if( $('.slider-for').length ){
			$('.slider-for').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: false,
			  fade: true,
			  asNavFor: '.slider-nav'
			});
		}
		if( $('.slider-nav').length ){
			$('.slider-nav').slick({
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  vertical: true,
			  asNavFor: '.slider-for',
			  dots: true,
			  //centerMode: true,
			  focusOnSelect: true
			});
		}

	});
	
	var win_width = $(window).width();
	
	// Using window smartresize instead of resize function
	$( window ).smartresize(function() {
		
		/* Mobile Bar Toggle  */
		if( win_width != $(window).width() ){
			win_width = $(window).width();
			setTimeout( function(){ $(".mobile-bar.active").length ?  $( ".mobile-header .mobile-bar-toggle" ).trigger( "click" ) : ''; }, 100 );
		}
				
		/* Pull Center Reset  */
		setTimeout( independentCenterMenuMargin, 300 );
		
		/* Sticky Menu */
		if($('.header-inner .sticky-head').length){
			setTimeout( independentStickyPart( '.header-inner' ), 100 ); 
		}
		
		/* Scroll Sticky */
		if($('.header-inner .sticky-scroll').length){
			setTimeout( independentStickyScrollUpPart( '.header-inner', 'header' ), 100 ); 
		}
		
		/* Mobile Header Sticky Menu */
		if($('.mobile-header-inner .sticky-head').length){
			setTimeout( independentStickyPart( '.mobile-header-inner' ), 100 ); 
		}
		
		/* Mobile Header Scroll Sticky */
		if($('.mobile-header-inner .sticky-scroll').length){
			setTimeout( independentStickyScrollUpPart( '.mobile-header-inner', '.mobile-header' ), 100 ); 
		}
		
	});
	
	$( window ).load(function() {
		if( $( ".independentgmap" ).length ){
			initindependentGmap();
		}
	});
	
	function isotopeArrange() {
		$( ".grid-layout > .isotope" ).find( "audio, video" ).each(function( index ) {
			$( this )[0].play();
			$( this )[0].pause();
		});
	}
	
	function independentStickyHeaderAdjust(elem_pos, elem_width){
		var win_width = $(window).width();
		if( win_width < 992 ){
			if( elem_pos == 'left' ){
				$('.sticky-header-space').css( 'left', '-'+ elem_width +'px' );
				$('body, .top-sliding-bar').css( 'padding-left', '0' );
			}else{
				$('.sticky-header-space').css( 'right', '-'+ elem_width +'px' );
				$('body, .top-sliding-bar').css( 'padding-right', '0' );
			}
		}else{
			if( elem_pos == 'left' ){
				$('.sticky-header-space').css( 'left', 0 );
				$('body, .top-sliding-bar').css( 'padding-left', elem_width +'px' );
			}else{
				$('.sticky-header-space').css( 'right', 0 );
				$('body, .top-sliding-bar').css( 'padding-right', elem_width +'px' );
			}	
		}	
	}

	function independentCenterMenuMargin(){
		//Center item margin fixing
		$.each([ 'topbar', 'logobar', 'navbar', 'mobile-header', 'footer-bottom' ], function( index, margin_key ) {
			
			var left_width = 0,
				right_width = 0,
				center_width = 0,
				margin_left = 0,
				parent_width = 0;

			if( $('.'+ margin_key +' .'+ margin_key +'-inner').length ){
			
				if( margin_key == 'mobile-header' )
					parent_width = $('.'+ margin_key +' .'+ margin_key +'-inner .custom-container').width();
				else
					parent_width = $('.'+ margin_key +' .'+ margin_key +'-inner').width();
				
				if( $('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-left').length ){
					left_width = $('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-left').width();
				}
				if( $('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-right').length ){
					right_width = $('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-right').width();
				}
				if( $('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-center').length ){
					center_width = $('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-center').width();
				}
					
				if( left_width + center_width + right_width ){
				
					if( margin_key == 'mobile-header' ){
						parent_width -= ( left_width + center_width + right_width );
						margin_left = parent_width / 2; 
					}else{
						parent_width = ( parent_width / 2 ) - ( center_width / 2 );
						margin_left = Math.floor( parent_width - left_width );
					}
					
					if( !$( "body.rtl" ).length ){
						$('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-center').css( 'margin-left', margin_left+'px' );
					}else{
						$('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-center').css( 'margin-right', margin_left+'px' );
					}
					
					$('.'+ margin_key +' .'+ margin_key +'-inner .'+ margin_key +'-items.pull-center').addClass("show-opacity");
					
				}
			}
		});
	}
	
	function independentStickyPart( main_class ){

		var outer_class = '.sticky-outer';	
		var lastScrollTop = 0;
		var header_top = 0;

		$(main_class + ' ' + outer_class).css( 'height', $(main_class + ' ' + outer_class).data( "height" ) );
		header_top = $(main_class + ' ' + outer_class).offset().top;

		$(window).scroll(function(event){
			
			var st = $(this).scrollTop();
			if( st > header_top ){
				$(main_class + ' .sticky-head').addClass('header-sticky');
			}else{
				$(main_class + ' .sticky-head').removeClass('header-sticky');
			}
			
			if( st == 0 ){
				$(main_class + ' .sticky-head').removeClass('header-sticky');
			}
			
			lastScrollTop = st;
		});	
	}
	
	function independentStickyScrollUpPart( main_class, sticky_div ){
		
		var outer_class = '.sticky-outer';	
		var out_height = '';
		var lastScrollTop = 0;
		var header_top = 0;
	
		$(main_class + ' ' + outer_class).css( 'height', $(main_class + ' ' + outer_class).data( "height" ) );
		out_height = '-' + $(main_class + ' ' + outer_class).outerHeight() + 'px';
		header_top = $(main_class + ' ' + outer_class).offset().top;
		sticky_div = $(sticky_div).height();
		
		$(window).scroll(function(event){

			var st = $(this).scrollTop();
			
			if( st < lastScrollTop && header_top < lastScrollTop ){
				// upscroll code
				$(main_class + ' .sticky-scroll').addClass('show-menu');
				$(main_class + ' .sticky-scroll.show-menu').css({'transform': 'translate3d(0px, 0px, 0px)'});
			}else{
				// downscroll code
				if( st < sticky_div ){
					$(main_class + ' .sticky-scroll').css({'transform': ''});
					$(main_class + ' .sticky-scroll.show-menu').removeClass('show-menu');
				}else{
					$(main_class + ' .sticky-scroll').css({'transform': 'translate3d(0px, '+ out_height +', 0px)'});
				}
			}
			
			if( st == 0 ){
				$(main_class + ' .sticky-scroll').css({'transform': ''});
				$(main_class + ' .sticky-scroll.show-menu').removeClass('show-menu');
			}
			
			lastScrollTop = st;
		});
		
	}
	
	function independentSetStickyOuterHeight(){
		$( ".sticky-outer" ).each(function() {

			var class_name = '';
			if( $( this ).parent( "div" ).hasClass( "mobile-header-inner" ) ){
				class_name = $( this ).parent( "div" ).attr("class");
				$( this ).parent( "div" ).attr("class", "");
			}
			
			if( $( this ).parent( "div" ).is('[class*=hidden-]') ){
				class_name = $( this ).parent( "div" ).attr("class");
				$( this ).parent( "div" ).attr("class", "");
			}
			
			$( this ).css({ 'position':'absolute', 'visibility':'hidden', 'display':'block', 'height':'auto' });
			$( this ).attr( "data-height", $( this ).outerHeight() );

			if( class_name != '' ){
				$( this ).parent( "div" ).attr("class", class_name);
			}
			$( this ).css({ 'position':'', 'visibility':'', 'display':'', 'height': $( this ).data( "height" ) });

		});
	}
	
	function independentAgon( canvas_ele ){
		var canvas = document.getElementById("canvas_agon");
		var cxt = canvas.getContext("2d");
		var agon_size = canvas_ele.attr( "data-size" );
		var agon_side = canvas_ele.attr( "data-side" );
		var div_val = 1;

		switch( parseInt( agon_side ) ){
			case 3:
				div_val = 6;
			break;
			case 4:
				div_val = 4;
			break;
			case 5:
				div_val = 3.3;
			break;
			case 6:
				div_val = 3;
			break;
			case 7:
				div_val = 2.8;
			break;
			case 8:
				div_val = 2.7;
			break;
			case 9:
				div_val = 2.6;
			break;
			case 10:
				div_val = 2.5;
			break;
		}

		// hexagon
		var numberOfSides = parseInt( agon_side ),
			size = parseInt( agon_size ),
			Xcenter = parseInt( agon_size ),
			Ycenter = parseInt( agon_size ),
			step  = 2 * Math.PI / numberOfSides,//Precalculate step value
			shift = (Math.PI / div_val);//(Math.PI / 180.0);// * 44;//Quick fix ;)

		cxt.beginPath();

		for (var i = 0; i <= numberOfSides;i++) {
			var curStep = i * step + shift;
		   cxt.lineTo (Xcenter + size * Math.cos(curStep), Ycenter + size * Math.sin(curStep));
		}

		/* Direct Output */
		cxt.fillStyle = '#333';
		cxt.fill();
	}
	
	function independentOwlSettings(c_owlCarousel){
		// Data Properties
		var loop = c_owlCarousel.data( "loop" );
		var margin = c_owlCarousel.data( "margin" );
		var center = c_owlCarousel.data( "center" );
		var nav = c_owlCarousel.data( "nav" );
		var dots_ = c_owlCarousel.data( "dots" );
		var items = c_owlCarousel.data( "items" );
		var items_tab = c_owlCarousel.data( "items-tab" );
		var items_mob = c_owlCarousel.data( "items-mob" );
		var duration = c_owlCarousel.data( "duration" );
		var smartspeed = c_owlCarousel.data( "smartspeed" );
		var scrollby = c_owlCarousel.data( "scrollby" );
		var autoheight = c_owlCarousel.data( "autoheight" );
		var autoplay = c_owlCarousel.data( "autoplay" );
		var rtl = $( "body.rtl" ).length ? true : false;

		$( c_owlCarousel ).owlCarousel({
			rtl : rtl,
			loop	: loop,
			autoplayTimeout	: duration,
			smartSpeed	: smartspeed,
			center: center,
			margin	: margin,
			nav		: nav,
			navText	: ['<i class="fa fa-angle-left">','<i class="fa fa-angle-right">'],
			dots	: dots_,
			autoplay	: autoplay,
			autoheight	: autoheight,
			slideBy		: scrollby,
			responsive:{
				0:{
					items: items_mob
				},
				544:{
					items: items_tab
				},
				992:{
					items: items
				}
			}
		});	
	}
	
	function independent_scroll_animation(){
		setTimeout( function() {
			var anim_time = 300;
			$('.independent-animate:not(.run-animate)').each( function() {
				
				var elem = $(this);
				var bottom_of_object = elem.offset().top;
				var bottom_of_window = $(window).scrollTop() + $(window).height();
				
				if( bottom_of_window > bottom_of_object ){
					setTimeout( function() {
						elem.addClass("run-animate");
					}, anim_time );
				}
				anim_time += 300;
				
			});
		}, 200 );
	}
	function initindependentGmap() {
		
		var map_styles = '{ "Aubergine" : [	{"elementType":"geometry","stylers":[{"color":"#1d2c4d"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#8ec3b9"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#1a3646"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#4b6878"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#64779e"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#4b6878"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#334e87"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#023e58"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#283d6a"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#6f9ba5"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#023e58"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#3C7680"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#304a7d"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#2c6675"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#255763"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#b0d5ce"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"color":"#023e58"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"color":"#283d6a"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#3a4762"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#0e1626"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#4e6d70"}]}], "Silver" : [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}], "Retro" : [{"elementType":"geometry","stylers":[{"color":"#ebe3cd"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#523735"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f1e6"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#c9b2a6"}]},{"featureType":"administrative.land_parcel","elementType":"geometry.stroke","stylers":[{"color":"#dcd2be"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#ae9e90"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#93817c"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#a5b076"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#447530"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#f5f1e6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#fdfcf8"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#f8c967"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#e9bc62"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#e98d58"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#db8555"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#806b63"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"transit.line","elementType":"labels.text.fill","stylers":[{"color":"#8f7d77"}]},{"featureType":"transit.line","elementType":"labels.text.stroke","stylers":[{"color":"#ebe3cd"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#b9d3c2"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#92998d"}]}], "Dark" : [{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2c2c2c"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}], "Night" : [{"elementType":"geometry","stylers":[{"color":"#242f3e"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#746855"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#242f3e"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#d59563"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#d59563"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#263c3f"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#6b9a76"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#38414e"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#212a37"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#9ca5b3"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#746855"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#1f2835"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#f3d19c"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#2f3948"}]},{"featureType":"transit.station","elementType":"labels.text.fill","stylers":[{"color":"#d59563"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#17263c"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#515c6d"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#17263c"}]}] }';
		
		var map_style_obj = JSON.parse(map_styles);
		
		var map_style_mode = [];
		var map_mode = '';
		var map_lang = '';
		var map_lat = '';
		var map_marker = '';
		var map_options = '';
		
		$( ".independentgmap" ).each(function( index ) {
			
			var gmap = this;

			if( $( gmap ).attr( "data-map-style" ) ){
				map_mode = $( gmap ).data("map-style");
				map_lang = $( gmap ).data("map-lang");
				map_lat = $( gmap ).data("map-lat");
				map_marker = $( gmap ).data("map-marker");
				if( map_mode === 'aubergine' )
					map_style_mode = map_style_obj.Aubergine;
				else if( map_mode === 'silver' )
					map_style_mode = map_style_obj.Silver;
				else if( map_mode === 'retro' )
					map_style_mode = map_style_obj.Retro;
				else if( map_mode === 'dark' )
					map_style_mode = map_style_obj.Dark;
				else if( map_mode === 'night' )
					map_style_mode = map_style_obj.Night;
				else if( map_mode === 'custom' ){
					var c_style = $( gmap ).attr( "data-custom-style" ) && $( gmap ).attr( "data-custom-style" ) != '' ? JSON.parse( $( gmap ).attr( "data-custom-style" ) ) : '[]';
					map_style_mode = c_style;
				}else{
					map_style_mode = "[]";
				}
			}
			
			if( $( gmap ).attr( "data-multi-map" ) && $( gmap ).attr( "data-multi-map" ) == 'true' ){
				
				var map_values = JSON.parse( $( gmap ).attr( "data-maps" ) );
				var map_wheel = $( gmap ).attr( "data-wheel" ) && $( gmap ).attr( "data-wheel" ) == 'true' ? true : false;
				var map_zoom = $( gmap ).attr( "data-zoom" ) && $( gmap ).attr( "data-zoom" ) != '' ? parseInt( $( gmap ).attr( "data-zoom" ) ) : 14;
				var map;

				var map_stat = 1;

				map_values.forEach( function( map_value ) {
					map_lat = map_value.map_latitude;
					map_lang = map_value.map_longitude;
					var LatLng = new google.maps.LatLng( map_lat, map_lang );
					var mapProp= {
						center: LatLng,
						scrollwheel: map_wheel,
						zoom: map_zoom,
						styles: map_style_mode
					};
					
					//Create Map
					if( map_stat ){
						var t_gmap = $( gmap );
						map = new google.maps.Map( t_gmap[0], mapProp );
						
						google.maps.event.addDomListener( window, 'resize', function() {
							var center = map.getCenter();
							google.maps.event.trigger( map, "resize" );
							map.setCenter( LatLng );
						});
						
						map_stat = 0;
					}
					
					//Map Marker
					var marker = new google.maps.Marker({
						position: LatLng,
						icon: map_value.map_marker,
						map: map
					});
					
					//Info Window
					if( map_value.map_info_opt == 'on' ) {
						var info_title = map_value.map_info_title;
						var info_addr = map_value.map_info_address;
						var contentString = '<div class="gmap-info-wrap"><h3>'+ info_title +'</h3><p>'+ info_addr +'</p></div>';
						var infowindow = new google.maps.InfoWindow({
						  content: contentString
						});
						marker.addListener( 'click', function() {
						  infowindow.open( map, marker );
						});
					}
				});
				
			}else{
			
				var LatLng = {lat: parseFloat(map_lat), lng: parseFloat(map_lang)};
				
				var map_wheel = $( gmap ).attr( "data-wheel" ) && $( gmap ).attr( "data-wheel" ) == 'true' ? true : false;
				var map_zoom = $( gmap ).attr( "data-zoom" ) && $( gmap ).attr( "data-zoom" ) != '' ? parseInt( $( gmap ).attr( "data-zoom" ) ) : 14;
				
				var mapProp= {
					center: LatLng,
					scrollwheel: map_wheel,
					zoom: map_zoom,
					styles: map_style_mode
				};
				var t_gmap = $( gmap );
				var map = new google.maps.Map( t_gmap[0], mapProp ); //document.getElementById("independentgmap")
				
				var marker = new google.maps.Marker({
				  position: LatLng,
				  icon: map_marker,
				  map: map
				});
				
				if( $( gmap ).attr( "data-info" ) == 1 ){
					var info_title = $( gmap ).attr( "data-info-title" ) ? $( gmap ).attr( "data-info-title" ) : '';
					var info_addr = $( gmap ).attr( "data-info-addr" ) ? $( gmap ).attr( "data-info-addr" ) : '';
					var contentString = '<div class="gmap-info-wrap"><h3>'+ info_title +'</h3><p>'+ info_addr +'</p></div>';
					var infowindow = new google.maps.InfoWindow({
					  content: contentString
					});
					marker.addListener( 'click', function() {
					  infowindow.open( map, marker );
					});
				}
				
				google.maps.event.addDomListener( window, 'resize', function() {
					var center = map.getCenter();
					google.maps.event.trigger(map, "resize");
					map.setCenter(LatLng);
				});
				
			}// data multi map false part end
			
		}); // end map each
		
	}
	
	function inedependentLazyLoad( $timeout, $lazy_parent ){
		if( independent_ajax_var.lazy_opt == '1' ){
			
			setTimeout(function(){
				$($lazy_parent).find("img:not(.lazy-active)").appear(function() {
					var cur_img = $( this );
					cur_img.addClass('lazy-process');
					cur_img.parents('.post-thumb-wrapper').css('height', cur_img.height());
					cur_img.attr('src', cur_img.attr( 'data-src' ));
					cur_img.imagesLoaded( function() {
						setTimeout(function() {
							cur_img.removeClass('lazy-process').addClass("lazy-active");
							cur_img.parents('.post-thumb-wrapper').css('height', 'auto');
						}, $timeout);
					});
				});
			}, 800);

			
			$($lazy_parent).on("scroll", function() { //".secondary-menu-area"
				$(this).find("img:not(.lazy-active)").appear(function() {
					var cur_img = $( this );

					cur_img.addClass('lazy-process');
					cur_img.parents('.post-thumb-wrapper').css('height', cur_img.height());
					cur_img.attr('src', cur_img.attr( 'data-src' ));
					cur_img.imagesLoaded( function() {
						setTimeout(function() {
							cur_img.removeClass('lazy-process').addClass("lazy-active");
							cur_img.parents('.post-thumb-wrapper').css('height', 'auto');
						}, $timeout);
					});
				});
			});
		}
	}

})( jQuery );
