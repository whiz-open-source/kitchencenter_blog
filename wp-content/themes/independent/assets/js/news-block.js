//News Blocks Script
(function( $ ) {
	"use strict";
	
	$( document ).ready(function() {
		
		var css_out = '';
		$( ".news-block-css-options" ).each(function() {
			var cur_block = this;
			var block_options = $(this).attr("data-options");		
			var data_opt = $.parseJSON( block_options );
			if( data_opt['block_style'] ){
				css_out += data_opt['block_style'];
				$(cur_block).remove();
			}
		});
		if( css_out != '' ){
			$('head').append( '<style id="independent-block-styles">'+ css_out +'</style>' );
		}
		
		/*Banner Code*/
		if( $('.independent-banner').length ){
			$( ".independent-banner" ).each(function() {
				
				var banner_height = 1;
				var current = this;
				
				banner_height = $(current).find('.banner-grid-parent').height();
				if( banner_height == 0 ){ banner_height = 1; }
				if( $(current).find('.banner-grid-100x100').length ) banner_height = 500;
				var div_val = 1100 / banner_height;
						
				if( $( window ).width() >= 768 ) {
					independent_banner_resize(div_val, current);
				}else{
					$(current).find('.banner-grid-parent').css({height : 'auto' });
				}
				
				$( window ).resize(function() {
					if( $( window ).width() >= 768 ) {
						independent_banner_resize(div_val, current);	
					}else{
						$(current).find('.banner-grid-parent').css({height : 'auto' });
					}
				});
			});
		}
	
		function independent_banner_resize(div_val, current){
			var banner_width = $(current).width();
			banner_width = parseInt( parseInt( banner_width ) / div_val );
			$(current).find('.banner-grid-parent').css({height : banner_width + 'px' });
		}
		
		/* Set Banner Image as Bg */
		if( $("span.banner-as-bg").length ){
			$( "span.banner-as-bg" ).each(function() {
				var cur_bann = $(this);
				if( cur_bann.attr("data-src") ){
					cur_bann.css('background-image','url('+ cur_bann.attr("data-src") +')')
				}
			});
		}
	
		$('.independent-block').find('.independent-slider').addClass('active');
		$('.independent-block').find('.independent-slider').find('.independent-news').addClass('active');
	
		//Slide Previous Click
		$('.independent-block').on( "click", ".independent-slider-previous:not(.disabled)", function() {
			//slide active
			var main_parent = $(this).parents('.independent-block');
			var target = $(this).parents('.independent-block').find('.independent-slider.active');
			
			var cur_len = $(target).find('.independent-news').length;
			var max_len = $(target).data('len');
			
			if( !$(target).hasClass('process') ){
				var active_slide = $(target).find('.independent-news.active');
				if( $(active_slide).is(":first-child") ){
					return false;
				}else{
					
					if( $(active_slide).is(":nth-child(2)") )
					$(main_parent).find('.independent-slider-previous').addClass('disabled');
					
					$(target).addClass('process');					
					$(active_slide).removeClass('active');
					$(active_slide).prev('.independent-news').addClass('active');
					
					$(main_parent).find('.independent-slider-next').removeClass('disabled');
					setTimeout(function(){
						$(target).removeClass('process');
					}, 300);
					
				}
			}
			return false;
		});
		
		//Slide Next Click
		$('.independent-block').on( "click", ".independent-slider-next:not(.disabled)", function() {
			
			var main_parent = $(this).parents('.independent-block');
			var block_id = $( main_parent ).attr("data-id");
			var slide_options = jQuery.extend(true, {}, independent_block_var.independent_block_params[block_id]);
			var target = $(this).parents('.independent-block').find('.independent-slider.active');
			var filter = '';
			var overlay = $(this).parents('.independent-block').find('.independent-content');
			
			if( $('.news-tabs a[href="#' + $(target).attr('id') + '"]').attr('data-id') ){
				filter = $('.news-tabs a[href="#' + $(target).attr('id') + '"]').attr('data-id');
			}
			if( !$(target).hasClass('process') ){
				$(target).addClass('process');
				var cur_len = $(target).find('.independent-news').length;
				var max_len = $(target).data('len');
				var active_slide = $(target).find('.independent-news.active');

				if( $(active_slide).index()+1 < cur_len ){
					
					$(active_slide).removeClass('active');
					$(active_slide).next('.independent-news').addClass('active');
					$(main_parent).find('.independent-slider-previous').removeClass('disabled');

					if( $(active_slide).next('.independent-news').is(":last-child") && $(target).find('.independent-news.active').index()+1 == max_len )
					$(main_parent).find('.independent-slider-next').addClass('disabled');

					setTimeout(function(){
						$(target).removeClass('process');
					}, 300);
				}else{
					if( cur_len < max_len  ){
						
						$(overlay).css({'height' : $(overlay).outerHeight() });
						$(overlay).find('.news-slide-loader').fadeIn(100);
						
						$(main_parent).find('.independent-slider-next, .independent-slider-previous').removeClass('disabled');
						
						if( cur_len+1 == max_len  )
						$(main_parent).find('.independent-slider-next').addClass('disabled');

						independentAjaxSlideFallback(slide_options, target, cur_len, active_slide, filter, overlay);
					}else{
						$(target).removeClass('process');
						$(main_parent).find('.independent-slider-next').addClass('disabled');
						$(main_parent).find('.independent-slider-previous').removeClass('disabled');
					}
				}
			}
			return false;
		});
		
		$('.independent-block').on( "click", ".independent-slider-previous.disabled, .independent-slider-next.disabled", function() {
			return false;
		});
		
		if( $('.type-infinite .independent-load-more').length ){
			var ajax_timer;
			$( window ).scroll(function() {
				if ( ajax_timer ) clearTimeout(ajax_timer);
				ajax_timer = setTimeout(function(){
					$('.type-infinite .independent-load-more').appear(function() {
						var block_options = $(this).parents('.independent-block').find('.news-block-options').attr("data-options");
						var data_opt = $.parseJSON( block_options );
						var load_after = data_opt["dynamic_options"]["loadmore_after"] != '' ? parseInt( data_opt["dynamic_options"]["loadmore_after"] ) : 0;
						var current_slide = $(this).parents('.independent-block').find('.independent-slider.active').find('.independent-news').length;
						if( load_after > current_slide || load_after == 0 ) $( this ).trigger( "click" );
					});
				}, 50);										 
				
			});		
		}
		
		//Load More Click
		$('.independent-block').on( "click", ".independent-load-more", function() {
	
			var load_more_parent = $(this).parent('li');
			$(this).hide();

			var main_parent = $(this).parents('.independent-block');
			var block_id = $( main_parent ).attr("data-id");
			var slide_options = jQuery.extend(true, {}, independent_block_var.independent_block_params[block_id]);

			var target = $(main_parent).find('.independent-slider.active');
			var filter = '';
			
			if( $('.news-tabs a[href="#' + $(target).attr('id') + '"]').attr('data-id') ){
				filter = $('.news-tabs a[href="#' + $(target).attr('id') + '"]').attr('data-id');
			}
			
			if( !$(target).hasClass('process') ){
				$(target).addClass('process');
				var cur_len = $(target).find('.independent-news').length;
				var max_len = $(target).data('len');
				
				if( cur_len < max_len  ){
					$(load_more_parent).find('img').fadeIn(300);
					independentLoadMoreFallback(slide_options, target, cur_len, max_len, load_more_parent, filter);
				}else{
					$(load_more_parent).find('.independent-load-more').hide();
				}
			}
			
			return false;
		});
		
		$( '.independent-slider.tab-pane.active' ).each(function() {
			var main_parent = $(this).parents('.independent-block');
			var max_len = $( this ).attr("data-len");
			if( $(main_parent).find('.independent-slider-previous').length ){
				if( max_len == 1 || max_len == 0 ){
					$(main_parent).find('.independent-slider-previous').addClass('disabled');
					$(main_parent).find('.independent-slider-next').addClass('disabled');
				}
			}
		});
		
		//Tab Click
		$('.independent-block').on('click', 'a[data-toggle="tab"]', function (e) {
	
			var main_parent = $(this).parents('.independent-block');
			
			$(main_parent).find("ul.news-tabs li").removeClass("active");
			$(this).parent("li").addClass("active");
			
			$(main_parent).find('.independent-slider-next').addClass('disabled');
			$(main_parent).find('.independent-slider-previous').addClass('disabled');
			
			var block_id = $( main_parent ).attr("data-id");
			var tab_options = jQuery.extend(true, {}, independent_block_var.independent_block_params[block_id]);
						
			if( $(this).parents('.independent-block').find('.independent-load-more').length ){
				var t_max_len = $( $(e.target).attr("href") ).attr("data-len");
				var t_len = $( $(e.target).attr("href") ).find('.independent-news.active').length;
	
				if( t_len != t_max_len ){
					$(this).parents('.independent-block').find('.independent-slider-nav').removeClass('hide');
					$(this).parents('.independent-block').find('.independent-load-more').show();
				}else{
					$(this).parents('.independent-block').find('.independent-slider-nav').addClass('hide');
				}
			}
	
			var parent = $(this).parents('.independent-block').find('.independent-content');
			var target = $(e.target).attr("href");
			var cur_id = $(this).data('id');
			if( $(target).length ){
				$(parent).find(".tab-pane.active").removeClass("active");
				$(parent).find(target).addClass("active");
			}else{
				if( !$(parent).hasClass('process') ){
					$(parent).addClass('process');
					$(parent).css({'height' : $(parent).outerHeight() });
					$(parent).find('.news-slide-loader').fadeIn(100);
					independentAjaxTabFallback(tab_options, target, cur_id, parent);
				}
			}
			
			var cur_len = $( $(e.target).attr("href") ).find('.independent-news.active').index()+1;
			var max_len = $( $(e.target).attr("href") ).attr("data-len");
			independentSetSlideNavStatus(main_parent, cur_len, max_len);
			
			return false;
		});
		
		$('.mega-dropdown-menu .independent-block').on('hover', 'a[data-toggle="tab"]', function (e) {
			var cur_t = this;
			$( cur_t ).trigger( "click" );
		});
		
		if( $('#mm-zozomenum').length ){
			$('#mm-zozomenum').find('.dropdown, .dropdown-menu').removeClass('dropdown mega-menu dropdown-menu mega-menu-container container');
			$('#mm-zozomenum').find('.mega-bgpattern, .mmenu-list-hide').remove();
		}
		
		/*More Tab Select Box Start*/
		$( ".more-tab-dropdown li a" ).on( "click", function() {
			$(this).parents('.dropdown').find('.tab-more-text').text( $(this).text() );
		});
		$( document ).on( "click", ".tab-more-text", function() {
			return false;
		});
		/*More Tab Select Box End*/
		
		/* newsBlocksReset() and newsBlocksResetBack() */
		if($( window ).width() <= 767 ){
			setTimeout(function() {
				newsBlocksReset();
			}, 100);
		}
		$( window ).resize(function() {
			if($( window ).width() >= 768 ){
				setTimeout(function() {
					newsBlocksResetBack();
				}, 100);			
			}else{
				setTimeout(function() {
					newsBlocksReset();
				}, 100);
			}
		});
		
		/*Megamenu Tab Start*/
		var timer;
		var delay = 200;
		var current;

		$('.mega-menu-container .news-tabs > li > a').on( "hover", function() {
			current = $(this);
			timer = setTimeout(function() {
				$(current).trigger('click');
			}, delay);
		}, function() {
			clearTimeout(timer);
		});
		
		/* News Ticker */
		if( $( ".easy-news-ticker" ).length ){
			$( ".easy-news-ticker" ).each(function() {
				independentTicker("#" + $(this).attr('id'));	
			});
		}
		
		/*Video Player*/
		if( $( ".video-play" ).length ){
			$( ".video-play" ).each(function() {
				var vdo_url = $(this).data('video');
				var frame_id = $(this).data('frame');
				var duration = 0;
				var videoid = 0;
				var par = this;
				if( vdo_url.search("vimeo.com") != -1 ){
					var str = vdo_url; 
					var res = str.replace("https://player.vimeo.com/video/", "");
					($).get( "http://vimeo.com/api/v2/video/"+res+".xml", function( data ) {
						var xmlDoc = $(data),
						duration = xmlDoc.find("duration");
						duration = secondsTimeSpanToHMS(duration.text());
						$(par).parents('.video-playlist').find('.video-duration').text(duration);
					});   
				}else if( vdo_url.search("youtube.com") ){
					var str = vdo_url; 
					var res = str.replace("https://www.youtube.com/embed/", "");
					var url1 = "https://www.googleapis.com/youtube/v3/videos?id="+res+"&key=AIzaSyCVc9XkvvfwWU3BLTAyYzq3rZ32K9Av6w4&part=snippet,contentDetails";
					($).ajax({
						async: false,
						type: 'GET',
						url: url1,
						success: function(data) {
							if (data.items.length > 0) {
								var vdo = data.items[0];
								duration = convert_time( vdo.contentDetails.duration );
								
								$(par).parents('.video-playlist').find('.video-duration').text(duration);
							}
						}
					}); 			
				}
			});
		}

		$( ".video-play" ).on( "click", function(e, trig) {
			var vdo_url = $(this).data('video');
			var auto_play = $(this).data('play');
			
			$( this ).parents('ul.video-playlist-slide').children('li').removeClass('active black-bg typo-white');
			$( this ).parents('li').addClass('active black-bg typo-white');
			
			$( this ).parents(".video-playlist-slide").find(".icon-control-pause").removeClass("icon-control-pause");
			$( this ).children(".post-format-icon.icon-control-play").addClass("icon-control-pause");
			
			var cus_auto_play = "";
			if ( trig != '1' && auto_play == 'yes' ){
				vdo_url = vdo_url + '?autoplay=1';
				cus_auto_play = "autoplay";
			}
			var frame_txt = '';
			if( vdo_url.search("vimeo.com") != -1 || vdo_url.search("youtube.com") != -1 ){
				frame_txt = '<iframe class="vdo-frame" src="'+ vdo_url +'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
			}else{
				frame_txt = '<video controls '+ cus_auto_play +'> <source src="'+ vdo_url +'" type="video/mp4"></video>';
			}
			$(this).parents('.independent-block').find('.video-player').html(frame_txt);
			if( cus_auto_play ){
				$(this).parents(".independent-block").find(".video-player video").mediaelementplayer();
			}
			e.preventDefault();
		});
		
		/*Video Duration*/
		if( $( ".video-playlist-slide" ).length ){
			$( ".video-playlist-slider").not( ".no-play-trigger" ).find(".video-playlist-slide li:first-child .video-play" ).trigger( "click", ["1"] );		
		}
	
		/*Video Playlist*/
		if( $( ".video-playlist-slider" ).length ){
			var play_visible = $(".video-playlist-slider").data("visible") ? $(".video-playlist-slider").data("visible") : 4;
			 $(".independent-block").not(".player-type-3").find('.video-playlist-slider').easyTicker({
				direction: 'up',
				//easing: 'easeInOutBack',
				speed: 'slow',
				interval: 5000,
				height: 'auto',
				visible: play_visible,
				mousePause: 0,
				controls: {
					up: '.video-playlist-next',
					down: '.video-playlist-prev',
				}
			}).data('easyTicker').stop();
		}
		
	});

	function secondsTimeSpanToHMS(s) {
		var h = Math.floor(s/3600); //Get whole hours
		s -= h*3600;
		var m = Math.floor(s/60); //Get remaining minutes
		s -= m*60;
		return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
	}
	
	function convert_time(duration) {
		var a = duration.match(/\d+/g);
	
		if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
			a = [0, a[0], 0];
		}
	
		if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
			a = [a[0], 0, a[1]];
		}
		if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
			a = [a[0], 0, 0];
		}
	
		duration = 0;
	
		if (a.length == 3) {
			duration = duration + parseInt(a[0]) * 3600;
			duration = duration + parseInt(a[1]) * 60;
			duration = duration + parseInt(a[2]);
		}
	
		if (a.length == 2) {
			duration = duration + parseInt(a[0]) * 60;
			duration = duration + parseInt(a[1]);
		}
	
		if (a.length == 1) {
			duration = duration + parseInt(a[0]);
		}
		var h = Math.floor(duration / 3600);
		var m = Math.floor(duration % 3600 / 60);
		var s = Math.floor(duration % 3600 % 60);
		return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
	}
	
	function independentTicker(NewsTickerName){
	
		var delay = $(NewsTickerName).data('delay');
		var parent = $(NewsTickerName).parents('.independent-ticker');
		
		$(NewsTickerName).easyTicker({
			direction: 'up',
			/*easing: 'easeInOutBack',*/
			speed: 'slow',
			interval: delay,
			height: 'auto',
			visible: 1,
			mousePause: 0,
			controls: {
				up: $( parent ).find('.easy-ticker-next'),
				down: $( parent ).find('.easy-ticker-prev'),
			}
		}).data('easyTicker');
		$(parent).removeClass('hide');
	}
	
	function independentSetSlideNavStatus(main_parent, cur_len, max_len){
		if( max_len == 1 || max_len == 0 ){
			$(main_parent).find('.independent-slider-previous').addClass('disabled');
			$(main_parent).find('.independent-slider-next').addClass('disabled');
		}else if(cur_len == max_len){
			$(main_parent).find('.independent-slider-previous').removeClass('disabled');
			$(main_parent).find('.independent-slider-next').addClass('disabled');	
		}else if(cur_len == 1){
			$(main_parent).find('.independent-slider-previous').addClass('disabled');
			$(main_parent).find('.independent-slider-next').removeClass('disabled');	
		}else if( ( max_len - cur_len ) >= 1 ){
			$(main_parent).find('.independent-slider-previous').removeClass('disabled');
			$(main_parent).find('.independent-slider-next').removeClass('disabled');
		}
	}
	
	//independent Functions
	function independentLoadMoreFallback(opt, target, cur_len, max_len, load_more_parent, filter){
		var data_opt = opt;
		data_opt["paged"] = cur_len;
		data_opt["temp_key"] = 'slide';
		
		if( filter != '' ){
			data_opt["filter_values"] = filter;	
		}
		
		$.ajax({
			type: "post",
			url: independent_ajax_var.admin_ajax_url,
			dataType: 'JSON',
			data: data_opt,
			error: function(data){
				console.log(independent_ajax_var.news_problem);
			},
			success: function(data){			
				var content = $( data["json"] ).hide().fadeIn(300);
				$(target).append( content );
				$(target).removeClass('process');
				if( cur_len + 1 == max_len ){
					$(load_more_parent).find('img').hide();
	
					$(load_more_parent).parents('.independent-slider-nav').addClass('hide');
	
				}else{
					$(load_more_parent).find('.independent-load-more').show();
					$(load_more_parent).find('img').hide();
				}
				//slide active code
			}
		});
		
	}
	
	function independentAjaxTabFallback(tab_opt, target, cur_id, parent){
		var data_tab_opt = '';
		data_tab_opt = tab_opt;
		data_tab_opt["filter_values"] = cur_id;
		data_tab_opt["temp_key"] = 'tab';
		data_tab_opt["slide_id"] = cur_id;

		$.ajax({
			type: "post",
			url: independent_ajax_var.admin_ajax_url,
			dataType: 'JSON',
			data: data_tab_opt,
			error: function(data){
				console.log(independent_ajax_var.news_problem);
			},
			success: function(data){			

				//Sample code
				var news_out = data["json"];
				$( news_out ).imagesLoaded( function() {
					$(parent).find(".tab-pane.active").removeClass("active");
					$(parent).append( news_out );
					
					setTimeout(function(){ 
						
						//Active new news
						$(parent).find(target).addClass("active"); 
						
						//Remove loader
						$(parent).css({'height' : 'auto' });
						$(parent).find('.news-slide-loader').fadeOut(300);
						$(parent).removeClass('process');
						
						//Check new block prev next button status
						if( $(target).parents('.independent-block').find('.independent-load-more').length ){
							var max_len = $(target).data('len');
							if( max_len == '1' ){
								$(target).parents('.independent-block').find('.independent-slider-nav').addClass('hide');
							}else{
								$(target).parents('.independent-block').find('.independent-slider-nav').removeClass('hide');
								$(target).parents('.independent-block').find('.independent-load-more').show();
							}
						}else if( $(target).parents('.independent-block').find('.independent-slider-previous').length ){
							
							var max_len = $(target).data('len');
							if( max_len == '1' ){
								$(target).parents('.independent-block').find('.independent-slider-previous').addClass('disabled');
								$(target).parents('.independent-block').find('.independent-slider-next').addClass('disabled');
							}else{
								$(target).parents('.independent-block').find('.independent-slider-previous').addClass('disabled');
								$(target).parents('.independent-block').find('.independent-slider-next').removeClass('disabled');	
							}
						}
						
					}, 50);
					
					//Lazyload
					inedependentLazyLoad( 200 );
					
				});
				
			}
		});
	}
	
	//independent Functions
	function independentAjaxSlideFallback(slide_opt, target, cur_len, active_slide, filter, overlay){
		var data_opt = '';
		data_opt = slide_opt;
		data_opt["paged"] = cur_len;
		data_opt["temp_key"] = 'slide';
		
		if( filter != '' ){
			data_opt["filter_values"] = filter;	
		}
		
		$.ajax({
			type: "post",
			url: independent_ajax_var.admin_ajax_url,
			dataType: 'JSON',
			data: data_opt,
			error: function(data){
				console.log(independent_ajax_var.news_problem);
			},
			success: function(data){			

				var news_out = data["json"];
				$( news_out ).imagesLoaded( function() {
					$(active_slide).removeClass('active');
					$(target).append( news_out );
					$(active_slide).next('.independent-news').removeClass('active');
					
					setTimeout(function(){ 

						//Active new news
						$(active_slide).next('.independent-news').addClass('active');
						
						//Remove loader
						$(overlay).css({'height' : 'auto' });
						$(overlay).find('.news-slide-loader').fadeOut(300);
						$(target).removeClass('process');
						
					}, 50);
					
					//Lazyload
					inedependentLazyLoad( 200 );
					
				});
			}
		});
	}
	
	function newsBlocksReset(){
		$('.independent-block' ).each(function() {
			if(  !$(this).parents('.mega-dropdown-menu').length ){
				if( $(this).find('.news-tabs').length ){
					if( $(this).find('.news-tabs .tab-more-text').length ){
						$(this).find('.news-tabs > .reponsive-tab-list').clone().prependTo( $(this).find('.more-tab-dropdown') );
						$(this).find('.news-tabs > .reponsive-tab-list').remove();
					}else if( $(this).find('.news-tabs .custom-more-dropdown').length ){
						$(this).find('.news-tabs > .reponsive-tab-list').clone().prependTo( $(this).find('.more-tab-dropdown') );
						$(this).find('.news-tabs > .reponsive-tab-list').remove();
					}else{
						var block_id = $( this ).attr("data-id");
						var tab_options = jQuery.extend(true, {}, independent_block_var.independent_block_params[block_id]);
						var more_text = tab_options["dynamic_options"]["more_text"] ? tab_options["dynamic_options"]["more_text"] : 'More';
						
						var dropdown_cont = '<li class="dropdown pull-right custom-more-dropdown" role="presentation"><a href="#" class="tab-more-text">'+ more_text +'</a><ul class="more-tab-dropdown dropdown-menu"></ul></li>';
						$(this).find('.news-tabs').append(dropdown_cont);
						$(this).find('.news-tabs > .reponsive-tab-list').clone().prependTo( $(this).find('.more-tab-dropdown') );
						$(this).find('.news-tabs > .reponsive-tab-list').remove();
					}
				}
			}
		});
	}
	
	function newsBlocksResetBack(){
		$('.independent-block' ).each(function() {
			if(  !$(this).parents('.mega-dropdown-menu').length ){
				if( $(this).find('.news-tabs').length ){
					if( $(this).find('.news-tabs .custom-more-dropdown').length ){
						$(this).find('.more-tab-dropdown  > .reponsive-tab-list').clone().insertBefore( $(this).find('.dropdown.pull-right') );
						$(this).find('.more-tab-dropdown  > .reponsive-tab-list').remove();
						$(this).find('.news-tabs li.custom-more-dropdown').remove();
					}else if( $(this).find('.news-tabs .tab-more-text').length ){
						$(this).find('.more-tab-dropdown  > .reponsive-tab-list').clone().insertBefore( $(this).find('.dropdown.pull-right') );
						$(this).find('.more-tab-dropdown  > .reponsive-tab-list').remove();
					}
				}
			}
		});
	}
	
	function inedependentLazyLoad( $timeout ){
		if( independent_ajax_var.lazy_opt == '1' ){
			$("img:not(.lazy-active)").appear(function() {
				var cur_img = $( this );

				cur_img.removeClass('lazy-initiate').addClass('lazy-process');
				cur_img.parents('.post-thumb-wrapper').css('height', cur_img.height());
				cur_img.attr('src', cur_img.attr( 'data-src' ));
				cur_img.imagesLoaded( function() {
					setTimeout(function() {
						cur_img.removeClass('lazy-process').addClass("lazy-active");
						cur_img.parents('.post-thumb-wrapper').css('height', 'auto');
					}, $timeout);
				});
				
			});
		}
	}
	
	window.addEventListener('load', function(){
		inedependentLazyLoad( 0 );
	}, false)

})( jQuery );