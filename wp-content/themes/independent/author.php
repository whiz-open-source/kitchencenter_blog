<?php
/**
 * The template for displaying author pages
 */
get_header();
$ahe = new independentHeaderElements;
$template = 'blog'; // template id
$aps = new independentPostSettings;
if( $aps->independentCheckTemplateExists( 'author' ) ){
	$template = 'author';
}elseif( $aps->independentCheckTemplateExists( 'archive' ) ){
	$template = 'archive';
}
$aps->independentSetPostTemplate( $template );
$template_class = $aps->independentTemplateContentClass();
?>
<div class="independent-content <?php echo esc_attr( 'independent-' . $template ); ?>">
	<?php $ahe->independentPageTitle( $template ); ?>

	<?php
		if( $aps->independentThemeOpt( $template.'-featured-slider' ) ){
			$ahe->independentFeaturedSlider( $template );
		}
	?>

	<div class="independent-content-inner">
		<div class="container">

			<div class="row">

				<div class="<?php echo esc_attr( $template_class['content_class'] ); ?>">
					<div id="primary" class="content-area">
						<main id="main" class="site-main <?php echo esc_attr( $template ); ?>-template>

							<?php
							if ( have_posts() ) :
								$template_shortcode = $aps->independentThemeOpt( $template . '-template-shortcode' );
								if( $template_shortcode ){
									$template_shortcode_model = $aps->independentThemeOpt( $template . '-template-shortcode-model' );
									$template_shortcode_model = $template_shortcode_model ? $template_shortcode_model : '1';
									parse_str($template_shortcode, $shortcode_atts);
									$model_fun = 'independent_news_block_form_'. $template_shortcode_model;
									echo ( ''. $model_fun($shortcode_atts, true) );
								}else{
									$template_shortcode = independent_default_block_values();
									parse_str($template_shortcode, $shortcode_atts);
									echo independent_news_block_form_default($shortcode_atts, true);
								}
							else :
								get_template_part( 'template-parts/post/content', 'none' );
							endif;
							?>

						</main><!-- #main -->
					</div><!-- #primary -->
				</div><!-- main col -->

				<?php if( $template_class['lsidebar_class'] != '' ) : ?>
				<div class="<?php echo esc_attr( $template_class['lsidebar_class'] ); ?>">
					<aside class="widget-area left-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
						<?php dynamic_sidebar( $template_class['left_sidebar'] ); ?>
					</aside>
				</div><!-- sidebar col -->
				<?php endif; ?>

				<?php if( $template_class['rsidebar_class'] != '' ) : ?>
				<div class="<?php echo esc_attr( $template_class['rsidebar_class'] ); ?>">
					<aside class="widget-area right-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
						<?php dynamic_sidebar( $template_class['right_sidebar'] ); ?>
					</aside>
				</div><!-- sidebar col -->
				<?php endif; ?>

			</div><!-- .row -->

		</div><!-- .container -->
	</div><!-- .independent-content-inner -->
</div><!-- .independent-content -->
<?php get_footer();
