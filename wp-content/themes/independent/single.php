<?php
/**
 * The template for displaying all single posts
 *
 */
get_header(); 
$ahe = new independentHeaderElements;
$aps = new independentPostSettings;
$template = 'single-post'; // template id
$aps->independentSetPostTemplate( $template );
$template_class = $aps->independentTemplateContentClass();
$full_width_class = '';
// Post View Count
$aps->independentSetPostViewCount( get_the_ID() );
?>
<div class="independent-content <?php echo esc_attr( 'independent-' . $template ); ?>">
	<?php $ahe->independentHeaderSlider('bottom'); ?>
	<?php $ahe->independentPageTitle( $template ); ?>
	
	<?php 
		if( $aps->independentCheckMetaValue( 'independent_post_featured_slider', $template.'-featured-slider' ) ){
			$ahe->independentFeaturedSlider( $template );
		}
		
	?>
	
	<?php
		if( $aps->independentCheckMetaValue( 'independent_post_full_wrap', 'single-post-full-wrap' ) ) : 
	?>
			<div class="post-full-thumb-wrap single-post-template">
				<?php
					if ( has_post_format( 'gallery' ) ) :
						$aps->independentGalleryFormat();
				?>
				<?php elseif ( has_post_format( 'video' ) ) :
					$video_type = get_post_meta( get_the_ID(), 'independent_post_video_type', true );
					$video_id = get_post_meta( get_the_ID(), 'independent_post_video_id', true );
					$video_modal = $aps->independentCheckMetaValue( 'independent_post_video_modal', $template.'-video-format' );
					$video_atts = array(
						'video_type'	=> $video_type,
						'video_id'		=> $video_id,
						'video_modal'	=> $video_modal
					);
				?>
		
					<div class="post-video-wrap">
						<?php $aps->independentVideoFormat( $video_atts ); ?>
					</div>
				<?php elseif ( has_post_format( 'quote' ) ) : ?>
					<div class="post-quote-full-wrap text-center">
						<?php $aps->independentQuoteFormat(); ?>
					</div>
				<?php elseif ( has_post_format( 'link' ) ) : ?>
					<div class="post-link-full-wrap text-center">
						<?php $aps->independentLinkFormat(); ?>
					</div>
				<?php elseif ( has_post_format( 'audio' ) ) : ?>
					<div class="post-audio-full-wrap">
						<?php $aps->independentAudioFormat(); ?>
					</div>
				<?php elseif( '' !== get_the_post_thumbnail() ) : ?>
					<div class="set-bg-img" data-src="<?php echo esc_url( get_the_post_thumbnail_url() ); ?>"></div>
				<?php endif; ?>
			<?php  if( $aps->independentCheckMetaValue( 'independent_post_overlay_opt', 'single-post-overlay-opt' ) == 1 ) : ?>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="post-overlay-items<?php echo ( has_post_format( 'image' ) ? ' thumb-exists' : '' ); ?>"> 
								<?php
								
									$post_elements = array();
									$post_oitems_opt = get_post_meta( get_the_ID(), 'independent_post_overlay_opt', true );
									if( $post_oitems_opt == '' || $post_oitems_opt == 'theme-default' ){
										$post_elements = $aps->independentThemeOpt( 'single-post-overlay-items' );		
										$post_elements = $post_elements['Enabled'];		
										if( array_key_exists( "placebo", $post_elements ) ) unset( $post_elements['placebo'] );						
									}else{
										$overlay_post_items = get_post_meta( get_the_ID(), 'independent_post_overlay_items', true );
										$t_post_items = explode( ',', $overlay_post_items );
										foreach ( $t_post_items as $element ) 
											$post_elements[$element] = $element;
									}
									$aps->independentPostOverlayItems( $post_elements );
								?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; // single-post-overlay-opt 0 or 1 ?>
			
		</div><!-- .post-full-thumb-wrap -->
	<?php endif; // single-post-full-wrap 0 or 1 ?>
	
	<div class="independent-content-inner">
		<div class="container">
			<div class="row">
		
				<div class="<?php echo esc_attr( $template_class['content_class'] ); ?>">
				
					<div id="primary" class="content-area">
					
						<?php echo independent_ads_out( $ahe->independentThemeOpt( 'article-top-ads-list' ) ); ?>
					
						<?php
						
							$post_elements = array();
							$post_oitems_opt = get_post_meta( get_the_ID(), 'independent_post_page_items_opt', true );
							if( $post_oitems_opt == '' || $post_oitems_opt == 'theme-default' ){
								$post_elements = $aps->independentThemeOpt( 'single-post-page-items' );		
								$post_elements = $post_elements['Enabled'];		
								if( array_key_exists( "placebo", $post_elements ) ) unset( $post_elements['placebo'] );						
							}else{
								$overlay_post_items = get_post_meta( get_the_ID(), 'independent_post_page_items', true );
								$t_post_items = explode( ',', $overlay_post_items );
								foreach ( $t_post_items as $element ) 
									$post_elements[$element] = $element;
							}
						
							// Dynamic Single Post Page Elements
							if( $post_elements ):
							
								/* Single Post Loop */
								while ( have_posts() ) : the_post();
							
									foreach ( $post_elements as $element => $value ) {
										
										switch( $element ) {
										
											case 'post-items': ?>
											
												<main id="main" class="site-main <?php echo esc_attr( $template ); ?>-template">
													<?php get_template_part( 'template-parts/post/content', 'single' ); ?>
												</main><!-- #main -->
												
											<?php
											break; // post-items
													
											case 'author-info':
	
												// Post Author Wrap
												$author_info = nl2br( get_the_author_meta('description') );
												if( $author_info ):
												?>
													<div class="author-info-wrapper clearfix">
														<?php get_template_part('template-parts/author/biography');	?>
													</div>
												<?php 
												endif;
	
											break; // author-info
											
											case 'review':
	
												// Review Wrap
												$star_rate = get_post_meta( get_the_ID(), 'independent_post_review_rating', true );
												$review_text = get_post_meta( get_the_ID(), 'independent_post_review_text', true );
												if( $star_rate || $review_text ){ ?>
													<div class="review-wrapper clearfix">
														<?php if( $star_rate ){ ?>
															<div class="news-rating-wrap">
																<h5><?php echo apply_filters( 'review_heading_text', esc_html( 'Reviews', 'independent' ) ); ?></h5>
																<div class="news-rating-inner">
																	<h3 class="rating-value"><?php echo esc_attr( $star_rate ); ?></h3>
																	<p class="rating-stars"><?php echo independent_star_rating( $star_rate ); ?></p>
																</div>
															</div><!-- .testimonial-rating -->
														<?php } 
														if( $review_text ){ ?>
															<div class="news-summary">
																<h5><?php echo apply_filters( 'review_summary_text', esc_html( 'Summary', 'independent' ) ); ?></h5>
																<div class="news-summary-text"><?php echo do_shortcode( $review_text ); ?></div>
															</div><!-- .testimonial-rating -->
														<?php } ?>
													</div>
												<?php
												}
	
											break; // review
													
											case 'post-nav': ?>
													
												<!-- Post Navigation -->
												<?php
													$prev_post = get_previous_post();
													$next_post = get_next_post();
												
												if( !empty( $prev_post ) || !empty( $next_post ) ):
													?>
													<div class="post-navigation-wrapper clearfix">
														<nav class="navigation post-navigation">
															<div class="nav-links">
																
																<?php
																if (!empty( $prev_post )): ?>
																<div class="nav-previous">
																	
																	<?php
																		$prev_title = $prev_post->post_title ? $prev_post->post_title : esc_html__( "Previous Post" , "independent" );
																	?>
																	<h6><a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>"><span class="nav-icon text-left">Previous</span><?php echo esc_html( $prev_title ); ?></a><h6>
																</div>
															<?php endif; ?>
																	
																<?php
																	if (!empty( $next_post )): ?>
																<div class="nav-next">
																	
																	<?php
																		$next_title = $next_post->post_title ? $next_post->post_title : esc_html__( "Next Post" , "independent" );
																	?>
																	<h6><a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>"><span class="nav-icon text-right">Next</span><?php echo esc_html( $next_title ); ?></a><h6>
																</div>
																<?php endif; ?>
																
															</div>
														</nav>
													</div>
													<?php 
												endif; // next or prev post exists if
	
											break; // post-nav
											
											case 'related-articles':
												// Related Post Slider
												get_template_part('template-parts/article/related');	
											break;
											
											case 'author-articles':
												// Related Post Slider
												get_template_part('template-parts/article/author');	
											break;
													
											case 'comment':?>
												
												<!-- Comments -->
												<div class="post-comments-wrapper clearfix">
												<?php 
													$comment_type = $aps->independentThemeOpt( 'comments-type' );
													if( $comment_type == 'wp' ){
														if ( comments_open() || get_comments_number() ) :
															echo '<div class="wp-comments-wrapper">';
																comments_template();
															echo '</div>';
														endif;								
													}else{
														echo '<div class="fb-comments-wrapper">';
															independent_fb_comments_code();
														echo '</div>';
													}  
												?>					
												</div>
												
											<?php
											break; // comment
											case 'article-inline-ads-list':
	
												// Article Inline Ads
												echo independent_ads_out( $ahe->independentThemeOpt( 'article-inline-ads-list' ) );
	
											break; // article-inline-ads-list
	
										} // switch
									}// foreach
								endwhile;
							endif; // Single Page Elements If
						?>
						
						<?php echo independent_ads_out( $ahe->independentThemeOpt( 'article-bottom-ads-list' ) ); ?>
						
					</div><!-- #primary -->
				</div><!-- main col -->
				
				<?php if( $template_class['lsidebar_class'] != '' ) : ?>
				<div class="<?php echo esc_attr( $template_class['lsidebar_class'] ); ?>">
					<aside class="widget-area left-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
						<?php dynamic_sidebar( $template_class['left_sidebar'] ); ?>
					</aside>
				</div><!-- sidebar col -->
				<?php endif; ?>
				
				<?php if( $template_class['rsidebar_class'] != '' ) : ?>
				<div class="<?php echo esc_attr( $template_class['rsidebar_class'] ); ?>">
					<aside class="widget-area right-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
						<?php dynamic_sidebar( $template_class['right_sidebar'] ); ?>
					</aside>
				</div><!-- sidebar col -->
				<?php endif; ?>
				
			</div><!-- row -->
			
		</div><!-- .container -->
	</div><!-- .independent-content-inner -->
</div><!-- .independent-content -->
<?php get_footer();