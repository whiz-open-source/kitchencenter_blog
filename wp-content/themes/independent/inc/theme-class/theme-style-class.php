<?php
class independentThemeStyles {
   
   	private $independent_options;
	private $exists_fonts = array();
   
    function __construct() {
		$this->independent_options = get_option( 'independent_options' );
    }
	
	function independentThemeColor(){
		$independent_options = $this->independent_options;
		return isset( $independent_options['theme-color'] ) && $independent_options['theme-color'] != '' ? $independent_options['theme-color'] : '#54a5f8';
	}
	
	function independent_theme_opt($field){
		$independent_options = $this->independent_options;
		return isset( $independent_options[$field] ) && $independent_options[$field] != '' ? $independent_options[$field] : '';
	}
	
	function independent_check_meta_value( $meta_key, $default_key ){
		$meta_opt = get_post_meta( get_the_ID(), $meta_key, true );
		$final_opt = isset( $meta_opt ) && ( empty( $meta_opt ) || $meta_opt == 'theme-default' ) ? $this->independent_theme_opt( $default_key ) : $meta_opt;
		return $final_opt;
	}
	
	function independent_container_width(){
		$independent_options = $this->independent_options;
		return isset( $independent_options['site-width'] ) && $independent_options['site-width']['width'] != '' ? absint( $independent_options['site-width']['width'] ) . $independent_options['site-width']['units'] : '1140px';
	}
	
	function independent_dimension_width($field){
		$independent_options = $this->independent_options;
		return isset( $independent_options[$field] ) && $independent_options[$field]['width'] != '' ? absint( $independent_options[$field]['width'] ) . $independent_options[$field]['units'] : '';
	}
	
	function independent_dimension_height($field){
		$independent_options = $this->independent_options;
		return isset( $independent_options[$field] ) && $independent_options[$field]['height'] != '' ? absint( $independent_options[$field]['height'] ) . $independent_options[$field]['units'] : '';
	}
	
	function independent_border_settings($field){
		$independent_options = $this->independent_options;
		if( isset( $independent_options[$field] ) ):
		
			$boder_style = isset( $independent_options[$field]['border-style'] ) && $independent_options[$field]['border-style'] != '' ? $independent_options[$field]['border-style'] : '';
			$border_color = isset( $independent_options[$field]['border-color'] ) && $independent_options[$field]['border-color'] != '' ? $independent_options[$field]['border-color'] : '';
			
			if( isset( $independent_options[$field]['border-top'] ) && $independent_options[$field]['border-top'] != '' ):
				echo '
				border-top-width: '. $independent_options[$field]['border-top'] .';
				border-top-style: '. $boder_style .';
				border-top-color: '. $border_color .';';
			endif;
			
			if( isset( $independent_options[$field]['border-right'] ) && $independent_options[$field]['border-right'] != '' ):
				echo '
				border-right-width: '. $independent_options[$field]['border-right'] .';
				border-right-style: '. $boder_style .';
				border-right-color: '. $border_color .';';
			endif;
			
			if( isset( $independent_options[$field]['border-bottom'] ) && $independent_options[$field]['border-bottom'] != '' ):
				echo '
				border-bottom-width: '. $independent_options[$field]['border-bottom'] .';
				border-bottom-style: '. $boder_style .';
				border-bottom-color: '. $border_color .';';
			endif;
			
			if( isset( $independent_options[$field]['border-left'] ) && $independent_options[$field]['border-left'] != '' ):
				echo '
				border-left-width: '. $independent_options[$field]['border-left'] .';
				border-left-style: '. $boder_style .';
				border-left-color: '. $border_color .';';
			endif;
			
		endif;
	}
	
	function independent_padding_settings($field){
		$independent_options = $this->independent_options;
	if( isset( $independent_options[$field] ) ):
	
		echo isset( $independent_options[$field]['padding-top'] ) && $independent_options[$field]['padding-top'] != '' ? 'padding-top: '. $independent_options[$field]['padding-top'] .';' : '';
		echo isset( $independent_options[$field]['padding-right'] ) && $independent_options[$field]['padding-right'] != '' ? 'padding-right: '. $independent_options[$field]['padding-right'] .';' : '';
		echo isset( $independent_options[$field]['padding-bottom'] ) && $independent_options[$field]['padding-bottom'] != '' ? 'padding-bottom: '. $independent_options[$field]['padding-bottom'] .';' : '';
		echo isset( $independent_options[$field]['padding-left'] ) && $independent_options[$field]['padding-left'] != '' ? 'padding-left: '. $independent_options[$field]['padding-left'] .';' : '';
	endif;
	}
	
	function independent_margin_settings( $field ){
		$independent_options = $this->independent_options;
	if( isset( $independent_options[$field] ) ):
	
		echo isset( $independent_options[$field]['margin-top'] ) && $independent_options[$field]['margin-top'] != '' ? 'margin-top: '. $independent_options[$field]['margin-top'] .';' : '';
		echo isset( $independent_options[$field]['margin-right'] ) && $independent_options[$field]['margin-right'] != '' ? 'margin-right: '. $independent_options[$field]['margin-right'] .';' : '';
		echo isset( $independent_options[$field]['margin-bottom'] ) && $independent_options[$field]['margin-bottom'] != '' ? 'margin-bottom: '. $independent_options[$field]['margin-bottom'] .';' : '';
		echo isset( $independent_options[$field]['margin-left'] ) && $independent_options[$field]['margin-left'] != '' ? 'margin-left: '. $independent_options[$field]['margin-left'] .';' : '';
	endif;
	}
	
	function independent_link_color($field, $fun){
		$independent_options = $this->independent_options;
	echo isset( $independent_options[$field][$fun] ) && $independent_options[$field][$fun] != '' ? '
	color: '. $independent_options[$field][$fun] .';' : '';
	}
	
	function independent_get_link_color($field, $fun){
		$independent_options = $this->independent_options;
		return isset( $independent_options[$field][$fun] ) && $independent_options[$field][$fun] != '' ? $independent_options[$field][$fun] : '';
	}
	
	function independent_bg_rgba($field){
		$independent_options = $this->independent_options;
	echo isset( $independent_options[$field]['rgba'] ) && $independent_options[$field]['rgba'] != '' ? 'background: '. $independent_options[$field]['rgba'] .';' : '';
	}
	
	function independent_bg_settings($field){
		$independent_options = $this->independent_options;
		if( isset( $independent_options[$field] ) ):
	echo '
	'. ( isset( $independent_options[$field]['background-color'] ) && $independent_options[$field]['background-color'] != '' ?  'background-color: '. $independent_options[$field]['background-color'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['background-image'] ) && $independent_options[$field]['background-image'] != '' ?  'background-image: url('. $independent_options[$field]['background-image'] .');' : '' ) .'
	'. ( isset( $independent_options[$field]['background-repeat'] ) && $independent_options[$field]['background-repeat'] != '' ?  'background-repeat: '. $independent_options[$field]['background-repeat'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['background-position'] ) && $independent_options[$field]['background-position'] != '' ?  'background-position: '. $independent_options[$field]['background-position'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['background-size'] ) && $independent_options[$field]['background-size'] != '' ?  'background-size: '. $independent_options[$field]['background-size'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['background-attachment'] ) && $independent_options[$field]['background-attachment'] != '' ?  'background-attachment: '. $independent_options[$field]['background-attachment'] .';' : '' ) .'
	';
		endif;
	}
	
	function independent_custom_font_face_create( $font_family, $cf_names ){
	
		$upload_dir = wp_upload_dir();
		$f_type = array('eot', 'otf', 'svg', 'ttf', 'woff');
		if ( in_array( $font_family, $cf_names ) ){
			$t_font_folder = $font_family;
			$t_font_name = sanitize_title( $font_family );
			$font_path = $upload_dir['baseurl'] . '/custom-fonts/' . str_replace( "'", "", $t_font_folder .'/'. $t_font_name );
			echo '@font-face { font-family: '. $t_font_folder .';';
			echo "src: url('". esc_url( $font_path ) .".eot'); /* IE9 Compat Modes */ src: url('". esc_url( $font_path ) .".eot') format('embedded-opentype'), /* IE6-IE8 */ url('". esc_url( $font_path ) .".woff2') format('woff2'), /* Super Modern Browsers */ url('". esc_url( $font_path ) .".woff') format('woff'), /* Pretty Modern Browsers */ url('". esc_url( $font_path ) .".ttf')  format('truetype'), /* Safari, Android, iOS */ url('". esc_url( $font_path ) .".svg') format('svg'); /* Legacy iOS */ }";
		}
		
	}
	
	function independent_custom_font_check($field){
		$independent_options = $this->independent_options;
		$cf_names = get_option( 'independent_custom_fonts_names' );
		if ( !empty( $cf_names ) && !in_array( $independent_options[$field]['font-family'], $this->exists_fonts ) ){
			$this->independent_custom_font_face_create( $independent_options[$field]['font-family'], $cf_names );
			array_push( $this->exists_fonts, $independent_options[$field]['font-family'] );
		}
	}
	
	function independent_typo_generate($field){
		$independent_options = $this->independent_options;
		if( isset( $independent_options[$field] ) ):
	echo '
	'. ( isset( $independent_options[$field]['color'] ) && $independent_options[$field]['color'] != '' ?  'color: '. $independent_options[$field]['color'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['font-family'] ) && $independent_options[$field]['font-family'] != '' ?  'font-family: '. $independent_options[$field]['font-family'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['font-weight'] ) && $independent_options[$field]['font-weight'] != '' ?  'font-weight: '. $independent_options[$field]['font-weight'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['font-style'] ) && $independent_options[$field]['font-style'] != '' ?  'font-style: '. $independent_options[$field]['font-style'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['font-size'] ) && $independent_options[$field]['font-size'] != '' ?  'font-size: '. $independent_options[$field]['font-size'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['line-height'] ) && $independent_options[$field]['line-height'] != '' ?  'line-height: '. $independent_options[$field]['line-height'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['letter-spacing'] ) && $independent_options[$field]['letter-spacing'] != '' ?  'letter-spacing: '. $independent_options[$field]['letter-spacing'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['text-align'] ) && $independent_options[$field]['text-align'] != '' ?  'text-align: '. $independent_options[$field]['text-align'] .';' : '' ) .'
	'. ( isset( $independent_options[$field]['text-transform'] ) && $independent_options[$field]['text-transform'] != '' ?  'text-transform: '. $independent_options[$field]['text-transform'] .';' : '' ) .'
	';
		endif;
	}
	
	function independent_hex2rgba($color, $opacity = 1) {
	 
		$default = '';
		//Return default if no color provided
		if(empty($color))
			  return $default; 
		//Sanitize $color if "#" is provided 
			if ($color[0] == '#' ) {
				$color = substr( $color, 1 );
			}
			//Check if color has 6 or 3 characters and get values
			if (strlen($color) == 6) {
					$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
			} elseif ( strlen( $color ) == 3 ) {
					$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
			} else {
					return $default;
			}
			//Convert hexadec to rgb
			$rgb =  array_map('hexdec', $hex);
	 
			//Check if opacity is set(rgba or rgb)
			if( $opacity == 'none' ){
				$output = implode(",",$rgb);
			}elseif( $opacity ){
				if(abs($opacity) > 1)
					$opacity = 1.0;
				$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
			}else {
				$output = 'rgb('.implode(",",$rgb).')';
			}
			//Return rgb(a) color string
			return $output;
	}
}