<?php
/**
 * independent news block functions and definitions
 */
add_action('wp_ajax_independent-ajax-slide', 'independent_zozo_ajax_slide');
add_action('wp_ajax_nopriv_independent-ajax-slide', 'independent_zozo_ajax_slide'); 
function independent_zozo_ajax_slide(){
	$nonce = $_POST['nonce'];  
    if ( ! wp_verify_nonce( $nonce, 'independent-ajax-nonce' ) )
        die ( esc_html__( 'Busted', 'independent' ) );
		
	if( $_POST['temp_key'] == 'tab' ){
		$block_options = array(
				'filter_name' => esc_attr( $_POST['filter_name'] ), 
				'filter_values' => esc_attr( $_POST['filter_values'] ), 
				'ppp' => esc_attr( $_POST['ppp'] ), 
				'paged' => esc_attr( $_POST['paged'] ), 
				'meta' => esc_attr( $_POST['meta'] ),
				'orderby' => esc_attr( $_POST['orderby'] ),
				'order' => esc_attr( $_POST['order'] ),
				'date' => esc_attr( $_POST['date'] ),
				'post_not' => esc_attr( $_POST['post_not'] ),
				'post_in' => esc_attr( $_POST['post_in'] ),
				'block_id' => esc_attr( $_POST['block_id'] ),
				'slide_id' => esc_attr( $_POST['slide_id'] ),
				'dynamic_options' => $_POST['dynamic_options'],
			);
		$return["json"] = independent_news_block_slider($block_options);
	}else{		
		$block_options = array(
				'filter_name' => esc_attr( $_POST['filter_name'] ), 
				'filter_values' => esc_attr( $_POST['filter_values'] ), 
				'ppp' => esc_attr( $_POST['ppp'] ), 
				'paged' => absint( $_POST['paged'] ) + 1, 
				'meta' => esc_attr( $_POST['meta'] ),
				'orderby' => esc_attr( $_POST['orderby'] ),
				'order' => esc_attr( $_POST['order'] ),
				'date' => esc_attr( $_POST['date'] ),
				'post_not' => esc_attr( $_POST['post_not'] ),
				'post_in' => esc_attr( $_POST['post_in'] ),
				'block_id' => esc_attr( $_POST['block_id'] ),
				'dynamic_options' => $_POST['dynamic_options'],	
			);
		$return["json"] = independent_news_block($block_options);
	}
	echo json_encode($return);
	exit;
}
/* Unique News Ids Generate Method */
function independent_get_unique_news_ids( $post_id = NULL ){
	static $unique_ids = array();
	if( $post_id ){
		array_push( $unique_ids, $post_id );
	}
	return $unique_ids;
}
/*News Blocks Generated Functions End*/

/*News Blocks Generated Functions End*/
function independent_news_block_tabs_only($block_options){
	
	$items = explode(',', esc_attr( $block_options['filter_values'] ) );
	$filter = $block_options['filter_name'];
	
	$tab_output = $more_tab_output = $output = '';
	
	if( isset( $block_options['dynamic_options']['tab_hide'] ) && $block_options['dynamic_options']['tab_hide'] == 'no' ){
	
		//this is code for first tab not all
		$all_text = esc_attr( $block_options['dynamic_options']['all_text'] );
		if( $block_options['slide_id'] != 'all' ){
			$block_options['filter_values'] = esc_attr( $block_options['slide_id'] );
		}else{
			$tab_output .= '<li class="active"><a data-toggle="tab" href="#independent-tab-all-'. esc_attr( $block_options['block_id'] ) .'">'. esc_attr( $all_text ) .'</a></li>';
		}
		//this is code for first tab not all - end
		
		$tab_limit = absint( $block_options['tab_limit'] );
		$i = 1;
	
		if( $filter == 'cat' ){
			foreach ($items as $tab) {
				$t = absint( $tab );
				if( term_exists( $t, 'category' ) ){
					if( $i < $tab_limit ){
						$first_tab_class = $i == 1 && $all_text == '' ? ' active' : ''; //this line code for first tab not all
						$tab_output .= '<li class="reponsive-tab-list'. $first_tab_class .'"><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_cat_name($t) .'</a></li>';
					}else{
						$more_tab_output .= '<li><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_cat_name($t) .'</a></li>';
					}
					$i++;
				} //term_exists
			}
		}elseif( $filter == 'tag' ){
			foreach ($items as $tab) {
				$t = absint( $tab );
				if( term_exists( $t, 'post_tag' ) ){
					if( $i < $tab_limit ){
						$tab_output .= '<li class="reponsive-tab-list"><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_term_by('id', $t, 'post_tag')->name .'</a></li>';
					}else{
						$more_tab_output .= '<li><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_term_by('id', $t, 'post_tag')->name .'</a></li>';
					}
					$i++;
				}
			}
		}elseif( $filter == 'author' ){
	
			foreach ($items as $tab) {
				if( get_the_author_meta( 'user_email', $tab ) ){
					
					if( $i < $tab_limit ){
						$tab_output .= '<li class="reponsive-tab-list"><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_the_author_meta( 'display_name', $tab ) .'</a></li>';
					}else{
						$more_tab_output .= '<li><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_the_author_meta( 'display_name', $tab ) .'</a></li>';
					}
					$i++;
				}
				
			}
		}
	
		$output = '<ul class="nav news-tabs">';
		$output .= $tab_output;
		if( $more_tab_output != '' ){
			$more_text = $block_options['dynamic_options']['more_text'];
			$output .= '<li class="dropdown pull-right" role="presentation" >';
				$output .= '<a href="#" class="tab-more-text" id="tab-more-text-'. esc_attr( $block_options['block_id'] ) .'">'. esc_attr( $more_text ) . '</a>';
				$output .= '<ul class="more-tab-dropdown dropdown-menu">';
					$output .=	$more_tab_output;
				$output .= '</ul>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output; 
}

function independent_news_block_tabs($block_options){
	
	$items = explode(',', esc_attr( $block_options['filter_values'] ) );
	$filter = $block_options['filter_name'];
	
	$tab_output = $more_tab_output = $output = '';
	
	if( isset( $block_options['dynamic_options']['tab_hide'] ) && $block_options['dynamic_options']['tab_hide'] == 'no' ){
	
		//this is code for first tab not all
		$all_text = esc_attr( $block_options['dynamic_options']['all_text'] );
		if( $block_options['slide_id'] != 'all' ){
			$block_options['filter_values'] = esc_attr( $block_options['slide_id'] );
		}else{
			$tab_output .= '<li class="active"><a data-toggle="tab" href="#independent-tab-all-'. esc_attr( $block_options['block_id'] ) .'">'. esc_attr( $all_text ) .'</a></li>';
		}
		//this is code for first tab not all - end
		
		$tab_limit = absint( $block_options['tab_limit'] );
		$i = 1;
	
		if( $filter == 'cat' ){
			foreach ($items as $tab) {
				$t = absint( $tab );
				if( term_exists( $t, 'category' ) ){
					if( $i < $tab_limit ){
						$first_tab_class = $i == 1 && $all_text == '' ? ' active' : ''; //this line code for first tab not all
						$tab_output .= '<li class="reponsive-tab-list'. $first_tab_class .'"><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_cat_name($t) .'</a></li>';
					}else{
						$more_tab_output .= '<li><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_cat_name($t) .'</a></li>';
					}
					$i++;
				} //term_exists
			}
		}elseif( $filter == 'tag' ){
			foreach ($items as $tab) {
				$t = absint( $tab );
				if( term_exists( $t, 'post_tag' ) ){
					if( $i < $tab_limit ){
						$tab_output .= '<li class="reponsive-tab-list"><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_term_by('id', $t, 'post_tag')->name .'</a></li>';
					}else{
						$more_tab_output .= '<li><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_term_by('id', $t, 'post_tag')->name .'</a></li>';
					}
					$i++;
				}
			}
		}elseif( $filter == 'author' ){
	
			foreach ($items as $tab) {
				if( get_the_author_meta( 'user_email', $tab ) ){
					
					if( $i < $tab_limit ){
						$tab_output .= '<li class="reponsive-tab-list"><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_the_author_meta( 'display_name', $tab ) .'</a></li>';
					}else{
						$more_tab_output .= '<li><a data-toggle="tab" href="#independent-tab-'. esc_attr( $tab ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-id="'. esc_attr( $tab ) .'">'. get_the_author_meta( 'display_name', $tab ) .'</a></li>';
					}
					$i++;
				}
				
			}
		}
	
		$output = '<ul class="nav news-tabs">';
		$output .= $tab_output;
		if( $more_tab_output != '' ){
			$more_text = $block_options['dynamic_options']['more_text'];
			$output .= '<li class="dropdown pull-right" role="presentation" >';
				$output .= '<a href="#" class="tab-more-text" id="tab-more-text-'. esc_attr( $block_options['block_id'] ) .'">'. esc_attr( $more_text ) . '</a>';
				$output .= '<ul class="more-tab-dropdown dropdown-menu">';
					$output .=	$more_tab_output;
				$output .= '</ul>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	
	$output .= '<div class="tab-content independent-content">';
	$output .= '<div class="news-slide-loader"><img src="'.esc_url( independent_news_loader() ).'" alt="'. esc_attr__('Loader..', 'independent') .'" /></div>';
			$output .= independent_news_block_slider($block_options);
	$output .= '</div>';
	return $output; 
}
function independent_news_block_slider($block_options){
	$post_per_tab = isset( $block_options['ppp'] ) && $block_options['ppp'] != 0 ? $block_options['ppp'] : 5 ;
	$post_count = independent_found_posts($block_options);
	$max_length = ceil( absint( $post_count ) / absint( $post_per_tab ) );
	$output = '<div class="independent-slider tab-pane" id="independent-tab-'. esc_attr( $block_options['slide_id'] ) .'-'. esc_attr( $block_options['block_id'] ) .'" data-len="'. esc_attr( $max_length ) .'">';
		$output .= independent_news_block($block_options);
	$output .= '</div><!--independent-slider-->';
	return $output;
}
function independent_news_block($block_options, $custom_loop = ''){
	$output = '';
	
	$block_modal = 'independent_news_block_modal_'.esc_attr( $block_options['dynamic_options']['modal'] );
	
	$date_query = isset( $block_options['date'] ) && $block_options['date'] != '' ? array( array( 'after' => absint( $block_options['date'] ) . ' days ago' ) ) : '';
	$post_not = isset( $block_options['post_not'] ) && $block_options['post_not'] != '' ? explode(',', esc_attr($block_options['post_not'])) : '';
	$post_in = isset( $block_options['post_in'] ) && $block_options['post_in'] != '' ? explode(',', esc_attr($block_options['post_in'])) : '';
	
	$independent_options = get_option( 'independent_options' );
	if( isset( $independent_options['duplicate-news'] ) && !$independent_options['duplicate-news'] ){
		$unique_ids = independent_get_unique_news_ids();
		if( $unique_ids ){
			if( !empty( $post_not ) && is_array( $post_not ) ){
				$post_not = array_merge( $post_not, $unique_ids ); 
			}else{
				$post_not = array();
				$post_not = array_merge( $post_not, $unique_ids ); 
			}
		}
	}
	
	$meta_key = esc_attr( $block_options['meta'] );
	if( $meta_key == 'likes' ){
		$meta_key = 'independent_post_fav_count';
	}elseif( $meta_key == 'views' ){
		$meta_key = 'independent_post_views_count';
	}elseif( $meta_key == 'rated' ){
		$meta_key = 'independent_post_review_rating';
	}
	
	$paged = $block_options['paged'];
	
	if( isset( $block_options['dynamic_options']['pagination'] ) && $block_options['dynamic_options']['pagination'] == 'yes' ){
		if( get_query_var('paged') ){
			$paged = esc_attr( get_query_var('paged') );
		}elseif( get_query_var('page') ){
			$paged = esc_attr( get_query_var('page') );
		}
	}
	
	$filter_name = esc_attr( $block_options['filter_name'] );
	$filter_values = esc_attr( $block_options['filter_values'] );
	
	if( $filter_name == 'tag' ){
		$filter_name = 'tag__in';
		$filter_values = explode(",", $filter_values);
	}
	
	$args = array(
				'posts_per_page' => esc_attr( $block_options['ppp'] ),
				'paged' => esc_attr( $paged ),
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				$filter_name => $filter_values,
				'meta_key' => esc_attr( $meta_key ),
				'orderby' => esc_attr( $block_options['orderby'] ),
				'order' => esc_attr( $block_options['order'] ),
				'date_query' => $date_query,
				'post__not_in' => $post_not,
				'post__in' => $post_in
			 );
	if( $custom_loop ){
		$loop = $GLOBALS['wp_query'];
		$loop->query_vars['posts_per_page'] = $block_options['ppp'];
		$excerpt_len = isset( $block_options['dynamic_options']['excerpt_len'] ) ? $block_options['dynamic_options']['excerpt_len'] : 20;
		add_filter( 'excerpt_length', __return_value( absint( $excerpt_len ) ) );
		$loop = new WP_Query($loop->query_vars);
	}else{
		$loop = new WP_Query($args);
	}
	
	
	$pagination_stat = 0;
	if( isset( $block_options['dynamic_options']['pagination'] ) && $block_options['dynamic_options']['pagination'] == 'yes' ){
		$pagination_stat = 1;
		$zps = new independentPostSettings;
	}
		
	
	if ( $loop->have_posts() ){
		$output = '<div class="independent-news active">';
			if (function_exists($block_modal)) {
				$output .= $block_modal($loop, $block_options['dynamic_options']);
			}
		$output .= '</div>';
		if( $pagination_stat ){
			ob_start();
			$zps->independentWpBootstrapPagination($args, $loop->max_num_pages);
			$output .= ob_get_clean();
		}
		wp_reset_postdata();
	}
	
	return $output;
}
function independent_found_posts($block_options){
	$date_query = isset( $block_options['date'] ) && $block_options['date'] != '' ? array( array( 'after' => absint( $block_options['date'] ) . ' days ago' ) ) : '';
	$post_not = isset( $block_options['post_not'] ) && $block_options['post_not'] != '' ? explode(',', esc_attr($block_options['post_not'])) : '';
	$post_in = isset( $block_options['post_in'] ) && $block_options['post_in'] != '' ? explode(',', esc_attr($block_options['post_in'])) : '';
	
	$meta_key = esc_attr( $block_options['meta'] );
	if( $meta_key == 'likes' ){
		$meta_key = 'independent_post_fav_count';
	}elseif( $meta_key == 'views' ){
		$meta_key = 'independent_post_views_count';
	}elseif( $meta_key == 'rated' ){
		$meta_key = 'independent_post_review_rating';
	}
	$query_count = array(
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page' => -1,
				esc_attr( $block_options['filter_name'] ) => esc_attr( $block_options['filter_values'] ),
				'meta_key' => esc_attr( $meta_key ),
				'orderby' => esc_attr( $block_options['orderby'] ),
				'order' => esc_attr( $block_options['order'] ),
				'date_query' => $date_query,
				'post__not_in' => $post_not,
				'post__in' => $post_in
			 );
	$loop_count = new WP_Query($query_count);
	$post_count = $loop_count->post_count;
	wp_reset_postdata();
	return $post_count;
}
/*News Blocks Generated Functions End*/
/***********
   independent Blocks Common Functions
 ***********/
/*independent Common Grid Post Function*/
function independent_blocksTitleAlign($position){
	$out = '';
	if( isset( $position ) && $position != '' ){
		$out .= 'text-'. esc_attr($position);
	}
	return $out;
}
function independent_common_block_grid_generate($dynamic_options){
	$block_class = isset( $dynamic_options['grid_align'] ) ? ' '. esc_attr( $dynamic_options['grid_align'] ) : '';
	$output = '<div class="post post-grid independent-block-post'. $block_class .' clearfix"><!--Single Post Start-->';
	
	$grid_items = isset( $dynamic_options['grid_items'] ) ? $dynamic_options['grid_items'] : array();
	$grid_primary_meta = isset( $dynamic_options['grid_primary_meta'] ) ? $dynamic_options['grid_primary_meta'] : array();
	$grid_secondary_meta = isset( $dynamic_options['grid_secondary_meta'] ) ? $dynamic_options['grid_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$excerpt_len = isset( $dynamic_options['excerpt_len'] ) ? $dynamic_options['excerpt_len'] : '';	
	$tit_var = isset( $dynamic_options['grid_title'] ) ? esc_attr( $dynamic_options['grid_title'] ) : '';
	$cat_tag_class = isset( $dynamic_options['cat_tag_style'] ) && $dynamic_options['cat_tag_style'] != '' ? ' category-tag-' . $dynamic_options['cat_tag_style'] : '';
	
	if( isset( $grid_items['Enabled'] ) && !empty( $grid_items['Enabled'] ) ){	
		foreach( $grid_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "image" : $output .= independent_common_block_grid_tumb($dynamic_options);
				break;
				case "title" : $output .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $output .= independent_general_block_meta($grid_primary_meta, $read_more, $cat_tag_class);
				break;
				case "secondary-meta" : $output .= independent_general_block_meta($grid_secondary_meta, $read_more, $cat_tag_class);
				break;
				case "content" : $output .= independent_common_block_grid_content( $excerpt_len );
				break;
				case "more" : $output .= independent_common_block_readmore( $read_more );
				break;
			}
		}
	}//isset
	$output .= '</div><!--Single Post End-->';
	
	return $output;	
}
function independent_common_block_biglist_generate($dynamic_options, $opp_pos = false){
	$block_class = isset( $dynamic_options['grid_align'] ) ? ' '. esc_attr( $dynamic_options['grid_align'] ) : '';
	$output = '<div class="post post-grid independent-block-post'. $block_class .' clearfix"><!--Single Post Start-->';
	$post_thumb = $post_content = $output = '';
	
	$grid_items = isset( $dynamic_options['grid_items'] ) ? $dynamic_options['grid_items'] : array();
	$grid_primary_meta = isset( $dynamic_options['grid_primary_meta'] ) ? $dynamic_options['grid_primary_meta'] : array();
	$grid_secondary_meta = isset( $dynamic_options['grid_secondary_meta'] ) ? $dynamic_options['grid_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$excerpt_len = isset( $dynamic_options['excerpt_len'] ) ? $dynamic_options['excerpt_len'] : '';	
	$tit_var = isset( $dynamic_options['grid_title'] ) ? esc_attr( $dynamic_options['grid_title'] ) : '';
	$cat_tag_class = isset( $dynamic_options['cat_tag_style'] ) && $dynamic_options['cat_tag_style'] != '' ? ' category-tag-' . $dynamic_options['cat_tag_style'] : '';
	
	if( isset( $grid_items['Enabled'] ) && !empty( $grid_items['Enabled'] ) ){	
		foreach( $grid_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "image" : $post_thumb .= independent_common_block_grid_tumb($dynamic_options);
				break;
				case "title" : $post_content .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $post_content .= independent_general_block_meta($grid_primary_meta, $read_more, $cat_tag_class);
				break;
				case "secondary-meta" : $post_content .= independent_general_block_meta($grid_secondary_meta, $read_more, $cat_tag_class);
				break;
				case "content" : $post_content .= independent_common_block_grid_content( $excerpt_len );
				break;
				case "more" : $post_content .= independent_common_block_readmore( $read_more );
				break;
			}
		}
	}//isset
	$output .= '</div><!--Single Post End-->';
	
	$block_class = isset( $dynamic_options['list_align'] ) ? ' '. esc_attr( $dynamic_options['list_align'] ) : '';
	$block_class .= $opp_pos ? ' post-list-vice-versa' : '';
	
	$output = '<div class="post post-list post-biglist independent-block-post'. $block_class .' clearfix">';
	$output .= $post_thumb != '' && !$opp_pos ? '<div class="post-list-thumb">'. $post_thumb .'</div>' : '';
	$output .= $post_content != '' ? '<div class="post-list-content">'. $post_content .'</div>' : '';
	$output .= $post_thumb != '' && $opp_pos ? '<div class="post-list-thumb">'. $post_thumb .'</div>' : '';
	$output .= '</div><!--Single Post End-->';
	
	return $output;	
}
/*independent Common List Post Function*/
function independent_common_block_list_generate($dynamic_options, $opp_pos = false){
	
	$list_items = isset( $dynamic_options['list_items'] ) ? $dynamic_options['list_items'] : array();
	$list_primary_meta = isset( $dynamic_options['list_primary_meta'] ) ? $dynamic_options['list_primary_meta'] : array();
	$list_secondary_meta = isset( $dynamic_options['list_secondary_meta'] ) ? $dynamic_options['list_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$excerpt_len = isset( $dynamic_options['excerpt_len'] ) ? $dynamic_options['excerpt_len'] : '';
	$tit_var = isset( $dynamic_options['list_title'] ) ? esc_attr( $dynamic_options['list_title'] ) : '';
	$post_thumb = $post_content = $output = '';
	if( isset( $list_items['Enabled'] ) && !empty( $list_items['Enabled'] ) ){	
		foreach( $list_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "image" : $post_thumb .= independent_common_block_list_tumb($dynamic_options);
				break;
				case "title" : $post_content .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $post_content .= independent_general_block_meta($list_primary_meta, $read_more);
				break;
				case "secondary-meta" : $post_content .= independent_general_block_meta($list_secondary_meta, $read_more);
				break;
				case "content" : $post_content .= independent_common_block_grid_content($excerpt_len);
				break;
				case "more" : $post_content .= independent_common_block_readmore( $read_more );
				break;
			}
		}
		
		$block_class = isset( $dynamic_options['list_align'] ) ? ' '. esc_attr( $dynamic_options['list_align'] ) : '';
		$block_class .= $opp_pos ? ' post-list-vice-versa' : '';
		
		$output = '<div class="post post-list independent-block-post'. $block_class .' clearfix">';
		$output .= $post_thumb != '' && !$opp_pos ? '<div class="post-list-thumb">'. $post_thumb .'</div>' : '';
		$output .= $post_content != '' ? '<div class="post-list-content">'. $post_content .'</div>' : '';
		$output .= $post_thumb != '' && $opp_pos ? '<div class="post-list-thumb">'. $post_thumb .'</div>' : '';
		$output .= '</div><!--Single Post End-->';
	}//isset
	return $output;	
}
/*independent Common Mini Grid Post Function*/
function independent_common_block_minigrid_generate($dynamic_options){
	
	$list_items = isset( $dynamic_options['list_items'] ) ? $dynamic_options['list_items'] : array();
	$list_primary_meta = isset( $dynamic_options['list_primary_meta'] ) ? $dynamic_options['list_primary_meta'] : array();
	$list_secondary_meta = isset( $dynamic_options['list_secondary_meta'] ) ? $dynamic_options['list_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$excerpt_len = isset( $dynamic_options['excerpt_len'] ) ? $dynamic_options['excerpt_len'] : '';
	$tit_var = isset( $dynamic_options['list_title'] ) ? esc_attr( $dynamic_options['list_title'] ) : '';
	$post_thumb = $post_content = $output = '';
	if( isset( $list_items['Enabled'] ) && !empty( $list_items['Enabled'] ) ){	
		foreach( $list_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "image" : $post_content .= independent_common_block_list_tumb($dynamic_options);
				break;
				case "title" : $post_content .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $post_content .= independent_general_block_meta($list_primary_meta, $read_more);
				break;
				case "secondary-meta" : $post_content .= independent_general_block_meta($list_secondary_meta, $read_more);
				break;
				case "content" : $post_content .= independent_common_block_grid_content($excerpt_len);
				break;
				case "more" : $post_content .= independent_common_block_readmore( $read_more );
				break;
			}
		}
		
		$block_class = isset( $dynamic_options['list_align'] ) ? ' '. esc_attr( $dynamic_options['list_align'] ) : '';
		
		$output = '<div class="post post-mini-grid independent-block-post'. $block_class .' clearfix">';
		$output .= $post_content != '' ? '<div class="post-mini-grid-content">'. $post_content .'</div>' : '';
		$output .= '</div><!-- .post-mini-grid -->';
	}//isset
	return $output;	
}
/*independent Mini Grid Post Function*/
function independent_common_block_mini_grid_generate($dynamic_options){
	
	$list_items = isset( $dynamic_options['list_items'] ) ? $dynamic_options['list_items'] : array();
	$list_primary_meta = isset( $dynamic_options['list_primary_meta'] ) ? $dynamic_options['list_primary_meta'] : array();
	$list_secondary_meta = isset( $dynamic_options['list_secondary_meta'] ) ? $dynamic_options['list_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$excerpt_len = isset( $dynamic_options['excerpt_len'] ) ? $dynamic_options['excerpt_len'] : '';
	$tit_var = isset( $dynamic_options['list_title'] ) ? esc_attr( $dynamic_options['list_title'] ) : '';
	$post_thumb = $post_content = $output = '';
	if( isset( $list_items['Enabled'] ) && !empty( $list_items['Enabled'] ) ){	
		foreach( $list_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "image" : $post_content .= independent_common_block_list_tumb($dynamic_options);
				break;
				case "title" : $post_content .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $post_content .= independent_general_block_meta($list_primary_meta, $read_more);
				break;
				case "secondary-meta" : $post_content .= independent_general_block_meta($list_secondary_meta, $read_more);
				break;
				case "content" : $post_content .= independent_common_block_grid_content($excerpt_len);
				break;
				case "more" : $post_content .= independent_common_block_readmore( $read_more );
				break;
			}
		}
		
		$block_class = isset( $dynamic_options['list_align'] ) ? ' '. esc_attr( $dynamic_options['list_align'] ) : '';
		
		$output = '<div class="post post-mini-grid independent-block-post'. $block_class .' clearfix">';
		$output .= $post_content != '' ? '<div class="post-mini-grid-content">'. $post_content .'</div>' : '';
		$output .= '</div><!--Single Post End-->';
	}//isset
	return $output;	
}
/*independent Common List Post Function*/
function independent_common_block_big_list_generate($dynamic_options){
	
	$block_class = isset( $dynamic_options['grid_align'] ) ? ' '. esc_attr( $dynamic_options['grid_align'] ) : '';
	$output = '';
	
	$post_thumb = $post_content = $output = '';
	
	$grid_items = isset( $dynamic_options['grid_items'] ) ? $dynamic_options['grid_items'] : array();
	$grid_primary_meta = isset( $dynamic_options['grid_primary_meta'] ) ? $dynamic_options['grid_primary_meta'] : array();
	$grid_secondary_meta = isset( $dynamic_options['grid_secondary_meta'] ) ? $dynamic_options['grid_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$excerpt_len = isset( $dynamic_options['excerpt_len'] ) ? $dynamic_options['excerpt_len'] : '';	
	$tit_var = isset( $dynamic_options['grid_title'] ) ? esc_attr( $dynamic_options['grid_title'] ) : '';
	$cat_tag_class = isset( $dynamic_options['cat_tag_style'] ) && $dynamic_options['cat_tag_style'] != '' ? ' category-tag-' . $dynamic_options['cat_tag_style'] : '';
	
	if( isset( $grid_items['Enabled'] ) && !empty( $grid_items['Enabled'] ) ){	
		foreach( $grid_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "image" : $post_thumb .= independent_common_block_grid_tumb($dynamic_options);
				break;
				case "title" : $post_content .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $post_content .= independent_general_block_meta($grid_primary_meta, $read_more, $cat_tag_class);
				break;
				case "secondary-meta" : $post_content .= independent_general_block_meta($grid_secondary_meta, $read_more, $cat_tag_class);
				break;
				case "content" : $post_content .= independent_common_block_grid_content( $excerpt_len );
				break;
				case "more" : $post_content .= independent_common_block_readmore( $read_more );
				break;
			}
		}
	}//isset	
	
	$output .= '<div class="post post-grid independent-block-post'. $block_class .' clearfix"><!--Single Post Start-->';
		$output .= $post_thumb != '' ? '<div class="post-list-thumb">'. $post_thumb .'</div>' : '';
		$output .= $post_content != '' ? '<div class="post-list-content">'. $post_content .'</div>' : '';
	$output .= '</div><!--Single Post End-->';
	
	return $output;	
}
function independent_common_block_title($tit_var){
	$title = get_the_title();
	if( $title ){
		if( $tit_var ){
			return '<div class="post-title-wrapper"><'. esc_attr( $tit_var ) .' class="post-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. get_the_title() .'</a></'. esc_attr( $tit_var ) .'></div>';
		}else{
			return '<div class="post-title-wrapper"><h5 class="post-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. esc_html( get_the_title() ) .'</a></h5></div>';
		}
	}
}
function independent_common_block_readmore($read_more){
	return '<div class="post-read-more"><a class="read-more" href="'. esc_url( get_permalink( get_the_ID() ) ) . '">'. esc_html( $read_more ) .'</a></div>';
}
function independent_common_block_overlay_items($dynamic_options){
	$grid_items = isset( $dynamic_options['ovelay_items'] ) ? $dynamic_options['ovelay_items'] : array();
	$grid_primary_meta = isset( $dynamic_options['grid_primary_meta'] ) ? $dynamic_options['grid_primary_meta'] : array();
	$grid_secondary_meta = isset( $dynamic_options['grid_secondary_meta'] ) ? $dynamic_options['grid_secondary_meta'] : array();
	$read_more = isset( $dynamic_options['read_more'] ) ? $dynamic_options['read_more'] : '';
	$tit_var = isset( $dynamic_options['grid_title'] ) ? esc_attr( $dynamic_options['grid_title'] ) : '';
	$cat_tag_class = isset( $dynamic_options['cat_tag_style'] ) && $dynamic_options['cat_tag_style'] != '' ? ' category-tag-' . $dynamic_options['cat_tag_style'] : '';
	
	$post_content = '';
	if( isset( $grid_items['Enabled'] ) && !empty( $grid_items['Enabled'] ) ){	
		foreach( $grid_items['Enabled'] as $items => $lable ){
			switch( $items ){
				case "title" : $post_content .= independent_common_block_title($tit_var);
				break;
				case "primary-meta" : $post_content .= independent_general_block_meta($grid_primary_meta, $read_more, $cat_tag_class);
				break;
				case "secondary-meta" : $post_content .= independent_general_block_meta($grid_secondary_meta, $read_more, $cat_tag_class);
				break;
				case "more" : $post_content .= independent_common_block_meta_readmore( $read_more );
				break;
			}
		}
	}//isset	
	$output = '<div class="post independent-block-overlay typo-white">';
		$output .= $post_content;
	$output .= '</div><!--Single Post End-->';
	
	return $output;	
}
function independent_custom_image_size_chk( $thumb_size, $custom_size = array(), $thumb_id = '' ){
	$img_sizes = $img_width = $img_height = $src = $img_src = '';
	$img_stat = 0;
	$custom_img_size = '';
	if( $thumb_size == "medium" || $thumb_size == "large" ){
	
		$img_sizes = independent_get_image_sizes($thumb_size);
		$img_width = $img_sizes[$thumb_size]['width'];
		$img_height = $img_sizes[$thumb_size]['height'];
		$img_stat = 1;
					
	}else{
		
		if( class_exists('Aq_Resize') ) {
			
			$thumb_id = $thumb_id == '' ? get_post_thumbnail_id( get_the_ID() ) : $thumb_id;
			
			$src = wp_get_attachment_image_src( absint( $thumb_id ), "full", false, '' );
			$img_width = $img_height = '';
			if( is_array( $custom_size ) ){
				$img_width = isset( $custom_size[0] ) ? $custom_size[0] : '';
				$img_height = isset( $custom_size[1] ) ? $custom_size[1] : '';
			}else{
				$custom_img_size = independentThemeOpt::independentStaticThemeOpt($thumb_size);
				$img_width = isset( $custom_img_size['width'] ) ? $custom_img_size['width'] : '';
				$img_height = isset( $custom_img_size['height'] ) ? $custom_img_size['height'] : '';
			}
			
			$cropped_img = !empty( $img_width ) ? aq_resize( $src[0], absint( $img_width ), absint( $img_height ), true, false ) : '';
			if( $cropped_img ){
				$img_src = isset( $cropped_img[0] ) ? $cropped_img[0] : '';
				$img_width = isset( $cropped_img[1] ) ? $cropped_img[1] : '';
				$img_height = isset( $cropped_img[2] ) ? $cropped_img[2] : '';
			}else{
				$img_stat = 1;
			}
		}
	}
	if( $img_stat ){
		$src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $thumb_size, false, '' );
		$img_src = $src[0];
		$img_width = isset( $src[1] ) ? $src[1] : '';
		$img_height = isset( $src[2] ) ? $src[2] : '';
	}
	
	return array( $img_src, $img_width, $img_height );
}
function independent_common_block_grid_tumb($dynamic_options){
	
	$output = '';
	
	if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ){
	
		$thumb_size = isset( $dynamic_options['grid_thumb'] ) ? esc_attr( $dynamic_options['grid_thumb'] ) : 'large';
		$custom_opt = '';
		if( $thumb_size == 'custom' ){
			$custom_opt = isset( $dynamic_options['grid_image_custom'] ) && $dynamic_options['grid_image_custom'] != '' ? explode( "x",  $dynamic_options['grid_image_custom'] ) : array();
		} 
		
		$img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt );
		$lazy_opt = independentThemeOpt::independentStaticThemeOpt('news-lazy-load');
		if( $lazy_opt ){
			$lazy_img = independentThemeOpt::independentStaticThemeOpt('news-lazy-load-img');
			if( isset( $lazy_img['id'] ) && $lazy_img['id'] != '' ){
				$pre_img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt, absint( $lazy_img['id'] ) );
				$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="'. esc_url( $pre_img_prop[0] ) .'" data-src="' . esc_url( $img_prop[0] ) . '" />';
			}else{
				$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="' . esc_url( $img_prop[0] ) . '" />';
			}
		}else{
			$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '" />';
		}
		$output .= '<div class="post-thumb-wrapper"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">';  
		
			$output .= $image;
			if( isset( $dynamic_options['post_icon'] ) && $dynamic_options['post_icon'] == 'yes' ){
				$format = get_post_format( get_the_ID() );
				$post_formats_array = array("image"=>"icon-picture", "video"=>"icon-camrecorder", "audio"=>"icon-music-tone-alt", "gallery"=>"icon-wallet", "quote"=>"fa fa-quote-right", "link"=>"icon-link");
				if( $format != '' ){
					if (array_key_exists($format, $post_formats_array)){
						$output .= '<span class="post-format-icon '. $post_formats_array[$format] .'">';
						if( $format == 'gallery' ){
							$img_ids = get_post_meta( get_the_ID(), 'independent_post_gallery', true );
							if( $img_ids != '' ){
								$img_count = count( explode( ",", $img_ids ) );
								$output .= '<span class="img-count">'. esc_html( $img_count ) .'</span>';
							}
						}
						$output .= '</span>';
					}
				}
			}

			if( $dynamic_options['ovelay_items'] != '' ){
				$output .= '<span class="gradient-black-overlay"></span></a>';
				$output .= independent_common_block_overlay_items($dynamic_options);
			}else{
				$output .= '</a>';
			}
			
			if( isset( $dynamic_options['post_promotion'] ) && $dynamic_options['post_promotion'] == 'yes' ){
				$promotion_opt = get_post_meta( get_the_ID(), 'independent_post_promotion_opt', true );
				$promotion_text = independentThemeOpt::independentStaticThemeOpt('news-promotion-text');
				if( $promotion_opt == 'yes' ){
					$output .= '<span class="post-promotion">'. esc_html( $promotion_text ) .'</span>';
				}
			}
		
			if( isset( $dynamic_options['cat_tag'] ) && $dynamic_options['cat_tag'] != 'no' ){
				$cat_tag_style = isset( $dynamic_options['cat_tag_style'] ) && $dynamic_options['cat_tag_style'] != '' ? ' category-tag-' . $dynamic_options['cat_tag_style'] : '';
				$categories = get_the_category(); 
				if ( ! empty( $categories ) ) $output .= '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" class="cat-tag-'. esc_attr( $categories[0]->term_id ) .' typo-white category-tag'. esc_attr( $cat_tag_style ) .'" >'. esc_attr( $categories[0]->name ) .'</a>';
			}
	
		$output .= '</div>';
		
	} //has_post_thumbnail
	return $output;
}
function independent_common_block_list_tumb($dynamic_options){
	$output = '';
	if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ){
		
		$thumb_size = isset( $dynamic_options['list_thumb'] ) ? esc_attr( $dynamic_options['list_thumb'] ) : 'large';
		$custom_opt = '';
		if( $thumb_size == 'custom' ){
			$custom_opt = isset( $dynamic_options['list_image_custom'] ) && $dynamic_options['list_image_custom'] != '' ? explode( "x",  $dynamic_options['list_image_custom'] ) : array();
		} 
		$img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt );
		
		$lazy_opt = independentThemeOpt::independentStaticThemeOpt('news-lazy-load');
		if( $lazy_opt ){
			$lazy_img = independentThemeOpt::independentStaticThemeOpt('news-lazy-load-img');
			if( isset( $lazy_img['id'] ) && $lazy_img['id'] != '' ){
				$pre_img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt, absint( $lazy_img['id'] ) );
				$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="'. esc_url( $pre_img_prop[0] ) .'" data-src="' . esc_url( $img_prop[0] ) . '" />';
			}else{
				$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="' . esc_url( $img_prop[0] ) . '" />';
			}
		}else{
			$image = '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive post-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '" />';
			
		}
		
		$output .= '<div class="post-thumb-wrapper"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'.  $image ;
		
		$post_icon = isset( $dynamic_options['post_icon'] ) ? esc_attr( $dynamic_options['post_icon'] ) : '';
		if( $post_icon == 'yes' ){
			$format = get_post_format( get_the_ID() );
			$post_formats_array = array("image"=>"icon-picture", "video"=>"icon-camrecorder", "audio"=>"icon-music-tone-alt", "gallery"=>"icon-wallet", "quote"=>"fa fa-quote-right", "link"=>"icon-link");
			if( $format != '' ){
				if (array_key_exists($format, $post_formats_array)){
					$output .= '<span class="post-format-icon '. $post_formats_array[$format] .'">';
					if( $format == 'gallery' ){
						$img_ids = get_post_meta( get_the_ID(), 'independent_post_gallery', true );
						if( $img_ids != '' ){
							$img_count = count( explode( ",", $img_ids ) );
							$output .= '<span class="img-count">'. esc_html( $img_count ) .'</span>';
						}
					}
					$output .= '</span>';
				}
			}
		}
		$output .= '</a>';
		
		if( isset( $dynamic_options['post_promotion'] ) && $dynamic_options['post_promotion'] == 'yes' ){
			$promotion_opt = get_post_meta( get_the_ID(), 'independent_post_promotion_opt', true );
			$promotion_text = independentThemeOpt::independentStaticThemeOpt('news-promotion-text');
			if( $promotion_opt == 'yes' ){
				$output .= '<span class="post-promotion">'. esc_html( $promotion_text ) .'</span>';
			}
		}
		
		$output .= '</div>';
	}//has_post_thumbnail
	return $output;
}
function independent_common_block_grid_content($excerpt_len){	
	ob_start();
	if( $excerpt_len <= 55 ){
		the_excerpt(); // for trim shortcodes and only gives text
	}else{
		the_content();
	}
	$content = ob_get_clean();
	$content = preg_replace("/\[caption(.*)\[\/caption\]/i", '', $content);
    $content = preg_replace('`\[[^\]]*\]`','',$content);
	$content = stripslashes(wp_filter_nohtml_kses($content));
	return '<div class="post-content-wrapper">'. wp_trim_words( $content , absint( $excerpt_len ) ) .'</div>';
}
function independent_general_block_meta( $meta_items_part, $read_more, $cat_tag_class = '' ){
	
	unset( $meta_items_part['disabled'] );
	
	//print_r( $meta_items_part ); echo "</br>";
	$meta_out = $output = '';
	if( isset( $meta_items_part ) && !empty($meta_items_part) ){
		$output = '<div class="news-meta-wrapper clearfix">';
		foreach( $meta_items_part as $meta_key => $meta_items ){
			$meta_class = $meta_key == 'Right' ? ' right-meta' : '';
			$output .= '<ul class="nav post-meta'. esc_attr( $meta_class ) .'">';
			foreach( $meta_items as $items => $lable ){
				switch( $items ){
					case "author-with-image" : $output .= independent_common_block_meta_authorimg();
					break;		
					case "author" : $output .= independent_common_block_meta_author();
					break;		
					case "date" : $output .= independent_common_block_meta_date();
					break;		
					case "comments" : $output .= independent_common_block_meta_comment();
					break;
					case "read-more" : $output .= independent_common_block_meta_readmore( $read_more );
					break;
					case "likes" : $output .= independent_common_block_meta_likes();
					break;
					case "views" : $output .= independent_common_block_meta_views();
					break;
					case "favourite" : $output .= independent_common_block_meta_favourite();
					break;
					case "category" : $output .= independent_common_block_meta_category();
					break;
					case "category-tag" : $output .= independent_common_block_meta_category_tag( $cat_tag_class );
					break;
					case "rating" : $output .= independent_common_block_meta_rating(get_the_ID());
					break;
					case "share" : $output .= independent_common_block_meta_shares(get_the_ID());
					break;
				}
			}
			$output .= '</ul>';
		}
		$output .= '</div>';
	}//isset
	return $output;
}
function independent_common_block_meta($dyn_meta, $dynamic_options){
	
	$meta_out = $output = '';
	if( isset( $dyn_meta ) && !empty($dyn_meta) ){
		foreach( $dyn_meta as $items ){
			switch( $items ){
				case "author-with-image" : $meta_out .= independent_common_block_meta_authorimg();
				break;		
				case "author" : $meta_out .= independent_common_block_meta_author();
				break;		
				case "date" : $meta_out .= independent_common_block_meta_date();
				break;		
				case "comments" : $meta_out .= independent_common_block_meta_comment();
				break;
				case "read-more" : $meta_out .= independent_common_block_meta_readmore($dynamic_options['read_more']);
				break;
				case "likes" : $meta_out .= independent_common_block_meta_likes();
				break;
				case "views" : $meta_out .= independent_common_block_meta_views();
				break;
				case "category" : $meta_out .= independent_common_block_meta_category();
				break;
				case "rating" : $meta_out .= independent_common_block_meta_rating(get_the_ID());
				break;
				case "share" : $meta_out .= independent_common_block_meta_shares(get_the_ID());
				break;
			}
		}
		$comments_count = wp_count_comments(get_the_ID());
		$output = '<div class="post-meta-wrapper">
						<ul class="nav post-meta">';
							$output .= $meta_out;
		$output .= '	</ul>
					</div>';
	}//isset
	return $output;
}
function independent_common_block_meta_authorimg(){
	return '<li class="post-author"><a rel="bookmark" class="media" href="'. esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) .'">'. independent_common_block_meta_author_image() .'<span class="media-body">'. independent_common_block_meta_author_name() .'</a></li>';
}
function independent_common_block_meta_author(){
	return '<li class="post-author"><a rel="bookmark" class="author-with-icon" href="'. esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) .'">'. independent_common_block_meta_author_name() .'</a></li>';
}
function independent_common_block_meta_author_image(){
	return get_avatar( get_the_author_meta( 'ID' ), 22, null, null, array( 'class' => 'img-circle' ) );
}
function independent_common_block_meta_author_name(){
	return '<span class="author-name">'. get_the_author() .'</span>';
}
function independent_common_block_meta_date(){
	$archive_year  = get_the_time('Y');
	$archive_month = get_the_time('m'); 
	$archive_day   = get_the_time('d');
	$title = get_the_title();
	if( isset( $title ) && !empty($title) )
		return '<li class="post-date"><a href="'. esc_url( get_day_link( $archive_year, $archive_month, $archive_day) ).'" ><span class="before-icon fa fa-clock-o"></span>'. get_the_time( get_option('date_format') ) .'</a></li>';
	else
		return '<li class="post-date"><a href="'. esc_url( get_permalink() ) .'" ><span class="before-icon fa fa-clock-o"></span>'. get_the_time( get_option('date_format') ) .'</a></li>';
}
function independent_common_block_meta_comment(){
	$comments_count = wp_count_comments(get_the_ID());
	return '<li class="post-comment"><a href="'. esc_url( get_comments_link( get_the_ID() ) ).'" rel="bookmark" class="comments-count-wrap"><span class="before-icon icon-speech"></span><span class="comments-count">'.$comments_count->total_comments.'</span></a></li>';
}
function independent_common_block_meta_readmore($read_more){
	return '<li class="post-read-more"><a class="read-more" href="'. esc_url( get_permalink( get_the_ID() ) ) . '">'. esc_html( $read_more ) .'</a></li>';
}
function independent_common_block_meta_likes(){
	$nps = new independentPostSettings;
	return '<li class="post-likes">'. $nps->independentMetaLikes(get_the_ID()) .'</li>';
}
function independent_common_block_meta_favourite(){
	$nps = new independentPostSettings;
	return '<li class="post-likes">'. $nps->independentMetaFavourite(get_the_ID()) .'</li>';
}
function independent_common_block_meta_views(){
	if( get_post_meta( get_the_ID(), 'independent_post_views_count', true ) )
	return '<li class="post-item-views"><span class="before-icon fa fa-eye"></span><span class="views-count">'. esc_attr( get_post_meta( get_the_ID(), 'independent_post_views_count', true ) ) .'</span></li>';
	
	return '';
}
function independent_common_block_meta_category(){
	$categories = get_the_category(); 
	$output = '';
	if ( ! empty( $categories ) ) {
		$output = '<li class="post-category"><a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a></li>';
	}
	return $output;
}
function independent_common_block_meta_category_tag( $cat_tag_class ){
	$categories = get_the_category(); 
	$output = '';
	if ( ! empty( $categories ) ) $output = '<li class="post-category"><a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" class="cat-tag-'. esc_attr( $categories[0]->term_id ) .' typo-white category-tag'. esc_attr( $cat_tag_class ) .'" >'. esc_attr( $categories[0]->name ) .'</a></li>';
	return $output;
}
function independent_common_block_meta_shares($post_id){
	ob_start();
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id), 'independent-large' );
?>
	<li class="post-share-wrapper">
		<ul class="nav social-icons">
			<li><a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink( $post_id )); ?>&t=<?php echo urlencode(get_the_title()); ?>" target="blank" class="social-facebook share-fb"><i class="fa fa-facebook"></i></a></li>
			<li><a href="http://twitter.com/home?status=<?php echo urlencode(get_the_title()); ?> <?php echo  esc_url( home_url( '/' ) )."?p=". $post_id; ?>" class="social-twitter share-twitter" title="<?php esc_attr_e( 'Click to send this page to Twitter!', 'independent' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
			<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php esc_url( the_permalink() ); ?>&title=<?php echo urlencode(get_the_title()); ?>&summary=&source=<?php echo bloginfo('name'); ?>" class="social-linkedin share-linkedin" target="_new"><i class="fa fa-linkedin"></i></a></li>
			<li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="social-google-plus share-gplus" target="blank"><i class="fa fa-google-plus"></i></a></li>
			<li><a href="http://pinterest.com/pin/create/button/?url=<?php esc_url( the_permalink() ); ?>&amp;media=<?php echo ( ! empty( $image[0] ) ? $image[0] : '' ); ?>&description=<?php echo urlencode(get_the_title()); ?>" class="social-pinterest share-pinterest" target="blank"><i class="fa fa-pinterest"></i></a></li>
		</ul>
	</li>
<?php
	$output = ob_get_clean(); 
	return $output;
}
function independent_common_block_meta_rating($post_id){
	ob_start();
	echo independent_zozo_columns_content( 'star_ratings', $post_id );
	$output = ob_get_clean();
	return '<li class="post-rating"><span>'. $output .'</span></li>'; 
}
/*Pagination Shortcode Function*/
function independent_pagination_shortcode($new_block, $short_content){
	if( $short_content != '' && $new_block != '' ){
		$re = "/(?<=independent_vc_block_).*?(?=\\s)/";     
		preg_match($re, $short_content, $matches);
		if( isset( $matches ) && $matches[0] != '' )
		$short_content =  str_replace('independent_vc_block_'.$matches[0], 'independent_vc_block_'.$new_block, $short_content);
		
		return $short_content;
	}else{
		return 0;
	}
}
function independent_text_trim($values){
	$values = str_replace(' ', '', $values);
	$values = rtrim($values, ',');
	return $values;
}
//Get Image Attributes WordPress Code
function independent_get_image_sizes($_size) {
	global $_wp_additional_image_sizes;
	$sizes = array();
	if ( in_array( $_size, array('thumbnail', 'medium', 'large') ) ) {
		$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
		$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
	} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
		$sizes[ $_size ] = array(
			'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
			'height' => $_wp_additional_image_sizes[ $_size ]['height'],
		);
	}
	return $sizes;
}
/*Subcategory Function*/ 
if( ! function_exists('independent_sub_category_listing') ) {
	function independent_sub_category_listing($cat_id){
		/*get sub category*/
		$args = array(
		'type'                     => 'post',
		'child_of'                 => $cat_id,
		'orderby'                  => 'name',
		'hide_empty'               => FALSE,
		'hierarchical'             => 1,
		'taxonomy'                 => 'category',
		); 
		$child_categories = get_categories($args );
		$sub_cats = '';
		if ( !empty ( $child_categories ) ){
			foreach ( $child_categories as $child_category ){
				$sub_cats .= $child_category->name .','. get_category_link( $child_category->term_id ) .',';
			}
			 $sub_cats = rtrim($sub_cats,',');
		}
		
		if( strpos($sub_cats,",") ){
			return $sub_cats;
			
		}else{
			return get_cat_name( $cat_id ) .','. get_category_link( $cat_id );
		}
		return $sub_cats;
	}
}
/*News Loader Image*/
if( ! function_exists('independent_news_loader') ) {
	function independent_news_loader(){
		$output = '';
		$zozo_options = get_option( 'independent_options' );
		$output = isset( $zozo_options['news-loader-img']['url'] ) && $zozo_options['news-loader-img']['url'] != '' ? esc_url( $zozo_options['news-loader-img']['url'] ) : esc_url( INDEPENDENT_ASSETS . '/images/news-loader.gif' );
		
		return $output;
	}
}
/*Block Dynamic Css*/
function independent_block_dynamic_css($independent_block_class, $style_arr){
	$independent_block_styles = '';
	$block_color = isset( $style_arr['block_color'] ) ? $style_arr['block_color'] : '';
	$title_transform = isset( $style_arr['title_transform'] ) ? $style_arr['title_transform'] : '';
	$title_size = isset( $style_arr['title_size'] ) ? $style_arr['title_size'] : '';
	
	if( $block_color != '' ){
		$independent_block_styles .= '.'. $independent_block_class .' .slide-nav > li > a, .'. $independent_block_class .' .dropdown-menu > li > a:focus, .'. $independent_block_class .' .dropdown-menu > li > a:hover, .'. $independent_block_class .' .dropdown.sub-dropdown:hover > a, .'. $independent_block_class .' .independent-block-title.title-style-1:before, .'. $independent_block_class .' .post-thumb-wrapper .category-tag { background-color: '. $block_color .' !important;}';
		
		$independent_block_styles .= '.'. $independent_block_class .' .independent-block-title span { background-color: '. $block_color .';}';
		
		$independent_block_styles .= '.'. $independent_block_class .' a:hover,.independent-block-css-3  a:focus, .'. $independent_block_class .' .typo-white a:hover, .'. $independent_block_class .' .typo-white a:focus, .'. $independent_block_class .' .post-meta-wrapper .post-meta > li:before, .'. $independent_block_class .' .post .post-meta-wrapper .post-read-more::after, .'. $independent_block_class .' .dropdown:hover > a, .dropdown:focus > a, .'. $independent_block_class .' .post-rating i {color: '. $block_color .' !important;}';
		
		$independent_block_styles .= '.'. $independent_block_class .' .post-thumb-wrapper a.category-tag:hover{color: #fff !important;}';
		$independent_block_styles .= '.'. $independent_block_class .' .post-thumb-wrapper a.category-tag:hover{background-color: #333 !important;}';
		
		$independent_block_styles .= '.'. $independent_block_class .' .independent-block-title { border-color:'. esc_attr( $block_color ) .'; }';
		
		$independent_block_styles .= '.'. $independent_block_class .'.block-type-2 { border-top-color:'. esc_attr( $block_color ) .'; }';
		
		$independent_block_styles .= '.'. $independent_block_class .' .nav.nav-tabs{ border-bottom-color: '. $block_color .' !important;}';
		$independent_block_styles .= '.'. $independent_block_class .' ::-moz-selection{background:'. $block_color .' !important;}';
		$independent_block_styles .= '.'. $independent_block_class .' ::selection{background:'. $block_color .' !important;}';
		$independent_block_styles .= '.'. $independent_block_class .' ::-webkit-selection{background:'. $block_color .' !important;}';
		$independent_block_styles .= '.'. $independent_block_class .' .dropdown-menu{border-color: '. $block_color .' !important;}';
		$independent_block_styles .= '.'. $independent_block_class .' .slide-nav > li > a:hover, .'. $independent_block_class .' .slide-nav > li > a:focus, .'. $independent_block_class .' .dropdown-menu > li > a:focus, .'. $independent_block_class .' .dropdown-menu > li > a:hover, .'. $independent_block_class .' .dropdown.sub-dropdown:hover > a {color:#fff !important;}';
	}
	
	if( $title_size ){
		$independent_block_styles .= '.'. $independent_block_class .' .independent-block-title span { font-size: '. esc_attr( $title_size ) .'; }';
	}
	
	if( $title_transform != '' ){
		$independent_block_styles .= '.'. $independent_block_class .' .independent-block-title span { text-transform: '. esc_attr( $title_transform ) .'; }';
	}
	
	return $independent_block_styles;
}
class independentBlockParams{
	
	function setindependentBlockParams( $block_id, $block_val ){
		global $block_params;
		$block_params[$block_id] = $block_val;
	}
	
	function setFilterindependentBlockParams(){
		global $block_params;
		wp_localize_script('independent-block', 'independent_block_var', array(
			'independent_block_params' => $block_params
		));
	}
}
add_action('wp_ajax_independent-redux-block', 'independent_redux_news_block');
add_action('wp_ajax_nopriv_independent-redux-block', 'independent_redux_news_block'); 
function independent_redux_news_block(){
	$nonce = $_POST['nonce'];  
    if ( ! wp_verify_nonce( $nonce, 'independent-redux-news-block' ) )
        die ( esc_html__( 'Busted', 'independent' ) );
	
	$json_array = array();
	
	if( isset( $_POST['block_id'] ) && $_POST['block_id'] != '' ){
		$block_id = $_POST['block_id'];
		$saved_stat = isset( $_POST['saved_stat'] ) ? $_POST['saved_stat'] : '';
		$saved_array = $_POST;
		if( $saved_stat ){
			//parse_str($saved_values, $saved_array);
			unset($saved_array['action']);
			unset($saved_array['nonce']);
			unset($saved_array['block_id']);
			unset($saved_array['saved_stat']);
		}
		
		wp_enqueue_script( 'independent-drag-drop', INDEPENDENT_CORE_URL . 'assets/js/drag-drop.js', array( 'jquery' ) );
		wp_enqueue_style( 'wp-color-picker' ); 
		
		include_once INDEPENDENT_THEME_ELEMENTS . "/template-block-class.php";
		$redux_block = new IndependentReduxNewsBlock();
		ob_start();
		$redux_block->independent_set_news_block_id( $block_id, $saved_array );
		$output = ob_get_clean();
		
		$json_array['block_form'] = $output;
	}
	
	echo json_encode( $json_array );
	exit;
}
//Drag and Drop Values Trimming
function independent_drag_and_drop_trim( $values ){
	$step_1 = str_replace( "``", '"', $values );
	$step_2 = str_replace( "`", '', $step_1 );
	$json_values = str_replace( '{"}', '{}', $step_2 );
	$array_values =  json_decode( $json_values, true );
	return $array_values;
}