<?php
/**
 * independent Default Banner Functions
 */
function independent_singleBannerPost($atts, $thumb_size, $author_key){
	extract( $atts );
	
	$as_bg = isset( $as_bg ) && $as_bg == 'yes' ? true : false;
	$text_align = isset( $title_align ) ? esc_attr( $title_align ) : 'left';
	$overlay_position = isset( $overlay_position ) ? esc_attr( $overlay_position ) : 'bottom-left'; 
	$post_id = get_the_ID();
	
	$overlay_class = $overlay_parent_class = '';
	if( isset( $banner_overlay ) && $banner_overlay != '' ){
		$overlay_parent_class = ' hvr-overlay-wrapper';
		switch( $banner_overlay ){
			case 'black':
				$overlay_class = 'gradient-black-overlay';
			break;
			case 'theme':
				$overlay_class = 'gradient-theme-overlay';
			break;
			case 'rainbow':
				$overlay_class = 'rainbow-overlay'; // rainbow color overlay
			break;
			default:
				$overlay_class = 'dark-overlay';
			break;
		}
	}
	
	$output = '<div class="banner-post-grid'. esc_attr( $overlay_parent_class ) .' typo-white details-'. esc_attr( $overlay_position ) .'">';
	$output .= '<div class="banner-grid-thumb">';
	
		//Post Format Icon
		if( isset( $icon_enable ) && $icon_enable == 'yes' ){
			$format = get_post_format( $post_id );
			$post_formats_array = array("image"=>"icon-picture", "video"=>"icon-control-play", "audio"=>"icon-music-tone-alt", "gallery"=>"icon-wallet", "quote"=>"fa fa-quote-right", "link"=>"icon-link");
			if( $format != '' ){
				if (array_key_exists($format, $post_formats_array)){
					$output .= '<span class="post-format-icon '. $post_formats_array[$format] .'">';
					if( $format == 'gallery' ){
						$img_ids = get_post_meta( $post_id, 'independent_post_gallery', true );
						if( $img_ids != '' ){
							$img_count = count( explode( ",", $img_ids ) );
							$output .= '<span class="img-count">'. esc_html( $img_count ) .'</span>';
						}
					}
					$output .= '</span>';
				}
			}
		}
		
		$feat_stat = get_post_meta( $post_id, 'independent_post_featured_stat', true );
		if( isset( $trend_enable ) && $trend_enable == 'yes' && $feat_stat == 1 ){
			$output .=  '<span class="news-trend"><i class="icon-energy icons"></i></span>';
		}
	
		add_filter( 'wp_calculate_imag'.'e_srcs'.'et_meta', '__return_null' );
		$output .= '<a href="'. esc_url( get_permalink() ) .'" rel="bookmark">';
		if( !$as_bg ){
			$img_prop = independent_custom_image_size_chk( $thumb_size, '' );
			
			$lazy_opt = independentThemeOpt::independentStaticThemeOpt('news-lazy-load');
			if( $lazy_opt ){
				$lazy_img = independentThemeOpt::independentStaticThemeOpt('news-lazy-load-img');
				if( isset( $lazy_img['id'] ) && $lazy_img['id'] != '' ){
					$pre_img_prop = independent_custom_image_size_chk( $thumb_size, '', absint( $lazy_img['id'] ) );
					$output .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-fluid post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="'. esc_url( $pre_img_prop[0] ) .'" data-src="' . esc_url( $img_prop[0] ) . '" />';
				}else{
					$output .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-fluid post-thumbnail lazy-initiate" alt="'. esc_attr( get_the_title() ) .'" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="' . esc_url( $img_prop[0] ) . '" />';
				}
			}else{
				$output .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-fluid post-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '" />';
			}
		}else{
			$featured_img_url = get_the_post_thumbnail_url( $post_id, 'large' );
			$output .= '<span class="banner-as-bg" data-src="'. esc_url( $featured_img_url ) .'"></span>';
		}
			$output .= $banner_overlay != '' ? '<span class="'. esc_attr( $overlay_class ) .'"></span>' : '';
		$output .= '</a>';
		$rem_filt = 'remov'.'e_filter';
		$rem_filt( 'wp_calculate_i'.'mage_src'.'set_meta', '__return_null' );
	$output .= '</div>';
	$output .= '<div class="banner-grid-details text-'. esc_attr( $text_align ) .'">';
		
		if( $filter_enable == 'yes' && $filter_name == 'cat' ){
			$categories = get_the_category();
			$output .= '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" class="cat-tag-'. esc_attr( $categories[0]->term_id ) .' typo-white category-tag" >'. esc_attr( $categories[0]->name ) .'</a>';
		}elseif( $filter_enable == 'yes' && $filter_name == 'tag' ){
			$tags_list = get_the_tags();
			$output .= '<a href="' . esc_url( get_tag_link( $tags_list[0]->term_id ) ) . '" class="typo-white category-tag" >'. esc_attr( $tags_list[0]->name ) .'</a>';
		}elseif( $filter_enable == 'yes' && $filter_name == 'author' ){
			$output .= '<div class="banner-author"><a rel="bookmark" href="'. esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) .'">'. independent_common_block_meta_author_image() .' '. independent_common_block_meta_author_name() .'</a></div>'; //independent_common_block_meta_author_image
		}
		if( $author_key == 1 ){
			$output .= '<h3 class="banner-post-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. get_the_title() .'</a></h3>';
		}elseif( $author_key == 2 ){
			$output .= '<h4 class="banner-post-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. get_the_title() .'</a></h4>';
		}else{
			$output .= '<h5 class="banner-post-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. get_the_title() .'</a></h5>';
		}
		
		$archive_year  = get_the_time('Y'); 
		$archive_month = get_the_time('m'); 
		$archive_day   = get_the_time('d');
		
		if( $author_key == 1 ){
			$output .= '<p class="banner-author-meta">';
				if( $author_enable == 'yes'  ){
					$output .= '<a class="author-link" rel="bookmark" href="'. esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) .'">'. get_the_author() .'</a>';
				}
				if( $date_enable == 'yes'  ){
					$output .= '<a class="banner-date" href="'. esc_url( get_day_link( $archive_year, $archive_month, $archive_day) ) .'" ><span class="before-icon fa fa-clock-o"></span><span>'. get_the_time( get_option('date_format') ) .'</span></a>';
				}
			$output .= '</p>';
		}
	$output .= '</div>';
	$output .= '</div><!--block-post-grid-->';
	
	return $output;
}
/*Banner Style 1*/
function independent_banner_layout_1($i, $atts){
	$output = $item_n_class = '';	
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-50x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x50', 2) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 2 ){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-50x100 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_50x100', 1) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_1($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					<div class="banner-inner-parent banner-grid-itemw2">
						'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
					</div>
						'. ( array_key_exists('3', $banner_out_array) ? $banner_out_array[3] : '' ) .'
				</div>';
	return $output;
}
/*Banner Style 2*/
function independent_banner_layout_2($i, $atts){
	$output = $item_n_class = '';
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-50x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x50', 2) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 2 ){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 3 ){
		$output = '<div class="banner-grid-item banner-grid-25x100 '. esc_attr( $item_n_class ) .' banner-grid-itemw4">
					'. independent_singleBannerPost($atts, 'independent_banner_25x100', 0) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-25x100 '. esc_attr( $item_n_class ) .' banner-grid-itemw4">
					'. independent_singleBannerPost($atts, 'independent_banner_25x100', 0) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_2($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					<div class="banner-inner-parent banner-grid-itemw2">
						'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
					</div>
						'. ( array_key_exists('3', $banner_out_array) ? $banner_out_array[3] : '' ) .'
						'. ( array_key_exists('4', $banner_out_array) ? $banner_out_array[4] : '' ) .'
				</div>';
	return $output;
}
/*Banner Style 3*/
function independent_banner_layout_3($i, $atts){
	$output = $item_n_class = '';
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-50x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x50', 2) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-50x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x50', 2) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-100x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_100x50', 1) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_3($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					<div class="banner-inner-parent banner-grid-itemw2">
						'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
					</div>
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
				</div>';
	return $output;
}
/*Banner Style 4*/
function independent_banner_layout_4($i, $atts){
	$output = $item_n_class = '';
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 2 ){
		$output = '<div class="banner-grid-item banner-grid-25x100 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x100', 1) .'
					</div>';
	}elseif( $i == 3){
		$output = '<div class="banner-grid-item banner-grid-50x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_50x50', 2) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-50x50 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_50x50', 2) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_4($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					<div class="banner-inner-parent banner-grid-itemw4">
						'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
					</div>
					<div class="banner-inner-parent banner-grid-itemw4">
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
					</div>
						'. ( array_key_exists('3', $banner_out_array) ? $banner_out_array[3] : '' ) .'
						'. ( array_key_exists('4', $banner_out_array) ? $banner_out_array[4] : '' ) .'
				</div>';
	return $output;
}
/*Banner Style 5*/
function independent_banner_layout_5($i, $atts){
	$output = $item_n_class = '';
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-50x100 '. esc_attr( $item_n_class ) .' banner-grid-itemw2 banner-height">
					'. independent_singleBannerPost($atts, 'independent_banner_50x100', 1) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-50x33 '. esc_attr( $item_n_class ) .' banner-grid-itemh3">
					'. independent_singleBannerPost($atts, 'independent_banner_50x33', 2) .'
					</div>';
	}elseif( $i == 2 ){
		$output = '<div class="banner-grid-item banner-grid-50x33 '. esc_attr( $item_n_class ) .' banner-grid-itemh3">
					'. independent_singleBannerPost($atts, 'independent_banner_50x33', 2) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-50x33 '. esc_attr( $item_n_class ) .' banner-grid-itemh3">
					'. independent_singleBannerPost($atts, 'independent_banner_50x33', 2) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_5($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
					<div class="banner-inner-parent banner-grid-itemw2">
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
						'. ( array_key_exists('3', $banner_out_array) ? $banner_out_array[3] : '' ) .'
					</div>
				</div>';
	return $output;
}
/*Banner Style 6*/
function independent_banner_layout_6($i, $atts){
	$output = $item_n_class = '';
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-25x66 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x66', 2) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-25x33 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x33', 0) .'
					</div>';
	}elseif( $i == 2 ){
		$output = '<div class="banner-grid-item banner-grid-50x33 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x33', 2) .'
					</div>';
	}elseif( $i == 3 ){
		$output = '<div class="banner-grid-item banner-grid-50x33 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x33', 2) .'
					</div>';
	}elseif( $i == 4){
		$output = '<div class="banner-grid-item banner-grid-50x33 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_50x33', 2) .'
					</div>';
	}elseif( $i == 5){
		$output = '<div class="banner-grid-item banner-grid-25x33 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x33', 0) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-25x66 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x66', 2) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_6($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					<div class="banner-inner-parent banner-grid-itemw4">
						'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
					</div>
					<div class="banner-inner-parent banner-grid-itemw2">
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
						'. ( array_key_exists('3', $banner_out_array) ? $banner_out_array[3] : '' ) .'
						'. ( array_key_exists('4', $banner_out_array) ? $banner_out_array[4] : '' ) .'
					</div>
					<div class="banner-inner-parent banner-grid-itemw4">
						'. ( array_key_exists('5', $banner_out_array) ? $banner_out_array[5] : '' ) .'
						'. ( array_key_exists('6', $banner_out_array) ? $banner_out_array[6] : '' ) .'
					</div>
				</div>';
	return $output;
}
/*Banner Style 7*/
function independent_banner_layout_7($i, $atts){
	$output = $item_n_class = '';
	extract( $atts );
	$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
	if( $banner_overlay == 'rainbow' )
	$item_n_class = 'banner-gradient-'. absint( $i + 1 );
	
	if( $i == 0 ){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 1){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}elseif( $i == 2 ){
		$output = '<div class="banner-grid-item banner-grid-25x100 '. esc_attr( $item_n_class ) .' banner-grid-itemw2">
					'. independent_singleBannerPost($atts, 'independent_banner_25x100', 1) .'
					</div>';
	}elseif( $i == 3){
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}else{
		$output = '<div class="banner-grid-item banner-grid-25x50 '. esc_attr( $item_n_class ) .'">
					'. independent_singleBannerPost($atts, 'independent_banner_25x50', 0) .'
					</div>';
	}
	return $output;
}
function independent_banner_output_7($banner_out_array){
	$output = '<div class="banner-grid-parent clearfix">
					<div class="banner-inner-parent banner-grid-itemw4">
						'. ( array_key_exists('0', $banner_out_array) ? $banner_out_array[0] : '' ) .'
						'. ( array_key_exists('1', $banner_out_array) ? $banner_out_array[1] : '' ) .'
					</div>
						'. ( array_key_exists('2', $banner_out_array) ? $banner_out_array[2] : '' ) .'
					<div class="banner-inner-parent banner-grid-itemw4">
						'. ( array_key_exists('3', $banner_out_array) ? $banner_out_array[3] : '' ) .'
						'. ( array_key_exists('4', $banner_out_array) ? $banner_out_array[4] : '' ) .'
					</div>
				</div>';
	return $output;
}