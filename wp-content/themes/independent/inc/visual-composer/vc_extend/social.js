!function($) {
	$('.social_followers_block .social-followers-img').on( "click", function(e){
		e.preventDefault();	
		var $img_id = $(this).attr('data-modal');
		var $parent = $(this).parents('.social_followers_block');
		$( $parent ).children('.social-followers-hidden').val($img_id);
		$( $parent ).children('.social-followers-img').removeClass('selected');
		$(this).addClass('selected');
	});    
}(window.jQuery);