!function($) {

	$( ".block-items-enabled" ).each(function( index ) {
		var auth_dis = $( this ).parent('div').find('.block-items-disabled');
		var cur_items = this;
		$( cur_items ).sortable({
		  connectWith: auth_dis,
		  update: function () {
			var $out = '';
			$( cur_items ).children( 'li' ).each(function( index ) {
				$out += $(this).attr('id') + ',';
			});
			$(cur_items).parents('.edit_form_line').children('.block-items-hidden').val($out);
		  }
		});
	});
	
	$( ".block-items-disabled" ).each(function( index ) {
		var auth_en = $( this ).parent('div').find('.block-items-enabled');
		$( this ).sortable({
		  connectWith: auth_en,
		});
	});
	
	jQuery( ".block-items-enabled" ).on( "sortstart", function( event, ui ) { 
		var parent = $( this ).parent('div').find('.block-items-disabled');
		$( parent ).addClass('droppable-active');
	});
	jQuery( ".block-items-disabled" ).on( "sortstart", function( event, ui ) { 
		var parent = $( this ).parent('div').find('.block-items-enabled');
		$( parent ).addClass('droppable-active');
	});
	jQuery( ".block-items-disabled, .block-items-enabled" ).on( "sortstop", function( event, ui ) { 
		var parent = $( this ).parent('div');
		$( parent ).find('.droppable-active').removeClass('droppable-active');
	});
	
	/*Drag and Drop Tools*/
	$( ".droppable .tools" ).each(function( index ) {
		$( this ).removeClass('draggable');
		$( this ).append('<span class="remove-element">x</span>');
	});
  	
  	$( ".droppable" ).sortable({
		  connectWith: "ul",
		  update: function () {
			var current_item = this;
			var $out = tools_values(current_item);
			$(current_item).parents('.edit_form_line').find('.tools-ids').val($out);
		  }
	});
	
    $( ".draggable" ).draggable({helper:'clone'});
	
    $( ".droppable" ).droppable({
      accept: ".draggable",
      drop: function( event, ui ) {
		var current_item = this;
	
		$(this).append($(ui.draggable).clone());
		$(this).find('.draggable').append('<span class="remove-element">x</span>');

		var $out = tools_values(current_item);
		
		$(current_item).parents('.edit_form_line').find('.tools-ids').val($out);
		$(current_item).find('.draggable').removeClass('draggable');
      }
    });
	
	/*remove element*/
	$('.droppable').on('click', '.remove-element', function() {
		var current_item = this;
		var parent = $(this).parents('.tools-header');
		$(current_item).parent().remove();
		var $out = tools_values(parent);
		$(parent).parents('.edit_form_line').find('.tools-ids').val($out);
	});

	function tools_values(current_item){
		var $out = '';
		$( current_item ).children( 'li' ).each(function( index ) {
			$out += $(this).attr('data-id') + ',';
		});
		return $out;
	}
	
}(window.jQuery);