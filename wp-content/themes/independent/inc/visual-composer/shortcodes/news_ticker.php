<?php 
/**
 * News Ticker shortcode
 * 
 */
if ( ! function_exists( 'independent_vc_news_ticker_shortcode' ) ) {
	function independent_vc_news_ticker_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( 'independent_vc_news_ticker', $atts );
		extract( $atts );
		$output = '';
		$output .= '<div class="news-ticker clearfix">';
			$output .= $title != '' ? '<div class="news-ticker-title theme-bg typo-white">'.$title.'</div>' : '';
			$output .= '<div class="news-ticker-slide hide independent-ticker">';
					
					$slide_delay = isset( $slide_delay ) ? absint($slide_delay) : '5000';
					$slide_nav = isset( $slide_nav ) ? $slide_nav : 'no';
					$data_attr = '';							
					$data_attr .= ' data-delay="'. esc_attr( $slide_delay ) .'" ';
					
					$output .= '<div id="easy-news-ticker'. independent_shortcode_rand_id() .'" class="easy-news-ticker" '.$data_attr.'>';
						$output .= '<ul>';
					$n_news = isset( $n_news ) && $n_news != 0 && $n_news != '' ? absint($n_news) : '2';
					$n_news = ( $n_news <= 2 ) ? 3 : absint( $n_news );
					
					$filter_slugs = esc_attr( $filter_slugs );
					if( $filter_tab == 'tag' ){
						$filter_tab = 'tag__in';
						$filter_slugs = explode(",", $filter_slugs);
					}
					
					$query = new WP_Query( array( esc_attr( $filter_tab ) => $filter_slugs, 'showposts' => $n_news ) );
					
					if ($query->have_posts()){
						while ($query->have_posts()){
							$query->the_post();
							$category = get_the_category();
							$output .= '<li>';
							$output .= '<a href="'. esc_url( get_category_link( $category[0]->term_id ) ) .'" class="news-ticker-category" rel="bookmark">'. $category[0]->cat_name .'</a> - ';
							$output .= '<a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. get_the_title() .'</a>';
							$output .= '</li>';		
						}
						wp_reset_postdata();
					}
						$output .= '</ul>';
					$output .= '</div><!--easy-news-ticker-->';
					if( $slide_nav == 'yes' ){
						$output .= '<div class="news-ticker-navs">';
							$output .= '<i class="fa fa-angle-left easy-ticker-prev banner-news-prev"></i><i class="fa fa-angle-right easy-ticker-next banner-news-next"></i>';
						$output .= '</div>';
					}
			$output .= '</div><!--news-ticker-slide-->';
		$output .= '</div><!--col-sm-12-->';
		return $output;
	}
}
if ( ! function_exists( 'independent_vc_news_ticker_shortcode_map' ) ) {
	function independent_vc_news_ticker_shortcode_map() {
				
		vc_map( 
			array(
				"name"					=> esc_html__( "News Ticker", "independent" ),
				"description"			=> esc_html__( "News Ticker Block.", 'independent' ),
				"base"					=> "independent_vc_news_ticker",
				"category"				=> esc_html__( "Blocks", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title', "independent" ),
						'param_name'	=> 'title',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Select Tag/Category", "independent" ),
						"param_name"	=> "filter_tab",
						"value"			=> array(
							esc_html__( "Category", "independent" )	=> "cat",
							esc_html__( "Tag", "independent" )		=> "tag",
						),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Filter by categories/tags slugs", "independent" ),
						"description"	=> esc_html__( "enter ID seprated by comma(,). eg: 3, 6", "independent" ),
						"param_name"	=> "filter_slugs",
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Number of News to Show", "independent" ),
						"description"	=> esc_html__( "enter number of new to show. eg: 6", "independent" ),
						"param_name"	=> "n_news",
					),
					//Slider
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Slide Delay', "independent" ),
						'param_name'	=> 'slide_delay',
						'value' 		=> '5000',
						'description'	=> esc_html__( 'Enter slide delay seconds in value(5000 = 5 Sec). eg: 5000', 'independent' ),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Slide Navigation", "independent" ),
						"param_name"	=> "slide_nav",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "yes",
							esc_html__( "No", "independent" )	=> "no",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'independent_vc_news_ticker_shortcode_map' );