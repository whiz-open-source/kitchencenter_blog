<?php 
/**
 * Independent Category Box
 */
if ( ! function_exists( "independent_vc_category_box_shortcode" ) ) {
	function independent_vc_category_box_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( "independent_vc_category_box", $atts );
		extract( $atts );
		$output = '';
	
		//Defined Variable
		$class_names = isset( $extra_class ) && $extra_class != '' ? ' ' . $extra_class : '';
		$cols = isset( $cols ) && $cols != '' ? $cols : '12';
		
		//Cats In
		$cats_avail = array();
		if( isset( $cat_ids ) && $cat_ids != '' ){
			$filter = preg_replace( '/\s+/', '', $cat_ids );
			$filter = explode( ',', rtrim( $filter, ',' ) );
			foreach( $filter as $cat ){
				if( term_exists( absint( $cat ), 'category' ) ){
					$cat_term = get_term_by( 'id', absint( $cat ), 'category' );	

					//post in array push
					if( isset( $cat_term->term_id ) )
						array_push( $cats_avail, absint( $cat_term->term_id ) );
				}
			}
		}
		
		if( !empty( $cats_avail ) ){
			$output .= '<div class="category-box-wrap'. esc_attr( $class_names ) .'">';
				$output .= '<div class="row">';
				foreach( $cats_avail as $cat_id ) {
					$image_id = get_term_meta ( absint( $cat_id ), 'category-image-id', true );	
					if( $image_id ){
						$feat_image_url = wp_get_attachment_url( $image_id );
						$term = get_term_by( 'id', absint( $cat_id ), 'category' );
						$output .= '<div class="col-md-'. esc_attr( $cols ) .'">';
							$output .= '<a href="'. esc_url( get_category_link( $cat_id ) ) .'" class="category-box-inner">';
								$output .= '<div class="category-box">';
									$output .= '<div class="category-thumb">';
										$output .= '<img src="'. esc_url( $feat_image_url ) .'" alt="'. esc_attr( $term->name ) .'" />';
									$output .= '</div><!-- .category-box -->';
										$output .= '<h6 class="category-box-name">'. esc_html( $term->name ) .'</h6>';
								$output .= '</div><!-- .category-box -->';
							$output .= '</a><!-- .category-box-inner -->';
						$output .= '</div><!-- .col -->';
					}
				}
				$output .= '</div><!-- .row -->';
			$output .= '</div><!-- .category-box-wrap -->';
		}
		
		return $output;
	}
}

if ( ! function_exists( "independent_vc_category_box_shortcode_map" ) ) {
	function independent_vc_category_box_shortcode_map() {
				
		vc_map( 
			array(
				"name"					=> esc_html__( "Category Box", "independent" ),
				"description"			=> esc_html__( "Category navigation box.", "independent" ),
				"base"					=> "independent_vc_category_box",
				"category"				=> esc_html__( "Shortcodes", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Extra Class", "independent" ),
						"param_name"	=> "extra_class",
						"value" 		=> ""
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Category ID\'s', "independent" ),
						'description'	=> esc_html__( 'Enter category id for which category image showing in box. example 14,2,7', 'independent' ),
						'param_name'	=> 'cat_ids',
						'value' 		=> ''
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Category Column", "independent" ),
						"param_name"	=> "cols",
						"value"			=> array(
							esc_html__( "1 Column", "independent" )		=> '12',
							esc_html__( "2 Columns", "independent" )	=> '6',
							esc_html__( "3 Columns", "independent" )	=> '4',
							esc_html__( "4 Columns", "independent" )	=> '3'
						),
						"std"	=> "4"
					),
				)
			) 
		);
	}
}
add_action( "vc_before_init", "independent_vc_category_box_shortcode_map" );