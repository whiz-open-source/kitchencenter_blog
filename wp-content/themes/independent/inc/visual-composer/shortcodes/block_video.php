<?php 
/**
 * Video News Block  
 */
 
if ( ! function_exists( 'independent_vc_block_video_playlist_shortcode' ) ) {
	function independent_vc_block_video_playlist_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( 'independent_vc_block_video_playlist', $atts );
		extract( $atts );
		
		$rand = independent_shortcode_rand_id();
	
		$output = $playlist = $playlist_first = '';
		
		$slide_animation = isset( $slide_animation ) ? esc_attr( $slide_animation ) : 'slideOutUp:slideInUp';
		$slide_nav = isset( $slide_nav ) ? esc_attr( $slide_nav ) : 'true';
		
		$css_class = isset( $extra_class ) ? $extra_class : '';
		$independent_block_class = 'independent-block-css-'.$rand ;
		
		$title_transform = isset( $title_transform ) ? esc_attr( $title_transform ) : 'capitalize';	
		$title_size = isset( $title_size ) && $title_size != '' ? $title_size : '17px';
		
		$style_arr = array(
			'title_transform' => $title_transform,
			'title_size' => $title_size
		);
		
		$independent_block_styles = '';
		if( $title_transform || $title_size != '' ){
			$css_class .= ' '.$independent_block_class;
			$independent_block_styles = independent_block_dynamic_css($independent_block_class, $style_arr);
		}
		
		$block_css_options = array(
			'block_style' => $independent_block_styles
		);
		
		$slide = explode(":",$slide_animation);
		$animatein = $animateout = '';
		if( count($slide) == 2 ){
			$animatein = $slide[1];
			$animateout = $slide[0];
		}else{
			$animatein = 'slideOutUp';
			$animateout = 'slideInUp';
		}
		
		$post_limit = isset( $post_limit ) && $post_limit != '' ? absint( $post_limit ) : '10';
		$auto_play = isset( $auto_play ) ? esc_attr( $auto_play ) : 'no';
		$player_modal = isset( $player_modal ) ? esc_attr( $player_modal ) : '1';
		$video_playlist_class = $player_modal == '4' ? ' no-play-trigger' : '';
		$parent_class = ' player-type-' . esc_attr( $player_modal );
		$ul_class = $player_modal == '3' ? ' nav navbar-nav' : '';
		
		if( $player_modal == '3' ){
			//$ul_class .=  ' owl-carousel';
		}
		
		$video_playlist_class .= $css_class != '' ? ' ' . $css_class : '';
		
		$query = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'ignore_sticky_posts' => 1,
			'nopaging' => true,
			'posts_per_page' => $post_limit,
			'tax_query' => array(
				array(                
					'taxonomy' => 'post_format',
					'field' => 'slug',
					'terms' => array( 
						'post-format-video'
					),
					'operator' => 'IN'
				)
			)
		);
		
		$video_opt = isset( $video_opt ) ? $video_opt : '';
		if( $video_opt == 'category' ){
			$query['category__in'] = isset( $video_categories ) && $video_categories != '' ? explode( ",", trim( $video_categories ) ) : '';
		}elseif( $video_opt == 'post' ){
			$query['post__in'] = isset( $video_posts ) && $video_posts != '' ? explode( ",", trim( $video_posts ) ) : '';
		}		
		
		$play_visibile = $player_modal == '4' ? 3 : 4;
		
		$loop = new WP_Query($query);
		if ($loop->have_posts()){
			$playlist .= '<div class="video-playlist-wrapper video-playlist-slider'. esc_attr( $video_playlist_class ) .'" data-visible="'. esc_attr( $play_visibile ) .'">';
				$playlist .= '<ul class="video-playlist-slide'. esc_attr( $ul_class ) .'" data-items="4" data-items-tab="2" data-items-mob="1" data-loop="1" data-nav="1" data-dots="0">';
			$limit = 5; $count = 1; $i = 0;
			$first_video_stat = 0;
			while ($loop->have_posts()){
			
				$loop->the_post();
				
				$video_type = get_post_meta( get_the_ID(),'independent_post_video_type', true );
				$video_id = get_post_meta( get_the_ID(),'independent_post_video_id', true );
				$video_url = '';
				if( $video_type == 'youtube' ){
					$video_url = 'https://www.youtube.com/embed/' . $video_id;
				}elseif( $video_type == 'vimeo' ){
					$video_url = 'https://player.vimeo.com/video/' . $video_id;
				}elseif( $video_type == 'custom' ){
					$video_url = $video_id;
				}
				$playlist .= '<li class="post post-list video-playlist clearfix">';
				
					if( $player_modal == '4' ){
						$playlist .= '<div class="video-poster post-list-thumb">';
							$playlist .= '<div class="post-thumb-wrapper">';
								$playlist .= '<div data-frame="video-playlist-frame-'.$rand.'" data-video="'. esc_url( $video_url ) .'" data-play="'. esc_attr( $auto_play ) .'" class="video-play" rel="bookmark">';	
									$img_prop = independent_custom_image_size_chk( '', 'independen-grid-medium' );
									$playlist .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive video-list-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '"/>';
									$playlist .= '<span class="fa fa-play post-format-icon"></span>';
								$playlist .= '</div>';
							$playlist .= '</div>';
							$playlist .= '<ul class="list-inline">';
								$playlist .= '<li class="video-duration"></li>';
								$count = get_post_meta( get_the_ID(), 'independent_post_views_count', true );
								if( $count )
								$playlist .= '<li class="post-item-views pull-right"><span class="icon-eye"></span><span>'. esc_attr( $count ) .'</span></li>';
							$playlist .= '</ul>';
						$playlist .= '</div><!--video-poster-->';
						
						if( !$first_video_stat ){
							$playlist_first .= '<div class="video-poster post-list-thumb">';
								$playlist_first .= '<div class="post-thumb-wrapper">';
									$playlist_first .= '<div data-frame="video-playlist-frame-'.$rand.'" data-video="'. esc_url( $video_url ) .'" data-play="'. esc_attr( $auto_play ) .'" class="video-play" rel="bookmark">';	
										$img_prop = independent_custom_image_size_chk( '', 'large' );
										$playlist_first .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive video-list-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '"/>';
										$playlist_first .= '<span class="fa fa-play post-format-icon"></span>';
									$playlist_first .= '</div>';
								$playlist_first .= '</div>';
							$playlist_first .= '</div><!--video-poster-->';
						}
						
					}else{
						$playlist .= '<div class="video-poster post-list-thumb">';
							$playlist .= '<div class="post-thumb-wrapper">';
								$playlist .= '<div data-frame="video-playlist-frame-'.$rand.'" data-video="'. esc_url( $video_url ) .'" data-play="'. esc_attr( $auto_play ) .'" class="video-play" rel="bookmark">';	
									$img_prop = independent_custom_image_size_chk( '', array( 130, 80 ) );
									$playlist .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-responsive video-list-thumbnail" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '"/>';
									$playlist .= '<span class="fa fa-play post-format-icon"></span>';
								$playlist .= '</div>';
							$playlist .= '</div>';
						$playlist .= '</div><!--video-poster-->';
						$playlist .= '<div class="video-content post-list-content">';
							$playlist .= '<div class="post-title-wrapper">';
								$playlist .= '<h6 class="post-title video-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. get_the_title() .'</a></h6>';
							$playlist .= '</div><!--title-wrapper-->';
							$playlist .= '<ul class="list-inline">';
								$playlist .= '<li class="video-duration"></li>';
								$count = get_post_meta( get_the_ID(), 'independent_post_views_count', true );
								if( $count )
								$playlist .= '<li class="post-item-views pull-right"><span class="icon-eye"></span><span>'. esc_attr( $count ) .'</span></li>';
							$playlist .= '</ul>';
						$playlist .= '</div><!--video-content-->';
					}
						
				$playlist .= '</li>';
				$first_video_stat = 1;
			}
				$playlist .= '</ul>';
			$playlist .= '</div>';
			wp_reset_postdata();
		}
		if( $player_modal == '1' ){
		
			$output .= '<div class="independent-block'. esc_attr( $parent_class ) .' '. esc_attr( $css_class ) .'" data-id="independent_block_id_'.esc_attr( $rand ).'">';
			
			if( $independent_block_styles != '' )
			$output .= '<input type="hidden" class="news-block-css-options" data-options="'. htmlspecialchars(json_encode($block_css_options), ENT_QUOTES, 'UTF-8') .'" />';
		
			$title_class = independent_blocksTitleAlign($title_position);
			$title_class .= isset( $title_style ) && $title_style != '' ? ' title-style-' . $title_style : '';
			if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_attr( $title ) .'</span></h4>';
				$output .= '<div class="row">';
					$output .= '<div class="col-lg-8 col-sm-12">';
						$output .= '<div class="video-player">';						
							$output .= $playlist_first;
						$output .= '</div>';
					$output .= '</div>';
					$output .= '<div class="col-lg-4 col-sm-12">';
						$output .= '<div class="video-playlist-outer">';
							$output .= $playlist;
							$output .= '<div class="video-playlist-nav"><i class="fa fa-angle-left easy-ticker-prev video-playlist-prev"></i><i class="fa fa-angle-right easy-ticker-next video-playlist-next"></i></div>';
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div><!--row-->';
			$output .= '</div><!--mblock30-->';
			
		}elseif( $player_modal == '2' ){
		
			$output .= '<div class="independent-block'. esc_attr( $parent_class ) .'">';
			if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( independent_blocksTitleAlign($title_position) ) .'"><span>'. esc_attr( $title ) .'</span></h4>';
				$output .= '<div class="row">';
					$output .= '<div class="col-md-12">';
						$output .= '<div class="video-player">';						
							$output .= $playlist_first;
						$output .= '</div>';
						$output .= '<div class="video-playlist-outer">';
							$output .= $playlist;
							$output .= '<div class="video-playlist-nav"><i class="fa fa-angle-left easy-ticker-prev video-playlist-prev"></i><i class="fa fa-angle-right easy-ticker-next video-playlist-next"></i></div>';
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div><!--row-->';
			$output .= '</div><!--mblock30-->';
			
		}elseif( $player_modal == '3' ){
			$output .= '<div class="independent-block'. esc_attr( $parent_class ) .'">';
				$title_class = independent_blocksTitleAlign($title_position);
				$title_class .= isset( $title_style ) && $title_style != '' ? ' title-style-' . $title_style : '';
				if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_html( $title ) .'</span></h4>';
				$output .= '<div class="row">';
					$output .= '<div class="col-md-12">';
						$output .= '<div class="video-player">';						
							$output .= $playlist_first;
						$output .= '</div>';
						$output .= '<div class="video-playlist-outer">';
							$output .= $playlist;
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div><!--row-->';
			$output .= '</div><!--mblock30-->';
			
		}else{
		
			$output .= '<div class="independent-block'. esc_attr( $parent_class ) .' '. esc_attr( $css_class ) .'" data-id="independent_block_id_'.esc_attr( $rand ).'">';
			
			if( $independent_block_styles != '' )
			$output .= '<input type="hidden" class="news-block-css-options" data-options="'. htmlspecialchars(json_encode($block_css_options), ENT_QUOTES, 'UTF-8') .'" />';
		
			$title_class = independent_blocksTitleAlign($title_position);
			$title_class .= isset( $title_style ) && $title_style != '' ? ' title-style-' . $title_style : '';
			if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_attr( $title ) .'</span></h4>';
				$output .= '<div class="row">';
					$output .= '<div class="col-md-9">';
						$output .= '<div class="video-player">';						
							$output .= $playlist_first;
						$output .= '</div>';
					$output .= '</div>';
					$output .= '<div class="col-md-3">';
						$output .= '<div class="video-playlist-outer">';
							$output .= $playlist;
							$output .= '<div class="video-playlist-nav"><i class="fa fa-angle-left easy-ticker-prev video-playlist-prev"></i><i class="fa fa-angle-right easy-ticker-next video-playlist-next"></i></div>';
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div><!--row-->';
			$output .= '</div><!--mblock30-->';
			
		}
						
		return $output;
	}
}
if ( ! function_exists( 'independent_vc_block_video_playlist_shortcode_map' ) ) {
	function independent_vc_block_video_playlist_shortcode_map() {
		vc_map( 
			array(
				"name"					=> esc_html__( "Video Playlist", "independent" ),
				"description"			=> esc_html__( "Show videos on playlist.", 'independent' ),
				"base"					=> "independent_vc_block_video_playlist",
				"category"				=> esc_html__( "Blocks", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Extra Classes', "independent" ),
						'param_name'	=> 'extra_class',
						'value' 		=> '',
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title', "independent" ),
						'param_name'	=> 'title',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Style", "independent" ),
						"param_name"	=> "title_style",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "",
							esc_html__( "Style 1", "independent" )	=> "1",
							esc_html__( "Style 2", "independent" )	=> "2",
							esc_html__( "Style 3", "independent" )	=> "3",
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title Size', "independent" ),
						'param_name'	=> 'title_size',
						'description'	=> esc_html__( 'This is settings for title font size. Example 12px', 'independent' ),
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Style", "independent" ),
						'description'	=> esc_html__( 'Choose block title style either capitalize, uppercase, etc..', 'independent' ),
						"param_name"	=> "title_transform",
						"value"			=> array(
							esc_html__( "Capitalize", "independent" )	=> "capitalize",
							esc_html__( "Upper Case", "independent" )	=> "uppercase",
							esc_html__( "Lower Case", "independent" )	=> "lowercase",
							esc_html__( "None", "independent" )	=> "none"
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Number of Posts to Show', "independent" ),
						'param_name'	=> 'post_limit',
						'value' 		=> '10',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Video List By", "independent" ),
						"param_name"	=> "video_opt",
						"value"			=> array(
							esc_html__( "All", "independent" )	=> "all",
							esc_html__( "By Category", "independent" )	=> "category",
							esc_html__( "By Post", "independent" )	=> "post"
						)
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Enter Categories Id\'s', "independent" ),
						'description'	=> esc_html__( 'Entered categories ids video post will showing on video player. Example 10, 12', "independent" ),
						'param_name'	=> 'video_categories',
						'value' 		=> '',
						"dependency"	=> array(
							"element"	=> "video_opt",
							"value"		=> "category"
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Enter Video Post Id\'s', "independent" ),
						'description'	=> esc_html__( 'Entered video post ids here to show on video player. Example 5, 8', "independent" ),
						'param_name'	=> 'video_posts',
						'value' 		=> '',
						"dependency"	=> array(
							"element"	=> "video_opt",
							"value"		=> "post"
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Video Auto Play", "independent" ),
						"param_name"	=> "auto_play",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "yes",
							esc_html__( "No", "independent" )	=> "no"
						)
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Video Player Type", "independent" ),
						"param_name"	=> "player_modal",
						"value"			=> array(
							esc_html__( "Type 1", "independent" )	=> "1",
							esc_html__( "Type 2", "independent" )	=> "2",
							esc_html__( "Type 3", "independent" )	=> "3",
							esc_html__( "Type 4", "independent" )	=> "4"
						)
					),
					//title
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Position", "independent" ),
						"param_name"	=> "title_position",
						"value"			=> array(
							esc_html__( "Left", "independent" )	=> "left",
							esc_html__( "Center", "independent" )=> "center",
							esc_html__( "Right", "independent" )	=> "right",
						),
						'group'			=> esc_html__( 'Title', 'independent' )
					),
					//Slider
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Video Playlist Slide Animation", "independent" ),
						"param_name"	=> "slide_animation",
						"value"			=> array(
							esc_html__( "Slide In-Up", "independent" )		=> "slideOutUp:slideInUp",
							esc_html__( "Fade Out-In", "independent" )		=> "fadeOut:fadeIn",
							esc_html__( "Flip X", "independent" )			=> "fadeOut:flipInX",
							esc_html__( "Flip Y", "independent" )			=> "fadeOut:flipInY",
							esc_html__( "Slide Down-Out", "independent" )	=> "slideOutDown:slideInDown",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Slide Navigation", "independent" ),
						"param_name"	=> "slide_nav",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "true",
							esc_html__( "No", "independent" )	=> "false",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'independent_vc_block_video_playlist_shortcode_map' );