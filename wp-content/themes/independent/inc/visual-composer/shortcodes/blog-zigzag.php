<?php 
/**
 * Independent Blog Zig Zag
 */
if ( ! function_exists( "independent_vc_blog_zigzag_shortcode" ) ) {
	function independent_vc_blog_zigzag_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( "independent_vc_blog_zigzag", $atts );
		extract( $atts );
		$output = '';
		
		$css_class = $independent_block_styles = $block_styles = $title_bg = '';
		
		//Defined Variable
		$title_transform = isset( $title_transform ) ? esc_attr( $title_transform ) : 'capitalize';	
		$title_size = isset( $title_size ) && $title_size != '' ? $title_size : '17px';
		$title_position = isset( $title_position ) && $title_position != '' ? $title_position : '';
		$title = isset( $title ) && $title != '' ? $title : '';
		
		$style_arr = array(
			'title_transform' => $title_transform,
			'title_size' => $title_size
		);
				
		$class_names = isset( $extra_class ) && $extra_class != '' ? ' ' . $extra_class : '';
		$post_per_page = isset( $post_per_page ) && $post_per_page != '' ? $post_per_page : '';
		$excerpt_length = isset( $excerpt_length ) && $excerpt_length != '' ? $excerpt_length : 0;
		$more_text = isset( $more_text ) && $more_text != '' ? $more_text : '';
		$title_head = isset( $title_head ) ? $title_head : 'h4';
		$cols = isset( $blog_cols ) ? $blog_cols : 12;	
		$cols_class = ' col-md-'. $cols;
		if( $blog_layout == 'list' ){
			$cols_class = ' col-md-12';
			$cols = 12;
		}
		$cols_class .= isset( $blog_layout ) ? ' blog-zz-'. $blog_layout : ' blog-zz-grid';	
		
		$thumb_size = isset( $image_size ) ? $image_size : '';
		$cus_thumb_size = '';
		if( $thumb_size == 'custom' ){
			$cus_thumb_size = isset( $custom_image_size ) && $custom_image_size != '' ? $custom_image_size : '';
		}
		
		$top_meta = isset( $top_meta ) && $top_meta != '' ? $top_meta : array( 'Enabled' => '' );
		$bottom_meta = isset( $bottom_meta ) && $bottom_meta != '' ? $bottom_meta : array( 'Enabled' => '' );

		$class_names .= isset( $text_align ) && $text_align != 'default' ? ' text-' . $text_align : '';
		
		// This is custom css options for main shortcode warpper
		$shortcode_css = '';
		$shortcode_rand_id = $rand_class = 'shortcode-rand-'. independent_shortcode_rand_id();
		
		//Spacing
		if( isset( $sc_spacing ) && !empty( $sc_spacing ) ){
			$sc_spacing = preg_replace( '!\s+!', ' ', $sc_spacing );
			$space_arr = explode( " ", $sc_spacing );
			$i = 1;
			
			$space_class_name = '.' . esc_attr( $rand_class ) . '.blog-wrapper .blog-zz-items-inner >';

			foreach( $space_arr as $space ){
				$shortcode_css .= $space != 'default' ? $space_class_name .' *:nth-child('. esc_attr( $i ) .') { margin-bottom: '. esc_attr( $space ) .'px; }' : '';
				$i++;
			}
		}
		
		if( $title_transform || $block_color != '' ){
			$shortcode_css .= independent_block_dynamic_css($rand_class, $style_arr);
		}

		//Cats In
		$cats_in = array();
		if( isset( $include_cats ) && $include_cats != '' ){
			$filter = preg_replace( '/\s+/', '', $include_cats );
			$filter = explode( ',', rtrim( $filter, ',' ) );
			foreach( $filter as $cat ){
				if( term_exists( $cat, 'category' ) ){
					$cat_term = get_term_by( 'slug', $cat, 'category' );	
					//post in array push
					if( isset( $cat_term->term_id ) )
						array_push( $cats_in, absint( $cat_term->term_id ) );	
				}
			}
		}
		
		//Cats Not In
		$cats_not_in = array();
		if( isset( $exclude_cats ) && $exclude_cats != '' ){
			$filter = preg_replace( '/\s+/', '', $exclude_cats );
			$filter = explode( ',', rtrim( $filter, ',' ) );
			foreach( $filter as $cat ){
				if( term_exists( $cat, 'category' ) ){
					$cat_term = get_term_by( 'slug', $cat, 'category' );	
					//post not in array push
					if( isset( $cat_term->term_id ) )
						array_push( $cats_not_in, absint( $cat_term->term_id ) );	
				}
			}
		}
		
		//Query Start
		global $wp_query;
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		
		$ppp = isset( $post_per_page ) && $post_per_page != '' ? $post_per_page : 2;
		$inc_cat_array = $cats_in ? array( 'taxonomy' => 'category', 'field' => 'id', 'terms' => $cats_in ) : '';
		$exc_cat_array = $cats_not_in ? array( 'taxonomy' => 'category', 'field' => 'id', 'terms' => $cats_not_in, 'operator' => 'NOT IN' ) : '';
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => absint( $ppp ),
			'paged' => $paged,
			'ignore_sticky_posts' => 1,
			'tax_query' => array(
				$inc_cat_array,
				$exc_cat_array
			)
			
		);
		$query = new WP_Query( $args );
			
		if ( $query->have_posts() ) {
			
			$blog_array = array(
				'excerpt_length' => $excerpt_length,
				'thumb_size' => $thumb_size,
				'cus_thumb_size' => $cus_thumb_size,
				'more_text' => $more_text,
				'top_meta' => $top_meta,
				'bottom_meta' => $bottom_meta,
				'list_layout' => 0,
				'title_head' => $title_head 
			);
			
			//Shortcode css ccde here
			$shortcode_css .= isset( $font_color ) && $font_color != '' ? '.' . esc_attr( $rand_class ) . '.blog-wrapper { color: '. esc_attr( $font_color ) .'; }' : '';
			
			if( $shortcode_css ) $class_names .= ' ' . $shortcode_rand_id . ' independent-inline-css';
			$elemetns = isset( $blog_items ) ? independent_drag_and_drop_trim( $blog_items ) : array( 'Enabled' => '' );
			$output .= '<div class="blog-wrapper'. esc_attr( $class_names ) .'" data-css="'. htmlspecialchars( json_encode( $shortcode_css ), ENT_QUOTES, 'UTF-8' ) .'">';
				
				$title_class = independent_blocksTitleAlign($title_position);
				$title_class .= isset( $title_style ) && $title_style != '' ? ' title-style-' . $title_style : '';
				if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_html( $title ) .'</span></h4>';
				
				$row_stat = 0;
				$cnt = 0;
				$single_stat = 1;
				
				// Start the Loop
				while ( $query->have_posts() ) : $query->the_post();
					$post_id = get_the_ID();
					$blog_array['post_id'] = $post_id;
					$blog_array['cnt'] = $cnt++;
					
					$thumb_out = $inner_out = '';

					$thumb_out = independent_blog_shortcode_elements( "thumb", $blog_array );
					
					if( isset( $elemetns['Enabled'] ) ) :
						foreach( $elemetns['Enabled'] as $element => $value ){
							$inner_out .= independent_blog_shortcode_elements( $element, $blog_array );
						}
					endif;
					
					if( $row_stat == 0 ) :
						$output .= '<div class="row">';
					endif;
					
					$vv_stat = $single_stat % 2;
					
					$blog_zz_class = !$vv_stat ? ' blog-zz-actived' : '';
					$blog_zz_class .= $vv_stat == 1 ? ' blog-zz-odd' : ' blog-zz-even';
					
					$output .= '<div class="'. esc_attr( $cols_class ) .'">';
						$output .= '<div class="blog-zz-inner'. esc_attr( $blog_zz_class ) .'">';
							$output .= $vv_stat == 1 ? $thumb_out : '';
							
							
							$output .= '<div class="blog-zz-items">';
								$output .= '<div class="blog-zz-items-inner">';
									$output .= $inner_out;
								$output .= '</div><!-- .blog-zz-items-inner -->';
							$output .= '</div><!-- .blog-zz-items -->';
							$output .= $vv_stat == 0 ? $thumb_out : '';							
						$output .= '</div><!-- .blog-zz-inner -->';
					$output .= '</div><!-- .col-md -->';
					
					$row_stat++;
					if( $row_stat == ( 12/ $cols ) ) :
						$output .= '</div><!-- .row -->';
						$row_stat = 0;
					endif;
					
					$single_stat++;
					
				endwhile;
				
			$output .= '</div><!-- .blog-wrapper -->';
			
		}// query exists
		
		// use reset postdata to restore orginal query
		wp_reset_postdata();
		
		return $output;
	}
}
if ( ! function_exists( "independent_vc_blog_zigzag_shortcode_map" ) ) {
	function independent_vc_blog_zigzag_shortcode_map() {
				
		vc_map( 
			array(
				"name"					=> esc_html__( "Blog Zig Zag", "independent" ),
				"description"			=> esc_html__( "Posts zig zag.", "independent" ),
				"base"					=> "independent_vc_blog_zigzag",
				"category"				=> esc_html__( "Shortcodes", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Extra Class", "independent" ),
						"param_name"	=> "extra_class",
						"value" 		=> "",
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title', "independent" ),
						'param_name'	=> 'title',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Position", "independent" ),
						"param_name"	=> "title_position",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "",
							esc_html__( "Left", "independent" )			=> "left",
							esc_html__( "Center", "independent" )	=> "center",
							esc_html__( "Right", "independent" )	=> "right",
							esc_html__( "Title with Tab", "independent" )	=> "tab"
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Style", "independent" ),
						"param_name"	=> "title_style",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "",
							esc_html__( "Style 1", "independent" )	=> "1",
							esc_html__( "Style 2", "independent" )	=> "2",
							esc_html__( "Style 3", "independent" )	=> "3",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Style", "independent" ),
						'description'	=> esc_html__( 'Choose block title style either capitalize, uppercase, etc..', 'independent' ),
						"param_name"	=> "title_transform",
						"value"			=> array(
							esc_html__( "Capitalize", "independent" )	=> "capitalize",
							esc_html__( "Upper Case", "independent" )	=> "uppercase",
							esc_html__( "Lower Case", "independent" )	=> "lowercase",
							esc_html__( "None", "independent" )	=> "none"
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title Size', "independent" ),
						'param_name'	=> 'title_size',
						'description'	=> esc_html__( 'This is settings for title font size. Example 12px', 'independent' ),
						'value' 		=> '',
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Post Per Page", "independent" ),
						"description"	=> esc_html__( "Here you can define post limits per page. Example 10", "independent" ),
						"param_name"	=> "post_per_page",
						"value" 		=> "",
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Excerpt Length", "independent" ),
						"description"	=> esc_html__( "Here you can define post excerpt length. Example 10", "independent" ),
						"param_name"	=> "excerpt_length",
						"value" 		=> "15"
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Include Categories", "independent" ),
						"description"	=> esc_html__( "This is filter categories. If you don't want portfolio filter, then leave this empty. Example slug: travel, web", "independent" ),
						"param_name"	=> "include_cats",
						"value" 		=> "",
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Exclude Categories", "independent" ),
						"description"	=> esc_html__( "Here you can mention unwanted categories. Example slug: travel, web", "independent" ),
						"param_name"	=> "exclude_cats",
						"value" 		=> "",
					),
					array(
						"type"			=> "textarea_raw_html",
						"heading"		=> esc_html__( "Read More Text", "independent" ),
						"description"	=> esc_html__( "Here you can enter read more text instead of default text.", "independent" ),
						"param_name"	=> "more_text",
						"value" 		=> esc_html__( "Read More", "independent" ),
					),
					array(
						"type"			=> "dropdown",
						"heading"		=> esc_html__( "Title Heading Tag", "independent" ),
						"description"	=> esc_html__( "This is an option for title heading tag", "independent" ),
						"param_name"	=> "title_head",
						"value"			=> array(
							esc_html__( "H1", "independent" )=> "h1",
							esc_html__( "H2", "independent" )=> "h2",
							esc_html__( "H3", "independent" )=> "h3",
							esc_html__( "H4", "independent" )=> "h4",
							esc_html__( "H5", "independent" )=> "h5",
							esc_html__( "H6", "independent" )=> "h6"
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> "colorpicker",
						"heading"		=> esc_html__( "Font Color", "independent" ),
						"description"	=> esc_html__( "Here you can put the font color.", "independent" ),
						"param_name"	=> "font_color",
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						'type'			=> 'drag_drop',
						'heading'		=> esc_html__( 'Post Items', 'independent' ),
						"description"	=> esc_html__( "This is settings for blog custom layout. here you can set your own layout. Drag and drop needed blog items to Enabled part.", "independent" ),
						'param_name'	=> 'blog_items',
						'dd_fields' => array ( 
							'Enabled' => array( 
								'title'	=> esc_html__( 'Title', 'independent' ),
								'category'	=> esc_html__( 'Category', 'independent' ),
								'author'	=> esc_html__( 'Author', 'independent' ),
								'excerpt'	=> esc_html__( 'Excerpt', 'independent' )
							),
							'disabled' => array(
								'top-meta'	=> esc_html__( 'Top Meta', 'independent' ),
								'bottom-meta'	=> esc_html__( 'Bottom Meta', 'independent' )
							)
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						'type'			=> 'drag_drop',
						'heading'		=> esc_html__( 'Post Top Meta', 'independent' ),
						"description"	=> esc_html__( "This is settings for blog shortcode post top meta.", "independent" ),
						'param_name'	=> 'top_meta',
						'dd_fields' => array ( 
							'Enabled' => array(),
							'disabled' => array(
								'category'	=> esc_html__( 'Category', 'independent' ),
								'author'	=> esc_html__( 'Author', 'independent' ),
								'more'	=> esc_html__( 'Read More', 'independent' ),
								'date'	=> esc_html__( 'Date', 'independent' ),
								'comment'	=> esc_html__( 'Comment', 'independent' )
							)
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						'type'			=> 'drag_drop',
						'heading'		=> esc_html__( 'Post Bottom Meta', 'independent' ),
						"description"	=> esc_html__( "This is settings for blog shortcode post bottom meta.", "independent" ),
						'param_name'	=> 'bottom_meta',
						'dd_fields' => array ( 
							'Enabled' => array(),
							'disabled' => array(
								'category'	=> esc_html__( 'Category', 'independent' ),
								'author'	=> esc_html__( 'Author', 'independent' ),
								'more'	=> esc_html__( 'Read More', 'independent' ),
								'date'	=> esc_html__( 'Date', 'independent' ),
								'comment'	=> esc_html__( 'Comment', 'independent' )
							)
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> "dropdown",
						"heading"		=> esc_html__( "Blog Zig Zag Layout", "independent" ),
						"description"	=> esc_html__( "This is an option for blog zig zag layout grid or list.", "independent" ),
						"param_name"	=> "blog_layout",
						"value"			=> array(
							esc_html__( "Grid", "independent" )	=> "grid",
							esc_html__( "List", "independent" )	=> "list"
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> "dropdown",
						"heading"		=> esc_html__( "Blog Columns", "independent" ),
						"description"	=> esc_html__( "This is an option for blog columns.", "independent" ),
						"param_name"	=> "blog_cols",
						"value"			=> array(
							esc_html__( "1 Column", "independent" )	=> "12",
							esc_html__( "2 Columns", "independent" )	=> "6",
							esc_html__( "3 Columns", "independent" )	=> "4",
							esc_html__( "4 Columns", "independent" )	=> "3",
						),
						'std'			=> '6',
						"dependency"	=> array(
								"element"	=> "blog_layout",
								"value"		=> "grid"
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> "dropdown",
						"heading"		=> esc_html__( "Text Align", "independent" ),
						"description"	=> esc_html__( "This is an option for blog text align", "independent" ),
						"param_name"	=> "text_align",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "default",
							esc_html__( "Left", "independent" )		=> "left",
							esc_html__( "Center", "independent" )	=> "center",
							esc_html__( "Right", "independent" )		=> "right"
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Image Size", "independent" ),
						"param_name"	=> "image_size",
						'description'	=> esc_html__( 'Choose thumbnail size for display different size image.', 'independent' ),
						"value"			=> array(
							esc_html__( "Medium", "independent" )=> "medium",
							esc_html__( "Large", "independent" )=> "large",
							esc_html__( "Thumbnail", "independent" )=> "thumbnail",
							esc_html__( "Custom", "independent" )=> "custom",
						),
						'std'			=> 'newsz_grid_2',
						'group'			=> esc_html__( 'Image', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Custom Image Size', "independent" ),
						'param_name'	=> 'custom_image_size',
						'description'	=> esc_html__( 'Enter custom image size. eg: 200x200', 'independent' ),
						'value' 		=> '',
						"dependency"	=> array(
								"element"	=> "image_size",
								"value"		=> "custom"
						),
						'group'			=> esc_html__( 'Image', 'independent' )
					),
					array(
						"type"			=> "textarea",
						"heading"		=> esc_html__( "Items Spacing", "independent" ),
						"description"	=> esc_html__( "Enter custom bottom space for each item on main wrapper. Your space values will apply like nth child method. If you leave this empty, default theme space apply for each child. If you want default value for any child, just type \"default\". It will take default value for that child. Example 10px 12px 8px", "independent" ),
						"param_name"	=> "sc_spacing",
						"value" 		=> "",
						"group"			=> esc_html__( "Spacing", "independent" ),
					)
				)
			) 
		);
	}
}
add_action( "vc_before_init", "independent_vc_blog_zigzag_shortcode_map" );