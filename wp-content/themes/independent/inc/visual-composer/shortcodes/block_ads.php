<?php 
/**
 * Ads Block shortcode
 * 
 */
if ( ! function_exists( 'independent_vc_block_ads_shortcode' ) ) {
	function independent_vc_block_ads_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( 'independent_vc_block_ads', $atts );
		extract( $atts );
		$output = '';
		/*This is Ads Code*/
		$ads_spot = isset( $ads_spot ) ? $ads_spot : '';
		$ads_txt = independent_ads_out( $ads_spot );
		if( $ads_txt ){
			$output .= '<div class="independent-artical-ads text-center">';
				$output .= isset( $title ) && $title != '' ? '<div class="ads-title"><span>' . esc_html( $title ) . '</span></div>' : '';
				$output .= $ads_txt;
			$output .= '</div>';
		}
		return $output;
	}
}
if ( ! function_exists( 'independent_vc_block_ads_shortcode_map' ) ) {
	function independent_vc_block_ads_shortcode_map() {
	
		$ads = array( 
			esc_html__( 'Header', 'independent' ) => 'header', 
			esc_html__( 'Footer', 'independent' ) => 'footer', 
			esc_html__( 'Sidebar', 'independent' ) => 'sidebar', 
			esc_html__( 'Artical Top', 'independent' ) => 'artical-top', 
			esc_html__( 'Artical Inline', 'independent' ) => 'artical-inline', 
			esc_html__( 'Artical Bottom', 'independent' ) => 'artical-bottom', 
			esc_html__( 'Custom1', 'independent' ) => 'custom1', 
			esc_html__( 'Custom2', 'independent' ) => 'custom2', 
			esc_html__( 'Custom3', 'independent' ) => 'custom3', 
			esc_html__( 'Custom4', 'independent' ) => 'custom4', 
			esc_html__( 'Custom5', 'independent' ) => 'custom5' 
		); 
				
		vc_map( 
			array(
				"name"					=> esc_html__( "Ads Block", "independent" ),
				"description"			=> esc_html__( "Ads Block Shortcode.", 'independent' ),
				"base"					=> "independent_vc_block_ads",
				"category"				=> esc_html__( "Blocks", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Ad Title', "independent" ),
						'param_name'	=> 'title',
						'description'	=> esc_html__( 'Optional - a title for the Ad, like - Advertisement - if you leave it blank the block will not have a title', "independent" ),
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Select Adspot ", "independent" ),
						"param_name"	=> "ads_spot",
						"value"			=> $ads
					),
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'independent_vc_block_ads_shortcode_map' );