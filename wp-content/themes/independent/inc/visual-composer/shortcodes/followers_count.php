<?php 
/**
 * Followers Count shortcode
 * 
 */
if ( ! function_exists( 'independent_vc_followers_count_shortcode' ) ) {
	function independent_vc_followers_count_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( 'independent_vc_followers_count', $atts );
		return independent_news_block_form_followers($atts);
		
	}
}
function independent_news_block_form_followers($atts){
	extract( $atts );
		
	$fb_id = $fb_id != '' ? esc_attr( $fb_id ) : '';
	$fb_token = $fb_token != '' ? esc_attr( $fb_token ) : '';
	
	$twitter_id = $twitter_id != '' ? esc_attr( $twitter_id ) : '';
	
	$gplus_id = $gplus_id != '' ? esc_attr( $gplus_id ) : '';
	$gplus_api = $gplus_api != '' ? esc_attr( $gplus_api ) : '';
	
	$yt_id = $yt_id != '' ? esc_attr( $yt_id ) : '';
	$yt_api = $yt_api != '' ? esc_attr( $yt_api ) : '';
	
	$pin_url = $pin_url != '' ? esc_url( $pin_url ) : '';
	$fb = independent_transient_followers($fb_id, $fb_token, 'fb');
	$twitter = independent_transient_followers($twitter_id, '', 'twit');
	$gplus = independent_transient_followers($gplus_id, $gplus_api, 'gplus');
	$yt = independent_transient_followers($yt_id, $yt_api, 'yt');
	$pin = independent_transient_followers($pin_id, '', 'pin');
	$t_modal = isset( $followers_modal ) && $followers_modal != '' ? esc_attr( $followers_modal ) : '1';
	$modal = ' modal-'.$t_modal;
	if( $t_modal == '2' || $t_modal == '4' ){
		$modal .= ' social-transparent typo-dark';
	}else{
		$modal .= ' typo-white';
	}
	$output = '';
	$output .= '<ul class="social-counts'. esc_attr( $modal ) .'">';
		
		if( $fb_enable == 'yes' ){ 
		$output .= '<li>
						<a href="'. ( $fb_url != '' ? esc_url( $fb_url ) : '#' ) .'" target="blank" class="social-facebook">
							<span class="fa fa-facebook"></span>
							<span class="followers-count">'. number_format( $fb ) .'</span>
							'. ( $fb_inner_txt != '' ? '<span class="follow-inner-text">'.esc_attr( $fb_inner_txt ).'</span>' : '' ) .'
							'. ( $fb_redirect_txt != '' ? '<span class="follow-redirect-text pull-right">'.esc_attr( $fb_redirect_txt ).'</span>' : '' ) .'
						</a>
					</li>';
		}
		if( $twitter_enable == 'yes' ){ 
		$output .= '<li>
						<a href="'. ( $twitter_url != '' ? esc_url( $twitter_url ) : '#' ) .'" target="blank" class="social-twitter">
							<span class="fa fa-twitter"></span>
							<span class="followers-count">'. number_format( $twitter ) .'</span>
							'. ( $twitter_inner_txt != '' ? '<span class="follow-inner-text">'.esc_attr( $twitter_inner_txt ).'</span>' : '' ) .'
							'. ( $twitter_redirect_txt != '' ? '<span class="follow-redirect-text pull-right">'.esc_attr( $twitter_redirect_txt ).'</span>' : '' ) .'
						</a>
					</li>';
		}
		if( $gplus_enable == 'yes' ){ 
		$output .= '<li>
						<a href="'. ( $gplus_url != '' ? esc_url( $gplus_url ) : '#' ) .'" target="blank" class="social-google-plus">
							<span class="fa fa-google-plus"></span>
							<span class="followers-count">'. number_format( $gplus ) .'</span>
							'. ( $gplus_inner_txt != '' ? '<span class="follow-inner-text">'.esc_attr( $gplus_inner_txt ).'</span>' : '' ) .'
							'. ( $gplus_redirect_txt != '' ? '<span class="follow-redirect-text pull-right">'.esc_attr( $gplus_redirect_txt ).'</span>' : '' ) .'
						</a>
					</li>';
		}
		if( $yt_enable == 'yes' ){ 
		$output .= '<li>
						<a href="'. ( $yt_url != '' ? esc_url( $yt_url ) : '#' ) .'" target="blank" class="social-youtube">
							<span class="fa fa-youtube"></span>
							<span class="followers-count">'. number_format( $yt ) .'</span>
							'. ( $yt_inner_txt != '' ? '<span class="follow-inner-text">'.esc_attr( $yt_inner_txt ).'</span>' : '' ) .'
							'. ( $yt_redirect_txt != '' ? '<span class="follow-redirect-text pull-right">'.esc_attr( $yt_redirect_txt ).'</span>' : '' ) .'
						</a>
					</li>';
		}
		if( $pin_enable == 'yes' ){ 
		$output .= '<li>
						<a href="'. ( $pin_url != '' ? esc_url( $pin_url ) : '#' ) .'" target="blank" class="social-pinterest">
							<span class="fa fa-pinterest"></span>
							<span class="followers-count">'. number_format( $pin ) .'</span>
							'. ( $pin_inner_txt != '' ? '<span class="follow-inner-text">'.esc_attr( $pin_inner_txt ).'</span>' : '' ) .'
							'. ( $pin_redirect_txt != '' ? '<span class="follow-redirect-text pull-right">'.esc_attr( $pin_redirect_txt ).'</span>' : '' ) .'
						</a>
					</li>';
		}
	$output .= '</ul>';	
	return $output;
}
if ( ! function_exists( 'independent_vc_followers_count_shortcode_map' ) ) {
	function independent_vc_followers_count_shortcode_map() {
				
		vc_map( independent_vc_map_block_followers() );
	}
}
add_action( 'vc_before_init', 'independent_vc_followers_count_shortcode_map' );
function independent_vc_map_block_followers(){
	$map_fields = array(
		"name"					=> esc_html__( "Followers/Subscriber Count", "independent" ),
		"description"			=> esc_html__( "Social websites followers/subscribers count", 'independent' ),
		"base"					=> "independent_vc_followers_count",
		"category"				=> esc_html__( "Blocks", "independent" ),
		"icon"					=> "zozo-vc-icon",
		"params"				=> array(
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Title', "independent" ),
				'param_name'	=> 'title',
				'value' 		=> '',
			),
			array(
				"type"			=> "img_select",
				"heading"		=> esc_html__( "Social Followers Modal", "independent" ),
				"description" => esc_html__( "Choose social followers modal.", 'independent' ),
				"param_name"	=> "followers_modal",
				"img_lists" => array ( 
					"1"	=> INDEPENDENT_ADMIN_URL . "/assets/images/social-counter/1.png",
					"2"	=> INDEPENDENT_ADMIN_URL . "/assets/images/social-counter/2.png",
					"3"	=> INDEPENDENT_ADMIN_URL . "/assets/images/social-counter/3.png",
					"4"	=> INDEPENDENT_ADMIN_URL . "/assets/images/social-counter/4.png"
				),
				"default"		=> "1"
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Facebook", "independent" ),
				"param_name"	=> "fb_enable",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
				"group"	=> esc_html__( 'Facebook', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Facebook URL', "independent" ),
				'param_name'	=> 'fb_url',
				'value' 		=> '',
				"group"	=> esc_html__( 'Facebook', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Facebook Page Id', "independent" ),
				'param_name'	=> 'fb_id',
				'value' 		=> '',
				"group"	=> esc_html__( 'Facebook', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Facebook Access Token', "independent" ),
				'param_name'	=> 'fb_token',
				'value' 		=> '',
				"group"	=> esc_html__( 'Facebook', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Facebook Inner Text', "independent" ),
				'param_name'	=> 'fb_inner_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Facebook', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Facebook Redirect Text', "independent" ),
				'param_name'	=> 'fb_redirect_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Facebook', 'independent' ),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Twitter", "independent" ),
				"param_name"	=> "twitter_enable",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
				"group"	=> esc_html__( 'Twitter', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Twitter URL', "independent" ),
				'param_name'	=> 'twitter_url',
				'value' 		=> '',
				"group"	=> esc_html__( 'Twitter', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Twitter User Name', "independent" ),
				'param_name'	=> 'twitter_id',
				'value' 		=> '',
				"group"	=> esc_html__( 'Twitter', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Twitter Inner Text', "independent" ),
				'param_name'	=> 'twitter_inner_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Twitter', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Twitter Redirect Text', "independent" ),
				'param_name'	=> 'twitter_redirect_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Twitter', 'independent' ),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Google Plus", "independent" ),
				"param_name"	=> "gplus_enable",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
				"group"	=> esc_html__( 'Google Plus', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Google Plus URL', "independent" ),
				'param_name'	=> 'gplus_url',
				'value' 		=> '',
				"group"	=> esc_html__( 'Google Plus', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Google Plus API', "independent" ),
				'param_name'	=> 'gplus_api',
				'value' 		=> '',
				"group"	=> esc_html__( 'Google Plus', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Google Plus Page Id', "independent" ),
				'param_name'	=> 'gplus_id',
				'value' 		=> '',
				"group"	=> esc_html__( 'Google Plus', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Google Plus Inner Text', "independent" ),
				'param_name'	=> 'gplus_inner_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Google Plus', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Google Plus Redirect Text', "independent" ),
				'param_name'	=> 'gplus_redirect_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Google Plus', 'independent' ),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Youtube", "independent" ),
				"param_name"	=> "yt_enable",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
				"group"	=> esc_html__( 'Youtube', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Youtube URL', "independent" ),
				'param_name'	=> 'yt_url',
				'value' 		=> '',
				"group"	=> esc_html__( 'Youtube', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Youtube API', "independent" ),
				'param_name'	=> 'yt_api',
				'value' 		=> '',
				"group"	=> esc_html__( 'Youtube', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Youtube Channel Id', "independent" ),
				'param_name'	=> 'yt_id',
				'value' 		=> '',
				"group"	=> esc_html__( 'Youtube', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Youtube Inner Text', "independent" ),
				'param_name'	=> 'yt_inner_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Youtube', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Youtube Redirect Text', "independent" ),
				'param_name'	=> 'yt_redirect_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Youtube', 'independent' ),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Pinterest", "independent" ),
				"param_name"	=> "pin_enable",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
				"group"	=> esc_html__( 'Pinterest', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Pinterest URL', "independent" ),
				'param_name'	=> 'pin_url',
				'value' 		=> '',
				"group"	=> esc_html__( 'Pinterest', 'independent' ),
				"description" => esc_html__( "This is external url for redirect.", 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Pinterest Page URL', "independent" ),
				'param_name'	=> 'pin_id',
				'value' 		=> '',
				"group"	=> esc_html__( 'Pinterest', 'independent' ),
				"description" => esc_html__( "This is the url for get pinterest count.", 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Pinterest Inner Text', "independent" ),
				'param_name'	=> 'pin_inner_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Pinterest', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Pinterest Redirect Text', "independent" ),
				'param_name'	=> 'pin_redirect_txt',
				'value' 		=> '',
				"group"	=> esc_html__( 'Pinterest', 'independent' ),
			),
		)
	);
	return $map_fields;
}