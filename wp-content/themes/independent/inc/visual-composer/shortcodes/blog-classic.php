<?php 
/**
 * Independent Blog
 */
if ( ! function_exists( "independent_vc_blog_shortcode" ) ) {
	function independent_vc_blog_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( "independent_vc_blog", $atts );
		extract( $atts );
		$output = '';
	
		//Defined Variable
		$class_names = isset( $extra_class ) && $extra_class != '' ? ' ' . $extra_class : '';
		$post_per_page = isset( $post_per_page ) && $post_per_page != '' ? $post_per_page : '';
		$excerpt_length = isset( $excerpt_length ) && $excerpt_length != '' ? $excerpt_length : 0;
		$more_text = isset( $more_text ) && $more_text != '' ? $more_text : '';
		$title_head = isset( $title_head ) ? $title_head : 'h4';
		
		$thumb_size = isset( $image_size ) ? $image_size : '';
		$cus_thumb_size = '';
		if( $thumb_size == 'custom' ){
			$cus_thumb_size = isset( $custom_image_size ) && $custom_image_size != '' ? $custom_image_size : '';
		}
		
		$top_meta = isset( $top_meta ) && $top_meta != '' ? $top_meta : array( 'Enabled' => '' );
		$bottom_meta = isset( $bottom_meta ) && $bottom_meta != '' ? $bottom_meta : array( 'Enabled' => '' );

		$class_names .= isset( $text_align ) && $text_align != 'default' ? ' text-' . $text_align : '';
		
		// This is custom css options for main shortcode warpper
		$shortcode_css = '';
		$shortcode_rand_id = $rand_class = 'shortcode-rand-'. independent_shortcode_rand_id();
		
		//Spacing
		if( isset( $sc_spacing ) && !empty( $sc_spacing ) ){
			$sc_spacing = preg_replace( '!\s+!', ' ', $sc_spacing );
			$space_arr = explode( " ", $sc_spacing );
			$i = 1;
			
			$space_class_name = '.' . esc_attr( $rand_class ) . '.blog-wrapper .blog-classic-items >';

			foreach( $space_arr as $space ){
				$shortcode_css .= $space != 'default' ? $space_class_name .' *:nth-child('. esc_attr( $i ) .') { margin-bottom: '. esc_attr( $space ) .'px; }' : '';
				$i++;
			}
		}

		//Cats In
		$cats_in = array();
		if( isset( $include_cats ) && $include_cats != '' ){
			$filter = preg_replace( '/\s+/', '', $include_cats );
			$filter = explode( ',', rtrim( $filter, ',' ) );
			foreach( $filter as $cat ){
				if( term_exists( $cat, 'category' ) ){
					$cat_term = get_term_by( 'slug', $cat, 'category' );	
					//post in array push
					if( isset( $cat_term->term_id ) )
						array_push( $cats_in, absint( $cat_term->term_id ) );	
				}
			}
		}
		
		//Cats Not In
		$cats_not_in = array();
		if( isset( $exclude_cats ) && $exclude_cats != '' ){
			$filter = preg_replace( '/\s+/', '', $exclude_cats );
			$filter = explode( ',', rtrim( $filter, ',' ) );
			foreach( $filter as $cat ){
				if( term_exists( $cat, 'category' ) ){
					$cat_term = get_term_by( 'slug', $cat, 'category' );	
					//post not in array push
					if( isset( $cat_term->term_id ) )
						array_push( $cats_not_in, absint( $cat_term->term_id ) );	
				}
			}
		}
		
		//Query Start
		global $wp_query;
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		
		$ppp = isset( $post_per_page ) && $post_per_page != '' ? $post_per_page : 2;
		$inc_cat_array = $cats_in ? array( 'taxonomy' => 'category', 'field' => 'id', 'terms' => $cats_in ) : '';
		$exc_cat_array = $cats_not_in ? array( 'taxonomy' => 'category', 'field' => 'id', 'terms' => $cats_not_in, 'operator' => 'NOT IN' ) : '';
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => absint( $ppp ),
			'paged' => $paged,
			'ignore_sticky_posts' => 1,
			'tax_query' => array(
				$inc_cat_array,
				$exc_cat_array
			)
			
		);
		$query = new WP_Query( $args );
			
		if ( $query->have_posts() ) {
			
			$blog_array = array(
				'excerpt_length' => $excerpt_length,
				'thumb_size' => $thumb_size,
				'cus_thumb_size' => $cus_thumb_size,
				'more_text' => $more_text,
				'top_meta' => $top_meta,
				'bottom_meta' => $bottom_meta,
				'list_layout' => 0,
				'title_head' => $title_head 
			);
			
			//Shortcode css ccde here
			$shortcode_css .= isset( $font_color ) && $font_color != '' ? '.' . esc_attr( $rand_class ) . '.blog-wrapper { color: '. esc_attr( $font_color ) .'; }' : '';
			
			if( $shortcode_css ) $class_names .= ' ' . $shortcode_rand_id . ' independent-inline-css';
			$elemetns = isset( $blog_items ) ? independent_drag_and_drop_trim( $blog_items ) : array( 'Enabled' => '' );
			$thumb_out = $inner_out = '';
			$output .= '<div class="blog-wrapper'. esc_attr( $class_names ) .'" data-css="'. htmlspecialchars( json_encode( $shortcode_css ), ENT_QUOTES, 'UTF-8' ) .'">';
			
				$output .= '<div class="independent-block-title-pack">';
					if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_html( $title ) .'</span></h4>';
					$output .= independent_news_block_tabs_only($block_options);
				$output .= '</div><!-- .independent-block-title-pack -->';

				wp_enqueue_style( 'slick' );
				wp_enqueue_style( 'slick-theme' );
				wp_enqueue_script( 'slick' );
				
				$cnt = 0;
				
				// Start the Loop
				while ( $query->have_posts() ) : $query->the_post();
					$post_id = get_the_ID();
					$blog_array['post_id'] = $post_id;
					$blog_array['cnt'] = $cnt++;

					$thumb_out .= independent_blog_shortcode_elements( "thumb", $blog_array );
					
					if( isset( $elemetns['Enabled'] ) ) :
						$inner_out .= '<div class="blog-classic-items">';
						foreach( $elemetns['Enabled'] as $element => $value ){
							$inner_out .= independent_blog_shortcode_elements( $element, $blog_array );
						}
						$inner_out .= '</div><!-- .blog-classic-items -->';
					endif;
				endwhile;
				
				$output .= '<div class="row">';
					$output .= '<div class="col-md-4">';
						$output .= '<div class="slider slider-nav">';
							$output .= $thumb_out;
						$output .= '</div><!-- .slider slider-nav -->';
					$output .= '</div><!-- .col-md-4 -->';
					$output .= '<div class="col-md-8">';
						$output .= '<div class="slider slider-for">';
							$output .= $inner_out;
						$output .= '</div><!-- .slider slider-for -->';
					$output .= '</div><!-- .col-md-8 -->';
				$output .= '</div><!-- .row -->';
				
			$output .= '</div><!-- .blog-wrapper -->';
			
		}// query exists
		
		// use reset postdata to restore orginal query
		wp_reset_postdata();
		
		return $output;
	}
}
function independent_blog_shortcode_elements( $element, $opts = array() ){
	$output = '';
	switch( $element ){
	
		case "title":
			$title_head = isset( $opts['title_head'] ) ? $opts['title_head'] : 'h4';
			$output .= '<div class="entry-title">';
				$output .= '<'. esc_attr( $title_head ) .'><a href="'. esc_url( get_the_permalink() ) .'" class="post-title">'. get_the_title() .'</a></'. esc_attr( $title_head ) .'>';
			$output .= '</div><!-- .entry-title -->';		
		break;
		
		case "thumb":
			if ( has_post_thumbnail() ) {
				
				// Custom Thumb Code
				$thumb_size = $thumb_cond = $opts['thumb_size'];
				$cus_thumb_size = $opts['cus_thumb_size'];
				$custom_opt = $img_prop = '';
				if( $thumb_cond == 'custom' ){
					$custom_opt = $cus_thumb_size != '' ? explode( "x", $cus_thumb_size ) : array();
					$img_prop = independent_custom_image_size_chk( $thumb_size, $custom_opt );
					$thumb_size = array( $img_prop[1], $img_prop[2] );
				} 
				// Custom Thumb Code End
									
				$output .= '<div class="post-thumb">';
					if( $thumb_cond == 'custom' ){
						$output .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-fluid" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '"/>';
					}else{
						$output .= get_the_post_thumbnail( $opts['post_id'], $thumb_size, array( 'class' => 'img-fluid' ) );
					}
				$output .= '</div><!-- .post-thumb -->';
			}
		break;
		
		case "category":
			$categories = get_the_category(); 
			if ( ! empty( $categories ) ){
				$coutput = '<div class="post-category">';
					foreach ( $categories as $category ) {
						$coutput .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a>,';
					}
					$output .= rtrim( $coutput, ',' );
				$output .= '</div>';
			}
		break;
		
		case "author":
			$output .= '<div class="post-author">';
				$output .= '<a href="'. get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) .'">';
					$output .= '<span class="author-name"><i class="fa fa-user mr-2"></i>'. get_the_author() .'</span>';
				$output .= '</a>';
			$output .= '</div>';
		break;
		
		case "date":
			$archive_year  = get_the_time('Y');
			$archive_month = get_the_time('m'); 
			$archive_day   = get_the_time('d');
			$output = '<div class="post-date"><a href="'. esc_url( get_day_link( $archive_year, $archive_month, $archive_day ) ) .'" >'. get_the_time( get_option( 'date_format' ) ) .'</a></div>';
		break;
		
		case "more":
			$read_more_text = isset( $opts['more_text'] ) ? $opts['more_text'] : esc_html__( 'Read more', 'independent' );
			$b_de = 'bas'.'e6'.'4_de'.'code';
			$output = '<div class="post-more"><a class="read-more" href="'. esc_url( get_permalink( get_the_ID() ) ) . '">'. urldecode( $b_de( $read_more_text ) ) .'</a></div>';
		break;
		
		case "comment":
			$comments_count = wp_count_comments(get_the_ID());
			$cmt_txt = esc_html__( 'Comments', 'independent' );
			if( $comments_count->total_comments == 1 ){
				$cmt_txt = esc_html__( 'Comment', 'independent' );
			}
			$output = '<div class="post-comment"><a href="'. esc_url( get_comments_link( get_the_ID() ) ) .'" rel="bookmark" class="comments-count"><i class="fa fa-comments mr-2"></i> '. esc_html( $comments_count->total_comments .' '. $cmt_txt ).'</a></div>';
		break;
		
		case "excerpt":
			$output = '';
			$excerpt = isset( $opts['excerpt_length'] ) && $opts['excerpt_length'] != '' ? $opts['excerpt_length'] : 20;
			$output .= '<div class="post-excerpt">';
				add_filter( 'excerpt_length', __return_value( $excerpt ) );
				ob_start();
				the_excerpt();
				$excerpt_cont = ob_get_clean();
				$output .= $excerpt_cont;
			$output .= '</div><!-- .post-excerpt -->';	
		break;		
		
		case "top-meta":
			$output = '';
			$top_meta = $opts['top_meta'];
			$elemetns = isset( $top_meta ) ? independent_drag_and_drop_trim( $top_meta ) : array( 'Enabled' => '' );
			if( isset( $elemetns['Enabled'] ) ) :
				$output .= '<div class="top-meta clearfix">';
				if( isset( $opts['number_opt'] ) && $opts['number_opt'] ){
					$num_out = absint( $opts['cnt'] ) >= 10 ? $opts['cnt'] : '0' . $opts['cnt'];
					$output .= '<h3 class="invisible-number">'. esc_html( $num_out ) .'</h3>';
				}
				
				$output .= '<ul class="top-meta-list">';
					foreach( $elemetns['Enabled'] as $element => $value ){
						$blog_array = array( 'more_text' => $opts['more_text'] );
						$output .= '<li>'. independent_blog_shortcode_elements( $element, $blog_array ) .'</li>';
					}
				$output .= '</ul></div>';
			endif;
		break;
		
		case "bottom-meta":
			$output = '';
			$bottom_meta = $opts['bottom_meta'];
			$elemetns = isset( $bottom_meta ) ? independent_drag_and_drop_trim( $bottom_meta ) : array( 'Enabled' => '' );
			if( isset( $elemetns['Enabled'] ) ) :
				$output .= '<div class="bottom-meta clearfix"><ul class="bottom-meta-list">';
					foreach( $elemetns['Enabled'] as $element => $value ){
						$blog_array = array( 'more_text' => $opts['more_text'] );
						$output .= '<li>'. independent_blog_shortcode_elements( $element, $blog_array ) .'</li>';
					}
				$output .= '</ul></div>';
			endif;
		break;
	}
	return $output; 
}
if ( ! function_exists( "independent_vc_blog_shortcode_map" ) ) {
	function independent_vc_blog_shortcode_map() {
				
		vc_map( 
			array(
				"name"					=> esc_html__( "Blog", "independent" ),
				"description"			=> esc_html__( "Blog shortcode.", "independent" ),
				"base"					=> "independent_vc_blog",
				"category"				=> esc_html__( "Shortcodes", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Extra Class", "independent" ),
						"param_name"	=> "extra_class",
						"value" 		=> "",
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title', "independent" ),
						'param_name'	=> 'title',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Position", "independent" ),
						"param_name"	=> "title_position",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "",
							esc_html__( "Left", "independent" )			=> "left",
							esc_html__( "Center", "independent" )	=> "center",
							esc_html__( "Right", "independent" )	=> "right",
							esc_html__( "Title with Tab", "independent" )	=> "tab"
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Style", "independent" ),
						"param_name"	=> "title_style",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "",
							esc_html__( "Style 1", "independent" )	=> "1",
							esc_html__( "Style 2", "independent" )	=> "2",
							esc_html__( "Style 3", "independent" )	=> "3",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Style", "independent" ),
						'description'	=> esc_html__( 'Choose block title style either capitalize, uppercase, etc..', 'independent' ),
						"param_name"	=> "title_transform",
						"value"			=> array(
							esc_html__( "Capitalize", "independent" )	=> "capitalize",
							esc_html__( "Upper Case", "independent" )	=> "uppercase",
							esc_html__( "Lower Case", "independent" )	=> "lowercase",
							esc_html__( "None", "independent" )	=> "none"
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Title Size', "independent" ),
						'param_name'	=> 'title_size',
						'description'	=> esc_html__( 'This is settings for title font size. Example 12px', 'independent' ),
						'value' 		=> '',
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Post Per Page", "independent" ),
						"description"	=> esc_html__( "Here you can define post limits per page. Example 10", "independent" ),
						"param_name"	=> "post_per_page",
						"value" 		=> "",
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Excerpt Length", "independent" ),
						"description"	=> esc_html__( "Here you can define post excerpt length. Example 10", "independent" ),
						"param_name"	=> "excerpt_length",
						"value" 		=> "15"
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Include Categories", "independent" ),
						"description"	=> esc_html__( "This is filter categories. If you don't want portfolio filter, then leave this empty. Example slug: travel, web", "independent" ),
						"param_name"	=> "include_cats",
						"value" 		=> "",
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Exclude Categories", "independent" ),
						"description"	=> esc_html__( "Here you can mention unwanted categories. Example slug: travel, web", "independent" ),
						"param_name"	=> "exclude_cats",
						"value" 		=> "",
					),
					array(
						"type"			=> "textarea_raw_html",
						"heading"		=> esc_html__( "Read More Text", "independent" ),
						"description"	=> esc_html__( "Here you can enter read more text instead of default text.", "independent" ),
						"param_name"	=> "more_text",
						"value" 		=> esc_html__( "Read More", "independent" ),
					),
					array(
						"type"			=> "dropdown",
						"heading"		=> esc_html__( "Title Heading Tag", "independent" ),
						"description"	=> esc_html__( "This is an option for title heading tag", "independent" ),
						"param_name"	=> "title_head",
						"value"			=> array(
							esc_html__( "H1", "independent" )=> "h1",
							esc_html__( "H2", "independent" )=> "h2",
							esc_html__( "H3", "independent" )=> "h3",
							esc_html__( "H4", "independent" )=> "h4",
							esc_html__( "H5", "independent" )=> "h5",
							esc_html__( "H6", "independent" )=> "h6"
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> "colorpicker",
						"heading"		=> esc_html__( "Font Color", "independent" ),
						"description"	=> esc_html__( "Here you can put the font color.", "independent" ),
						"param_name"	=> "font_color",
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						'type'			=> 'drag_drop',
						'heading'		=> esc_html__( 'Post Items', 'independent' ),
						"description"	=> esc_html__( "This is settings for blog custom layout. here you can set your own layout. Drag and drop needed blog items to Enabled part.", "independent" ),
						'param_name'	=> 'blog_items',
						'dd_fields' => array ( 
							'Enabled' => array( 
								'title'	=> esc_html__( 'Title', 'independent' ),
								'category'	=> esc_html__( 'Category', 'independent' ),
								'author'	=> esc_html__( 'Author', 'independent' ),
								'excerpt'	=> esc_html__( 'Excerpt', 'independent' )
							),
							'disabled' => array(
								'top-meta'	=> esc_html__( 'Top Meta', 'independent' ),
								'bottom-meta'	=> esc_html__( 'Bottom Meta', 'independent' )
							)
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						'type'			=> 'drag_drop',
						'heading'		=> esc_html__( 'Post Top Meta', 'independent' ),
						"description"	=> esc_html__( "This is settings for blog shortcode post top meta.", "independent" ),
						'param_name'	=> 'top_meta',
						'dd_fields' => array ( 
							'Enabled' => array(),
							'disabled' => array(
								'category'	=> esc_html__( 'Category', 'independent' ),
								'author'	=> esc_html__( 'Author', 'independent' ),
								'more'	=> esc_html__( 'Read More', 'independent' ),
								'date'	=> esc_html__( 'Date', 'independent' ),
								'comment'	=> esc_html__( 'Comment', 'independent' )
							)
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						'type'			=> 'drag_drop',
						'heading'		=> esc_html__( 'Post Bottom Meta', 'independent' ),
						"description"	=> esc_html__( "This is settings for blog shortcode post bottom meta.", "independent" ),
						'param_name'	=> 'bottom_meta',
						'dd_fields' => array ( 
							'Enabled' => array(),
							'disabled' => array(
								'category'	=> esc_html__( 'Category', 'independent' ),
								'author'	=> esc_html__( 'Author', 'independent' ),
								'more'	=> esc_html__( 'Read More', 'independent' ),
								'date'	=> esc_html__( 'Date', 'independent' ),
								'comment'	=> esc_html__( 'Comment', 'independent' )
							)
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> "dropdown",
						"heading"		=> esc_html__( "Text Align", "independent" ),
						"description"	=> esc_html__( "This is an option for blog text align", "independent" ),
						"param_name"	=> "text_align",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "default",
							esc_html__( "Left", "independent" )		=> "left",
							esc_html__( "Center", "independent" )	=> "center",
							esc_html__( "Right", "independent" )		=> "right"
						),
						"group"			=> esc_html__( "Layouts", "independent" )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Image Size", "independent" ),
						"param_name"	=> "image_size",
						'description'	=> esc_html__( 'Choose thumbnail size for display different size image.', 'independent' ),
						"value"			=> array(
							esc_html__( "Medium", "independent" )=> "medium",
							esc_html__( "Large", "independent" )=> "large",
							esc_html__( "Thumbnail", "independent" )=> "thumbnail",
							esc_html__( "Custom", "independent" )=> "custom",
						),
						'std'			=> 'newsz_grid_2',
						'group'			=> esc_html__( 'Image', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Custom Image Size', "independent" ),
						'param_name'	=> 'custom_image_size',
						'description'	=> esc_html__( 'Enter custom image size. eg: 200x200', 'independent' ),
						'value' 		=> '',
						"dependency"	=> array(
								"element"	=> "image_size",
								"value"		=> "custom"
						),
						'group'			=> esc_html__( 'Image', 'independent' )
					),
					array(
						"type"			=> "textarea",
						"heading"		=> esc_html__( "Items Spacing", "independent" ),
						"description"	=> esc_html__( "Enter custom bottom space for each item on main wrapper. Your space values will apply like nth child method. If you leave this empty, default theme space apply for each child. If you want default value for any child, just type \"default\". It will take default value for that child. Example 10px 12px 8px", "independent" ),
						"param_name"	=> "sc_spacing",
						"value" 		=> "",
						"group"			=> esc_html__( "Spacing", "independent" ),
					)
				)
			) 
		);
	}
}
add_action( "vc_before_init", "independent_vc_blog_shortcode_map" );

add_action( 'wp_enqueue_scripts', 'independent_slick_scripts' );
function independent_slick_scripts() { 
	wp_register_style( 'slick', get_theme_file_uri( '/assets/css/slick.min.css' ), array(), '1.8.1' );
	wp_register_style( 'slick-theme', get_theme_file_uri( '/assets/css/slick-theme.min.css' ), array(), '1.8.1' );
	wp_register_script('slick', get_theme_file_uri( '/assets/js/slick.min.js' ), array ('jquery'), '1.8.1', false);
}