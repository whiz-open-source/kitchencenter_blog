<?php 
/**
 * Banner shortcode
 */
get_template_part( 'inc/visual-composer/banner', 'functions' ); 
if ( ! function_exists( 'independent_vc_banner_shortcode' ) ) {
	function independent_vc_banner_shortcode( $atts, $content = NULL ) {
		
		$atts = vc_map_get_attributes( 'independent_vc_banner', $atts );
		$rand = independent_shortcode_rand_id(); 
		extract( $atts );
		
		$output = '';
		
		$independent_block_styles = $independent_block_class = '';
		$independent_block_class = 'independent-block-css-'.$rand ;
		if( $banner_gutter != '' ){
			$gutter = absint( $banner_gutter ) . 'px';			
			$independent_block_styles .= '.'. $independent_block_class .' .banner-grid-item{ padding: '. esc_attr( $gutter ) .'; }'.' .'.$independent_block_class .' .banner-grid-parent{ margin: -'. esc_attr( $gutter ) .'; }';
		}
		
		if( isset( $banner_link_color ) && $banner_link_color != '' ){
			$independent_block_styles .= '.'. $independent_block_class .' a { color: '. esc_attr( $banner_link_color ) .'; }';
		}
		if( isset( $banner_linkh_color ) && $banner_linkh_color != '' ){
			$independent_block_styles .= '.'. $independent_block_class .' a:hover { color: '. esc_attr( $banner_linkh_color ) .'; }';
		}
		
		$banner_overlay = isset( $banner_overlay ) && $banner_overlay != '' ? esc_attr( $banner_overlay ) : ''; 
		$as_bg = isset( $as_bg ) && $as_bg == 'yes' ? true : false;
		
		$filter = isset( $post_filter ) ? $post_filter : 'recent';
		$filter_by = isset( $post_filter_by ) ? $post_filter_by : 'recent';
		$orderby = $meta_key = $days = $post_in = '';
		$order = 'DESC';
		
		if( $filter == 'recent' ){ // ascending order
			$order = 'DESC';
		}elseif( $filter == 'asc' ){ // ascending order
			$order = 'ASC';
		}elseif( $filter == 'random' ){
			$orderby = 'rand';
		}
		
		if( $filter_by == 'likes' ){ // likes 
			$meta_key = 'likes';
			$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
		}elseif( $filter_by == 'views' ){ //views
			$meta_key = 'views';
			$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
		}elseif( $filter_by == 'rated' ){ //rated
			$meta_key = 'rated';
			$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
		}elseif( $filter_by == 'comment' ){ // comment
			$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'comment_count';			
		}elseif( $filter_by == 'days' ){ // days
			$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'date';
			$days = isset( $days_count ) && $days_count != '' ? absint( $days_count ) : '10';
		}elseif( $filter_by == 'custom' ){ // comment
			$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'post__in';
			$post_in = isset( $include_post_ids ) && $include_post_ids != '' ? $include_post_ids : '';
		}
		
		$post_not_in = isset( $exclude_post_ids ) && $exclude_post_ids != '' ? $exclude_post_ids : '';
			
		$filter_name = isset( $filter_name ) ? esc_attr( $filter_name ) : '';
		$filter_values = isset( $$filter_name ) ? esc_attr( $$filter_name ) : '';
		$filter_values = str_replace(' ', '', $filter_values);
		$filter_values = rtrim($filter_values, ',');
		$post_per_tab = isset( $post_per_tab ) ? esc_attr( $post_per_tab ) : '5';
		
		$date_query = $days != '' ? array( array( 'after' => absint( $days ) . ' days ago' ) ) : '';
		$post_not = $post_not_in != '' ? explode(',', esc_attr($post_not_in)) : '';
		$post_in = $post_in != '' ? explode(',', esc_attr($post_in)) : '';
		
		// Theme Update from v1.0.4
		$independent_options = get_option( 'independent_options' );
		if( isset( $independent_options['duplicate-news'] ) && !$independent_options['duplicate-news'] ){
			$unique_ids = independent_get_unique_news_ids();
			if( $unique_ids ){
				if( !empty( $post_not ) && is_array( $post_not ) ){
					$post_not = array_merge( $post_not, $unique_ids ); 
				}else{
					$post_not = array();
					$post_not = array_merge( $post_not, $unique_ids ); 
				}
			}
		}
		
		$meta_key = $meta_key;
		if( $meta_key == 'likes' ){
			$meta_key = 'independent_post_fav_count';
		}elseif( $meta_key == 'views' ){
			$meta_key = 'independent_post_views_count';
		}elseif( $meta_key == 'rated' ){
			$meta_key = 'independent_post_review_rating';
		}
		
		$modal_array = array('layout-1' => '4', 'layout-2' => '5', 'layout-3' => '3', 'layout-4' => '5', 'layout-5' => '4', 'layout-6' => '7', 'layout-7' => '5');
		$banner_mod = isset( $banner_modal ) ? esc_attr( $banner_modal ) : '1';
		
		$banner_items = '';
		$items_count = 0;
		$banner_class = '';
		if( $banner_mod != 'dynamic' ){
			$banner_class = ' banner-layout-'.$banner_mod;
			$items_count = $modal_array['layout-'.$banner_mod];
		}elseif( isset( $banner_tools ) && $banner_tools != '' ){	
			$banner_tools = str_replace(',',' ',$banner_tools);
			$banner_items = rtrim($banner_tools, ' ');
			$banner_items =  explode( ' ', $banner_items );
			$items_count = count( $banner_items );
		}
		$slide_attr = $nav_class = '';
		$slide_items = $items_count;
		if( isset( $banner_slide ) && $banner_slide != 'no' ){
		
			$slide_items = isset( $banner_slide_item ) ? absint( $banner_slide_item ) * $items_count : $items_count ;
			
			$slide_attr .= ' data-items="1"';
			$slide_attr .= ' data-items-mob="1"';
			$slide_attr .= ' data-items-tab="1"';
			$slide_attr .= ' data-loop="'. ( isset( $banner_slide_loop ) ? esc_attr( $banner_slide_loop ) : 'false' ) .'"';
			$slide_attr .= ' data-play="'. ( isset( $banner_slide_play ) ? esc_attr( $banner_slide_play ) : 'false' ) .'"';
			$slide_attr .= ' data-nav="'. ( isset( $banner_slide_nav ) ? esc_attr( $banner_slide_nav ) : 'false' ) .'"';
			$slide_attr .= ' data-dots="'. ( isset( $banner_slide_page ) ? esc_attr( $banner_slide_page ) : 'false' ) .'"';
			$slide_attr .= ' data-timeout="'. ( isset( $banner_slide_timeout ) ? absint( $banner_slide_timeout ) : '5000' ) .'"';
			$slide_attr .= ' data-speed="'. ( isset( $banner_slide_speed ) ? absint( $banner_slide_speed ) : '700' ) .'"';
			
			if( $banner_slide_nav == 'true' && isset( $banner_slide_nav_modal ) && $banner_slide_nav_modal == 'normal' ){
				$nav_class .= isset( $banner_slide_nav_position ) ? ' nav-'. esc_attr( $banner_slide_nav_position ) : '';
				$nav_class .= isset( $banner_slide_nav_align ) ? ' nav-'. esc_attr( $banner_slide_nav_align ) : '';
			}else{
				$nav_class .= isset( $banner_slide_nav_align ) ? ' nav-wide' : '';
			}
		}
		
		if( $filter_name == 'tag' ){
			$filter_name = 'tag__in';
			$filter_values = explode(",", $filter_values);
		}
		$args = array(
				'posts_per_page' => $slide_items,
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				$filter_name => $filter_values,
				'meta_key' => esc_attr( $meta_key ),
				'orderby' => $orderby,
				'order' => $order,
				'date_query' => $date_query,
				'post__not_in' => $post_not,
				'post__in' => $post_in
			 );	
		$query = new WP_Query( $args );
		
		$block_css_options = array(
			'block_style' => $independent_block_styles
		);
		$banner_class .= ' '.$independent_block_class;
		$banner_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'independent_vc_banner', $atts );
		$banner_class .= $as_bg ? ' independent-banner-as-bg' : '';
		
		$output .= '<div class="independent-banner'. esc_attr( $banner_class ) .'">';
		
		if( $independent_block_styles != '' )
		$output .= '<input type="hidden" class="news-block-css-options" data-options="'. htmlspecialchars(json_encode($block_css_options), ENT_QUOTES, 'UTF-8') .'" />';	
		$i = 0;
		
		$item_n_class = '';
		
		if ($query->have_posts()){
			$output .= isset( $banner_slide ) && $banner_slide != 'no'  ? '<div class="owl-carousel owl-banner'. esc_attr( $nav_class ) .'" '. $slide_attr .'>' : '';	
			$slide_item_out = '';
			
			$banner_out_array = array();
			$banner_layout_fun = 'independent_banner_layout_'.$banner_mod;
			$banner_layout_out = 'independent_banner_output_'.$banner_mod;
			
			while ($query->have_posts()){
				$query->the_post();
				$unique_ids = independent_get_unique_news_ids( get_the_ID() );
				if( $banner_mod != 'dynamic' ){					
					$banner_out_array[$i] = $banner_layout_fun($i, $atts);
				}
				else{ //banner mode dynamic
					//50x100,50x50,25x25,25x25,25x100,33x100
					if( isset( $banner_items[$i] ) ){
					
						$item = $banner_items[$i];
						
						if( $banner_overlay == 'rainbow' ){
							if( $i < 7 ){
								$item_n_class = 'banner-gradient-'. absint( $i + 1 );
							}else{
								$rem = $i % 6;
								$item_n_class = 'banner-gradient-'. absint( $rem );
							}					
						}
						
						switch($item){
							case '67x100': //big square
								$slide_item_out .= '<div class="banner-grid-item banner-grid-67x100 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_67x100', 1);
								$slide_item_out .= '</div>';
							break;
							case '50x100': //big square
								$slide_item_out .= '<div class="banner-grid-item banner-grid-50x100 banner-grid-itemw2 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_50x100', 1);
								$slide_item_out .= '</div>';
							break;
							case '50x50': //horizontal
								$slide_item_out .= '<div class="banner-grid-item banner-grid-50x50 banner-grid-itemw2 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_50x50', 2);
								$slide_item_out .= '</div>';
							break;
							case '33x50': //horizontal
								$slide_item_out .= '<div class="banner-grid-item banner-grid-33x50 banner-grid-itemw3 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_33x50', 2);
								$slide_item_out .= '</div>';
							break;
							case '25x50': //small square
								$slide_item_out .= '<div class="banner-grid-item banner-grid-25x50 banner-grid-itemw4 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_25x50', 0);
								$slide_item_out .= '</div>';
							break;
							case '25x100': //vertical
								$slide_item_out .= '<div class="banner-grid-item banner-grid-25x100 banner-grid-itemw4 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_25x100', 0);
								$slide_item_out .= '</div>';
							break;
							case '33x100': //vertical
								$slide_item_out .= '<div class="banner-grid-item banner-grid-33x100 banner-grid-itemw3 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_33x100', 0);
								$slide_item_out .= '</div>';
							break;
							case '50x33': //vertical
								$slide_item_out .= '<div class="banner-grid-item banner-grid-50x33 banner-grid-itemw2 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_50x33', 2);
								$slide_item_out .= '</div>';
							break;
							case '25x66': //vertical
								$slide_item_out .= '<div class="banner-grid-item banner-grid-25x66 banner-grid-itemw4 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_25x66', 2);
								$slide_item_out .= '</div>';
							break;
							case '25x33': //vertical
								$slide_item_out .= '<div class="banner-grid-item banner-grid-25x33 banner-grid-itemw4 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_25x33', 0);
								$slide_item_out .= '</div>';
							break;
							case '100x100': //big square
								$slide_item_out .= '<div class="banner-grid-item banner-grid-100x100 '. esc_attr( $item_n_class ) .'">';
									$slide_item_out .= independent_singleBannerPost($atts, 'independent_banner_100x100', 1);
								$slide_item_out .= '</div>';
							break;
						}
						
					}
				}
				
				
				$i++;
				if( $i == $items_count ){
					if( $banner_mod != 'dynamic' ){
						$output .= $banner_layout_out($banner_out_array);
					}else{
						$output .= '<div class="banner-grid-parent clearfix">';
							$output .= $slide_item_out;	
						$output .= '</div>';
					}					
					$i = 0; $slide_item_out = '';
					$banner_out_array = array();
				}
				
			}
			
			if( $i != 0 ){
				if( $banner_mod != 'dynamic' ){
					$output .= $banner_layout_out($banner_out_array);
				}else{
					$output .= '<div class="banner-grid-parent clearfix">';
						$output .= $slide_item_out;	
					$output .= '</div>';
				}
			}
			
			wp_reset_postdata();
			$output .= isset( $banner_slide ) && $banner_slide != 'no'  ? '</div><!--owl-banner-->' : '';
		}
					
		$output .= '</div><!--independent-banner-->';
		
		return $output;
	}
}
if ( ! function_exists( 'independent_vc_banner_shortcode_map' ) ) {
	function independent_vc_banner_shortcode_map() {
	
		$cats_show = $tags_show = $authors = esc_html__( 'Example: 2, 3, 4', 'independent' ); 
				
		vc_map( 
			array(
				"name"					=> esc_html__( "Banner", "independent" ),
				"description"			=> esc_html__( "This is shortcode to build banner and have multiple layouts.", 'independent' ),
				"base"					=> "independent_vc_banner",
				"category"				=> esc_html__( "Blocks", "independent" ),
				"icon"					=> "zozo-vc-icon",
				"params"				=> array(
					/*General*/
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Title Alignment", "independent" ),
						"param_name"	=> "title_align",
						"value"			=> array(
							esc_html__( "Default", "independent" )	=> "default",
							esc_html__( "Left", "independent" )	=> "left",
							esc_html__( "Right", "independent" )	=> "right",
							esc_html__( "Center", "independent" )	=> "center",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Banner Image as Background", "independent" ),
						"param_name"	=> "as_bg",
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "no",
							esc_html__( "Yes", "independent" )	=> "yes"
						),
						"std"	=> "no",
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Category/Tag Name", "independent" ),
						"param_name"	=> "filter_enable",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "yes",
							esc_html__( "No", "independent" )	=> "no",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Author Name", "independent" ),
						"param_name"	=> "author_enable",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "yes",
							esc_html__( "No", "independent" )	=> "no",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Date", "independent" ),
						"param_name"	=> "date_enable",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "yes",
							esc_html__( "No", "independent" )	=> "no",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Post Format Icon", "independent" ),
						"param_name"	=> "icon_enable",
						"value"			=> array(
							esc_html__( "Yes", "independent" )	=> "yes",
							esc_html__( "No", "independent" )	=> "no",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Post Trending Icon", "independent" ),
						"param_name"	=> "trend_enable",
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "no",
							esc_html__( "Yes", "independent" )	=> "yes",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Banner Modal", "independent" ),
						"param_name"	=> "banner_modal",
						"value"			=> array(
							esc_html__( "Modal 1", "independent" )	=> "1",
							esc_html__( "Modal 2", "independent" )	=> "2",
							esc_html__( "Modal 3", "independent" )	=> "3",
							esc_html__( "Modal 4", "independent" )	=> "4",
							esc_html__( "Modal 5", "independent" )	=> "5",
							esc_html__( "Modal 6", "independent" )	=> "6",
							esc_html__( "Modal 7", "independent" )	=> "7",
							esc_html__( "Dynamic", "independent" )	=> "dynamic",
						),
					),
					array(
						"type"			=> 'thumb_toolbox',
						"heading"		=> esc_html__( "Tools", "independent" ),
						"param_name"	=> "banner_tools",
						"all_tools"		=> "100x100,67x100,50x100,33x100,33x50,50x50,25x50,25x100,50x33,25x66,25x33",
						"value"			=> "",
						"dependency" => array( "element" => "banner_modal", "value" => 'dynamic' ),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Banner Gutter Size', "independent" ),
						'param_name'	=> 'banner_gutter',
						'description'	=> esc_html__( 'Enter here banner gutter size you want. eg: 2', "independent" ),
					),
					/*Filter*/
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Select Tag/Category", "independent" ),
						"param_name"	=> "filter_name",
						"value"			=> array(
							esc_html__( "Category", "independent" )	=> "cat",
							esc_html__( "Tag", "independent" )		=> "tag",
							esc_html__( "Author", "independent" )	=> "author",
						),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Category Id\'s', "independent" ),
						'param_name'	=> 'cat',
						'value' 		=> '',
						'description'	=> $cats_show,
						"dependency"	=> array(
								"element"	=> "filter_name",
								"value"		=> "cat"
						),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Tag Slug\'s', "independent" ),
						'param_name'	=> 'tag',
						'value' 		=> '',
						'description'	=> $tags_show,
						"dependency"	=> array(
								"element"	=> "filter_name",
								"value"		=> "tag"
						),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Authors ID\'s', "independent" ),
						'param_name'	=> 'author',
						'value' 		=> '',
						'description'	=> $authors,
						"dependency"	=> array(
								"element"	=> "filter_name",
								"value"		=> "author"
						),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Filter", "independent" ),
						"param_name"	=> "post_filter",
						"value"			=> array(
							esc_html__( "Recent News(Descending)", "independent" )	=> "recent",
							esc_html__( "Older News(Ascending)", "independent" )		=> "asc",
							esc_html__( "Random", "independent" )					=> "random"
						),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Filter By", "independent" ),
						"param_name"	=> "post_filter_by",
						"value"			=> array(
							esc_html__( "None", "independent" )				=> "none",
							esc_html__( "Most Likes", "independent" )		=> "likes",
							esc_html__( "Most Views", "independent" )		=> "views",
							esc_html__( "High Rated", "independent" )		=> "rated",
							esc_html__( "Most Commented", "independent" )	=> "comment",
							esc_html__( "From Custom Days", "independent" )	=> "days",
							esc_html__( "Custom Posts IDs", "independent" )	=> "custom"
						),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Enter Days', "independent" ),
						'param_name'	=> 'days_count',
						'description'	=> esc_html__( 'if enter 10 means, it\'s showing last 10 days posts.', "independent" ),
						'group'			=> esc_html__( 'Filter', 'independent' ),
						"dependency" => array( "element" => "post_filter_by", "value" => 'days' ),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Include Post ID\'s', "independent" ),
						'param_name'	=> 'include_post_ids',
						'description'	=> esc_html__( 'Manually enter post id\'s for include. These post ordered not based on Ascending, Descending or Random. eg: 21, 15, 30', "independent" ),
						'group'			=> esc_html__( 'Filter', 'independent' ),
						"dependency" => array( "element" => "post_filter_by", "value" => 'custom' ),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Exclude Post ID\'s', "independent" ),
						'param_name'	=> 'exclude_post_ids',
						'description'	=> esc_html__( 'Manually enter post id\'s for exclude. eg: 21, 15, 30', "independent" ),
						'group'			=> esc_html__( 'Filter', 'independent' )
					),
					/*Overlay*/
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Overlay Styles", "independent" ),
						"param_name"	=> "banner_overlay",
						"value"			=> array(
							esc_html__( "Black Gradient", "independent" )	=> "black",
							esc_html__( "Theme Gradient", "independent" )	=> "theme",
							esc_html__( "Flat Black", "independent" )		=> "dark",
							esc_html__( "Rainbow", "independent" )			=> "rainbow",
							esc_html__( "None", "independent" )				=> "",
						),
						'group'			=> esc_html__( 'Overlay', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Overlay Details Position", "independent" ),
						"param_name"	=> "overlay_position",
						"value"			=> array(
							esc_html__( "Top Left", "independent" )	=> "top-left",
							esc_html__( "Top Right", "independent" )	=> "top-right",
							esc_html__( "Middle", "independent" )	=> "middle",
							esc_html__( "Bottom Left", "independent" )	=> "bottom-left",
							esc_html__( "Bottom Right", "independent" )	=> "bottom-right",
						),
						'group'			=> esc_html__( 'Overlay', 'independent' )
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__( 'Banner Link Color', "independent" ),
						'param_name'	=> 'banner_link_color',
						'description'	=> esc_html__( 'Choose this banner link color.', 'independent' ),
						'group'			=> esc_html__( 'Overlay', 'independent' )
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__( 'Banner Link Hover/Active Color', "independent" ),
						'param_name'	=> 'banner_linkh_color',
						'description'	=> esc_html__( 'Choose this banner link hover and active color.', 'independent' ),
						'group'			=> esc_html__( 'Overlay', 'independent' )
					),
					//Slide
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Banner Slide", "independent" ),
						"param_name"	=> "banner_slide",
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "no",
							esc_html__( "Yes", "independent" )	=> "yes",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Loop", "independent" ),
						"param_name"	=> "banner_slide_loop",
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "false",
							esc_html__( "Yes", "independent" )	=> "true",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Autoplay", "independent" ),
						"param_name"	=> "banner_slide_play",
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "false",
							esc_html__( "Yes", "independent" )	=> "true",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Navigation", "independent" ),
						"param_name"	=> "banner_slide_nav", 
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "false",
							esc_html__( "Yes", "independent" )	=> "true",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Enable Pagination", "independent" ),
						"param_name"	=> "banner_slide_page",
						"value"			=> array(
							esc_html__( "No", "independent" )	=> "false",
							esc_html__( "Yes", "independent" )	=> "true",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Number of Slide to Show', "independent" ),
						'param_name'	=> 'banner_slide_item',
						'value'			=> '2',
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Slide Timeout', "independent" ),
						'param_name'	=> 'banner_slide_timeout',
						'value'			=> '5000',
						'description'	=> esc_html__( 'This is setting for slide interval. eg: 1000 = 1 second', "independent" ),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Slide Autoplay Speed', "independent" ),
						'param_name'	=> 'banner_slide_speed',
						'value'			=> '700',
						'description'	=> esc_html__( 'This is setting for slide speed. eg: 1000 = 1 second', "independent" ),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Navigation Modal", "independent" ),
						"param_name"	=> "banner_slide_nav_modal",
						"value"			=> array(
							esc_html__( "Normal", "independent" )	=> "normal",
							esc_html__( "Wide", "independent" )	=> "wide",
						),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Navigation Position", "independent" ),
						"param_name"	=> "banner_slide_nav_position",
						"value"			=> array(
							esc_html__( "Bottom", "independent" )	=> "bottom",
							esc_html__( "Top", "independent" )		=> "top",							
						),
						"dependency" => array( "element" => "banner_slide_nav_modal", "value" => 'normal' ),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Navigation Alignment", "independent" ),
						"param_name"	=> "banner_slide_nav_align",
						"value"			=> array(
							esc_html__( "Left", "independent" )		=> "left",
							esc_html__( "Center", "independent" )	=> "center",
							esc_html__( "Right", "independent" )		=> "right",	
							esc_html__( "Seperate", "independent" )	=> "seperate"						
						),
						"dependency" => array( "element" => "banner_slide_nav_modal", "value" => 'normal' ),
						'group'			=> esc_html__( 'Slide', 'independent' )
					),
					array(
						'type' => 'css_editor',
						'heading' => esc_html__( 'Css', 'independent' ),
						'param_name' => 'css',
						'group' => esc_html__( 'Design options', 'independent' ),
					),
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'independent_vc_banner_shortcode_map' );