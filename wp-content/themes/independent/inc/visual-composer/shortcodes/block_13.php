<?php 
/**
 *		# Block 13 Shortcode #
 * -- Single News --
 */
if ( ! function_exists( 'independent_vc_block_13_shortcode' ) ) { 
	function independent_vc_block_13_shortcode( $atts, $content = NULL ) {
		$atts = vc_map_get_attributes( 'independent_vc_block_13', $atts ); 
		return independent_news_block_form_13($atts);
	}
}
function independent_news_block_form_13($atts, $custom_loop = ''){
	extract( $atts );
	$rand = independent_shortcode_rand_id(); 
	$output = '';
	/*Design Option Start*/
	$css = '';
	extract(shortcode_atts(array( 
		'css' => '',
	), $atts));
	$css_class = $independent_block_styles = $block_styles = $title_bg = '';
	$independent_block_class = 'independent-block-css-'.$rand ;
	
	$css_class = isset( $extra_class ) ? $extra_class : '';
	
	$title_transform = isset( $title_transform ) ? esc_attr( $title_transform ) : 'capitalize';	
	$title_size = isset( $title_size ) && $title_size != '' ? $title_size : '17px';
	$block_color = isset( $block_color ) && $block_color != '' ? $block_color : '';
	$tab_limit = isset( $tab_limit ) && $tab_limit != '' ? $tab_limit : '2';
	$title_position = isset( $title_position ) && $title_position != '' ? $title_position : '';
	$title = isset( $title ) && $title != '' ? $title : '';
	
	$style_arr = array(
		'title_transform' => $title_transform,
		'block_color' => $block_color,
		'title_size' => $title_size
	);
	if( $title_transform || $block_color != '' ){
		$css_class .= ' '.$independent_block_class;
		$independent_block_styles = independent_block_dynamic_css($independent_block_class, $style_arr);
	}
	
	//Grid Spacing
	if( isset( $sc_grid_spacing ) && !empty( $sc_grid_spacing ) ){
		$sc_grid_spacing = preg_replace( '!\s+!', ' ', $sc_grid_spacing );
		$space_arr = explode( " ", $sc_grid_spacing );
		$i = 1;
		$space_class_name = '.' . esc_attr( $independent_block_class ) . ' .independent-news .post-grid >';
		foreach( $space_arr as $space ){
			$independent_block_styles .= $space != '-' ? $space_class_name .' div:nth-child('. esc_attr( $i ) .') { margin-bottom: '. esc_attr( $space ) .'px; }' : '';
			$i++;
		}
	}
	
	//Category Tag Color
	if( isset( $cat_tag_bg_color ) && !empty( $cat_tag_bg_color ) ){
		$independent_block_styles .= '.' . esc_attr( $independent_block_class ) . ' .independent-news .independent-block-post .category-tag { background-color: '. esc_attr( $cat_tag_bg_color ) .' !important; }';
		$independent_block_styles .= '.' . esc_attr( $independent_block_class ) . ' .independent-news .independent-block-post .category-tag:after { border-top-color: '. esc_attr( $cat_tag_bg_color ) .' !important; }';
	}
	if( isset( $cat_tag_color ) && !empty( $cat_tag_color ) ){
		$independent_block_styles .= '.' . esc_attr( $independent_block_class ) . ' .independent-news .independent-block-post .category-tag { color: '. esc_attr( $cat_tag_color ) .' !important; }';
	}
	
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'independent_vc_block_new', $atts );
	$css_class .= isset( $animate_type ) ? ' animate-news-fade-in-' . $animate_type : 'animate-news-fade-in-down';
	/*Design Option End*/
	
	/*Dynamic Options*/
	$grid_items = isset( $grid_items ) ? independent_drag_and_drop_trim( $grid_items ) : '';
	$grid_primary_meta = isset( $grid_primary_meta ) ? independent_drag_and_drop_trim( $grid_primary_meta ) : '';	
	$grid_secondary_meta = isset( $grid_secondary_meta ) ? independent_drag_and_drop_trim( $grid_secondary_meta ) : '';
	
	$ovelay_items = '';
	if( isset( $overlay_options ) && $overlay_options != 'no' ){
		$ovelay_items = isset( $block_grid_ovelay_items ) ? independent_drag_and_drop_trim( $block_grid_ovelay_items ) : '';
	}
	
	$content_len = isset( $excerpt_len ) && absint( $excerpt_len ) ? absint( $excerpt_len ) : esc_html__( 'More', 'independent' );
	$all_text = isset( $first_tab_text ) && $first_tab_text != '' ? esc_attr( $first_tab_text ) : '';
	$more_text = isset( $more_text ) && $more_text != '' ? esc_attr( $more_text ) : 20;
	$readmore_text = isset( $readmore_text ) && $readmore_text != ''  ? esc_attr( $readmore_text ) : esc_html__( 'Read More', 'independent' );
	$filter_type = isset( $ajax_filter_type ) && $ajax_filter_type != ''  ? esc_attr( $ajax_filter_type ) : 'default';
	$loadmore_after = isset( $infinite_loadmore ) && $infinite_loadmore != ''  ? absint( $infinite_loadmore ) : '';
	$pagination = isset( $pagination ) ? esc_attr( $pagination ) : 'no';
	$grid_thumb_size = isset( $grid_thumb_size ) ? esc_attr( $grid_thumb_size ) : 'independent_grid_1';
	$grid_image_custom = isset( $custom_image_size ) ? esc_attr( $custom_image_size ) : '';
	$grid_align = isset( $block_grid_align ) ? esc_attr( $block_grid_align ) : '';
	$cat_tag = isset( $cat_tag ) ? esc_attr( $cat_tag ) : '';
	$cat_tag_style = isset( $cat_tag_style ) ? esc_attr( $cat_tag_style ) : '';
	$post_icon = isset( $post_icon ) ? esc_attr( $post_icon ) : '';
	$grid_title = isset( $grid_title_variation ) ? esc_attr( $grid_title_variation ) : 'h4';
	$tab_hide = isset( $tab_hide ) ? esc_attr( $tab_hide ) : 'no';
	$post_promotion = isset( $post_promotion ) ? esc_attr( $post_promotion ) : 'no';
	
	$css_class .= $filter_type != 'default' ? ' no-slide-verlay' : '';
	
	$dynamic_options = array('grid_items' => $grid_items, 'grid_primary_meta' => $grid_primary_meta, 'grid_secondary_meta' => $grid_secondary_meta, 'modal' => '13', 'excerpt_len' => $content_len, 'all_text' => $all_text, 'more_text' => $more_text, 'read_more' => $readmore_text, 'filter_type' => $filter_type, 'loadmore_after' => $loadmore_after, 'pagination' => $pagination, 'ovelay_items' => $ovelay_items, 'grid_thumb' => $grid_thumb_size, 'grid_image_custom' => $grid_image_custom, 'grid_align' => $grid_align, 'cat_tag' => $cat_tag, 'cat_tag_style' => $cat_tag_style, 'post_icon' => $post_icon, 'post_promotion' => $post_promotion, 'grid_title' => $grid_title, 'tab_hide' => $tab_hide );
	$filter = isset( $post_filter ) ? $post_filter : 'recent';
	$filter_by = isset( $post_filter_by ) ? $post_filter_by : 'recent';
	$orderby = $meta_key = $days = $post_in = '';
	$order = 'DESC';
	
	if( $filter == 'recent' ){ // ascending order
		$order = 'DESC';
	}elseif( $filter == 'asc' ){ // ascending order
		$order = 'ASC';
	}elseif( $filter == 'random' ){
		$orderby = 'rand';
	}
	
	if( $filter_by == 'likes' ){ // likes 
		$meta_key = 'likes';
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
	}elseif( $filter_by == 'views' ){ //views
		$meta_key = 'views';
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
	}elseif( $filter_by == 'rated' ){ //rated
		$meta_key = 'rated';
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'meta_value_num';			
	}elseif( $filter_by == 'comment' ){ // comment
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'comment_count';			
	}elseif( $filter_by == 'days' ){ // days
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'date';
		$days = isset( $days_count ) && $days_count != '' ? absint( $days_count ) : '10';
	}elseif( $filter_by == 'custom' ){ // comment
		$orderby = !empty( $orderby ) ? $orderby . ' meta_value_num' : 'post__in';
		$post_in = isset( $include_post_ids ) && $include_post_ids != '' ? $include_post_ids : '';
	}
	
	$post_not_in = isset( $exclude_post_ids ) && $exclude_post_ids != '' ? $exclude_post_ids : '';
		
	$filter_name = isset( $filter_name ) ? esc_attr( $filter_name ) : '';
	$filter_values = isset( $$filter_name ) ? esc_attr( $$filter_name ) : '';
	$filter_values = str_replace(' ', '', $filter_values);
	$filter_values = rtrim($filter_values, ',');
	
	$post_per_tab = isset( $post_per_tab ) ? esc_attr( $post_per_tab ) : '5';
	
	$slide_id = $all_text != '' ? 'all' : preg_replace('/^([^,]*).*$/', '$1', $filter_values);
	
	$independent_ajax_nonce = wp_create_nonce('independent-ajax-nonce');
	$block_options = array(
			'action' => 'independent-ajax-slide',
			'nonce' => $independent_ajax_nonce,
			'filter_name' => $filter_name, 
			'filter_values' => $filter_values, 
			'ppp' => $post_per_tab, 
			'paged' => 1, 
			'meta' => $meta_key,
			'orderby' => $orderby,
			'order' => $order,
			'date' => $days,
			'post_not' => $post_not_in,
			'post_in' => $post_in,
			'block_id' => $rand,
			'slide_id' => $slide_id,
			'tab_limit' => $tab_limit,
			'dynamic_options' => $dynamic_options,
		);	
		
		$block_css_options = array(
			'block_style' => $independent_block_styles
		);			
	
	$nbp = new independentBlockParams;
	$nbp->setindependentBlockParams( "independent_block_id_". esc_attr( $rand ), $block_options );
	
	$output = '<div class="independent-block independent-block-13 '. esc_attr( $css_class ) .'" data-id="independent_block_id_'.esc_attr( $rand ).'">';
		$output .= '<input type="hidden" class="news-block-options" id="independent_block_id_'.esc_attr( $rand ).'" />';
		if( $independent_block_styles != '' )
		$output .= '<input type="hidden" class="news-block-css-options" data-options="'. htmlspecialchars(json_encode($block_css_options), ENT_QUOTES, 'UTF-8') .'" />';
		
		$title_class = independent_blocksTitleAlign($title_position);
		$title_class .= isset( $title_style ) && $title_style != '' ? ' title-style-' . $title_style : '';
		if( $title_position != 'tab' ){
			if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_html( $title ) .'</span></h4>';
		}
		
		$t_count = count( explode(',', $filter_values ) );
		if( ( isset( $ajax_filter ) && $ajax_filter == 'yes' ) || $t_count > 1 ){
			
			if( $t_count == 1 ){
				$output .= '<div class="independent-content">';
					$output .= '<div class="news-slide-loader"><img src="'.esc_url( independent_news_loader() ).'" alt="'. esc_attr__('Loader..', 'independent') .'" /></div>';
					$output .= independent_news_block_slider($block_options);
				$output .= '</div> <!--independent-content-->';
			}else{
				if( $title_position == 'tab' ){
					$output .= '<div class="independent-block-title-pack">';
						if( $title != '' )$output .= '<h4 class="independent-block-title '. esc_attr( $title_class ) .'"><span>'. esc_html( $title ) .'</span></h4>';
						$output .= independent_news_block_tabs_only($block_options);
					$output .= '</div><!-- .independent-block-title-pack -->';
					$output .= '<div class="tab-content independent-content">';
					$output .= '<div class="news-slide-loader"><img src="'.esc_url( independent_news_loader() ).'" alt="'. esc_attr__('Loader..', 'independent') .'" /></div>';
							$output .= independent_news_block_slider($block_options);
					$output .= '</div>';
				}else{
					$output .= independent_news_block_tabs($block_options);
				}
			}
			
			if( isset( $ajax_filter ) && $ajax_filter == 'yes' ){
				if( $pagination != 'yes' ){
					$output .= '<div class="independent-slider-nav type-'. esc_attr( $filter_type ) .'">';
						$output .= '<ul class="slide-nav list-inline '. esc_attr( independent_blocksTitleAlign($nav_align) ) .'">';
							if( $filter_type == 'default' ){
								$output .= '<li><a href="#" class="independent-slider-previous disabled"><i class="fa fa-angle-left"></i></a></li>';
								$output .= '<li><a href="#" class="independent-slider-next"><i class="fa fa-angle-right"></i></a></li>';
							}else{
								$output .= '<li>
												<span class="independent-load-more btn">'. esc_attr( $loadmore_text ) .'</span>
												<img src="'.esc_url( independent_news_loader() ).'" alt="'. esc_attr__('Loader..', 'independent') .'" />
											</li>';
							}
						$output .= '</ul>';
					$output .= '</div> <!--news-slide-nav-->';	
				}
			}
		}else{
			$output .= '<div class="independent-content">';
				$output .= independent_news_block($block_options, $custom_loop);
			$output .= '</div> <!--independent-content-->';
		}
	$output .= '</div>';
	
	return $output;
}
function independent_news_block_modal_13($loop, $dynamic_options){
	$output = '';
	while ($loop->have_posts()){
		$loop->the_post();
		
		// Theme Update from v1.0.4
		$unique_ids = independent_get_unique_news_ids( get_the_ID() );
		
		$output .= '<div class="row">';
			$output .= '<div class="col-md-12"><!--top col start-->';
				$output .= independent_common_block_grid_generate($dynamic_options);
			$output .= '</div>';
		$output .= '</div>';
	}
	return $output;
}
if ( ! function_exists( 'independent_vc_block_13_shortcode_map' ) ) {
	function independent_vc_block_13_shortcode_map() {
		vc_map( independent_vc_map_block_13());
	
		
	}
}
add_action( 'vc_before_init', 'independent_vc_block_13_shortcode_map' );
function independent_vc_map_block_13(){
	
	$cats_show = $tags_show = $authors = esc_html__( 'Example: 2, 3, 4', 'independent' ); 
			
	$map_fields = array(
		"name"					=> esc_html__( "Block 13", "independent" ),
		"description"			=> esc_html__( "Single News.", 'independent' ),
		"base"					=> "independent_vc_block_13",
		"category"				=> esc_html__( "Blocks", "independent" ),
		"icon"					=> "zozo-vc-icon",
		"params"				=> array(
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Extra Classes', "independent" ),
				'param_name'	=> 'extra_class',
				'value' 		=> '',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Title', "independent" ),
				'param_name'	=> 'title',
				'value' 		=> '',
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Position", "independent" ),
				"param_name"	=> "title_position",
				"value"			=> array(
					esc_html__( "Default", "independent" )	=> "",
					esc_html__( "Left", "independent" )			=> "left",
					esc_html__( "Center", "independent" )	=> "center",
					esc_html__( "Right", "independent" )	=> "right",
					esc_html__( "Title with Tab", "independent" )	=> "tab"
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Position", "independent" ),
				"param_name"	=> "title_position",
				"value"			=> array(
					esc_html__( "Left", "independent" )			=> "left",
					esc_html__( "Center", "independent" )	=> "center",
					esc_html__( "Right", "independent" )	=> "right",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Style", "independent" ),
				"param_name"	=> "title_style",
				"value"			=> array(
					esc_html__( "Default", "independent" )	=> "",
					esc_html__( "Style 1", "independent" )	=> "1",
					esc_html__( "Style 2", "independent" )	=> "2",
					esc_html__( "Style 3", "independent" )	=> "3",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Style", "independent" ),
				'description'	=> esc_html__( 'Choose block title style either capitalize, uppercase, etc..', 'independent' ),
				"param_name"	=> "title_transform",
				"value"			=> array(
					esc_html__( "Capitalize", "independent" )	=> "capitalize",
					esc_html__( "Upper Case", "independent" )	=> "uppercase",
					esc_html__( "Lower Case", "independent" )	=> "lowercase",
					esc_html__( "None", "independent" )	=> "none"
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Title Size', "independent" ),
				'param_name'	=> 'title_size',
				'description'	=> esc_html__( 'This is settings for title font size. Example 12px', 'independent' ),
				'value' 		=> '',
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Navigation Alignment", "independent" ),
				"param_name"	=> "nav_align",
				"value"			=> array(
					esc_html__( "Left", "independent" )	=> "left",
					esc_html__( "Center", "independent" )	=> "center",
					esc_html__( "Right", "independent" )	=> "right",
					esc_html__( "Seperate", "independent" )	=> "seperate"
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Excerpt Length', "independent" ),
				'param_name'	=> 'excerpt_len',
				'description'	=> esc_html__( 'Enter integer value for current block content length. eg: 20', 'independent' ),
				'value' 		=> '20',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Tab Dropdown Text', "independent" ),
				'param_name'	=> 'more_text',
				'description'	=> esc_html__( 'This is text show on more dropdown label', 'independent' ),
				'value' 		=> esc_html__( 'More', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Read More Text', "independent" ),
				'param_name'	=> 'readmore_text',
				'description'	=> esc_html__( 'This is for read more button label', 'independent' ),
				'value' 		=> esc_html__( 'Read More', 'independent' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Load More Text', "independent" ),
				'param_name'	=> 'loadmore_text',
				'description'	=> esc_html__( 'This is for load more button label. this is only working when ajax filter on and filter type will be load more.', 'independent' ),
				'value' 		=> esc_html__( 'Load More..', 'independent' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__( 'Block Color', "independent" ),
				'param_name'	=> 'block_color',
				'description'	=> esc_html__( 'Choose this color to change current block title, link and some hover colors.', 'independent' ),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Enable Category Tag", "independent" ),
				'description'	=> esc_html__( 'Enable this option to show tag the category with specified color.', 'independent' ),
				"param_name"	=> "cat_tag",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Category Tag Style", "independent" ),
				'description'	=> esc_html__( 'Choose style of category tag.', 'independent' ),
				"param_name"	=> "cat_tag_style",
				"value"			=> array(
					esc_html__( "Default", "independent" )	=> "",
					esc_html__( "Classic", "independent" )	=> "classic"
				)
			),
			array(
				"type"			=> 'colorpicker',
				"heading"		=> esc_html__( "Category Tag Background Color", "independent" ),
				'description'	=> esc_html__( 'Choose category tag background color. This color overwrite which color you gave for the category tag.', 'independent' ),
				"param_name"	=> "cat_tag_bg_color",
				"value"			=> ""
			),
			array(
				"type"			=> 'colorpicker',
				"heading"		=> esc_html__( "Category Tag Color", "independent" ),
				'description'	=> esc_html__( 'Choose category tag fore color.', 'independent' ),
				"param_name"	=> "cat_tag_color",
				"value"			=> ""
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Enable Post Format Icon", "independent" ),
				'description'	=> esc_html__( 'Enable to show post format icons on thumbnail.', 'independent' ),
				"param_name"	=> "post_icon",
				"value"			=> array(
					esc_html__( "Yes", "independent" )	=> "yes",
					esc_html__( "No", "independent" )	=> "no",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Enable Promoted Label", "independent" ),
				'description'	=> esc_html__( 'Enable to show promotion text which you entered o theme option. It will show only which posts are enabled promotion option.', 'independent' ),
				"param_name"	=> "post_promotion",
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes"					
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Disable Block Tab", "independent" ),
				'description'	=> esc_html__( 'Choose yes to disable news block tab.', 'independent' ),
				"param_name"	=> "tab_hide",
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Animation Type", "independent" ),
				'description'	=> esc_html__( 'Choose animation type for load post with animation.', 'independent' ),
				"param_name"	=> "animate_type",
				"value"			=> array(
					esc_html__( "Animate From Down", "independent" )	=> "up",
					esc_html__( "Animate From Up", "independent" )	=> "down",
					esc_html__( "Animate From Right", "independent" )	=> "right",
					esc_html__( "Animate From Left", "independent" )	=> "left",
					esc_html__( "Animate None", "independent" )	=> "none"
				),
			),
			//Filters
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Ajax Filter", "independent" ),
				"param_name"	=> "ajax_filter",
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Ajax Filter Type", "independent" ),
				"param_name"	=> "ajax_filter_type",
				"value"			=> array(
					esc_html__( "Next/Prev", "independent" )	=> "default",
					esc_html__( "Load More", "independent" )	=> "load-more",
					esc_html__( "Infinite Scroll", "independent" )	=> "infinite",
				),
				"dependency"	=> array(
						"element"	=> "ajax_filter",
						"value"		=> "yes"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Select Tag/Category", "independent" ),
				"param_name"	=> "filter_name",
				"value"			=> array(
					esc_html__( "Category", "independent" )	=> "cat",
					esc_html__( "Tag", "independent" )		=> "tag",
					esc_html__( "Author", "independent" )	=> "author",
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Category ID\'s', "independent" ),
				'param_name'	=> 'cat',
				'value' 		=> '',
				'description'	=> $cats_show,
				"dependency"	=> array(
						"element"	=> "filter_name",
						"value"		=> "cat"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Tag ID\'s', "independent" ),
				'param_name'	=> 'tag',
				'value' 		=> '',
				'description'	=> $tags_show,
				"dependency"	=> array(
						"element"	=> "filter_name",
						"value"		=> "tag"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Authors ID\'s', "independent" ),
				'param_name'	=> 'author',
				'value' 		=> '',
				'description'	=> $authors,
				"dependency"	=> array(
						"element"	=> "filter_name",
						"value"		=> "author"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Minimum Tab to Show', "independent" ),
				'param_name'	=> 'tab_limit',
				'description'	=> esc_html__( 'Limited number of selected tags/categories show in tab. remaining shift to more dropdown. ', "independent" ),
				'value' 		=> '2',
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Post to Show', "independent" ),
				'param_name'	=> 'post_per_tab',
				'description'	=> esc_html__( 'Number of post to show on first load.', "independent" ),
				'value' 		=> '5',
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Filter", "independent" ),
				"param_name"	=> "post_filter",
				"value"			=> array(
					esc_html__( "Recent News(Descending)", "independent" )	=> "recent",
					esc_html__( "Older News(Ascending)", "independent" )		=> "asc",
					esc_html__( "Random", "independent" )					=> "random"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Filter By", "independent" ),
				"param_name"	=> "post_filter_by",
				"value"			=> array(
					esc_html__( "None", "independent" )				=> "none",
					esc_html__( "Most Likes", "independent" )		=> "likes",
					esc_html__( "Most Views", "independent" )		=> "views",
					esc_html__( "High Rated", "independent" )		=> "rated",
					esc_html__( "Most Commented", "independent" )	=> "comment",
					esc_html__( "From Custom Days", "independent" )	=> "days",
					esc_html__( "Custom Posts IDs", "independent" )	=> "custom"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Enter Days', "independent" ),
				'param_name'	=> 'days_count',
				'description'	=> esc_html__( 'if enter 10 means, it\'s showing last 10 days posts.', "independent" ),
				'group'			=> esc_html__( 'Filter', 'independent' ),
				"dependency" => array( "element" => "post_filter_by", "value" => 'days' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Include Post ID\'s', "independent" ),
				'param_name'	=> 'include_post_ids',
				'description'	=> esc_html__( 'Manually enter post id\'s for include. These post ordered not based on Ascending, Descending or Random. eg: 21, 15, 30', "independent" ),
				'group'			=> esc_html__( 'Filter', 'independent' ),
				"dependency" => array( "element" => "post_filter_by", "value" => 'custom' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Exclude Post ID\'s', "independent" ),
				'param_name'	=> 'exclude_post_ids',
				'description'	=> esc_html__( 'Manually enter post id\'s for exclude. eg: 21, 15, 30', "independent" ),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'First Tab Text', "independent" ),
				'param_name'	=> 'first_tab_text',
				'description'	=> esc_html__( 'This is first tab text. default shown All.(if don\'t want all tab means just leave it blank.)', 'independent' ),
				'value' 		=> 'All',
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Infinite to Load More', "independent" ),
				'param_name'	=> 'infinite_loadmore',
				'description'	=> esc_html__( 'Enter value here when infinite stop and show loadmore after some loads. eg: 3', 'independent' ),
				'value' 		=> '',
				"dependency" => array( "element" => "ajax_filter_type", "value" => 'infinite' ),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Pagination", "independent" ),
				"param_name"	=> "pagination",
				'description'	=> esc_html__( 'If you choose pagination yes, ajax filter must be select no.', 'independent' ),
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
				"dependency"	=> array(
						"element"	=> "ajax_filter",
						"value"		=> "no"
				),
				'group'			=> esc_html__( 'Filter', 'independent' )
			),
			//grid items
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Grid Item Alignment", "independent" ),
				"param_name"	=> "block_grid_align",
				"value"			=> array(
					esc_html__( "Left", "independent" )	=> "",
					esc_html__( "Center", "independent" )=> "text-center",
					esc_html__( "Right", "independent" )	=> "text-right",
				),
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Grid Items', 'independent' ),
				"description"	=> esc_html__( "This is settings for block grid items. Drag and drop needed meta items to enabled part.", "independent" ),
				'param_name'	=> 'grid_items',
				'dd_fields' => array ( 
					'Enabled' => array( 
						'image'=> esc_html__( 'Image', 'independent' ),
						'title'	=> esc_html__( 'Title', 'independent' ),
						'content'	=> esc_html__( 'Content', 'independent' )
					),
					'disabled' => array(
						'primary-meta'	=> esc_html__( 'Primary Meta', 'independent' ),
						'secondary-meta'	=> esc_html__( 'Secondary Meta', 'independent' ),
						'more'	=> esc_html__( 'Read More', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Top Meta Items', 'independent' ),
				"description"	=> esc_html__( "This is settings for block 1 top meta items. Drag and drop needed meta items to Enabled part(Left or Right).", "independent" ),
				'param_name'	=> 'grid_primary_meta',
				'dd_fields' => array ( 
					'Left' => array( 
						'author'=> esc_html__( 'Author Name', 'independent' ),
						'date'	=> esc_html__( 'Date', 'independent' )				
					),
					'Right' => array( 
						'comments'	=> esc_html__( 'Comments', 'independent' )				
					),
					'disabled' => array(
						'author-with-image'	=> esc_html__( 'Author Image', 'independent' ),
						'read-more'	=> esc_html__( 'Read More', 'independent' ),
						'likes'	=> esc_html__( 'Like', 'independent' ),
						'views'	=> esc_html__( 'View', 'independent' ),
						'favourite'	=> esc_html__( 'Favourite', 'independent' ),
						'category'	=> esc_html__( 'Category', 'independent' ),
						'share'	=> esc_html__( 'Share', 'independent' ),
						'rating'=> esc_html__( 'Rating', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Bottom Meta Items', 'independent' ),
				"description"	=> esc_html__( "This is settings for block 1 bottom meta items. Drag and drop needed meta items to Enabled part(Left or Right).", "independent" ),
				'param_name'	=> 'grid_secondary_meta',
				'dd_fields' => array ( 
					'Left' => array( 
						'likes'	=> esc_html__( 'Like', 'independent' )			
					),
					'Right' => array( 
						'read-more'	=> esc_html__( 'Read More', 'independent' )		
					),
					'disabled' => array(
						'author'=> esc_html__( 'Author Name', 'independent' ),
						'date'	=> esc_html__( 'Date', 'independent' ),
						'comments'	=> esc_html__( 'Comments', 'independent' ),
						'author-with-image'	=> esc_html__( 'Author Image', 'independent' ),
						'views'	=> esc_html__( 'View', 'independent' ),
						'favourite'	=> esc_html__( 'Favourite', 'independent' ),
						'category'	=> esc_html__( 'Category', 'independent' ),
						'share'	=> esc_html__( 'Share', 'independent' ),
						'rating'=> esc_html__( 'Rating', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Title Variation", "independent" ),
				"param_name"	=> "grid_title_variation",
				'description'	=> esc_html__( 'Choose title tag for grid title.', 'independent' ),
				"value"			=> array(
					esc_html__( "H2", "independent" ) => "h2",
					esc_html__( "H3", "independent" ) => "h3",
					esc_html__( "H4", "independent" ) => "h4",
					esc_html__( "H5", "independent" ) => "h5",
					esc_html__( "H6", "independent" ) => "h6",
				),
				'std'			=> 'h4',
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Image Size", "independent" ),
				"param_name"	=> "grid_thumb_size",
				'description'	=> esc_html__( 'Choose thumbnail size for display different size image.', 'independent' ),
				"value"			=> array(
					esc_html__( "Large", "independent" )=> "large",
					esc_html__( "Medium", "independent" )=> "medium",
					esc_html__( "independent Grid 1", "independent" )=> "independent_grid_1",
					esc_html__( "independent Grid 2", "independent" )=> "independent_grid_2",
					esc_html__( "independent Grid 3", "independent" )=> "independent_grid_3",
					esc_html__( "independent Grid 4", "independent" )=> "independent_grid_4",
					esc_html__( "Custom", "independent" )=> "custom",
				),
				'std'			=> 'large',
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__( 'Custom Image Size', "independent" ),
				'param_name'	=> 'custom_image_size',
				'description'	=> esc_html__( 'Enter custom image size. eg: 200x200', 'independent' ),
				'value' 		=> '',
				"dependency"	=> array(
						"element"	=> "grid_thumb_size",
						"value"		=> "custom"
				),
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				"type"			=> 'dropdown',
				"heading"		=> esc_html__( "Overlay Options", "independent" ),
				"param_name"	=> "overlay_options",
				'description'	=> esc_html__( 'Enable image overlay options.', 'independent' ),
				"value"			=> array(
					esc_html__( "No", "independent" )	=> "no",
					esc_html__( "Yes", "independent" )	=> "yes",
				),
				'group'			=> esc_html__( 'Grid Items', 'independent' )
			),
			array(
				'type'			=> 'drag_drop',
				'heading'		=> esc_html__( 'Grid Overlay Items', 'independent' ),
				"description"	=> esc_html__( "Enable needed items overlay on image. Drag and drop needed meta items to enabled part.", "independent" ),
				'param_name'	=> 'block_grid_ovelay_items',
				'dd_fields' => array ( 
					'Enabled' => array( 
						'title'	=> esc_html__( 'Title', 'independent' )						
					),
					'disabled' => array(
						'primary-meta'	=> esc_html__( 'Primary Meta', 'independent' ),
						'secondary-meta'	=> esc_html__( 'Secondary Meta', 'independent' )
					)
				),
				"group"			=> esc_html__( "Grid Items", "independent" ),
				"dependency"	=> array(
						"element"	=> "overlay_options",
						"value"		=> "yes"
				)
			),
			array(
				"type"			=> "textarea",
				"heading"		=> esc_html__( "Grid Items Spacing", "independent" ),
				"description"	=> esc_html__( "Enter custom bottom space for each grid item on main wrapper. Your space values will apply like nth child method. If you leave this empty, default theme space apply for each child. If you want default value for any child, just type \"-\". It will take default value for that child. Example 10 12 8", "independent" ),
				"param_name"	=> "sc_grid_spacing",
				"value" 		=> "",
				"group"			=> esc_html__( "Spacing", "independent" ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__( 'Css', 'independent' ),
				'param_name' => 'css',
				'group' => esc_html__( 'Design options', 'independent' ),
			),
		)
	);
	return $map_fields;
}