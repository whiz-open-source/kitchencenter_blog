<?php
/**
 * Loads Visual Composer New Modules
 *
 * @package     Desirable
 * @subpackage  Includes/Visual Composer
 * @author      Zozothemes
 */
/**
 * Visual Composer custom shortcodes
 */
global $block_params;
/* independent Blocks */
get_template_part( 'inc/visual-composer/shortcodes/banner' );
get_template_part( 'inc/visual-composer/shortcodes/block_1' );
get_template_part( 'inc/visual-composer/shortcodes/block_2' );
get_template_part( 'inc/visual-composer/shortcodes/block_3' );
get_template_part( 'inc/visual-composer/shortcodes/block_4' );
get_template_part( 'inc/visual-composer/shortcodes/block_5' );
get_template_part( 'inc/visual-composer/shortcodes/block_6' );
get_template_part( 'inc/visual-composer/shortcodes/block_7' );
get_template_part( 'inc/visual-composer/shortcodes/block_8' );
get_template_part( 'inc/visual-composer/shortcodes/block_9' );
get_template_part( 'inc/visual-composer/shortcodes/block_10' );
get_template_part( 'inc/visual-composer/shortcodes/block_11' );
get_template_part( 'inc/visual-composer/shortcodes/block_12' );
get_template_part( 'inc/visual-composer/shortcodes/block_13' );
get_template_part( 'inc/visual-composer/shortcodes/block_14' );
get_template_part( 'inc/visual-composer/shortcodes/block_15' );
get_template_part( 'inc/visual-composer/shortcodes/block_16' );
get_template_part( 'inc/visual-composer/shortcodes/block_17' );
get_template_part( 'inc/visual-composer/shortcodes/block_18' );
get_template_part( 'inc/visual-composer/shortcodes/block_19' );
get_template_part( 'inc/visual-composer/shortcodes/block_20' );
get_template_part( 'inc/visual-composer/shortcodes/block_21' );
get_template_part( 'inc/visual-composer/shortcodes/block_ads' );
get_template_part( 'inc/visual-composer/shortcodes/followers_count' );
get_template_part( 'inc/visual-composer/shortcodes/news_ticker' );
get_template_part( 'inc/visual-composer/shortcodes/block_video' );
get_template_part( 'inc/visual-composer/shortcodes/slide_block' );
get_template_part( 'inc/visual-composer/shortcodes/mailchimp' );
get_template_part( 'inc/visual-composer/shortcodes/blog-classic' );
get_template_part( 'inc/visual-composer/shortcodes/blog-zigzag' );
get_template_part( 'inc/visual-composer/shortcodes/blog-masonry' );
get_template_part( 'inc/visual-composer/shortcodes/section-title' );
get_template_part( 'inc/visual-composer/shortcodes/category-box' );