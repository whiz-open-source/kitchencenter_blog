<?php
/**
 * Additional features to allow styling of the templates
 */
function independent_custom_scripts() {
	
	global $independent_custom_styles;
	
	if( independent_po_exists() ):
		
		// Page Styles
		require_once INDEPENDENT_THEME_ELEMENTS . '/page-styles.php';
		ob_start();
		independent_page_custom_styles();
		$independent_custom_styles = ob_get_clean();
		wp_add_inline_style( 'independent-theme-style', $independent_custom_styles );
		
	elseif( is_single() && has_post_format( 'quote' ) ):
		$meta_opt = get_post_meta( get_the_ID(), 'independent_post_quote_modal', true );
		if( $meta_opt != '' && $meta_opt != 'theme-default' ) :
			$aps = new independentPostSettings;
			$theme_color = $aps->independentThemeColor();
			$rgba_08 = $aps->independentHex2Rgba( $theme_color, '0.8' ); 
			$blockquote_bg_opt = $aps->independentCheckMetaValue( 'independent_post_quote_modal', 'single-post-quote-format' );
			ob_start();
			$aps->independentQuoteDynamicStyle( 'single-post', $blockquote_bg_opt, $theme_color, $rgba_08 );
			$independent_custom_styles .= ob_get_clean();
		endif;
	elseif( is_single() && has_post_format( 'link' ) ):
		$meta_opt = get_post_meta( get_the_ID(), 'independent_post_link_modal', true );
		if( $meta_opt != '' && $meta_opt != 'theme-default' ) :
			$aps = new independentPostSettings;
			$theme_color = $aps->independentThemeColor();
			$rgba_08 = $aps->independentHex2Rgba( $theme_color, '0.8' ); 
			$blockquote_bg_opt = $aps->independentCheckMetaValue( 'independent_post_link_modal', 'single-post-link-format' );
			ob_start();
			$aps->independentLinkDynamicStyle( 'single-post', $blockquote_bg_opt, $theme_color, $rgba_08 );
			$independent_custom_styles .= ob_get_clean();
		endif;
	endif;
	
	if( is_single() ){
		// Page Styles
		require_once INDEPENDENT_THEME_ELEMENTS . '/page-styles.php';
		ob_start();
		independent_post_custom_styles();
		$independent_custom_styles .= ob_get_clean();
	}
	
	if( is_single() && !empty( $independent_custom_styles ) ):
		wp_add_inline_style( 'independent-theme-style', $independent_custom_styles );
	endif;
	
}
add_action( 'wp_enqueue_scripts', 'independent_custom_scripts' );

function independent_rtl_body_classes( $classes ) {
    $classes[] = 'rtl';
    return $classes;     
}

function independent_lazy_body_classes( $classes ) {
    $classes[] = 'independent-lazy-active';
    return $classes;     
}

add_action('wp_ajax_independent-post-featured-active', 'independent_post_featured_active');
function independent_post_featured_active(){
	$nonce = $_POST['nonce'];
  
    if ( ! wp_verify_nonce( $nonce, 'independent-post-featured' ) )
        die ( esc_html__( 'Busted!', 'independent' ) );
	
	update_post_meta( esc_attr( $_POST['postid'] ), 'independent_post_featured_stat', esc_attr($_POST['featured-stat']) );
	exit;
}
add_action('wp_ajax_independent-custom-sidebar-export', 'independent_custom_sidebar_export');
function independent_custom_sidebar_export(){
	$nonce = $_POST['nonce'];
  
    if ( ! wp_verify_nonce( $nonce, 'independent-sidebar-featured' ) )
        die ( esc_html__( 'Busted!', 'independent' ) );
	
	$sidebar = get_option('independent_custom_sidebars');
	echo ( ''. $sidebar );
	
	exit;
}
function independent_ads_out( $field ){
	$ato = new independentThemeOpt;
	$output = '';
	if( $ato->independentThemeOpt( $field.'-ads-text' ) ){
		$ads_hide = '';
		if( $ato->independentThemeOpt( $field.'-ads-md' ) == 'no' ){
			$ads_hide .= 'hidden-xs-up ';
		}
		if( $ato->independentThemeOpt( $field.'-ads-sm' ) == 'no' ){
			$ads_hide .= 'hidden-md-down ';
		}
		if( $ato->independentThemeOpt( $field.'-ads-xs' ) == 'no' ){
			$ads_hide .= 'hidden-sm-down ';
		}
		$ads_code = $ato->independentThemeOpt( $field.'-ads-text' );
		$output = '<div class="adv-wrapper '. esc_attr( $ads_hide ) .'">'. apply_filters( 'independent_ads_output_form', $ads_code ) .'</div>';
	}
	return $output;
}
function independent_po_exists( $post_id = '' ){
	$post_id = $post_id ? $post_id : get_the_ID();
	$stat = get_post_meta( $post_id, 'independent_page_layout', true );
	
	if( $stat )
		return true;
	else
		return false;
}
if( ! function_exists('independent_mailchimp') ) {
	function independent_mailchimp(){
		$nonce = $_POST['nonce'];
	  
		if ( ! wp_verify_nonce( $nonce, 'independent-mailchimp' ) )
			die ( esc_html__( 'Busted', 'independent' ) );
		if( isset( $_POST['independent_mc_number'] ) ) {
			
			$first_name = 'zozo-mc-first_name' . esc_attr( $_POST['independent_mc_number'] );
			$last_name = 'zozo-mc-last_name' . esc_attr( $_POST['independent_mc_number'] );
			$email = 'zozo-mc-email' . esc_attr( $_POST['independent_mc_number'] );
			$success = 'independent_mc_success' . esc_attr( $_POST['independent_mc_number'] );
			$failure = 'independent_mc_failure' . esc_attr( $_POST['independent_mc_number'] );
			$listid = 'independent_mc_listid' . esc_attr( $_POST['independent_mc_number'] );
				
			$ato = new independentThemeOpt;
			$mc_key = $ato->independentThemeOpt( 'mailchimp-api' );
			$mcapi = new MCAPI( $mc_key );
			
			$merge_vars = array();
			$merge_vars['FNAME'] = isset($_POST[$first_name]) ? strip_tags($_POST[$first_name]) : '';
			$merge_vars['LNAME'] = isset($_POST[$last_name]) ? strip_tags($_POST[$last_name]) : '';
			$subscribed = $mcapi->listSubscribe(strip_tags($_POST[$listid]), strip_tags($_POST[$email]), $merge_vars);
			
			if ($subscribed != false) {
				echo stripslashes($_POST[$success]);
			}else{
				echo stripslashes($_POST[$failure]);
			}
		}
		wp_die();
	}
	add_action('wp_ajax_nopriv_zozo-mc', 'independent_mailchimp');
	add_action('wp_ajax_zozo-mc', 'independent_mailchimp');
}
if( ! function_exists('independent_star_rating') ) {
	function independent_star_rating( $rate ){
		$out = '';
		for( $i=1; $i<=5; $i++ ){
			
			if( $i == round($rate) ){
				if ( $i-0.5 == $rate ) {
					$out .= '<i class="fa fa-star-half-o"></i>';
				}else{
					$out .= '<i class="fa fa-star"></i>';
				}
			}else{
				if( $i < $rate ){
					$out .= '<i class="fa fa-star"></i>';
				}else{
					$out .= '<i class="fa fa-star-o"></i>';
				}
			}
		}// for end
		
		return $out;
	}
}
/*Search Options*/
if( ! function_exists('independent_search_post') ) {
	function independent_search_post($query) {
		if ($query->is_search) {
			$query->set('post_type',array('post'));
		}
	return $query;
	}
}
if( ! function_exists('independent_search_page') ) {
	function independent_search_page($query) {
		if ($query->is_search) {
			$query->set('post_type',array('page'));
		}
	return $query;
	}
}
if( ! function_exists('independent_search_setup') ) {
	function independent_search_setup(){
		$ato = new independentThemeOpt;
		$search_cont = $ato->independentThemeOpt( 'search-content' );
		if( $search_cont == "post" ){
			add_filter('pre_get_posts','independent_search_post');
		}elseif( $search_cont == "page" ){
			add_filter('pre_get_posts','independent_search_page');
		}
	}
	add_action( 'after_setup_theme', 'independent_search_setup' );
}
//Return same value for filter
if( ! function_exists('__return_value') ) {
	function __return_value( $value ) {
		return function () use ( $value ) {
			return $value; 
		};
	}
}
if( !function_exists( 'independentGetCSSAnimation' ) && class_exists( 'Vc_Manager' ) ) {
	function independentGetCSSAnimation( $css_animation ) {
		$output = '';
		if ( '' !== $css_animation && 'none' !== $css_animation ) {
			wp_enqueue_script( 'waypoints' );
			wp_enqueue_style( 'animate-css' );
			$output = ' wpb_animate_when_almost_visible wpb_' . $css_animation . ' ' . $css_animation;
		}
	
		return $output;
	}
}
/*Facebook Comments Code*/
if( ! function_exists('independent_fb_comments_code') ) {
	function independent_fb_comments_code(){
		$ato = new independentThemeOpt;
		$fb_width = $ato->independentThemeOpt( 'fb-comments-width' );
		$fb_width = isset( $fb_width['width'] ) && $fb_width['width'] != '' ? esc_attr( $fb_width['width'] ) : '300px';
		$comment_num = $ato->independentThemeOpt( 'fb-comments-number' );
		$fb_number = $comment_num != '' ? absint( $comment_num ) : '5';
?>
		<div class="fb-comments" data-href="<?php esc_url( the_permalink() ); ?>" data-width="<?php echo esc_attr( $fb_width ); ?>" data-numposts="<?php echo esc_attr( $fb_number ); ?>"></div>
	<?php
	}
}
if( !function_exists( 'independent_shortcode_rand_id' ) ) {
	function independent_shortcode_rand_id() {
		static $shortcode_rand = 1;
		return $shortcode_rand++;
	}
}
function independent_get_categories_now( $string, $arg1, $arg2 ) {
    $cats = get_categories( array(
		'orderby' => 'name',
		'order'   => 'ASC'
	) );
    return $cats;
}
add_filter( 'independent_get_categories', 'independent_get_categories_now', 10 );

//Check title with tagline
function independent_display_header_text() {
	if ( ! current_theme_supports( 'custom-header', 'header-text' ) )
	return false;
	
	$text_color = get_theme_mod( 'header_textcolor', get_theme_support( 'custom-header', 'default-text-color' ) );
	return 'blank' !== $text_color;
}

// Wp List Categories Count Span
add_filter( 'wp_list_categories', 'independent_cat_count_span' );
function independent_cat_count_span($links) {
	$links = str_replace('</a> (', ' <span class="cat-count">(', $links);
	$links = str_replace(')', ')</span></a>', $links);
	return $links;
}

/**
* Get the upload URL/path in right way (works with SSL).
*
* @param $param string "basedir" or "baseurl"
* @return string
*/
function independent_fn_get_upload_dir_var( $param ) {
$upload_dir = wp_upload_dir();
$url = $upload_dir[ $param ];

if ( $param === 'baseurl' && is_ssl() ) {
$url = str_replace( 'http://', 'https://', $url );
}

return $url;
}