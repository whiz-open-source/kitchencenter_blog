<?php
/*
 * The header for independent theme
 */
$ahe = new IndependentHeaderElements;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="5NXznGZ-9CeltdZiiubhpqbEP7bajmO0OKPYUQrxEAU" />
	
<script type="text/javascript">
        //<![CDATA[
            var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-117965496-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
//]]>
</script>	
	
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<?php wp_head(); ?>
</head>
<?php
	$rtl = $ahe->independentThemeOpt('rtl');
	if( $rtl ) add_filter( 'body_class','independent_rtl_body_classes' );
	
	$lazy_opt = $ahe->independentThemeOpt('news-lazy-load');
	if( $lazy_opt ) add_filter( 'body_class','independent_lazy_body_classes' );

	$smooth_scroll = $ahe->independentThemeOpt('smooth-opt');
	$scroll_time = $scroll_dist = '';
	if( $smooth_scroll ){
		$scroll_time = $ahe->independentThemeOpt('scroll-time');
		$scroll_dist = $ahe->independentThemeOpt('scroll-distance');
	}

?>
<body <?php body_class(); ?> data-scroll-time="<?php echo esc_attr( $scroll_time ); ?>" data-scroll-distance="<?php echo esc_attr( $scroll_dist ); ?>">
<?php
	/*
	 * Section Top - independent_section_top - 5
	 * Mobile Header - independentMobileHeader - 10
	 * Mobile Bar - independentMobileBar - 20
	 * Secondary Menu Space - independentHeaderSecondarySpace - 30
	 * Top Sliding Bar - independentHeaderTopSliding - 40
	 * Full Search - independentFullSearchWrap - 50
	 */

	do_action('independent_body_action');
?>
<?php if( $ahe->independentPageLoader() ) : ?>
	<div class="page-loader"></div>
<?php endif; ?>
<div id="page" class="independent-wrapper<?php echo esc_attr( $ahe->independentThemeLayout() ); ?>">

	<?php $ahe->independentHeaderSlider('top'); ?>
	
	<header class="independent-header<?php echo esc_attr( $ahe->independentHeaderLayout() ); ?>">
		<?php $ahe->independentHeaderBar(); ?>
	</header>
	
	<div class="independent-content-wrapper">
