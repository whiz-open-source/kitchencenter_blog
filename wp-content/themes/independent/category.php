<?php
/**
 * The template for displaying category pages
 */
get_header();
$ahe = new independentHeaderElements;
$template = 'blog'; // template id
$aps = new independentPostSettings;
$category = get_category( get_query_var( 'cat' ) );
$cat_key = 'category-' . $category->cat_ID;
if( $aps->independentCheckCategoryTemplateExists( $cat_key ) ){
	$template = $cat_key;
}elseif( $aps->independentCheckTemplateExists( 'category' ) ){
	$template = 'category';
}elseif( $aps->independentCheckTemplateExists( 'archive' ) ){
	$template = 'archive';
}
$aps->independentSetPostTemplate( $template );
$template_class = $aps->independentTemplateContentClass();
?>
<div class="independent-content <?php echo esc_attr( 'independent-' . $template ); ?>">
	<?php
		if( $aps->independentThemeOpt( $template.'-featured-slider' ) ){
			$ahe->independentFeaturedSlider( $template );
		}
	?>

	<div class="independent-content-inner">
		<div class="container">

			<div class="row">

			<?php
				//Category Banner
				$banner_model = $aps->independentThemeOpt( $template.'-banner' );
				if( $banner_model != '' && $banner_model != 'none' ){
					$banner_style = $aps->independentThemeOpt( $template.'-banner-style' );
					echo '
					<div class="category-banner">
						<div class="col-md-12">';
							$category_banner_args = apply_filters( 'independent_category_banner_args', 'trend_enable="yes" post_filter_by="custom" banner_gutter="3" banner_link_color="#ffffff" banner_linkh_color="#d0d0d0"' );
							$category_banner_sc = '[independent_vc_banner banner_modal="'. esc_attr( $banner_model ) .'" banner_overlay="'. esc_attr( $banner_style ) .'" cat="'. esc_attr( $category->cat_ID ) .'" '. ( $category_banner_args ) .']';
							echo do_shortcode( $category_banner_sc );
					echo '
						</div>
					</div>';
				}
			?>
				<div class="<?php echo esc_attr( $template_class['content_class'] ); ?>">
					<div id="primary" class="content-area">
						<main class="site-main <?php echo esc_attr( $template ); ?>-template">

							<?php $ahe->independentPageTitle( $template ); ?>

							<?php
							if ( have_posts() ) :
								$template_shortcode = $aps->independentThemeOpt( $template . '-template-shortcode' );
								if( $template_shortcode ){
									$template_shortcode_model = $aps->independentThemeOpt( $template . '-template-shortcode-model' );
									$template_shortcode_model = $template_shortcode_model ? $template_shortcode_model : '1';
									parse_str($template_shortcode, $shortcode_atts);
									$model_fun = 'independent_news_block_form_'. $template_shortcode_model;
									echo ( ''. $model_fun($shortcode_atts, true) );
								}else{
									$template_shortcode = independent_default_block_values();
									parse_str($template_shortcode, $shortcode_atts);
									echo independent_news_block_form_default($shortcode_atts, true);
								}
							else :
								get_template_part( 'template-parts/post/content', 'none' );
							endif;
							?>

						</main><!-- #main -->
					</div><!-- #primary -->
				</div><!-- main col -->

				<?php if( $template_class['lsidebar_class'] != '' ) : ?>
				<div class="<?php echo esc_attr( $template_class['lsidebar_class'] ); ?>">
					<aside class="widget-area left-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
						<?php dynamic_sidebar( $template_class['left_sidebar'] ); ?>
					</aside>
				</div><!-- sidebar col -->
				<?php endif; ?>

				<?php if( $template_class['rsidebar_class'] != '' ) : ?>
				<div class="<?php echo esc_attr( $template_class['rsidebar_class'] ); ?>">
					<aside class="widget-area right-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
						<?php dynamic_sidebar( $template_class['right_sidebar'] ); ?>
					</aside>
				</div><!-- sidebar col -->
				<?php endif; ?>

			</div><!-- row -->

		</div><!-- .container -->
	</div><!-- .independent-content-inner -->
</div><!-- .independent-content -->
<?php get_footer();
