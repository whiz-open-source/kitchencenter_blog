<?php
class NewsZWeatherShortcodes {

	public static function independentShortWeather( $atts ){
		$atts = shortcode_atts( array(
			'latitude' => '',
			'longitude' => '',
			'timezone' => '',
			'model' => '1',
			'lang' => 'en'
		), $atts );
		
		//'[independent_short_weather latitude="" longitude="" timezone="" model="1" lang="1"]';
		
		$lat = $atts['latitude'];
		$lon = $atts['longitude'];
		$timezone = $atts['timezone'];
		$wmodel = $atts['model'];
		$lang = $atts['lang'];
		
		$nw = new independent_zozo_weather_widget;
		
		$transient_name = "independent_weather_" . trim( $lat ) . trim( $lon );
		$weather_data = get_transient( $transient_name );
		
		$independent_option = get_option( 'independent_options' );
		$open_weather_api = isset( $independent_option['open-weather-api'] ) ? $independent_option['open-weather-api'] : '';
		
		$weather_data_stat = 1;
		
		ob_start();
		if( $open_weather_api ){
			if( $weather_data ){
				$nw->independent_frame_weather_out(  $weather_data, $timezone, $wmodel, true );
			}else{
				$weather_data = $nw->independent_get_weather_data( $lat, $lon, $open_weather_api, $transient_name, $lang );
				$nw->independent_frame_weather_out(  $weather_data, $timezone, $wmodel, true );
			}
		}else{
			$weather_data_stat = 0;
		}
		
		if( !$weather_data_stat ){
			echo '<p>'. esc_html__( "Sorry! Unable to communicate with Open Weather Map.", "independent" ) .'</p>';
		}
		
		$output = ob_get_clean();
		return $output;
		
	}
}
add_shortcode( 'independent_short_weather', array( 'NewsZWeatherShortcodes', 'independentShortWeather' ) );