<?php
add_action( 'widgets_init', 'independent_zozo_weather_load_widget' );
function independent_zozo_weather_load_widget() {
	register_widget( 'independent_zozo_weather_widget' );
}
class independent_zozo_weather_widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	public function __construct() {
		$widget_ops = array( 'classname' => 'zozo_weather', 'description' => esc_html__('A widget that displays about weather', 'independent') );
		$control_ops = array('id_base' => 'zozo_weather_widget' );
		parent::__construct( 'zozo_weather_widget', esc_html__('Independent Weather', 'independent'), $widget_ops, $control_ops );
	}
	/**
	 * How to display the widget on the screen.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$lat = $instance['latitude'];
		$lon = $instance['longitude'];
		$timezone = $instance['timezone'];
		$wmodel = $instance['wmodel'];
		$lang = $instance['lang'];

		/* Before widget (defined by themes). */
		echo wp_kses_post( $before_widget );
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo ( $title != '' ? wp_kses_post( $before_title . $title . $after_title ) : '' );
		?>
			
			<div class="weather-widget widget-content">
			<?php
				
				$transient_name = "independent_weather_" . trim( $lat ) . trim( $lon );
				$weather_data = get_transient( $transient_name );
				
				$independent_option = get_option( 'independent_options' );
				$open_weather_api = isset( $independent_option['open-weather-api'] ) ? $independent_option['open-weather-api'] : '';
				
				$weather_data_stat = 1;
				
				if( $open_weather_api ){
					if( $weather_data ){
						$this->independent_frame_weather_out(  $weather_data, $timezone, $wmodel );
					}else{
						$weather_data = $this->independent_get_weather_data( $lat, $lon, $open_weather_api, $transient_name, $lang );
						$this->independent_frame_weather_out(  $weather_data, $timezone, $wmodel );
					}
				}else{
					$weather_data_stat = 0;
				}
				
				if( !$weather_data_stat ){
					echo '<p>'. esc_html__( "Sorry! Unable to communicate with Open Weather Map.", "independent" ) .'</p>';
				}
				
			?>
			</div>
			
		<?php
		/* After widget (defined by themes). */
		echo wp_kses_post( $after_widget );
	}
	/**
	 * Update the widget settings.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['latitude'] = esc_attr( $new_instance['latitude'] );
		$instance['longitude'] = esc_attr( $new_instance['longitude'] );
		$instance['timezone'] = esc_attr( $new_instance['timezone'] );
		$instance['wmodel'] = esc_attr( $new_instance['wmodel'] );
		$instance['lang'] = esc_attr( $new_instance['lang'] );
		return $instance;
	}
	public function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array( 'title' => '', 'latitude' => '', 'longitude' => '', 'timezone' => '', 'wmodel' => '1', 'lang' => 'en' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e('Title:', 'independent'); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" style="width:96%;" type="text" />'independent'
		</p>
		
		<!-- Latitude: Text Input -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'latitude' ) ); ?>"><?php esc_html_e('Latitude:', 'independent'); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'latitude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'latitude' ) ); ?>" value="<?php echo esc_attr( $instance['latitude'] ); ?>" style="width:96%;" type="text" />
		</p>
		
		<!-- Longitude: Text Input -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'longitude' ) ); ?>"><?php esc_html_e('Longitude:', 'independent'); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'longitude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'longitude' ) ); ?>" value="<?php echo esc_attr( $instance['longitude'] ); ?>" style="width:96%;" type="text" />
		</p>
		
		<!-- Timezone -->
		<?php $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL); ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'timezone' ) ); ?>"><?php esc_html_e('Timezone:', 'independent'); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'timezone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'timezone' ) ); ?>">
		<?php
			foreach( $tzlist as $timez ){
				echo '<option value="'. esc_attr( $timez ) .'" '. ( $instance['timezone'] == $timez ? 'selected' : '' ) .'>'. esc_attr( $timez ) .'</option>';
			}
		?>
			</select>
		</p>
		
		<!-- Weather Model -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'wmodel' ) ); ?>"><?php esc_html_e('Weather Model:', 'independent'); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'wmodel' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'wmodel' ) ); ?>">
				<option value="1" <?php echo $instance['wmodel'] == '1' ? 'selected' : ''; ?>><?php esc_html_e( 'Classic', 'independent' ); ?></option>
				<option value="2" <?php echo $instance['wmodel'] == '2' ? 'selected' : ''; ?>><?php esc_html_e( 'Modern', 'independent' ); ?></option>
			</select>
		</p>
		
		<!-- Language: Text Input -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'lang' ) ); ?>"><?php esc_html_e('Language Code:', 'independent'); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'lang' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'lang' ) ); ?>" value="<?php echo esc_attr( $instance['lang'] ); ?>" style="width:96%;" type="text" />
			<span><?php esc_html_e( 'You can specify language code here. Example Arabic (ar); Czech (cz); Greek (el); Persian(Farsi) (fa); Galician (gl); Hungarian (hu); Japanese (ja); Korean (kr); Latvian (la); Lithuanian (lt); Macedonian (mk); Slovak (sk); Slovenian (sl); Vietnamese (vi).', 'independent' ); ?></span>
		</p>
		
	<?php
	}
	
	public function independent_get_weather_data( $lat, $lon, $api, $transient_name, $lang ){
		$open_weathermap_url = 'http://api.openweathermap.org/data/2.5/forecast?lat=' . esc_attr( $lat ) . '&lon=' . esc_attr( $lon ) . '&lang=' . esc_attr( $lang ) . '&appid=' . esc_attr( $api );
		$response = wp_remote_get( $open_weathermap_url );
		if( is_wp_error( $response ) ) {
			$weather_data_stat = 0;
		}else{
			$weather_data = $response['body'];
			set_transient( $transient_name, $weather_data, 15 * MINUTE_IN_SECONDS ); // 15 mins once 
			return $weather_data;
		}
		return "";
	}
	
	public function independent_frame_weather_out( $weather_data, $timez, $wmodel, $wshort = false ){
			
		$date = new DateTime('now', new DateTimeZone($timez));
		$loc_date = $date->format('Y-m-d');
		$loc_time = $date->format('H');
		
		$weather_data = json_decode( $weather_data, true );
		if( isset( $weather_data['cod'] ) && $weather_data['cod'] == 200 ){
			$weather_list = isset( $weather_data['list'] ) && is_array( $weather_data['list'] ) ? $weather_data['list'] : '';
			
			$last_weather = '';
			$main_rep = 1; $rep_count = 0;
				
				if( !$wshort ){
					echo '<div class="weather-title">';
						echo '<h5>'. $weather_data['city']['name'] .'</h5>';
					echo '</div>';
				}
				
				$weather_final = array();
			
				foreach( $weather_list as $weather ){
				
					$weather_dt = isset( $weather['dt_txt'] ) ? explode( " ", $weather['dt_txt'] ) : array();
					$weather_hour = isset( $weather_dt[1] ) ? explode( ":", $weather_dt[1] ) : array();
					
					if( isset( $weather_hour[0] ) ){
						
						if( $main_rep && $loc_date == $weather_dt[0] ){
						
							if( absint( $loc_time - $weather_hour[0] ) <= 3 ){
								$weather_final[$rep_count++] = $last_weather ? $last_weather : $weather;
								$main_rep = 0;
							}else{
								$last_weather = $weather;
							}

						}elseif( $weather_hour[0] == '12' && $loc_date != $weather_dt[0] ){
							$weather_final[0] = isset( $weather_final[0] ) ? $weather_final[0] : $weather;
							$weather_final[$rep_count++] = $weather;
							$main_rep = 0;
						}
						
						
						
					}
									
				} // foreach
				
				$main_weather = isset( $weather_final['0'] ) ? $weather_final['0'] : '';
				
				if( isset( $main_weather['main']['temp'] ) ){
				
					echo '<div class="weather-main weather-calc-default '. esc_attr( 'weather-model-' . $wmodel ) .'">';
							
						if( $wshort ){
						
							echo '<div class="weather-short-wrap clearfix">';
							
								echo '<div class="weather-area">';
									echo '<strong>'. $weather_data['city']['name'] .'</strong>';
								echo '</div><!-- .weather-area -->';
								
								echo '<div class="weather-img">';
									$kelvins = $main_weather['main']['temp'];
									$celsius  = $kelvins - 273.15;
									$fahrenheit = ( $kelvins * 1.8 ) - 459.67;
									
									$icon_url = INDEPENDENT_CORE_URL . 'assets/images/weather/model-'. esc_attr( $wmodel ) .'/' . $main_weather['weather'][0]['icon'] . '.png';
									echo '<img src="'. esc_url( $icon_url ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" />';
								echo '</div><!-- .weather-img -->';
								
								echo '<div class="weather-details">';
									echo '<div class="weather-value clearfix">';
										echo '<span class="weather-temp" data-wdefault="'. (int)( $celsius ) .'" data-walter="'. (int)( $fahrenheit ) .'">' . (int)( $celsius ) . '</span>';
										echo '<span class="weather-degree">&deg;</span>';
										echo '<span class="weather-celsius" data-wdefault="'. esc_html__( 'C', 'independent' ) .'" data-walter="'. esc_html__( 'F', 'independent' ) .'">'. esc_html__( 'C', 'independent' ) .'</span>';
									echo '</div><!-- .weather-value -->';
								echo '</div><!-- .weather-details -->';
	
							echo '</div><!-- .weather-single-wrap -->';
							
						}else{
						
							
							// Single Weather
							echo '<div class="weather-single-wrap clearfix">';
								echo '<div class="weather-img">';
									$kelvins = $main_weather['main']['temp'];
									$celsius  = $kelvins - 273.15;
									$fahrenheit = ( $kelvins * 1.8 ) - 459.67;
									
									$icon_url = INDEPENDENT_CORE_URL . 'assets/images/weather/model-'. esc_attr( $wmodel ) .'/' . $main_weather['weather'][0]['icon'] . '.png';
									echo '<img src="'. esc_url( $icon_url ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" />';
									echo '<span class="weather-stat">' . $main_weather['weather'][0]['main'] . '</span>';
									echo '<span class="weather-stat-sub">(' . $main_weather['weather'][0]['description'] . ')</span>';
								echo '</div><!-- .weather-img -->';
								echo '<div class="weather-details">';
								
									echo '<div class="weather-value clearfix">';
										echo '<h3><span class="weather-temp" data-wdefault="'. (int)( $celsius ) .'" data-walter="'. (int)( $fahrenheit ) .'">' . (int)( $celsius ) . '</span></h3>';
										echo '<span class="weather-degree">&deg;</span>';
										echo '<span class="weather-celsius" data-wdefault="'. esc_html__( 'C', 'independent' ) .'" data-walter="'. esc_html__( 'F', 'independent' ) .'">'. esc_html__( 'C', 'independent' ) .'</span>';
									echo '</div><!-- .weather-value -->';
									
									echo '<div class="weather-info clearfix">';
										$wind_speed = $main_weather['wind']['speed'];
										$wind_speed_kph = round( ( $wind_speed * 3.6 ), 1 );
										$wind_speed_mph = round( ( $wind_speed / 0.44704 ), 1 );
										
										echo '<div class="weather-wind-mph clearfix"><img src="'. esc_url( INDEPENDENT_CORE_URL . 'assets/images/weather/info/wind.png' ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" /><span class="weather-wind-value" data-wdefault="'. $wind_speed_mph .'" data-walter="'. $wind_speed_kph .'">' . $wind_speed_mph . '</span><span class="weather-wind-mph-txt" data-wdefault="'. esc_html__( 'mph', 'independent' ) .'" data-walter="'. esc_html__( 'kmph', 'independent' ) .'">'. esc_html__( 'mph', 'independent' ) .'</span></div>';
										echo '<div class="weather-clouds clearfix"><img src="'. esc_url( INDEPENDENT_CORE_URL . 'assets/images/weather/info/clouds.png' ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" /><span>' . $main_weather['clouds']['all'] . '%</span></div>';
										echo '<div class="weather-humidity clearfix"><img src="'. esc_url( INDEPENDENT_CORE_URL . 'assets/images/weather/info/humidity.png' ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" /><span>' . $main_weather['main']['humidity'] . '%</span></div>';
									echo '</div><!-- .weather-info -->';
										
								echo '</div><!-- .weather-details -->';
								echo '<div class="weather-min-max clearfix">';
									$min_kelvins = $main_weather['main']['temp_min'];
									$min_celsius  = $min_kelvins - 273.15;	
									$min_fahrenheit = ( $min_kelvins * 1.8 ) - 459.67;
									$max_kelvins = $main_weather['main']['temp_max'];
									$max_celsius  = $max_kelvins - 273.15;
									$max_fahrenheit = ( $max_kelvins * 1.8 ) - 459.67;
									
									echo '<div class="weather-max-temp clearfix">';
										echo '<span class="weather-temp" data-wdefault="'. (int)( $max_celsius ) .'" data-walter="'. (int)( $max_fahrenheit ) .'">' . (int)( $max_celsius ) . '</span>';
										echo '<span class="weather-degree">&deg;</span>';
										echo'<img src="'. esc_url( INDEPENDENT_CORE_URL . 'assets/images/weather/info/temp-up.png' ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" />';
									echo '</div><!-- .weather-max-temp -->';
									
									echo '<div class="weather-min-temp clearfix">';
										echo'<img src="'. esc_url( INDEPENDENT_CORE_URL . 'assets/images/weather/info/temp-down.png' ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" />';
										echo '<span class="weather-temp" data-wdefault="'. (int)( $min_celsius ) .'" data-walter="'. (int)( $min_fahrenheit ) .'">' . (int)( $min_celsius ) . '</span>';
										echo '<span class="weather-degree">&deg; </span>';
									echo '</div><!-- .weather-min-temp -->';
									
								echo '</div><!-- .weather-min-max -->';
							echo '</div><!-- .weather-single-wrap -->';
							
							// Weather List
							echo '<div class="weather-days-report clearfix">';
							$list_count = 0;
							foreach( $weather_final as $weather ){
								if( $list_count < 5 ){
									$kelvins = $weather['main']['temp'];
									$celsius  = $kelvins - 273.15;
									$fahrenheit = ( $kelvins * 1.8 ) - 459.67;
									
									echo '<div class="weather-img-wrap">';
									
										echo '<div class="weather-value clearfix">';
											echo '<div><span class="weather-temp" data-wdefault="'. (int)( $celsius ) .'" data-walter="'. (int)( $fahrenheit ) .'">' . (int)( $celsius ) . '</span></div>';
											echo '<span class="weather-degree">&deg;</span>';
											echo '<span class="weather-celsius" data-wdefault="'. esc_html__( 'C', 'independent' ) .'" data-walter="'. esc_html__( 'F', 'independent' ) .'">'. esc_html__( 'C', 'independent' ) .'</span>';
										echo '</div><!-- .weather-value -->';
			
										echo '<div class="weather-img">';
											$icon_url = INDEPENDENT_CORE_URL . 'assets/images/weather/model-'. esc_attr( $wmodel ) .'/' . $weather['weather'][0]['icon'] . '.png';
											echo '<img src="'. esc_url( $icon_url ) .'" alt="'. esc_html__( 'Weather', 'independent' ) .'" />';
										echo '</div><!-- .weather-img -->';
										echo '<span class="weather-day">' . date( "D", strtotime( $weather['dt_txt'] ) ) . '</span>';
									echo '</div><!-- .weather-img-wrap -->';
									
									$list_count++;
								}// 5 count
							}
							echo '</div><!-- .weather-days-report -->';	
						
						} // weather short;
						
					echo '</div><!-- .weather-main -->';
				} //check temp exists
			
		}else{
			echo '<p>'. esc_html__( "Sorry! Try with valid API key.", "independent" ) .'</p>';
		}
	}
}
?>