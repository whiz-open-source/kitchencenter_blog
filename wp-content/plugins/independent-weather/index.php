<?php
/**
 * @package Independent
 * @version 1.0
 */
/*
Plugin Name: Independent Weather
Plugin URI: http://zozothemes.com/
Description: This is just a member register plugin, it's support only Independent Theme.
Version: 1.0
Author: zozothemes
Author URI: http://zozothemes.com/
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$cur_theme = wp_get_theme();	
if ( $cur_theme->get( 'Name' ) != 'Independent' && $cur_theme->get( 'Name' ) != 'Independent Child' ){
	return;
}

define( 'INDEPENDENT_WEATHER_DIR', plugin_dir_path( __FILE__ ) );
define('INDEPENDENT_WEATHER_URL', plugin_dir_url( __FILE__ ) );

load_plugin_textdomain( 'independent', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

require_once( INDEPENDENT_WEATHER_DIR . 'function.php' );
require_once( INDEPENDENT_WEATHER_DIR . 'shortcodes.php' );