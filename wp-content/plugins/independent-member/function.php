<?php

class IndependentRegisterShortcodes {

	public static function independentSigninTrigger( $atts ) {
		$atts = shortcode_atts( array(
			'avatar' => false,
			'logged_text' => esc_html__( 'Hello!', 'independent' ),
			'siginin_text' => esc_html__( 'Sign in', 'independent' ),
			'siginout_text' => esc_html__( 'Sign out', 'independent' )
		), $atts );
		
		$output = "";
		
		global $user_login;
		global $current_user;
		
		ob_start();
		
		if ( !is_user_logged_in() ) :
		?>
			<a href="#" class="login-form-trigger"><?php echo esc_html( $atts['siginin_text'] ); ?></a>
			<?php
			add_action( 'independent_footer_action', array( 'IndependentRegisterShortcodes', 'independentLogRegisterForm' ), 10 );
		else : 
			$author_link = get_author_posts_url( $current_user->ID );
		?>
			<div class="logged-in log-form-trigger-wrap"> 
				<?php echo '<span class="log-text">' . esc_html( $atts['logged_text'] ) . '</span>'; ?>
				<?php echo ( $atts['avatar'] == true ? get_avatar( $current_user->ID, 50, null, null, array( 'class' => 'img-circle' ) ) : '' ); ?>
				<a class="author-link" href="<?php echo esc_url( $author_link ); ?>"><?php echo esc_attr( $current_user->display_name ); ?></a>
				<a href="<?php echo esc_url( wp_logout_url( home_url( '/' ) ) ); ?>" class="independent-logout"><?php echo esc_html( $atts['siginout_text'] ); ?></a>
			</div>
		<?php
		endif;
		
		$output .= ob_get_clean();
		return $output;
	}
	
	public static function independentLogRegisterForm(){
	?>
		<div class="independent-login-parent">
			<div class="independent-login-inner">
				<?php echo do_shortcode( '[independent_login_form][independent_forgot_form][independent_registration_form]' ); ?>
			</div>
		</div>
	<?php
	}
	
	public static function independentLoginForm( $atts ){
		ob_start();
	?>

		<div class="login-form">
			<span class="close independent-login-close"></span>
			<form id="login" action="login" method="post">
				<h3 class="text-center"><?php esc_html_e( 'Login', 'independent' ); ?></h3>
				<p class="status"></p>
				<p>
					<label for="username"><?php esc_html_e( 'Username', 'independent' ); ?></label>
					<input id="username" type="text" name="username" class="form-control">
				</p>
				<p>
					<label for="password"><?php esc_html_e( 'Password', 'independent' ); ?></label>
					<input id="password" type="password" name="password" class="form-control">
				</p>
				<p>
					<?php if ( get_option( 'users_can_register' ) ) : ?>
						<a class="register-trigger" href="#"><?php esc_html_e( 'Register', 'independent' ); ?></a>
						<?php echo ' / '; ?>
					<?php endif; ?>
					<a class="lost-password-trigger" href="#"><?php esc_html_e( 'Lost your password?', 'independent' ); ?></a>
				</p>
				<input class="submit_button btn btn-default" type="submit" value="<?php esc_html_e( 'Login', 'independent' ); ?>" name="submit">
				<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
			</form>
		</div>
				
	<?php
	
		$output = ob_get_clean();
		return $output;
	
	}
	
	public static function independentRegisterForm( $atts ){
		ob_start();
		
		if ( get_option( 'users_can_register' ) ) : ?>
			<div class="registration-form form-state-hide">
				<span class="fa fa-angle-left move-to-prev-form"></span>
				<form id="registration" class="ajax-auth" action="registration" method="post">    
					<h3 class="text-center"><?php esc_html_e( 'Register', 'independent' ); ?></h3>
					<p class="status"></p>
					<p>
						<label for="name"><?php esc_html_e( 'Your Name', 'independent' ); ?></label>
						<input id="name" type="text" name="name" class="form-control">
					</p>
					<p>
						<label for="email"><?php esc_html_e( 'Your Email*', 'independent' ); ?></label>
						<input id="email" type="text" name="email" class="form-control">
					</p>
					<p>
						<label for="nick_name"><?php esc_html_e( 'Nick Name', 'independent' ); ?></label>
						<input id="nick_name" type="text" name="nick_name" class="form-control">
					</p>
					<p>
						<label for="username"><?php esc_html_e( 'Choose Username*', 'independent' ); ?></label>
						<input id="username" type="text" name="username" class="form-control">
					</p>
					<p>
						<label for="password"><?php esc_html_e( 'Choose Password*', 'independent' ); ?></label>
						<input id="password" type="password" name="password" class="form-control">
					</p>
					<input class="submit_button btn btn-default" type="submit" value="<?php esc_html_e( 'Register', 'independent' ); ?>" name="submit">
					<?php wp_nonce_field( 'ajax-register-nonce', 'security' ); ?>
				</form>
			</div>
		<?php endif; // user can register 
		
		$output = ob_get_clean();
		return $output;
		
	}
	
	public static function independentForgotForm( $atts ){
		ob_start();
		?>
		
			<div class="lost-password-form form-state-hide">
				<span class="fa fa-angle-left move-to-prev-form"></span>
				<form id="forgot_password" class="ajax-auth" action="forgot_password" method="post">    
					<h3 class="text-center"><?php esc_html_e( 'Forgot Password', 'independent' ); ?></h3>
					<p class="status"></p>  
					<?php wp_nonce_field('ajax-forgot-nonce', 'forgotsecurity'); ?>  
					<p>
						<label for="user_login"><?php esc_html_e( 'Username or E-mail', 'independent' ); ?></label>
						<input id="user_login" type="text" class="required form-control" name="user_login">
					</p>
					<input class="submit_button btn btn-default" type="submit" value="<?php esc_html_e( 'Submit', 'independent' ); ?>">
				</form>
			</div>
					
		<?php
		
		$output = ob_get_clean();
		return $output;
		
	}
	
 }
add_shortcode( 'independent_signin_trigger', array( 'IndependentRegisterShortcodes', 'independentSigninTrigger' ) );
add_shortcode( 'independent_login_form', array( 'IndependentRegisterShortcodes', 'independentLoginForm' ) );
add_shortcode( 'independent_forgot_form', array( 'IndependentRegisterShortcodes', 'independentForgotForm' ) );
add_shortcode( 'independent_registration_form', array( 'IndependentRegisterShortcodes', 'independentRegisterForm' ) );

/*User Register and Login with Ajax*/
add_action( 'wp_ajax_ajaxlogin', 'independent_ajax_login' );
add_action( 'wp_ajax_nopriv_ajaxlogin', 'independent_ajax_login' );
if( ! function_exists('independent_ajax_login') ) {
	function independent_ajax_login(){
			
		// First check the nonce, if it fails the function will break
		check_ajax_referer( 'ajax-login-nonce', 'security' );
	
		// Nonce is checked, get the POST data and sign user on
		$pswd = addslashes( $_POST['password'] );
		$info = array();
		$info['user_login'] = esc_attr( $_POST['username'] );
		$info['user_password'] = stripslashes( $pswd );
		$info['remember'] = true;
	
		$user_signon = wp_signon( $info, false );
		if ( is_wp_error($user_signon) ){
			echo json_encode(array('loggedin'=>false, 'message' => esc_html__( 'Wrong username or password.', 'independent' ) ) );
		} else {
			echo json_encode(array('loggedin'=>true, 'message' => esc_html__( 'Login successful, redirecting...', 'independent' ) ) );
		}
	
		exit;
	}
}

add_action( 'wp_ajax_ajaxregister', 'independent_ajax_register' );
add_action( 'wp_ajax_nopriv_ajaxregister', 'independent_ajax_register' );
if( ! function_exists('independent_ajax_register') ) {
	function independent_ajax_register(){
			
		// First check the nonce, if it fails the function will break
		check_ajax_referer( 'ajax-register-nonce', 'security' );
	
		// Nonce is checked, get the POST data and sign user on
		$email = esc_attr( $_POST['email'] );
		$username = esc_attr( $_POST['username'] );
		
		$userdata = array();
		$userdata['first_name'] = esc_attr( $_POST['name'] );
		$userdata['user_email'] = esc_attr( $_POST['email'] );
		$userdata['nickname'] = esc_attr( $_POST['nick_name'] );
		$userdata['user_login'] = esc_attr( $_POST['username'] );
		$userdata['user_pass'] = esc_attr( $_POST['password'] );
		
		$status = false;
		$msg = '';
		
		if( is_email( $email ) && validate_username( $username ) ) {

			$user_id = wp_insert_user( $userdata ) ;
			if( !is_wp_error($user_id) ) {
				$status = true;
				$msg = esc_html__( 'Registered successful, redirecting....', 'independent' );
			} else {
				$status = false;
				$msg = $user_id->get_error_message();
			}

		}else{
			$status = false;
			$msg = esc_html__( 'Enter valid email/user name!.', 'independent' );
		}
	
		echo json_encode( array( 'register' => $status, 'message' => $msg ) );
	
		exit;
	}
}

add_action( 'wp_ajax_lost_pass', 'independent_lost_pass_callback' );
add_action( 'wp_ajax_nopriv_lost_pass', 'independent_lost_pass_callback' );
if( ! function_exists('independent_lost_pass_callback') ) {
	function independent_lost_pass_callback(){
	 
		// First check the nonce, if it fails the function will break
		check_ajax_referer( 'ajax-forgot-nonce', 'security' );
		
		global $wpdb;
		
		$account = esc_attr( $_POST['user_login'] );
		
		if( empty( $account ) ) {
			$error = esc_html__( 'Enter an username or e-mail address.', 'independent' );
		} else {
			if(is_email( $account )) {
				if( email_exists($account) ) 
					$get_by = 'email';
				else	
					$error = esc_html__( 'There is no user registered with that email address.', 'independent' );			
			}
			else if (validate_username( $account )) {
				if( username_exists($account) ) 
					$get_by = 'login';
				else	
					$error = esc_html__( 'There is no user registered with that username.', 'independent' );				
			}
			else
				$error = esc_html__( 'Invalid username or e-mail address.', 'independent' );		
		}	
		
		if(empty ($error)) {
			// lets generate our new password
			$random_password = wp_generate_password();
	
				
			// Get user data by field and data, fields are id, slug, email and login
			$user = get_user_by( $get_by, $account );
				
			$update_user = wp_update_user( array ( 'ID' => $user->ID, 'user_pass' => $random_password ) );
				
			// if  update user return true then lets send user an email containing the new password
			if( $update_user ) {
				
				$from = ''; // Set whatever you want like mail@yourdomain.com
				
				if(!(isset($from) && is_email($from))) {		
					$sitename = strtolower( $_SERVER['SERVER_NAME'] );
					if ( substr( $sitename, 0, 4 ) == 'www.' ) {
						$sitename = substr( $sitename, 4 );					
					}
					$from = 'admin@'.$sitename; 
				}
				
				$to = $user->user_email;
				$subject = 'Your new password';
				$sender = 'From: '.get_option('name').' <'.$from.'>' . "\r\n";
				
				$message = 'Your new password is: '.$random_password;
					
				$headers[] = 'MIME-Version: 1.0' . "\r\n";
				$headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers[] = "X-Mailer: PHP \r\n";
				$headers[] = $sender;
					
				$mail = wp_mail( $to, $subject, $message, $headers );
				if( $mail ) 
					$success = esc_html__( 'Check your email address for you new password.', 'independent' );
				else
					$error = esc_html__( 'System is unable to send you mail containg your new password.', 'independent' );						
			} else {
				$error = esc_html__( 'Oops! Something went wrong while updaing your account.', 'independent' );
			}
		}
		
		if( ! empty( $error ) )
			echo json_encode(array('loggedin'=>false, 'message'=> $error ));
				
		if( ! empty( $success ) )
			echo json_encode(array('loggedin'=>false, 'message'=> $success ));
					
		exit;
	}
}