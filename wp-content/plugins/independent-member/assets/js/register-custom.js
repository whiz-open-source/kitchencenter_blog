//News Log/Register Script
(function( $ ) {
	"use strict";
	
	$( document ).ready(function() {
	
		/* Independent Login/Register Code */
		$( ".login-form-trigger, .independent-login-close" ).click(function() {
			$('.independent-login-parent').toggleClass('login-open');
			return false;
		});
				
		$( ".move-to-prev-form" ).click(function() {
			$('.independent-login-parent .lost-password-form, .independent-login-parent .registration-form').removeClass('form-state-show').addClass('form-state-hide');
			$('.independent-login-parent .login-form').removeClass('form-state-hide').addClass('form-state-show');	
			return false;
		});
		
		$( ".register-trigger" ).click(function() {
			$('.independent-login-parent .lost-password-form, .independent-login-parent .login-form').removeClass('form-state-show').addClass('form-state-hide');
			$('.independent-login-parent .registration-form').removeClass('form-state-hide').addClass('form-state-show');	
			return false;
		});
		
		$( ".lost-password-trigger" ).click(function() {
			$('.independent-login-parent .registration-form, .independent-login-parent .login-form').removeClass('form-state-show').addClass('form-state-hide');
			$('.independent-login-parent .lost-password-form').removeClass('form-state-hide').addClass('form-state-show');
			return false;
		});
		
		//move-to-prev-form
		
		// Perform AJAX login on form submit
		$( document ).on( 'submit', 'form#login', function(e) {
			
			if( $('form#login #username').val() != '' && $('form#login #password').val() != '' ){
				$('form#login p.status').show().text(independent_ajax_var.loadingmessage);
				($).ajax({
					type: 'post',
					dataType: 'json',
					url: independent_ajax_var.admin_ajax_url,
					data: { 
						'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
						'username': $('form#login #username').val(),
						'password': $('form#login #password').val(),
						'security': $('form#login #security').val() },
					success: function(data){
						$('form#login p.status').text(data.message);
						if (data.loggedin == true){
							window.location.reload();
						}
					}
				});
				e.preventDefault();
			}else{
				$('form#login p.status').text(independent_ajax_var.valid_login);
				return false;
			}
		});
		
		// Perform AJAX register on form submit
		$( document ).on( 'submit', 'form#registration', function(e) {
			if( $('form#registration #email').val() != '' && $('form#registration #username').val() != '' && $('form#registration #password').val() != '' ){
				$('form#registration p.status').show().text(independent_ajax_var.loadingmessage);
	
				($).ajax({
					type: 'post',
					dataType: 'json',
					url: independent_ajax_var.admin_ajax_url,
					data: { 
						'action': 'ajaxregister', //calls wp_ajax_nopriv_ajaxlogin
						'name': $('form#registration #name').val(),
						'email': $('form#registration #email').val(),
						'nick_name': $('form#registration #nick_name').val(),
						'username': $('form#registration #username').val(),
						'password': $('form#registration #password').val(), 
						'security': $('form#registration #security').val() },
					success: function(data){
						$('form#registration p.status').text(data.message);
						if (data.register == true){
							
							$('form#registration p.status').text(data.message);
							setTimeout(function() {
								$('.independent-login-parent .lost-password-form, .independent-login-parent .registration-form').removeClass('form-state-show').addClass('form-state-hide');
								$('.independent-login-parent .login-form').removeClass('form-state-hide').addClass('form-state-show');	
							}, 1000);
							
						}else{
							$('form#registration p.status').text(data.message);	
						}
					}
				});
				e.preventDefault();
			}else{
				$('form#registration p.status').text(independent_ajax_var.req_reg);
				return false;
			}
		});
		
		// Lost Password Ajax
		$( document ).on( 'submit', 'form#forgot_password', function(e) {
			if( $('#user_login').val() != '' ){
				
				$('p.status', this).show().text(independent_ajax_var.loadingmessage);

				($).ajax({
					type: 'post',
					dataType: 'json',
					url: independent_ajax_var.admin_ajax_url,
					data: { 
						'action': 'lost_pass', 
						'user_login': $('#user_login').val(), 
						'security': $('#forgotsecurity').val(), 
					},
					success: function(data){					
						$('form#forgot_password p.status').text(data.message);
					}
				});
				e.preventDefault();
				return false;
			}else{
				$('form#forgot_password p.status').text(independent_ajax_var.valid_email);	
				return false;
			}
		});

	}); // doc ready

})( jQuery );