<?php
	
class independentRedux{

	function __construct(){
		define( 'INDEPENDENT_CORE_REDUX', plugin_dir_path(__FILE__) . 'admin/ReduxCore' );
	}
	
	function independentReduxInit(){
		require_once( INDEPENDENT_CORE_REDUX . '/framework.php' );
		//require_once( INDEPENDENT_CORE_DIR . 'admin/theme-config/config.php' );
	}

}

$independent_redux = new independentRedux();
$independent_redux->independentReduxInit();