<?php 
/*
	Plugin Name: Independent Core
	Plugin URI: http://zozothemes.com/
	Description: Core plugin for independent theme.
	Version: 1.0.7
	Author: zozothemes
	Author URI: http://zozothemes.com/
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$cur_theme = wp_get_theme();	
if ( $cur_theme->get( 'Name' ) != 'Independent' && $cur_theme->get( 'Name' ) != 'Independent Child' ){
	return;
}

define( 'INDEPENDENT_CORE_DIR', plugin_dir_path( __FILE__ ) );
define('INDEPENDENT_CORE_URL', plugin_dir_url( __FILE__ ) );

// Load independent Core Plugin Textdomain
load_plugin_textdomain( 'independent-core', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

//Maintenance 
require_once( INDEPENDENT_CORE_DIR . 'maintenance/maintenance.php' );

require_once( INDEPENDENT_CORE_DIR . 'independent-redux.php' );

// independent Shortcode
require_once( INDEPENDENT_CORE_DIR . 'admin/shortcodes/shortcodes.php' );

// independent Theme Custom Font Upload Option
require_once( INDEPENDENT_CORE_DIR . 'custom-font-code/custom-fonts.php' );

// independent Widgets
require_once( INDEPENDENT_CORE_DIR . 'widgets/about_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/ads_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/latest_post_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/post_slider_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/popular_post_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/tab_post_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/most_view_post_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/author_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/contact_info_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/instagram_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/social_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/tweets_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/mailchimp_widget.php' );
require_once( INDEPENDENT_CORE_DIR . 'widgets/post_categories.php' );

// Category Meta Field
require_once( INDEPENDENT_CORE_DIR . 'inc/independent-category-meta.php' );

function independent_core_admin_scripts_method() {
	
	// Add the color picker css file       
	wp_enqueue_style( 'wp-color-picker' ); 
	
	wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/assets/css/font-awesome.css' ), array(), '4.7.0' );
	wp_enqueue_style( 'simple-line-icons', get_theme_file_uri( '/assets/css/simple-line-icons.css' ), array(), '1.0' );

	wp_enqueue_style( 'independent-core-custom-style', plugins_url( '/admin/assets/css/theme-custom.css' , __FILE__ ), false, '1.0.0' );
    wp_enqueue_script( 'independent-core-custom', plugins_url( '/admin/assets/js/theme-custom.js' , __FILE__ ), array( 'jquery', 'wp-color-picker' ) );
	
	//Admin Localize Script
	wp_localize_script('independent-core-custom', 'independent_core_admin_ajax_var', array(
		'admin_ajax_url' => admin_url('admin-ajax.php'),
		'font_nonce' => wp_create_nonce('independent-font-nounce'), 
		'process' => esc_html__( 'Processing', 'independent-core' ),
		'font_del_pbm' => esc_html__( 'Font Deletion Problem', 'independent-core' )
	));
		
}
add_action( 'admin_enqueue_scripts', 'independent_core_admin_scripts_method' );

function independent_core_load_scripts() {

    wp_enqueue_style( 'independent-core-style', plugins_url( '/assets/css/independent-core-custom.css' , __FILE__ ), false, '1.0' );
	wp_register_script( 'bitcoin-js', INDEPENDENT_CORE_URL . 'assets/js/bitcoin.js', array( 'jquery' ), '1.0', true );	
	wp_enqueue_script( 'endlessRiver', INDEPENDENT_CORE_URL . 'assets/js/endlessRiver.min.js', array( 'jquery' ), '1.0', true );
 
}
add_action('wp_enqueue_scripts', 'independent_core_load_scripts');

/*Author Social Links*/
if( ! function_exists('independent_author_contactmethods') ) {
	function independent_author_contactmethods( $contactmethods ) {
		$contactmethods['twitter'] = esc_html__('Twitter URL', 'independent-core');
		$contactmethods['facebook'] = esc_html__('Facebook URL', 'independent-core');
		$contactmethods['vimeo'] = esc_html__('Vimeo URL', 'independent-core');
		$contactmethods['youtube'] = esc_html__('Youtube URL', 'independent-core');
		
		return $contactmethods;
	}
	add_filter('user_contactmethods','independent_author_contactmethods',10,1);
}

/*Facebook Comments JS*/
if( ! function_exists('independent_fb_comments_js') ) {
	function independent_fb_comments_js(){
		$ato = new independentThemeOpt;
		$comment_type = $ato->independentThemeOpt( 'comments-type' );
		if( $comment_type == 'fb' && is_single() ) :
			$fb_dev_api = $ato->independentThemeOpt( 'fb-developer-key' );
		?>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=<?php echo esc_attr( $fb_dev_api ); ?>";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
		<?php
		endif;
	}
	add_action( 'independent_body_action', 'independent_fb_comments_js', 50 );
}

/* Add Admin Table Columns */
function independent_columns_head( $defaults ) {
	if ( current_user_can( 'manage_options' ) ) {
		$defaults['independent_post_featured_stat'] = esc_html__( 'Featured', 'independent' );
	}
    return $defaults;
}
add_filter('manage_post_posts_columns', 'independent_columns_head');

/* Add Admin Table Coulmn */
function independent_columns_content( $column_name, $post_ID ) {
	if ( current_user_can( 'manage_options' ) ) {
		if ( $column_name == 'independent_post_featured_stat' ) {
			$meta = get_post_meta( $post_ID, 'independent_post_featured_stat', true );
			$out = '<label class="independent-switch">
						<input type="checkbox" data-post="'.$post_ID.'" class="independent-post-featured-status" '. ( $meta == 1 ? 'checked' : '' ) .'>
						<div class="independent-slider round"></div>
					</label><br />
					<span id="post-featured-stat-msg-'.$post_ID.'"></span>';
			echo ( $out );
		}
	}
}
add_action('manage_post_posts_custom_column', 'independent_columns_content', 10, 2);

/*Add Custom Column in Admin Posts list table*/
if( ! function_exists('independent_zozoStarMaking') ) {
	function independent_zozoStarMaking($rate){
		$out = '';
		for($i=1; $i<=5; $i++){
			
			if( $i == round($rate) ){
				if ( $i-0.5 == $rate ) {
					$out .= '<i class="fa fa-star-half-o"></i>';
				}else{
					$out .= '<i class="fa fa-star"></i>';
				}
			}else{
				if( $i < $rate ){
					$out .= '<i class="fa fa-star"></i>';
				}else{
					$out .= '<i class="fa fa-star-o"></i>';
				}
			}
		}// for end
		
		return $out;
	}
}
// ADD NEW COLUMN
function independent_zozo_columns_head($defaults) {
    $defaults['star_ratings'] = esc_html__( 'Rating', 'independent' );
    return $defaults;
}
add_filter('manage_posts_columns', 'independent_zozo_columns_head');

// SHOW THE FEATURED IMAGE
function independent_zozo_columns_content($column_name, $post_ID) {
    if( $column_name == 'star_ratings' ) {
        $srate = get_post_meta( $post_ID, 'independent_post_review_rating', true );
        if( $srate != '' && $srate != 0 ) {			
			if( $column_name == 'single' ) {
				return $srate;
			}else{
				$stars = independent_zozoStarMaking( $srate );
				echo wp_kses_post( $stars );
			}
        }
    }
}
add_action('manage_posts_custom_column', 'independent_zozo_columns_content', 10, 2);

// Facebook Share Code
//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
	return $output . ' prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

function insert_fb_in_head() {
    global $post;
    if ( !is_single() ) //if it is not a post or a page
        return;
	
	ob_start();
	the_excerpt();
	$excerpt = ob_get_clean();	
	
	echo '<meta property="og:title" content="' . get_the_title( $post->ID ) . '"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="' . esc_url( get_permalink( $post->ID ) ) . '"/>
<meta property="og:site_name" content="'. get_bloginfo( 'name' ) .'"/>
<meta property="og:description" content="'. $excerpt .'"/>';
	
	if( has_post_thumbnail( $post->ID ) ) {
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '
<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>
<meta property="og:image:width" content="' . esc_attr( $thumbnail_src[1] ) . '"/>
<meta property="og:image:height" content="' . esc_attr( $thumbnail_src[2] ) . '"/>
';
	}
	
    echo "";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

//Get server software
function independent_get_server_software(){
	return $_SERVER['SERVER_SOFTWARE'];
}

//Get remote address
function independent_get_remote_ip(){
	return $_SERVER['REMOTE_ADDR'];
}

function independent_core_drag_drop_settings_field( $settings, $value, $field_name, $field_id ) {
	$dd_fields = isset( $value ) && $value != '' ? $value : $settings['dd_fields'];

	if( !is_array( $dd_fields ) ){
		$dd_fields = stripslashes( $dd_fields );
		$dd_json = $meta = $dd_fields;
		$part_array = json_decode( $dd_json, true );
	}else{
		$dd_json = $meta = json_encode( $dd_fields );
		$part_array = json_decode( $dd_json, true );
	}
	
	$t_part_array = array();
	$f_part_array = array();

	foreach( $part_array as $key => $value ){
		$t_part_array[$key] = $value != '' ? independent_post_option_drag_drop_multi( $key, $value ) : '';
	}

	$output = '<div class="meta-drag-drop-multi-field">';
	foreach( $t_part_array as $key => $value ){
			$output .= '<h4>'. esc_html( $key ) .'</h4>';
			$output .= $value;
	}
	$output .= '<input class="wpb_vc_param_value meta-drag-drop-multi-value" id="' . esc_attr( $field_id ) . '" name="' . esc_attr( $field_name ) . '" value="'. htmlspecialchars( $meta, ENT_QUOTES, 'UTF-8' ) .'" data-params="'. htmlspecialchars( $meta, ENT_QUOTES, 'UTF-8' ) .'" type="text">';
	$output .= '</div>';
	
	return $output;
}

add_action( 'plugins_loaded', 'independent_call_vc_things' );

function independent_call_vc_things() {

	//Theme Config
	require_once( INDEPENDENT_CORE_DIR . 'admin/config/config.php' );
	
	if ( class_exists( 'Vc_Manager' ) ) {
	
		require_once( INDEPENDENT_CORE_DIR . "widgets/blocks_widget.php");  
		require_once( INDEPENDENT_CORE_DIR . "inc/blocks_widget_class.php");
	
		/* VC Shortcodes */
		add_shortcode( 'independent_vc_banner', 'independent_vc_banner_shortcode' );
		add_shortcode( 'independent_vc_block_1', 'independent_vc_block_1_shortcode' );
		add_shortcode( 'independent_vc_block_2', 'independent_vc_block_2_shortcode' );
		add_shortcode( 'independent_vc_block_3', 'independent_vc_block_3_shortcode' );
		add_shortcode( 'independent_vc_block_4', 'independent_vc_block_4_shortcode' );
		add_shortcode( 'independent_vc_block_5', 'independent_vc_block_5_shortcode' );
		add_shortcode( 'independent_vc_block_6', 'independent_vc_block_6_shortcode' );
		add_shortcode( 'independent_vc_block_7', 'independent_vc_block_7_shortcode' );
		add_shortcode( 'independent_vc_block_8', 'independent_vc_block_8_shortcode' );
		add_shortcode( 'independent_vc_block_9', 'independent_vc_block_9_shortcode' );
		add_shortcode( 'independent_vc_block_10', 'independent_vc_block_10_shortcode' );
		add_shortcode( 'independent_vc_block_11', 'independent_vc_block_11_shortcode' );
		add_shortcode( 'independent_vc_block_12', 'independent_vc_block_12_shortcode' );
		add_shortcode( 'independent_vc_block_13', 'independent_vc_block_13_shortcode' );
		add_shortcode( 'independent_vc_block_14', 'independent_vc_block_14_shortcode' );
		add_shortcode( 'independent_vc_block_15', 'independent_vc_block_15_shortcode' );
		add_shortcode( 'independent_vc_block_16', 'independent_vc_block_16_shortcode' );
		add_shortcode( 'independent_vc_block_17', 'independent_vc_block_17_shortcode' );
		add_shortcode( 'independent_vc_block_18', 'independent_vc_block_18_shortcode' );
		add_shortcode( 'independent_vc_block_19', 'independent_vc_block_19_shortcode' );
		add_shortcode( 'independent_vc_block_20', 'independent_vc_block_20_shortcode' );
		add_shortcode( 'independent_vc_block_21', 'independent_vc_block_21_shortcode' );
		add_shortcode( 'independent_vc_block_ads', 'independent_vc_block_ads_shortcode' );
		add_shortcode( 'independent_vc_followers_count', 'independent_vc_followers_count_shortcode' );
		add_shortcode( 'independent_vc_news_ticker', 'independent_vc_news_ticker_shortcode' );
		add_shortcode( 'independent_vc_block_video_playlist', 'independent_vc_block_video_playlist_shortcode' );
		add_shortcode( 'independent_vc_slide_block', 'independent_vc_slide_block_shortcode' );
		add_shortcode( 'independent_vc_mailchimp', 'independent_vc_mailchimp_shortcode' );
		add_shortcode( 'independent_vc_blog', 'independent_vc_blog_shortcode' );
		add_shortcode( 'independent_vc_blog_zigzag', 'independent_vc_blog_zigzag_shortcode' );
		add_shortcode( 'independent_vc_blog_masonry', 'independent_vc_blog_masonry_shortcode' );
		add_shortcode( 'independent_vc_section_title', 'independent_vc_section_title_shortcode' );
		add_shortcode( 'independent_vc_category_box', 'independent_vc_category_box_shortcode' );
		
		vc_add_shortcode_param('social_followers', 'independent_social_followers_settings_field', get_template_directory_uri() . '/inc/visual-composer/vc_extend/social.js');
		vc_add_shortcode_param('blocks_items', 'independent_blocks_items_settings_field', get_template_directory_uri() . '/inc/visual-composer/vc_extend/blocks-items-new.js');
		vc_add_shortcode_param('thumb_toolbox', 'independent_blocks_tools_settings_field', get_template_directory_uri() . '/inc/visual-composer/vc_extend/blocks-items-new.js');
	}
}

require_once( INDEPENDENT_CORE_DIR . 'admin/metabox/metaboxes/meta_box.php' );