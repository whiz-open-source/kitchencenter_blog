<?php
/**
 * Template coming soon default
 */
 
 //get maintenance header
 require_once( INDEPENDENT_CORE_DIR . 'maintenance/header.php' );
 
 $independent_option = get_option( 'independent_options' );
 $address = isset( $independent_option['maintenance-address'] ) ? $independent_option['maintenance-address'] : '';
 $email = isset( $independent_option['maintenance-email'] ) ? $independent_option['maintenance-email'] : '';
 $phone = isset( $independent_option['maintenance-phone'] ) ? $independent_option['maintenance-phone'] : '';
 
?>

<div class="container text-center maintenance-wrap">

	<div class="row">
		<div class="col-md-12">
			<h1 class="maintenance-title"><?php esc_html_e( 'Coming Soon', 'independent' ); ?></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<h4><?php esc_html_e( 'Phone', 'independent' ); ?></h4>
			<div class="maintenance-phone">
				<?php echo esc_html(  $phone ); ?>
			</div>
		</div>
		<div class="col-md-4">
			<h4><?php esc_html_e( 'Address', 'independent' ); ?></h4>
			<div class="maintenance-address">
				<?php echo wp_kses_post( $address ); ?>
			</div>
		</div>
		<div class="col-md-4">
			<h4><?php esc_html_e( 'Email', 'independent' ); ?></h4>
			<div class="maintenance-email">
				<?php echo esc_html(  $email ); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 maintenance-footer">
			<p><?php esc_html_e( 'We are currently working on an awesome new site, which will be ready soon. Stay Tuned!', 'independent' ); ?></p>
		</div>
	</div>
	
</div>

<?php
 //get maintenance header
 require_once( INDEPENDENT_CORE_DIR . 'maintenance/footer.php' );
?>