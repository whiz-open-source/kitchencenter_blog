<?php
add_action( 'widgets_init', 'independent_most_view_post_load_widget' );
function independent_most_view_post_load_widget() {
	register_widget( 'independent_most_view_post_widget' );
}
class independent_most_view_post_widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'independent_most_view_post_widget', 'description' => esc_html__( 'A widget that displays your most viewed posts from all categories or a certain', 'independent' ) );
		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'independent_most_view_post_widget' );
		/* Create the widget. */
		parent::__construct( 'independent_most_view_post_widget', esc_html__( 'Independent Most Viewed Posts', 'independent' ), $widget_ops, $control_ops );
	}
	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$categories = $instance['categories'];
		$number = $instance['number'];
		$serial = $instance['serial'];
		
		$query = array('showposts' => $number, 'nopaging' => 0, 'post_status' => 'publish', 'ignore_sticky_posts' => 1, 'meta_key' => 'independent_post_views_count', 'orderby' => 'meta_value_num', 'cat' => $categories);
		
		$loop = new WP_Query($query);
		if ($loop->have_posts()) :
		
		/* Before widget (defined by themes). */
		echo $before_widget;
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
			
			$num = 1;
		?>
			<div class="widg-content">
				<div class="row most-viewed-posts">
				
				<?php  while ($loop->have_posts()) : $loop->the_post(); ?>
					<?php 
						$format = get_post_format( get_the_ID() );
					?>
					<div class="col-lg-6 col-md-4 col-sm-6">					
						<div class="most-viewed-post-item">
						
							<div class="most-viewed-post-image">
								<?php
									if( isset( $serial ) && $serial == 1 ){
										echo '<span class="most-viewed-index">'. esc_attr( $num ) .'</span>';
									}
								?>
								<?php 
									if ( has_post_thumbnail() ) {
										$post_img = independent_custom_image_size_chk( '', array( 160, 100 ) );
										$num++;
								?>
									<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
										<img src="<?php echo esc_url( $post_img[0] ) ?>" width="<?php echo esc_attr( $post_img[1] ) ?>" height="<?php echo esc_attr( $post_img[2] ) ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
									</a>
								<?php
									}
								?>
							</div>
							<div class="most-viewed-post-title">
								<h6><a class="post-title" href="<?php echo get_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h6>
							</div>
						</div>					
					</div><!-- .col -->
				
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
				<?php endif; ?>
				
				</div><!-- .row -->
			</div>
			
		<?php
		/* After widget (defined by themes). */
		echo $after_widget;
	}
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['categories'] = $new_instance['categories'];
		$instance['number'] = strip_tags( $new_instance['number'] );
		$instance['serial'] = strip_tags( $new_instance['serial'] );
		return $instance;
	}
	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array( 'title' => esc_html__('Most Viewed Posts', 'independent'), 'number' => 6, 'categories' => '', 'serial' => 0 );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'independent'); ?></label>
			<input  type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>"  />
		</p>
		
		<!-- Category -->
		<p>
		<label for="<?php echo $this->get_field_id('categories'); ?>"><?php esc_html_e('Filter by Category:', 'independent'); ?></label> 
		<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
			<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>><?php esc_html_e('All categories', 'independent'); ?></option>
			<?php $categories = get_categories('hide_empty=1&depth=1&type=post'); ?>
			<?php foreach($categories as $category) { ?>
			<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
			<?php } ?>
		</select>
		</p>
		
		<!-- Number of posts -->
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e('Number of posts to show:', 'independent'); ?></label>
			<input  type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" size="3" />
		</p>
		
		<!-- Serial -->
		<p>
		<label for="<?php echo $this->get_field_id('serial'); ?>"><?php esc_html_e('Show Serial:', 'independent'); ?></label> 
		<select id="<?php echo $this->get_field_id('serial'); ?>" name="<?php echo $this->get_field_name('serial'); ?>" class="widefat" style="width:100%;">
			<option value='0' <?php if ('0' == $instance['serial']) echo 'selected="selected"'; ?>><?php esc_html_e('No', 'independent'); ?></option>
			<option value='1' <?php if ('1' == $instance['serial']) echo 'selected="selected"'; ?>><?php esc_html_e('Yes', 'independent'); ?></option>
		</select>
		</p>
		
	<?php
	}
}
?>