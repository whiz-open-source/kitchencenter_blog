<?php
add_action( 'widgets_init', 'independent_post_slider_load_widget' );
function independent_post_slider_load_widget() {
	register_widget( 'independent_post_slider_widget' );
}
class independent_post_slider_widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'independent_post_slider_widget', 'description' => esc_html__( 'A widget that displays your latest post as slider from all categories or a certain', 'independent' ) );
		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'independent_post_slider_widget' );
		/* Create the widget. */
		parent::__construct( 'independent_post_slider_widget', esc_html__( 'Independent Post Slider', 'independent' ), $widget_ops, $control_ops );
	}
	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$categories = $instance['categories'];
		$number = $instance['number'];
		
		$query = array('showposts' => $number, 'nopaging' => 0, 'post_status' => 'publish', 'ignore_sticky_posts' => 1, 'cat' => $categories);
		
		$loop = new WP_Query($query);
		if ($loop->have_posts()) :
		
		/* Before widget (defined by themes). */
		echo $before_widget;
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
			
			$num = 1;
		?>
			<div class="widg-content">
				<div class="owl-carousel post-widget-slider" data-items="1">
				
				<?php  while ($loop->have_posts()) : $loop->the_post(); ?>
					<?php 
						$format = get_post_format( get_the_ID() );
						$categories = get_the_category();
					?>
				
					<div class="slide-item">
						<div class="slide-item-inner">
							<div class="slide-image">
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) : ?>
									<a href="<?php echo get_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('medium', array('class' => 'img-responsive')); ?></a>
								<?php endif; ?>		
							</div><!-- .slide-image -->
							<div class="slide-item-text">
								<a href="<?php echo get_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
								<div class="widget-meta-wrap">
									<?php if ( ! empty( $categories ) ) { ?>
									<div class="post-category">
										<span class="before-icon fa fa-folder-o"></span>
										<a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>"><?php echo esc_html( $categories[0]->name ); ?></a>
									</div>
									<?php } ?>
									<div class="post-comment">
										<span class="before-icon fa fa-clock-o"></span>
										<span class="f-date"><?php the_time( get_option('date_format') ); ?></span>
									</div>
								</div>
							</div><!-- .slide-item-text -->
						</div><!-- .slide-item-inner -->
					</div><!-- .slide-item -->				
				
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
				<?php endif; ?>
				
				</div><!-- .post-widget-slider -->
			</div>
			
		<?php
		/* After widget (defined by themes). */
		echo $after_widget;
	}
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['categories'] = $new_instance['categories'];
		$instance['number'] = strip_tags( $new_instance['number'] );
		return $instance;
	}
	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array( 'title' => esc_html__('Latest Posts', 'independent'), 'number' => 5, 'categories' => '', 'serial' => 0 );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'independent'); ?></label>
			<input  type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>"  />
		</p>
		
		<!-- Category -->
		<p>
		<label for="<?php echo $this->get_field_id('categories'); ?>"><?php esc_html_e('Filter by Category:', 'independent'); ?></label> 
		<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
			<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>><?php esc_html_e('All categories', 'independent'); ?></option>
			<?php $categories = get_categories('hide_empty=1&depth=1&type=post'); ?>
			<?php foreach($categories as $category) { ?>
			<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
			<?php } ?>
		</select>
		</p>
		
		<!-- Number of posts -->
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e('Number of posts to show:', 'independent'); ?></label>
			<input  type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" size="3" />
		</p>
		
	<?php
	}
}
?>