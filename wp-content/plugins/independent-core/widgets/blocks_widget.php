<?php
/**
 * Plugin Name: independent_block_widget
 */

class independent_block_widget extends WP_Widget {

	var $block_id = '1';
	private $block_map;
	private $independent_param_default_array;

	/**
	 * Widget setup.
	 */
	public function __construct() {
		

		$id = 'independent_widget_block_'. $this->block_id;
		$block_map_fun = 'independent_vc_map_block_'. $this->block_id;

		$this->block_map = $block_map_fun();
		
		$widget_ops = array( 'classname' => 'widget_'. $this->block_map['base'], 'description' => esc_html__('A widget that displays news blocks', 'independent') );
		parent::__construct( $id, esc_html__('[Independent] ', 'independent') . $this->block_map['name'], $widget_ops );
		
		$this->independent_param_default_array = $this->independent_param_default_values();
		wp_enqueue_script( 'independent-drag-drop', INDEPENDENT_CORE_URL . 'assets/js/drag-drop.js', array( 'jquery' ) );
	}

	/**
	 * How to display the widget on the screen.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		echo wp_kses_post( $before_widget );
			$block_form = 'independent_news_block_form_'. $this->block_id;
			echo ( $block_form( $instance ) );
		echo wp_kses_post( $after_widget );
	}

	private function independent_param_default_values() {
	    $buffy_array = array();
	    if (!empty($this->block_map['params'])) {
	        foreach ($this->block_map['params'] as $param) {
	            $buffy_array[$param['param_name']] = isset( $param['value'] ) && $param['value'] != '' ? $param['value'] : '';
	        }
	    }
	    return $buffy_array;
	}
	
	/**
	 * Update the widget settings.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
	    foreach ($this->independent_param_default_array as $param_name => $param_value) {
	        // we must check for isset, because otherwise we will end up with NULL if a field type is not declare above
		    // like the problem we had with the css att/field
		    if (isset($new_instance[$param_name])) {
			    $instance[$param_name] = $new_instance[$param_name];
		    }
	    }
	    return $instance;
	}
	public function form( $instance ) {

		/* Set up some default widget settings. */
		$instance = wp_parse_args((array) $instance, $this->independent_param_default_array);
		if (!empty($this->block_map['params'])) {
			foreach ($this->block_map['params'] as $param) {
			
				$depend = isset( $param['dependency'] ) ? $param['dependency']['element'] : '';
				$depend_val = isset( $param['dependency'] ) ? $param['dependency']['value'] : '';
			
				switch ($param['type']) {
					 case 'textfield':
					 	?>
						<div class="custom-vc-widget-field"<?php echo ( $depend != '' ? ' data-depend="'. esc_attr( $depend ) .'"' : '' ); ?> <?php echo ( $depend_val != '' ? ' data-depend-val="'. esc_attr( $depend_val ) .'"' : '' ); ?>>
							<label for="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>"><?php echo esc_html( $param['heading'] ); ?></label>
							<input id="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>" name="<?php echo esc_attr( $this->get_field_name($param['param_name']) ); ?>" value="<?php echo esc_attr( $instance[ $param['param_name'] ] ); ?>" class="widefat" type="text" />
							<h5><?php echo isset( $param['description'] ) ? $param['description'] : ''; ?></h5>
						</div>
						<?php
					 break;
						
					case 'dropdown':
					 	?>
						<div class="custom-vc-widget-field"<?php echo ( $depend != '' ? ' data-depend="'. esc_attr( $depend ) .'"' : '' ); ?> <?php echo ( $depend_val != '' ? ' data-depend-val="'. esc_attr( $depend_val ) .'"' : '' ); ?>>
							<label for="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>"><?php echo esc_html( $param['heading'] ); ?></label>
							<select id="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>" name="<?php echo esc_attr( $this->get_field_name($param['param_name']) ); ?>" class="custom-vc-widget-select widefat" data-org="<?php echo esc_attr( $param['param_name'] ); ?>" >
								<?php foreach( $param['value'] as $key => $value ) { ?>
								<option value='<?php echo esc_attr($value); ?>' <?php if ( $value == $instance[ $param['param_name'] ]) echo 'selected="selected"'; ?>><?php echo esc_attr( $key ); ?></option>
								<?php } ?>
							</select>
							<h5><?php echo isset( $param['description'] ) ? $param['description'] : ''; ?></h5>
						</div>
						<?php
					 break;
						
					case 'drag_drop':
					 	?>
						<div class="custom-vc-widget-field"<?php echo ( $depend != '' ? ' data-depend="'. esc_attr( $depend ) .'"' : '' ); ?> <?php echo ( $depend_val != '' ? ' data-depend-val="'. esc_attr( $depend_val ) .'"' : '' ); ?>>
							<label for="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>"><?php echo esc_html( $param['heading'] ); ?></label>
							<?php 
								$value = $instance[ $param['param_name'] ];
								$field_id = $this->get_field_id( $param['param_name'] );
								$field_name = $this->get_field_name( $param['param_name'] );
								echo independent_core_drag_drop_settings_field( $param, $value, $field_name, $field_id );
							?>
							<h5><?php echo esc_html( $param['description'] ); ?></h5>
						</div>
						<?php
					 break;
						
					case 'colorpicker':
					 	?>
						<div class="custom-vc-widget-field"<?php echo ( $depend != '' ? ' data-depend="'. esc_attr( $depend ) .'"' : '' ); ?> <?php echo ( $depend_val != '' ? ' data-depend-val="'. esc_attr( $depend_val ) .'"' : '' ); ?>>
							<label for="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>"><?php echo esc_html( $param['heading'] ); ?></label>
							<input class="independent-color-picker" type="text" id="<?php echo ( $this->get_field_id( $param['param_name'] ) ); ?>" name="<?php echo ( $this->get_field_name( $param['param_name'] ) ); ?>" value="<?php echo esc_attr( $instance[ $param['param_name'] ]	 ); ?>" />
							<h5><?php echo esc_html( $param['description'] ); ?></h5>
						</div>
						<?php
					 break;
					 
					 case 'img_select':
					 	?>
						<div class="custom-vc-widget-field"<?php echo ( $depend != '' ? ' data-depend="'. esc_attr( $depend ) .'"' : '' ); ?> <?php echo ( $depend_val != '' ? ' data-depend-val="'. esc_attr( $depend_val ) .'"' : '' ); ?>>
							<label for="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>"><?php echo esc_html( $param['heading'] ); ?></label>
							<?php
							$output = '';
							$img_array = $param['img_lists'];
							if( $img_array != '' ){
								$output .= '<ul class="img-select">';
								foreach( $img_array as $key => $url ){
									$output .= '<li data-id="'. esc_attr( $key ) .'" class="'. ( $instance[ $param['param_name'] ] == $key ? 'selected' : '' ) .'"><img src="'. esc_url( $url ) .'" /></li>';
								}
								$output .= '</ul>';
								
							}
							echo ( $output );
							?>
							<input id="<?php echo esc_attr( $this->get_field_id($param['param_name']) ); ?>" name="<?php echo esc_attr( $this->get_field_name($param['param_name']) ); ?>" value="<?php echo esc_attr( $instance[ $param['param_name'] ] ); ?>" class="wpb_vc_param_value img-select-value" type="hidden" />
							<h5><?php echo isset( $param['description'] ) ? $param['description'] : ''; ?></h5>
						</div>
						<?php
					 break;
				}
			}
		}
		?>
	<?php
	}
}
?>