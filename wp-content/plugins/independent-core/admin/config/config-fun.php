<?php
/* independent Config Repeated Code Class */

class independentConfigFun {

	private $independent_options;
   
    function __construct() {
		$this->independent_options = get_option( 'independent_options' );
    }
	
	function independentGetAdminThemeOpt( $field ){
		$independent_options = $this->independent_options;
		return isset( $independent_options[$field] ) && $independent_options[$field] != '' ? $independent_options[$field] : '';
	}
	
	function themeCategories(){

		$categories = apply_filters( 
			'independent_get_categories',
			get_categories( array(
				'orderby' => 'name',
				'order'   => 'ASC'
			) )
		);
		$category_array = array();
		foreach ( $categories as $category ) {
			$category_array['category-'.$category->term_id] = $category->name;
		}
		return $category_array;
		
	}
	
	function themeMarginFields( $field ){
		
		$dimesin_array = array(
			'id'             => $field.'-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => array('px'),
			'units_extended' => 'false',
			'title'          => esc_html__('Margin Option', 'independent'),
			'subtitle'       => esc_html__('Set margin top/right/bottom/left.', 'independent'),
			'default'            => array(
				'margin-top'     => '', 
				'margin-right'   => '', 
				'margin-bottom'  => '', 
				'margin-left'    => '',
			)
		);
		
		return $dimesin_array;
		
	}
	
	function themeAdsList( $field, $template_name, $position = '' ){
		$ads_list = array(
			'id'       => $field.'-ads-list',
			'type'     => 'select',
			'title'    => sprintf( esc_html__( 'Ads List %1$s', 'independent' ), $position ),
			'desc'     => sprintf( esc_html__( 'Choose ads list to show in %1$s.', 'independent' ), $template_name ),
			'options'  => array(
				'header' => esc_html__( 'Header Ads', 'independent' ),
				'footer' => esc_html__( 'Footer Ads', 'independent' ),
				'sidebar' => esc_html__( 'Sidebar Ads', 'independent' ),
				'artical-top' => esc_html__( 'Artical Top Ads', 'independent' ),
				'artical-inline' => esc_html__( 'Artical Inline Ads', 'independent' ),
				'artical-bottom' => esc_html__( 'Artical Bottom Ads', 'independent' ),
				'custom1' => esc_html__( 'Custom 1 Ads', 'independent' ),
				'custom2' => esc_html__( 'Custom 2 Ads', 'independent' ),
				'custom3' => esc_html__( 'Custom 3 Ads', 'independent' ),
				'custom4' => esc_html__( 'Custom 4 Ads', 'independent' ),
				'custom5' => esc_html__( 'Custom 5 Ads', 'independent' ),
			),
			'default'  => ''
		);
		return $ads_list;
	}
	
	function themeAdsFields( $field ){
		
		$ads = array(
				array(
					'id'       => $field.'-ads-text',
					'type'     => 'textarea',
					'title'    => esc_html__( 'Adsense Code', 'independent' ),
					'subtitle'     => esc_html__( 'Place your Google adsense code here or enter custom ad link code', 'independent' ),
					'default'  => ''
				),
				array(
					'id'       => $field.'-ads-md',
					'type'     => 'button_set',
					'title'    => esc_html__( 'Enable on Desktop', 'independent' ),
					'subtitle'     => esc_html__( 'choose yes to enable ads on desktop view.', 'independent' ),
					'options' => array(
						'yes' => esc_html__( 'Yes', 'independent' ),
						'no'  => esc_html__( 'No', 'independent' ),
					),
					'default'  => 'yes'
				),
				array(
					'id'       => $field.'-ads-sm',
					'type'     => 'button_set',
					'title'    => esc_html__( 'Enable on Tablet', 'independent' ),
					'subtitle'     => esc_html__( 'choose yes to enable ads on tablet view.', 'independent' ),
					'options' => array(
						'yes' => esc_html__( 'Yes', 'independent' ),
						'no'  => esc_html__( 'No', 'independent' ),
					),
					'default'  => 'yes'
				),
				array(
					'id'       => $field.'-ads-xs',
					'type'     => 'button_set',
					'title'    => esc_html__( 'Enable on Mobile', 'independent' ),
					'subtitle'     => esc_html__( 'choose yes to enable ads on mobile view.', 'independent' ),
					'options' => array(
						'yes' => esc_html__( 'Yes', 'independent' ),
						'no'  => esc_html__( 'No', 'independent' ),
					),
					'default'  => 'yes'
				),
			);
		return $ads;
	}
	
	function themeSkinSettings($field, $extras = array()){
	
		$line_height = isset( $extras['line_height'] ) ? $extras['line_height'] : false;
		
		$theme_skin_set = array(
			array(
                'id'       => $field.'-typography',
                'type'     => 'typography',
                'title'    => __( 'Typography', 'independent' ),
                'subtitle' => __( 'Specify the font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> $line_height,
				'text-transform' => true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '',
                    'font-family' => '',
                    'font-weight' => '',
                ),
            ),
			array(
                'id'       => $field.'-background',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Background', 'independent' ),
                'subtitle' => esc_html__( 'Choose background color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
            ),
			array(
                'id'       => $field.'-link-color',
                'type'     => 'link_color',
                'title'    => esc_html__( 'Links Color', 'independent' ),
                'subtitle' => esc_html__( 'Choose link color options.', 'independent' ),
                'default'  => array(
                    'regular' => '',
                    'hover'   => '',
                    'active'  => '',
                )
            ),
			array(
                'id'       => $field.'-border',
                'type'     => 'border',
                'title'    => esc_html__( 'Border', 'independent' ),
                'subtitle' => esc_html__( 'Set border option.', 'independent' ),
				'all'      => false,
                'default'  => array(
                    'border-color'  => '',
                    'border-style'  => 'none',
                    'border-top'    => '',
                    'border-right'  => '',
                    'border-bottom' => '',
                    'border-left'   => ''
                )
            ),
			array(
				'id'             => $field.'-padding',
				'type'           => 'spacing',
				'mode'           => 'padding',
				'units'          => array('px'),
				'units_extended' => 'false',
				'title'          => __('Padding Option', 'independent'),
				'subtitle'       => __('Set padding for this bar.', 'independent'),
				'default'            => array(
					'pading-top'     => '', 
					'pading-right'   => '', 
					'pading-bottom'  => '', 
					'pading-left'    => '',
				)
			),
			array(
                'id'       => $field.'-background-all',
                'type'     => 'background',
                'title'    => esc_html__( 'Background Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for background.', 'independent' ),
                'default'   => '',
            ),
			array(
				'id'             => $field.'-margin',
				'type'           => 'spacing',
				'mode'           => 'margin',
				'units'          => array('px'),
				'units_extended' => 'false',
				'title'          => __('Margin Option', 'independent'),
				'subtitle'       => __('Set margin for this bar.', 'independent'),
				'default'            => array(
					'margin-top'     => '', 
					'margin-right'   => '', 
					'margin-bottom'  => '', 
					'margin-left'    => '',
				)
			),
		);
		return $theme_skin_set;
	}
	
	function themeSidebarsList( $field, $extras ){
	
		$default = isset( $extras['default'] ) ? $extras['default'] : '';
		$title = isset( $extras['title'] ) ? $extras['title'] : '';
	
		$sidebars_array = array(
			array(
                'id'       => $field.'-layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Layouts', 'independent' ),
                'desc'     => esc_html__( 'Choose your layouts.', 'independent' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '3-3-3-3' => array(
                        'alt' => esc_html__( 'Layout 1', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-1.png'
                    ),
                    '4-4-4' => array(
                        'alt' => esc_html__( 'Layout 2', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-2.png'
                    ),
                    '3-6-3' => array(
                        'alt' => esc_html__( 'Layout 3', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-3.png'
                    ),
					'6-6' => array(
                        'alt' => esc_html__( 'Layout 4', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-4.png'
                    ),
					'9-3' => array(
                        'alt' => esc_html__( 'Layout 5', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-5.png'
                    ),
					'3-9' => array(
                        'alt' => esc_html__( 'Layout 6', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-6.png'
                    ),
					'12' => array(
                        'alt' => esc_html__( 'Layout 7', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/footer-layouts/footer-7.png'
                    ),
                ),
                'default'  => '3-3-3-3'
            ),
			array(
                'id'       => $field.'-sidebar-1',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose First Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing first column.', 'independent' ),
                'data'     => 'sidebars'
            ),
			array(
                'id'       => $field.'-sidebar-2',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Second Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing second column.', 'independent' ),
                'data'     => 'sidebars',
            ),
			array(
                'id'       => $field.'-sidebar-3',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Third Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing third column.', 'independent' ),
                'data'     => 'sidebars',
            ),
			array(
                'id'       => $field.'-sidebar-4',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Fourth Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing fourth column.', 'independent' ),
                'data'     => 'sidebars',
            )
		);
		
		return $sidebars_array;
	}
	
	function themeFontColor( $field ){
		$color_array = array(
			array(
				'id'      => $field.'-color',
				'type'    => 'color',
				'title'   => esc_html__( 'Font Color', 'independent' ),
				'desc'    => esc_html__( 'This is font color for current field.', 'independent' ),
				'validate' => 'color',
			)
		);
		return $color_array;
	}
	
	function themePageTitleItems( $field ){
		$page_title_array = '';
		if( $field == 'template-blog' ){
			$page_title_array = array(
				array(
					'id'      => $field.'-pagetitle-items',
					'type'    => 'sorter',
					'title'   => esc_html__( 'Page Title Items', 'independent' ),
					'desc'    => esc_html__( 'Needed page title items drag from disabled and put enabled.', 'independent' ),
					'options' => array(
						'disabled' => array(
							'description' => esc_html__( 'Page Title Description', 'independent' )
						),
						'Left'  => array(
							'title' => esc_html__( 'Page Title Text', 'independent' ),
						),
						'Center'  => array(
							
						),
						'Right'  => array(
							'breadcrumb'	=> esc_html__( 'Breadcrumb', 'independent' )
						)
					),
				)
			);
		}elseif( $field == 'template-author' ){
			$page_title_array = array(
				array(
					'id'      => $field.'-pagetitle-items',
					'type'    => 'sorter',
					'title'   => esc_html__( 'Page Title Items', 'independent' ),
					'desc'    => esc_html__( 'Needed page title items drag from disabled and put enabled.', 'independent' ),
					'options' => array(
						'disabled' => array(

						),
						'Left'  => array(
							'author-info' => esc_html__( 'Author Info', 'independent' ),
						),
						'Center' => array(

						),
						'Right'  => array(
							'breadcrumb'	=> esc_html__( 'Breadcrumb', 'independent' )
						)						
					),
				)
			);
		}elseif( strpos( $field, 'category' ) ){
			$page_title_array = array(
				array(
					'id'      => $field.'-pagetitle-items',
					'type'    => 'sorter',
					'title'   => esc_html__( 'Category Page Title Items', 'independent' ),
					'desc'    => esc_html__( 'Needed page title items drag from disabled and put enabled.', 'independent' ),
					'options' => array(
						'disabled' => array(
							'description' => esc_html__( 'Category Description', 'independent' )
						),
						'Left'  => array(
							'title' => esc_html__( 'Category Title', 'independent' ),
						),
						'Center'  => array(
							
						),
						'Right'  => array(
							'breadcrumb'	=> esc_html__( 'Breadcrumb', 'independent' )
						)
					),
				)
			);
		}else{
			$page_title_array = array(
				array(
					'id'      => $field.'-pagetitle-items',
					'type'    => 'sorter',
					'title'   => esc_html__( 'Page Title Items', 'independent' ),
					'desc'    => esc_html__( 'Needed page title items drag from disabled and put enabled.', 'independent' ),
					'options' => array(
						'disabled' => array(

						),
						'Left'  => array(
							'title' => esc_html__( 'Page Title Text', 'independent' ),
						),
						'Center' => array(

						),
						'Right'  => array(
							'breadcrumb'	=> esc_html__( 'Breadcrumb', 'independent' )
						)						
					),
				)
			);
		}
		return $page_title_array;
	}
	
	function themeSliders( $slide ){
	
		$items = $margin = array();
		
		if( $slide == 'blog' ){
			$items = array(
				'id'       => $slide.'-slide-items',
				'type'     => 'text',
				'title'    => esc_html__( 'Items to Display', 'independent' ),
				'desc'     => esc_html__( 'Enter slider items to display', 'independent' ),
				'default'  => '1'
			);
		}else{
			$items = array(
				'id'       => $slide.'-slide-items',
				'type'     => 'text',
				'title'    => esc_html__( 'Items to Display', 'independent' ),
				'desc'     => esc_html__( 'Enter slider items to display', 'independent' ),
				'default'  => '3'
			);
		}
		
		if( $slide == 'related' ){
			$margin = array(
				'id'       => $slide.'-slide-margin',
				'type'     => 'text',
				'title'    => esc_html__( 'Margin', 'independent' ),
				'desc'     => esc_html__( 'Enter margin( item spacing )', 'independent' ),
				'default'  => '10'
			);
		}else{
			$margin = array(
				'id'       => $slide.'-slide-margin',
				'type'     => 'text',
				'title'    => esc_html__( 'Margin', 'independent' ),
				'desc'     => esc_html__( 'Enter margin( item spacing )', 'independent' ),
				'default'  => '0'
			);
		}
	
		$slider_array = array(
			$items,
			array(
				'id'       => $slide.'-slide-tab',
				'type'     => 'text',
				'title'    => esc_html__( 'Items to Display Tab', 'independent' ),
				'desc'     => esc_html__( 'Enter items to display tablet', 'independent' ),
				'default'  => '1'
			),
			array(
				'id'       => $slide.'-slide-mobile',
				'type'     => 'text',
				'title'    => esc_html__( 'Items to Display on Mobile', 'independent' ),
				'desc'     => esc_html__( 'Enter items to display on mobile view', 'independent' ),
				'default'  => '1'
			),
			array(
				'id'       => $slide.'-slide-scrollby',
				'type'     => 'text',
				'title'    => esc_html__( 'Items Scrollby', 'independent' ),
				'desc'     => esc_html__( 'Enter slider items scrollby', 'independent' ),
				'default'  => '1'
			),
			array(
				'id'       => $slide.'-slide-autoplay',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Slide Autoplay', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable slide autoplay', 'independent' ),
				'options' => array(
					'true' => esc_html__( 'Yes', 'independent' ),
					'false'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'true'
			),
			array(
				'id'       => $slide.'-slide-center',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Slide Center', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable slide center', 'independent' ),
				'options' => array(
					'true' => esc_html__( 'Yes', 'independent' ),
					'false'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'false'
			),
			array(
				'id'       => $slide.'-slide-duration',
				'type'     => 'text',
				'title'    => esc_html__( 'Slide Duration', 'independent' ),
				'desc'     => esc_html__( 'Enter slide duration for each (in Milli Seconds)', 'independent' ),
				'default'  => '5000'
			),
			array(
				'id'       => $slide.'-slide-smartspeed',
				'type'     => 'text',
				'title'    => esc_html__( 'Slide Smart Speed', 'independent' ),
				'desc'     => esc_html__( 'Enter slide smart speed for each (in Milli Seconds)', 'independent' ),
				'default'  => '250'
			),
			array(
				'id'       => $slide.'-slide-infinite',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Infinite Loop', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable infinite loop', 'independent' ),
				'options' => array(
					'true' => esc_html__( 'Yes', 'independent' ),
					'false'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'false'
			),
			$margin,
			array(
				'id'       => $slide.'-slide-pagination',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Pagination', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable pagination', 'independent' ),
				'options' => array(
					'true' => esc_html__( 'Yes', 'independent' ),
					'false'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'false'
			),
			array(
				'id'       => $slide.'-slide-navigation',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Navigation', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable navigation', 'independent' ),
				'options' => array(
					'true' => esc_html__( 'Yes', 'independent' ),
					'false'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'false'
			),
			array(
				'id'       => $slide.'-slide-autoheight',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Auto Height', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable slide item auto height', 'independent' ),
				'options' => array(
					'true' => esc_html__( 'Yes', 'independent' ),
					'false'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'false'
			)
		);
		
		return $slider_array;
	}
	
	function independentThemeOptTemplate( $template, $template_cname, $template_sname ){

		$template_t = $this->themeSkinSettings('template-'.$template);
		$template_article = $this->themeSkinSettings($template.'-article');
		$page_title_items = $this->themePageTitleItems('template-'.$template);
		$color = $this->themeFontColor('template-'.$template);
		$template_article_color = $this->themeFontColor($template.'-article');
		
		$page_tit = $page_tit_desc = '';
		if( $template == 'blog' ){
			$page_tit = array(
				'id'       => $template.'-page-title',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Page Title', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'This is a title for %1$s page. HTML code allowed here.', 'independent' ), $template_sname ),
				'default'  => esc_html__( 'Multiuse Theme', 'independent' )
			);
			$page_tit_desc = array(
				'id'		=> $template.'-page-desc',
				'type'		=> 'textarea',
				'title'		=> sprintf( esc_html__( '%1$s Page Description', 'independent' ), $template_cname ),
				'subtitle'	=> sprintf( esc_html__( 'This is description for %1$s page. HTML code allowed here.', 'independent' ), $template_sname ),
				'default'	=> '',
			);
		}
		
		$banner_model = $banner_style = '';
		if( $template != 'blog' && $template != 'archive' && $template != 'tag' && $template != 'author' && $template != 'search' ){
			$banner_model = array(
				'id'       => $template.'-banner',
				'type'     => 'select',
				'title'    => sprintf( esc_html__( '%1$s Banner Model', 'independent' ), $template_cname ),
				'desc'     => sprintf( esc_html__( 'Choose %1$s category banner model. If you enable this, banner showing on top of the current category page.', 'independent' ), $template_sname ),
				'options'  => array(
					'none' => esc_html__( 'None', 'independent' ),
					'1' => esc_html__( 'Banner Model 1', 'independent' ),
					'2' => esc_html__( 'Banner Model 2', 'independent' ),
					'3' => esc_html__( 'Banner Model 3', 'independent' ),
					'4' => esc_html__( 'Banner Model 4', 'independent' ),
					'5' => esc_html__( 'Banner Model 5', 'independent' ),
					'6' => esc_html__( 'Banner Model 6', 'independent' ),
					'7' => esc_html__( 'Banner Model 7', 'independent' ),
				),
				'default'  => 'none'
			);
			$banner_style = array(
				'id'       => $template.'-banner-style',
				'type'     => 'select',
				'title'    => sprintf( esc_html__( '%1$s Banner Style', 'independent' ), $template_cname ),
				'desc'     => sprintf( esc_html__( 'Choose %1$s category banner style', 'independent' ), $template_sname ),
				'options'  => array(
					'' => esc_html__( 'None', 'independent' ),
					'black' => esc_html__( 'Black Gradient', 'independent' ),
					'theme' => esc_html__( 'Theme Gradient', 'independent' ),
					'dark' => esc_html__( 'Flat Black', 'independent' ),
					'rainbow' => esc_html__( 'Rainbow', 'independent' )
				),
				'default'  => '',
				'required' => array( $template.'-banner', '!=', 'none' )
			);
		}
		
		
		$template_array = array(
			array(
                'id'       => $template.'-tag-color',
                'type'     => 'color',
                'title'    => sprintf( esc_html__( '%1$s Tag Color', 'independent' ), $template_cname ),
                'desc' => sprintf( esc_html__( 'This is tag color for %1$s category.', 'independent' ), $template_sname ),
                'default'  => '',
            ),
			array(
				'id'       => $template.'-page-title-opt',
				'type'     => 'switch',
				'title'    => sprintf( esc_html__( '%1$s Page Title', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title.', 'independent' ), $template_sname ),
				'default'  => 1,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-pagetitle-settings-start',
				'type'     => 'section',
				'title'    => esc_html__( 'Page Title Settings', 'independent' ),
				'subtitle' => esc_html__( 'This is page title style settings for this template', 'independent' ),
				'indent'   => true, 
				'required' 		=> array($template.'-page-title-opt', '=', 1)
			),
			$color[0],
			$template_t[2],
			$template_t[3],
			$template_t[4],
			$template_t[5],
			array(
				'id'       => $template.'-page-title-parallax',
				'type'     => 'switch',
				'title'    => esc_html__( 'Background Parallax', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title background parallax.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-page-title-bg',
				'type'     => 'switch',
				'title'    => esc_html__( 'Background Video', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title background video.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-page-title-video',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Page Title Background Video', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Set page title background video for %1$s page. Only allowed youtube video id. Example: UWF7dZTLW4c', 'independent' ), $template_sname ),
				'required' => array($template.'-page-title-bg', '=', 1),
				'default'  => ''
			),
			array(
                'id'       => $template.'-page-title-overlay',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Page Title Overlay', 'independent' ),
                'subtitle' => esc_html__( 'Choose page title overlay rgba color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
            ),
			$page_tit,
			$page_tit_desc,
			$page_title_items[0],
			array(
				'id'     => $template.'-pagetitle-settings-end',
				'type'   => 'section',
				'indent' => false, 
			),
			array(
				'id'       => $template.'-featured-slider',
				'type'     => 'switch',
				'title'    => sprintf( esc_html__( '%1$s Featured Slider', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s featured slider.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
			),
			$banner_model,
			$banner_style,
			array(
				'id'       => $template.'-settings-start',
				'type'     => 'section',
				'title'    => sprintf( esc_html__( '%1$s Settings', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'This is settings for %1$s', 'independent' ), $template_sname ),
				'indent'   => true
			),
			array(
				'id'       => $template.'-page-template',
				'type'     => 'image_select',
				'title'    => sprintf( esc_html__( '%1$s Template', 'independent' ), $template_cname ),
				'desc'     => sprintf( esc_html__( 'Choose your current %1$s page template.', 'independent' ), $template_sname ),
				'options'  => array(
					'no-sidebar' => array(
						'alt' => esc_html__( 'No Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/1.png'
					),
					'right-sidebar' => array(
						'alt' => esc_html__( 'Right Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/2.png'
					),
					'left-sidebar' => array(
						'alt' => esc_html__( 'Left Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/3.png'
					),
					'both-sidebar' => array(
						'alt' => esc_html__( 'Both Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/4.png'
					)
				),
				'default'  => 'right-sidebar'
			),
			array(
				'id'       => $template.'-left-sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Left Sidebar', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Select widget area for showing %1$s page on left sidebar.', 'independent' ), $template_sname ),
				'data'     => 'sidebars',
				'required' 		=> array($template.'-page-template', '=', array( 'left-sidebar', 'both-sidebar' ))
			),
			array(
				'id'       => $template.'-right-sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Right Sidebar', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Select widget area for showing %1$s page on right sidebar.', 'independent' ), $template_sname ),
				'data'     => 'sidebars',
				'default'  => 'sidebar-1',
				'required' 		=> array($template.'-page-template', '=', array( 'right-sidebar', 'both-sidebar' ))
			),
			array(
				'id'       => $template.'-sidebar-sticky',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sidebar Sticky', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable sidebar sticky.', 'independent' ),
				'default'  => 1,
				'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
				'required' => array($template.'-page-template', '!=', 'no-sidebar')
			),
			array(
				'id'       => $template.'-page-hide-sidebar',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sidebar on Mobile', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to show or hide sidebar on mobile.', 'independent' ),
				'default'  => 1,
				'on'       => esc_html__( 'Show', 'independent' ),
				'off'      => esc_html__( 'Hide', 'independent' ),
				'required' => array($template.'-page-template', '!=', 'no-sidebar')
			),
			array(
				'id'       => $template.'-template-shortcode-model',
				'class'		=> 'template-block-shortcode-select',
				'type'     => 'image_select',
				 'title'    => sprintf( esc_html__( '%1$s Block Model', 'independent' ), $template_cname ),
				'desc'		=> sprintf( esc_html__( 'Choose block model and edit below.', 'independent' ), $template_sname ),
				'options'  => array(
					'1' => array(
						'alt' => esc_html__( 'Block 1', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block1.png'
					),
					'2' => array(
						'alt' => esc_html__( 'Block 2', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block2.png'
					),
					'3' => array(
						'alt' => esc_html__( 'Block 3', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block3.png'
					),
					'4' => array(
						'alt' => esc_html__( 'Block 4', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block4.png'
					),
					'5' => array(
						'alt' => esc_html__( 'Block 5', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block5.png'
					),
					'6' => array(
						'alt' => esc_html__( 'Block 6', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block6.png'
					),
					'7' => array(
						'alt' => esc_html__( 'Block 7', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block7.png'
					),
					'8' => array(
						'alt' => esc_html__( 'Block 8', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block8.png'
					),
					'9' => array(
						'alt' => esc_html__( 'Block 9', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block9.png'
					),
					'10' => array(
						'alt' => esc_html__( 'Block 10', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block10.png'
					),
					'11' => array(
						'alt' => esc_html__( 'Block 11', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block11.png'
					),
					'12' => array(
						'alt' => esc_html__( 'Block 12', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block12.png'
					),
					'13' => array(
						'alt' => esc_html__( 'Block 13', 'independent' ),
						'img' => get_template_directory_uri() . '/admin/assets/images/vc-block13.png'
					),
				),
				'default'  => '6'
			),
			array(
				'id'		=> $template.'-template-shortcode',
				'class'		=> 'template-block-shortcode-hook',
				'type' 		=> 'textarea',
				'title' 	=> sprintf( esc_html__( '%1$s Template Shortcode', 'independent' ), $template_cname ),
				'desc'		=> sprintf( esc_html__( 'This is field for choose %1$s template shortcode.', 'independent' ), $template_sname ),
				'default' 	=> '',
			),
		);
		
		return $template_array;
	}
	
	function independentThemeOptCPT( $field_name, $field_sname, $default ){
		$cpt_array = array(
			array(
				'id'       => 'cpt-'. esc_attr( $field_sname ) .'-slug',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Slug', 'independent' ), $field_name ),
				'desc'     => sprintf( esc_html__( 'Enter %1$s slug for register custom post type.', 'independent' ), $field_sname ),
				'default'  => $default['slug']
			),
			array(
				'id'       => 'cpt-'. esc_attr( $field_sname ) .'-category-slug',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Category Slug', 'independent' ), $field_name ),
				'desc'     => sprintf( esc_html__( 'Enter category slug for %1$s custom post type.', 'independent' ), $field_sname ),
				'default'  => $default['cat_slug']
			),
			array(
				'id'       => 'cpt-'. esc_attr( $field_sname ) .'-tag-slug',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Tag Slug', 'independent' ), $field_name ),
				'desc'     => sprintf( esc_html__( 'Enter %1$s slug for portfolio custom post type.', 'independent' ), $field_sname ),
				'default'  => $default['tag_slug']
			)
		);
		
		return $cpt_array;
	}
	
	function independentGetThemeTemplatesKey(){
		$independent_opt = $this->independent_options;
		return isset( $independent_opt['theme-templates'] ) ? $independent_opt['theme-templates'] : array();
	}
}