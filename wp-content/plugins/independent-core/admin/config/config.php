<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
	
	require_once( INDEPENDENT_CORE_DIR . 'admin/config/config-fun.php' );
	$acf = new IndependentConfigFun;

    // This is your option name where all the Redux data is stored.
    $opt_name = "independent_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */
	 
    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'independent' ),
        'page_title'           => __( 'Independent Theme Options', 'independent' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );


    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'independent' ), $v );
    }

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */
	//General Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'General', 'independent' ),
        'id'               => 'general',
        'desc'             => esc_html__( 'These are the general settings of independent theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home'
    ) );
	
	//General -> Layout
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Layout', 'independent' ),
        'id'         => 'general-layout',
        'desc'       => esc_html__( 'This is the setting for theme layouts', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'page-layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Page Layout', 'independent' ),
				'subtitle' => esc_html__( 'Choose page layout', 'independent' ),
				'options' => array(
					'boxed' => esc_html__( 'Boxed', 'independent' ),
					'wide'  => esc_html__( 'Wide', 'independent' )
				),
				'default'  => 'wide'
			),
			array(
                'id'			=> 'site-width',
                'type'			=> 'dimensions',
                'units'			=> array( 'px' ),
                'units_extended'=> 'false',
                'title'			=> esc_html__( 'Site Width', 'independent' ),
                'subtitle'		=> esc_html__( 'Set the site width here.', 'independent' ),
                'height'		=> false,
                'default'		=> array(
                    'width'	=> 1240,
                    'units'=> 'px'
                ),
				'required' 		=> array('page-layout', '!=', 'full')
            ),
			array(
                'id'       => 'page-content-padding',
                'type'     => 'spacing',
                'mode'     => 'padding',
                'all'      => false,
                'units'    => array( 'px' ),
                'units_extended'=> 'false',
                'title'    => esc_html__( 'Page Content Padding', 'independent' ),
                'subtitle' => esc_html__( 'Set the top/right/bottom/left padding of page content.', 'independent' ),
                'default'  => array(
                    'padding-top'    => '',
                    'padding-right'  => '',
                    'padding-bottom' => '',
                    'padding-left'   => ''
                )
            ),
		)
    ) );
	
	//General -> Loaders
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Loaders', 'independent' ),
        'id'         => 'general-loadres',
        'desc'       => esc_html__( 'This is the setting for Page Loader', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'page-loader',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Page Loader', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable Page Loader', 'independent' ),
				'options' => array(
					'yes' => esc_html__( 'Yes', 'independent' ),
					'no'  => esc_html__( 'No', 'independent' ),
				),
				'default'  => 'no'
			),
			array(
                'id'       => 'page-loader-img',
                'type'     => 'media',
				'library_filter'  => array('gif'),
                'url'      => true,
                'title'    => esc_html__( 'Page Loader Image', 'independent' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( 'Upload Page Loader Image', 'independent' ),
				'required' 		=> array('page-loader', '=', 'yes')
            ),
			array(
                'id'       => 'infinite-loader-img',
                'type'     => 'media',
				'library_filter'  => array('gif'),
                'url'      => true,
                'title'    => esc_html__( 'Infinite Scroll Image', 'independent' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( 'Upload Infinite Scroll Image', 'independent' ),
            ),
			array(
                'id'       => 'news-loader-img',
                'type'     => 'media',
				'library_filter'  => array('gif'),
                'url'      => true,
                'title'    => esc_html__( 'News Loader Image', 'independent' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( 'Upload news loader image.', 'independent' ),
            )
		)
    ) );
	
	//General -> Theme Logo
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo', 'independent' ),
        'id'         => 'general-logo',
        'desc'       => esc_html__( 'This is the setting for Theme Logo', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'logo',
                'type'     => 'media',
                'url'      => true,
				'preview'  => true,
                'title'    => esc_html__( 'Logo', 'independent' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( 'Upload theme logo', 'independent' ),
            ),
			array(
				'id'       => 'logo-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Logo Height (Optional)', 'independent' ),
				'desc'     => esc_html__( 'Mention here logo max height (Optional). Ex: 80', 'independent' ),
				'default'  => array(
					'height'  => '',
					'units'=> 'px'
				),
			),
			array(
				'id'       => 'sticky-logo',
				'type'     => 'media',
				'url'      => true,
				'preview'  => true,
				'title'    => esc_html__( 'Sticky Logo', 'independent' ),
				'compiler' => 'true',
				'subtitle' => esc_html__( 'Upload theme sticky logo', 'independent' ),
			),
			array(
				'id'       => 'sticky-logo-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Sticky Logo Height (Optional)', 'independent' ),
				'desc'     => esc_html__( 'Mention here sticky logo max height (Optional). Ex: 80', 'independent' ),
				'default'  => array(
					'height'  => '',
					'units'=> 'px'
				),
			),
			array(
				'id'       => 'mobile-logo',
				'type'     => 'media',
				'url'      => true,
				'preview'  => true,
				'title'    => esc_html__( 'Mobile Logo', 'independent' ),
				'compiler' => 'true',
				'subtitle' => esc_html__( 'Upload theme mobile logo', 'independent' ),
			),
			array(
				'id'       => 'mobile-logo-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Mobile Logo Height (Optional)', 'independent' ),
				'desc'     => esc_html__( 'Mention here mobile logo max height (Optional). Ex: 80', 'independent' ),
				'default'  => array(
					'height'  => '',
					'units'=> 'px'
				),
			)
		)
    ) );
	
	//General -> API's
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'API', 'independent' ),
        'id'         => 'general-api',
        'desc'       => esc_html__( 'This is the setting for API', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'mailchimp-api',
				'type'     => 'password',
				'title'    => esc_html__( 'Mailchimp API Key', 'independent' ),
				'subtitle' => esc_html__( 'Place here your registered mailchimp API key.', 'independent' ),
			),
			array(
				'id'       => 'google-api',
				'type'     => 'password',
				'title'    => esc_html__( 'Google Map API Key', 'independent' ),
				'subtitle' => esc_html__( 'Place here your registered google map API key.', 'independent' ),
			),
			array(
				'id'       => 'open-weather-api',
				'type'     => 'password',
				'title'    => esc_html__( 'Open Weather Map API Key', 'independent' ),
				'subtitle' => esc_html__( 'Place here your registered open weather map API key.', 'independent' ),
				'description' => esc_html__( 'Register open weather map website and then create api. Check with https://openweathermap.org/ ', 'independent' ),
			)			
		)
    ) );
	
	//General -> Comments
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Comments', 'independent' ),
        'id'         => 'general-comments',
        'desc'       => esc_html__( 'This is the setting for comments', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'comments-type',
                'type'     => 'select',
                'title'    => esc_html__( 'Comments Type', 'independent' ),
				'subtitle' => esc_html__( 'This option will be showing comment like facebook or default wordpress.', 'independent' ),
                'options'  => array(
                    'wp' => esc_html__( 'WordPress Comment', 'independent' ),
                    'fb' => esc_html__( 'Facebook Comment', 'independent' ),
                ),
                'default'  => 'wp'
            ),
			array(
                'id'       => 'comments-like',
                'type'     => 'switch',
                'title'    => esc_html__( 'Comments Like', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to show or hide comments likes to single post comments.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
				'required' 		=> array('comments-type', '=', 'wp')
            ),
			array(
                'id'       => 'comments-share',
                'type'     => 'switch',
                'title'    => esc_html__( 'Comments Share', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to show or hide comments share to single post comments.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
				'required' 		=> array('comments-type', '=', 'wp')
            ),
			array(
				'id'       => 'fb-developer-key',
				'type'     => 'password',
				'title'    => esc_html__( 'Facebook Developer API', 'independent' ),
				'subtitle' => esc_html__( 'Enter facebook developer API key.', 'independent' ),
				'required' 		=> array('comments-type', '=', 'fb')
			),
			array(
                'id'       => 'fb-comments-number',
                'type'     => 'text',
                'title'    => esc_html__( 'Number of Comments', 'independent' ),
                'subtitle'     => esc_html__( 'Enter number of comments to display.', 'independent' ),
                'default'  => '',
				'required' 		=> array('comments-type', '=', 'fb')
            ),
			array(
				'id'       => 'fb-comments-width',
				'type'     => 'dimensions',
				'units'    => array( 'px' ),
				'units_extended'=> 'false',
				'height'    => false,
				'title'    => esc_html__( 'Facebook Comments Width', 'independent' ),
				'subtitle'     => esc_html__( 'Increase or decrease facebook comments wrapper width.', 'independent' ),
				'default'  => array(
					'width'  => 500,
					'units'=> 'px'
				),
				'required' 		=> array('comments-type', '=', 'fb')
			),
		)
    ) );
	
	//General -> Smooth Scroll
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Smooth Scroll', 'independent' ),
        'id'         => 'general-smooth',
        'desc'       => esc_html__( 'This is the setting for page smooth scroll', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'smooth-opt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Smooth Scroll Option', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to append smooth scroll js to website.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' )
            ),
			array(
                'id'       => 'scroll-time',
                'type'     => 'text',
                'title'    => esc_html__( 'Scroll Time', 'independent' ),
                'subtitle'     => esc_html__( 'Enter smooth scroll time in milliseconds. Example 600', 'independent' ),
                'default'  => '600',
				'required' 		=> array('smooth-opt', '=', '1')
            ),
			array(
                'id'       => 'scroll-distance',
                'type'     => 'text',
                'title'    => esc_html__( 'Scroll Distance', 'independent' ),
                'subtitle'     => esc_html__( 'Enter smooth scroll distance in value. Example 30', 'independent' ),
                'default'  => '30',
				'required' 		=> array('smooth-opt', '=', '1')
            )
		)
    ) );
	
	//General -> Media Settings
	Redux::setSection( $opt_name, array(
		'title'      => esc_html__( 'Media Settings', 'independent' ),
		'id'         => 'general-media',
		'desc'       => esc_html__( 'This is the setting for media sizes', 'independent' ),
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'independent_grid_large',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid Large Size', 'independent'),
				'desc'       => esc_html__( 'This image used in gallery large grid. If you don\'t want this size means just leave this empty. Default 440 x 260', 'independent' ),
				'default'  => array(
					'width'   => '440', 
					'height'  => '260'
				),
			),
			array(
				'id'       => 'independent_grid_medium',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid Medium Size', 'independent'),
				'desc'       => esc_html__( 'This image used in gallery medium grid. If you don\'t want this size means just leave this empty. Default 390 x 231', 'independent' ),
				'default'  => array(
					'width'   => '390', 
					'height'  => '231'
				),
			),
			array(
				'id'       => 'independent_grid_small',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid Small Size', 'independent'),
				'desc'       => esc_html__( 'This image used in gallery small grid. If you don\'t want this size means just leave this empty. Default 220 x 130', 'independent' ),
				'default'  => array(
					'width'   => '220', 
					'height'  => '130'
				),
			),
			array(
				'id'       => 'independent_team_medium',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Team Medium Size', 'independent'),
				'desc'       => esc_html__( 'This image used in team shorcode. If you don\'t want this size means just leave this empty. Default 300 x 300', 'independent' ),
				'default'  => array(
					'width'   => '300', 
					'height'  => '300'
				),
			),
			array(
				'id'   => 'media_blocks_info',
				'type' => 'info',
				'desc' => esc_html__( 'Following image crop size settings only for news blocks images.', 'independent' )
			),
			array(
				'id'       => 'independent_grid_1',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid 1 Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 544 x 316', 'independent' ),
				'default'  => array(
					'width'   => '544', 
					'height'  => '316'
				),
			),
			array(
				'id'       => 'independent_grid_2',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid 2 Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 356 x 240', 'independent' ),
				'default'  => array(
					'width'   => '356', 
					'height'  => '240'
				),
			),
			array(
				'id'       => 'independent_grid_3',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid 3 Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 356 x 206', 'independent' ),
				'default'  => array(
					'width'   => '356', 
					'height'  => '206'
				),
			),
			array(
				'id'       => 'independent_grid_4',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Grid 4 Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 130 x 80', 'independent' ),
				'default'  => array(
					'width'   => '130', 
					'height'  => '80'
				),
			),
			array(
				'id'   => 'media_banner_info',
				'type' => 'info',
				'desc' => esc_html__( 'Following image crop size settings only for news banner images.', 'independent' )
			),
			array(
				'id'       => 'independent_banner_67x100',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 67%x100% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 827 x 460', 'independent' ),
				'default'  => array(
					'width'   => '827', 
					'height'  => '460'
				),
			),
			array(
				'id'       => 'independent_banner_50x50',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 50%x50% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 620 x 230', 'independent' ),
				'default'  => array(
					'width'   => '620', 
					'height'  => '230'
				),
			),
			array(
				'id'       => 'independent_banner_33x50',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 33%x50% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 414 x 230', 'independent' ),
				'default'  => array(
					'width'   => '414', 
					'height'  => '230'
				),
			),
			array(
				'id'       => 'independent_banner_25x50',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 25%x50% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 310 x 230', 'independent' ),
				'default'  => array(
					'width'   => '310', 
					'height'  => '230'
				),
			),
			array(
				'id'       => 'independent_banner_50x100',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 50%x100% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 620 x 460', 'independent' ),
				'default'  => array(
					'width'   => '620', 
					'height'  => '460'
				),
			),
			array(
				'id'       => 'independent_banner_25x100',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 25%x100% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 310 x 460', 'independent' ),
				'default'  => array(
					'width'   => '310', 
					'height'  => '460'
				),
			),
			array(
				'id'       => 'independent_banner_33x100',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 33%x100% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 620 x 460', 'independent' ),
				'default'  => array(
					'width'   => '620', 
					'height'  => '460'
				),
			),
			array(
				'id'       => 'independent_banner_100x100',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 100%x100% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 1240 x 460', 'independent' ),
				'default'  => array(
					'width'   => '1240', 
					'height'  => '460'
				),
			),
			array(
				'id'       => 'independent_banner_50x33',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 50%x33% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 620 x 154', 'independent' ),
				'default'  => array(
					'width'   => '620', 
					'height'  => '154'
				),
			),
			array(
				'id'       => 'independent_banner_25x66',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 25%x66% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 310 x 307', 'independent' ),
				'default'  => array(
					'width'   => '310', 
					'height'  => '307'
				),
			),
			array(
				'id'       => 'independent_banner_25x33',
				'type'     => 'dimensions',
				'title'    => esc_html__('Independent Banner 25%x33% Size', 'independent'),
				'desc'       => esc_html__( 'This image used in news blocks shorcode. If you don\'t want this size means just leave this empty. Default 310 x 154', 'independent' ),
				'default'  => array(
					'width'   => '310', 
					'height'  => '154'
				),
			)
		)
	) );
	
	//General -> News
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'News', 'independent' ),
        'id'         => 'general-news',
        'desc'       => esc_html__( 'This is the setting for news', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'duplicate-news',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Duplicate News', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable duplicate news on page first loading. But this options not for ajax news filters.', 'independent' ),
				'options' => array(
					'1' => esc_html__( 'Enable', 'independent' ),
					'0'  => esc_html__( 'Disable', 'independent' ),
				),
				'default'  => '0'
			),
			array(
				'id'		=>'news-promotion-text',
				'type' 		=> 'text',
				'title' 	=> esc_html__( 'Promotion Text', 'independent' ), 
				'desc'		=> esc_html__( 'This is the promotion text. it will show the promotion ribbon when active promotion option on post.', 'independent' ),
				'default' 	=> '',
			),
			array(
				'id'       => 'news-lazy-load',
				'type'     => 'button_set',
				'title'    => esc_html__( 'News Lazy Load', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable news images lazy load.', 'independent' ),
				'options' => array(
					'1' => esc_html__( 'Enable', 'independent' ),
					'0'  => esc_html__( 'Disable', 'independent' ),
				),
				'default'  => '0'
			),
			array(
                'id'       => 'news-lazy-load-img',
                'type'     => 'media',
                'url'      => true,
				'preview'  => true,
                'title'    => esc_html__( 'Lazy Load Preview Image', 'independent' ),
				'desc'		=> esc_html__( 'Choose preview image for lazy load and this is must. If you did not choose any image default preview image will be load from base64.', 'independent' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( 'Upload preview lazy load image', 'independent' ),
				'required' 		=> array('news-lazy-load', '=', '1')
            )
		)
    ) );
	
	//General -> Demo Style
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Demo Style', 'independent' ),
        'id'         => 'general-demostyle',
        'desc'       => esc_html__( 'This is the setting for demo style code.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'demo-id-style',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Demo Style', 'independent' ),
                'subtitle' => esc_html__( 'Select demo style to overwrite existing style. If you choose anything here(except none) it will overwrite the style of which demo you imported.', 'independent' ),
                'options'  => array(
					'none'		=> esc_html__( 'None', 'independent' ),
					'demo-main'		=> esc_html__( 'Main Demo', 'independent' ),
					'demo-technology'	=> esc_html__( 'Technology', 'independent' ),
                    'demo-travel'	=> esc_html__( 'Travel', 'independent' ),
					'demo-sports'	=> esc_html__( 'Sports', 'independent' ),
					'demo-general-news'	=> esc_html__( 'General News', 'independent' ),
					'demo-food'	=> esc_html__( 'Food', 'independent' ),
					'demo-rtl'	=> esc_html__( 'RTL Demo', 'independent' ),
					'demo-daily-news'	=> esc_html__( 'Daily News', 'independent' ),
					'demo-organic'	=> esc_html__( 'Organic', 'independent' ),
					'demo-lifestyle'	=> esc_html__( 'Lifestyle', 'independent' ),
					'demo-business'	=> esc_html__( 'Business', 'independent' ),
					'demo-fitness'	=> esc_html__( 'Fitness', 'independent' ),
					'demo-fruits'	=> esc_html__( 'Fruits', 'independent' ),
					'demo-medical'	=> esc_html__( 'Medical', 'independent' ),
					'demo-weets'	=> esc_html__( 'Weets', 'independent' ),
					'demo-citynews'	=> esc_html__( 'City News', 'independent' ),
					'demo-game3'	=> esc_html__( 'Game Blog', 'independent' ),
					'demo-cryptocurrency'	=> esc_html__( 'Crypto Currency', 'independent' ),
					'demo-independent-times'	=> esc_html__( 'Independent Times', 'independent' ),
					'demo-photography-blog'	=> esc_html__( 'Photography Demo', 'independent' )
                ),
                'default'  => 'none'
            ),
		)
    ) );
	
	//General -> RTL
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'RTL', 'independent' ),
        'id'         => 'general-rtl',
        'desc'       => esc_html__( 'This is the setting for theme view RTL', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'rtl',
                'type'     => 'switch',
                'title'    => esc_html__( 'RTL', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable RTL to change theme right to left view.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
		)
    ) );
	
	//ADS
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'ADS', 'independent' ),
        'id'               => 'ads',
        'desc'             => esc_html__( 'These are the ads settings of independent Theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-television'
    ) );
	//ADS -> Header Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Ads', 'independent' ),
        'id'         => 'ads-header',
        'desc'       => esc_html__( 'These are header ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('header')
    ) );
	//ADS -> Footer Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Ads', 'independent' ),
        'id'         => 'ads-footer',
        'desc'       => esc_html__( 'These are footer ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('footer')
    ) );
	//ADS -> Sidebar Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Sidebar Ads', 'independent' ),
        'id'         => 'ads-sidebar',
        'desc'       => esc_html__( 'These are sidebar ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('sidebar')
    ) );
	//ADS -> Artical Top Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Artical Top Ads', 'independent' ),
        'id'         => 'ads-artical-top',
        'desc'       => esc_html__( 'These are artical top ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('artical-top')
    ) );
	//ADS -> Artical Inline Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Artical Inline Ads', 'independent' ),
        'id'         => 'ads-artical-inline',
        'desc'       => esc_html__( 'These are artical inline ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('artical-inline')
    ) );
	//ADS -> Artical Bottom Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Artical Bottom Ads', 'independent' ),
        'id'         => 'ads-artical-bottom',
        'desc'       => esc_html__( 'These are artical bottom ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('artical-bottom')
    ) );
	//ADS -> Custom1 Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom1 Ads', 'independent' ),
        'id'         => 'ads-custom1',
        'desc'       => esc_html__( 'These are custom1 ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('custom1')
    ) );
	//ADS -> Custom2 Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom2 Ads', 'independent' ),
        'id'         => 'ads-custom2',
        'desc'       => esc_html__( 'These are custom2 ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('custom2')
    ) );
	//ADS -> Custom3 Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom3 Ads', 'independent' ),
        'id'         => 'ads-custom3',
        'desc'       => esc_html__( 'These are custom3 ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('custom3')
    ) );
	//ADS -> Custom4 Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom4 Ads', 'independent' ),
        'id'         => 'ads-custom4',
        'desc'       => esc_html__( 'These are custom4 ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('custom4')
    ) );
	//ADS -> Custom5 Ads
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom5 Ads', 'independent' ),
        'id'         => 'ads-custom5',
        'desc'       => esc_html__( 'These are custom5 ads settings of independent Theme', 'independent' ),
        'subsection' => true,
        'fields'     => $acf->themeAdsFields('custom5')
    ) );
	
	//Skin Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Skin', 'independent' ),
        'id'               => 'skin',
        'desc'             => esc_html__( 'These are theme skin/color options', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-paint-brush'
    ) );
	
	//Skin -> Theme Skin
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Theme Skin', 'independent' ),
        'id'         => 'skin-general',
        'desc'       => esc_html__( 'This is the setting for theme skin', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'theme-color',
                'type'     => 'color',
                'title'    => esc_html__( 'Theme Color', 'independent' ),
				'subtitle' => esc_html__( 'Choose theme color.', 'independent' ),
				'validate' => 'color',
                'default'  => '#0172ff'
            ),
			array(
                'id'       => 'theme-link-color',
                'type'     => 'link_color',
                'title'    => esc_html__( 'General Links Color', 'independent' ),
                'subtitle' => esc_html__( 'Choose general link color for theme.', 'independent' ),
                'default'  => array(
                    'regular' => '#282828',
                    'hover'   => '#0172ff',
                    'active'  => '#0172ff',
                )
            ),
		)
    ) );
	
	//Skin -> Body Background
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Body Background', 'independent' ),
        'id'         => 'skin-body',
        'desc'       => esc_html__( 'This is the setting for theme body background.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(         
				'id'       => 'body-background',
				'type'     => 'background',
				'title'    => __( 'Body Background Settings', 'independent'),
				'subtitle' => __( 'This is settings for body background with image, color, etc.', 'independent' ),
				'default'  => array(
					'background-color' => '#f6f6f6',
				)
			),
			array(         
				'id'       => 'body-content-background',
				'type'     => 'background',
				'title'    => __( 'Body Content Background Settings', 'independent'),
				'subtitle' => __( 'This is settings for body content background with image, color, etc.', 'independent' ),
				'default'  => array(
					'background-color' => '#ffffff',
				)
			)
		)
    ) );
	
	//Typography Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Typography', 'independent' ),
        'id'               => 'typography',
        'desc'             => esc_html__( 'These are the theme typograhpy options', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-font'
    ) );
	
	//Typography -> Theme General Typography
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General Typography', 'independent' ),
        'id'         => 'typography-general',
        'desc'       => esc_html__( 'This is the setting for theme general typograhpy', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'body-typography',
                'type'     => 'typography',
                'title'    => __( 'Body Fonts', 'independent' ),
                'subtitle' => __( 'Specify the body font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '#818181',
                    'font-size'   => '14px',
                    'font-family' => 'Poppins',
                    'font-weight' => '400',
					'line-height' => '28px'
                ),
            ),
			array(
                'id'       => 'h1-typography',
                'type'     => 'typography',
                'title'    => __( 'H1 Fonts', 'independent' ),
                'subtitle' => __( 'Specify the h1 font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '34px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '42px'
                ),
            ),
			array(
                'id'       => 'h2-typography',
                'type'     => 'typography',
                'title'    => __( 'H2 Fonts', 'independent' ),
                'subtitle' => __( 'Specify the h2 font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '28px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '36px'
                ),
            ),
			array(
                'id'       => 'h3-typography',
                'type'     => 'typography',
                'title'    => __( 'H3 Fonts', 'independent' ),
                'subtitle' => __( 'Specify the h3 font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '24px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '32px'
                ),
            ),
			array(
                'id'       => 'h4-typography',
                'type'     => 'typography',
                'title'    => __( 'H4 Fonts', 'independent' ),
                'subtitle' => __( 'Specify the h4 font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '20px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '29px'
                ),
            ),
			array(
                'id'       => 'h5-typography',
                'type'     => 'typography',
                'title'    => __( 'H5 Fonts', 'independent' ),
                'subtitle' => __( 'Specify the h5 font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '18px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '26px'
                ),
            ),
			array(
                'id'       => 'h6-typography',
                'type'     => 'typography',
                'title'    => __( 'H6 Fonts', 'independent' ),
                'subtitle' => __( 'Specify the h6 font properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '16px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '25px'
                ),
            ),
		)
    ) );
	
	//Typography -> Theme Widgets Typography
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Widgets Typography', 'independent' ),
        'id'         => 'typography-widgets',
        'desc'       => esc_html__( 'This is the setting for theme widgets typograhpy', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'widgets-title',
                'type'     => 'typography',
                'title'    => __( 'Widgets Title Typography', 'independent' ),
                'subtitle' => __( 'Specify the widget title typography properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '18px',
                    'font-family' => 'Poppins',
                    'font-weight' => '700',
					'line-height' => '28px'
                ),
            ),
			array(
                'id'       => 'widgets-content',
                'type'     => 'typography',
                'title'    => __( 'Widgets Content Typography', 'independent' ),
                'subtitle' => __( 'Specify the widget content typography properties.', 'independent' ),
                'google'   => true,
				'letter-spacing'=> true,
				'line-height'=> true,
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '14px',
                    'font-family' => 'Poppins',
                    'font-weight' => '400',
					'line-height' => '28px'
                ),
            ),
		)
    ) );
	
	//Header Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Header', 'independent' ),
        'id'               => 'header',
        'desc'             => esc_html__( 'These are header general settings of independent theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-credit-card-alt'
    ) );
	
	//Header -> Header General
	$header_mainmenu_skin = $acf->themeSkinSettings('main-menu');
	$secondary_space_skin = $acf->themeSkinSettings('secondary-space', array( 'line_height' => true )); 
	$header_dropdown_skin = $acf->themeSkinSettings('dropdown-menu', array( 'line_height' => true ));
	$header_top_slide_skin = $acf->themeSkinSettings('top-sliding', array( 'line_height' => true ));
	
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header General', 'independent' ),
        'id'         => 'header-general',
        'desc'       => esc_html__( 'This is the setting for Header General', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'header-layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Header Layout', 'independent' ),
				'subtitle' => esc_html__( 'Choose header layout boxed or wide.', 'independent' ),
				'options' => array(
					'boxed' => esc_html__( 'Boxed', 'independent' ),
					'wide'  => esc_html__( 'Wide', 'independent' ),
				),
				'default'  => 'wide',
				'required' 		=> array('page-layout', '=', 'wide')
			),
			array(
                'id'       => 'header-type',
                'type'     => 'select',
                'title'    => esc_html__( 'Header Type', 'independent' ),
                'subtitle' => esc_html__( 'Select header type for matching your site.', 'independent' ),
                'options'  => array(
					'default'		=> esc_html__( 'Default', 'independent' ),
					'left-sticky'	=> esc_html__( 'Left Sticky', 'independent' ),
                    'right-sticky'	=> esc_html__( 'Right Stikcy', 'independent' ),
                ),
                'default'  => 'default'
            ),
			array(         
				'id'       => 'header-background',
				'type'     => 'background',
				'title'    => __( 'Header Background Settings', 'independent'),
				'subtitle' => __( 'This is settings for header background with image, color, etc.', 'independent' ),
				'default'  => array(
					'background-color' => '',
				),
				'required' 		=> array('header-type', '=', 'default')
			),
			array(
				'id'      => 'header-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Header Items', 'independent' ),
				'desc'    => esc_html__( 'Needed items for header, drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'Normal' => array(
						'header-logo'	=> esc_html__( 'Logo Section', 'independent' ),
						'header-nav'	=> esc_html__( 'Nav Bar', 'independent' )
					),
					'Sticky' => array(
						
					),
					'disabled' => array(
						'header-topbar'	=> esc_html__( 'Top Bar', 'independent' ),
					)
				),
				'required' 		=> array('header-type', '=', 'default')
			),
			array(
                'id'       => 'header-fields-custom-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Custom Fields Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for header custom fields.', 'independent' ),
                'indent'   => true, 
            ),
			array(
				'id'		=>'header-phone-text',
				'type' 		=> 'textarea',
				'title' 	=> esc_html__( 'Phone Number Custom Text', 'independent' ), 
				'desc'		=> esc_html__( 'This is the phone number field, you can assign here any custom text. Few HTML allowed here.', 'independent' ),
				'default' 	=> '',
			),
			array(
				'id'		=>'header-address-text',
				'type' 		=> 'textarea',
				'title' 	=> esc_html__( 'Address Custom Text', 'independent' ), 
				'desc'		=> esc_html__( 'This is the address field, you can assign here any custom text. Few HTML allowed here.', 'independent' ),
				'default' 	=> '',
			),
			array(
				'id'		=>'header-email-text',
				'type' 		=> 'textarea',
				'title' 	=> esc_html__( 'Email Custom Text', 'independent' ), 
				'desc'		=> esc_html__( 'This is the email field, you can assign here any email id. Example companyname@yourdomain.com', 'independent' ),
				'default' 	=> '',
			),
			array(
                'id'     => 'header-fields-custom-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-slider-setting-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Slider Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for header slider.', 'independent' ),
                'indent'   => true, 
				'required' 		=> array('header-type', '=', 'default')
            ),
			array(
                'id'       => 'header-slider-position',
                'type'     => 'select',
                'title'    => esc_html__( 'Header Slider Position', 'independent' ),
                'subtitle' => esc_html__( 'Select header slider position matching your page.', 'independent' ),
                'options'  => array(
					'bottom'		=> esc_html__( 'Below Header', 'independent' ),
					'top'	=> esc_html__( 'Above Header', 'independent' ),
                    'none'	=> esc_html__( 'None', 'independent' ),
                ),
                'default'  => 'none'
            ),
			array(
                'id'     => 'header-slider-setting-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-sticky-setting-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Sticky/Transparent Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for sticky part.', 'independent' ),
                'indent'   => true, 
				'required' 		=> array('header-type', '=', 'default')
            ),
			array(
                'id'       => 'header-absolute',
                'type'     => 'switch',
                'title'    => esc_html__( 'Header Absolute', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable header absolute option to show transparent header for page.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'sticky-part',
                'type'     => 'switch',
                'title'    => esc_html__( 'Header Sticky Part', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable stciky part to sticky which items are placed in Sticky Part at Header Items.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'sticky-part-scrollup',
                'type'     => 'switch',
                'title'    => esc_html__( 'Sticky Scroll Up', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable stciky part to sticky only scroll up. This also only sticky which items are placed in Sticky Part at Header Items.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
				'required' 		=> array('sticky-part', '!=', 0)
            ),
			array(
                'id'     => 'header-sticky-setting-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-mainmenu-setting-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Main Menu Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for mainmenu.', 'independent' ),
                'indent'   => true, 
				'required' 		=> array('header-type', '=', 'default')
            ),
			array(
                'id'       => 'mainmenu-menutype',
                'type'     => 'select',
                'title'    => esc_html__( 'Menu Type', 'independent' ),
                'options'  => array(
                    'advanced' => esc_html__( 'Advanced Menu', 'independent' ),
                    'normal' => esc_html__( 'Normal Menu', 'independent' ),
                ),
                'default'  => 'advanced'
            ),
			array(
                'id'       => 'menu-tag',
                'type'     => 'switch',
                'title'    => esc_html__( 'Menu Tag', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable menu tag for menu items like Hot, Trend, New.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
				'required' 		=> array('mainmenu-menutype', '=', 'advanced')
            ),
			array(
				'id'       => 'menu-tag-hot-text',
				'type'     => 'text',
				'title'    => esc_html__('Hot Menu Tag Text', 'independent'),
				'subtitle' => esc_html__('Set this text to show hot menu tag.', 'independent'),
				'default'  => esc_html__( 'Hot', 'independent' ),
				'required' 		=> array('menu-tag', '!=', 0)
			),
			array(
				'id'       => 'menu-tag-hot-bg',
				'type'     => 'color',
				'title'    => esc_html__('Hot Menu Tag Background', 'independent'),
				'subtitle' => esc_html__('Set hot menu tag background color.', 'independent'),
				'default'  => '#dd2525',
				'validate' => 'color',
				'required' 		=> array('menu-tag', '!=', 0)
			),
			array(
				'id'       => 'menu-tag-new-text',
				'type'     => 'text',
				'title'    => esc_html__('New Menu Tag Text', 'independent'),
				'subtitle' => esc_html__('Set this text to show new menu tag.', 'independent'),
				'default'  => esc_html__( 'New', 'independent' ),
				'required' 		=> array('menu-tag', '!=', 0)
			),
			array(
				'id'       => 'menu-tag-new-bg',
				'type'     => 'color',
				'title'    => esc_html__('New Menu Tag Background', 'independent'),
				'subtitle' => esc_html__('Set new menu tag background color.', 'independent'),
				'default'  => '#7100e2',
				'validate' => 'color',
				'required' 		=> array('menu-tag', '!=', 0)
			),
			array(
				'id'       => 'menu-tag-trend-text',
				'type'     => 'text',
				'title'    => esc_html__('Trend Menu Tag Text', 'independent'),
				'subtitle' => esc_html__('Set this text to show trend menu tag.', 'independent'),
				'default'  => esc_html__( 'Trend', 'independent' ),
				'required' 		=> array('menu-tag', '!=', 0)
			),
			array(
				'id'       => 'menu-tag-trend-bg',
				'type'     => 'color',
				'title'    => esc_html__('Trend Menu Tag Background', 'independent'),
				'subtitle' => esc_html__('Set trend menu tag background color.', 'independent'),
				'default'  => '#0172ff',
				'validate' => 'color',
				'required' 		=> array('menu-tag', '!=', 0)
			),
			array(
                'id'     => 'header-mainmenu-setting-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-mainmenu-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Main Menu Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for mainmenu. Here you can set mainmenu font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$header_mainmenu_skin[0],
			array(
				'id'       => 'header-mainmenu-bg-color',
				'type'     => 'color',
				'title'    => __('Main Menu Background Color', 'independent'), 
				'subtitle' => __('Pick a background color for main menu.', 'independent'),
				'validate' => 'color',
			),
			array(
				'id'       => 'header-mainmenu-bg-hcolor',
				'type'     => 'color',
				'title'    => __('Main Menu Background Hover Color', 'independent'), 
				'subtitle' => __('Pick a background hover color for main menu.', 'independent'),
				'validate' => 'color',
			),
			array(
                'id'     => 'header-mainmenu-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'secondary-menu-setting-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Secondary Menu Space Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for secondary.', 'independent' ),
                'indent'   => true, 
				'required' 		=> array('header-type', '=', 'default')
            ),
			array(
                'id'       => 'secondary-menu',
                'type'     => 'switch',
                'title'    => esc_html__( 'Secondary Menu', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable secondary menu.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'secondary-menu-type',
                'type'     => 'select',
                'title'    => esc_html__( 'Secondary Menu Type', 'independent' ),
                'options'  => array(
                    'left-push'		=> esc_html__( 'Left Push', 'independent' ),
                    'left-overlay'	=> esc_html__( 'Left Overlay', 'independent' ),
					'right-push'		=> esc_html__( 'Right Push', 'independent' ),
                    'right-overlay'	=> esc_html__( 'Right Overlay', 'independent' ),
					'full-overlay'	=> esc_html__( 'Full Page Overlay', 'independent' ),
                ),
                'default'  => 'right-overlay',
				'required' 		=> array('secondary-menu', '!=', 0)
            ),
			array(
				'id'       => 'secondary-menu-space-width',
				'type'     => 'dimensions',
				'units'		=> array( 'px' ),
				'units_extended'=> 'false',
				'height'    => false,
				'title'    => esc_html__( 'Secondary Menu Space Width', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease secondary menu space width. this options only use if you enable secondary menu.', 'independent' ),
				'default'  => array(
					'width'  => 350,
					'units'=> 'px'
				),
				'required' 		=> array(
					array( 'secondary-menu', '!=', 0),
					array( 'secondary-menu-type', '!=', 'full-overlay')
				)
			),
			array(
                'id'     => 'secondary-menu-setting-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-secondary-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Secondary Menu Space Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for secondary menu space. Here you can set secondary menu space font color, link color, etc..', 'independent' ),
                'indent'   => true, 
				'required' 		=> array(
					array( 'header-type', '=', 'default' ),
					array( 'secondary-menu', '=', 1 )
				)
            ),
			$secondary_space_skin[0],
			$secondary_space_skin[2],
			$secondary_space_skin[3],
			$secondary_space_skin[4],
			array(         
				'id'       => 'secondary-space-background',
				'type'     => 'background',
				'title'    => __('Secondary Space Background', 'independent'),
				'subtitle' => __('Secondary space background with image, color, etc.', 'independent'),
				'default'  => array(
					'background-color' => '',
				)
			),
			array(
                'id'     => 'header-secondary-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-dropdown-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Dropdown Menu Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for dropdown menu. Here you can set dropdown menu font color, link color, etc..', 'independent' ),
                'indent'   => true, 
				'required' 		=> array('header-type', '=', 'default')
            ),
			$header_dropdown_skin[0],
			$header_dropdown_skin[1],
			$header_dropdown_skin[2],
			$header_dropdown_skin[3],
			array(
                'id'     => 'header-dropdown-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-top-sliding-switch',
                'type'     => 'switch',
                'title'    => esc_html__( 'Top Sliding Bar Enable', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable top sliding bar. Here you can show you sidebars width column based.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'header-top-sliding-device',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Show on Devices', 'independent' ),
                'desc'     => esc_html__( 'Enable or disable top sliding bar for mobile, tab or desktop. This option from big devices. If desktop not enable and tab enable means it\'s hide sliding bar all the devices.', 'independent' ),
                'multi'    => true,
                'options'  => array(
                    'desktop' => 'Desktop',
                    'tab' => 'Tablet',
                    'mobile' => 'Mobile'
                ),
                'default'  => array( 'desktop', 'tab' ),
				'required' 		=> array('header-top-sliding-switch', '=', 1 )
            ),
			array(
                'id'       => 'header-top-slide-settings-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Top Slide Settings', 'independent' ),
                'indent'   => true, 
				'required' 		=> array( 
					array('header-type', '=', 'default' ),
					array('header-top-sliding-switch', '=', 1 )
				)
            ),
			array(
                'id'       => 'header-top-sliding-cols',
                'type'     => 'select',
                'title'    => esc_html__( 'Secondary Menu Type', 'independent' ),
                'options'  => array(
                    '3'		=> esc_html__( '4 Columns', 'independent' ),
                    '4'		=> esc_html__( '3 Columns', 'independent' ),
					'6'		=> esc_html__( '2 Columns', 'independent' ),
                    '12'	=> esc_html__( '1 Column', 'independent' ),
                ),
                'default'  => '3'
            ),
			array(
                'id'       => 'header-top-sliding-sidebar-1',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose First Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing first column of top sliding bar.', 'independent' ),
                'data'     => 'sidebars',
				'required' 		=> array('header-top-sliding-cols', '<=', '12')
            ),
			array(
                'id'       => 'header-top-sliding-sidebar-2',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Second Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing second column of top sliding bar.', 'independent' ),
                'data'     => 'sidebars',
				'required' 		=> array('header-top-sliding-cols', '<=', '6')
            ),
			array(
                'id'       => 'header-top-sliding-sidebar-3',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Third Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing third column of top sliding bar.', 'independent' ),
                'data'     => 'sidebars',
				'required' 		=> array('header-top-sliding-cols', '<=', '4')
            ),
			array(
                'id'       => 'header-top-sliding-sidebar-4',
                'type'     => 'select',
                'title'    => esc_html__( 'Choose Fourth Column', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing fourth column of top sliding bar.', 'independent' ),
                'data'     => 'sidebars',
				'required' 		=> array('header-top-sliding-cols', '<=', '3')
            ),
			array(
                'id'     => 'header-top-slide-settings-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-top-sliding-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Top Sliding Bar Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for header top sliding bar. Here you can set top sliding bar font color, link color, etc..', 'independent' ),
                'indent'   => true, 
				'required' 		=> array( 
					array('header-type', '=', 'default' ),
					array('header-top-sliding-switch', '=', 1 )
				)
            ),
			$header_top_slide_skin[0],
			$header_top_slide_skin[1],
			$header_top_slide_skin[2],
			$header_top_slide_skin[3],
			$header_top_slide_skin[4],
			array(
                'id'     => 'header-top-sliding-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'search-toggle-form',
                'type'     => 'select',
                'title'    => esc_html__( 'Toggle Search Modal', 'independent' ),
				'subtitle' => esc_html__( 'Select serach box toggle modal.', 'independent' ),
                'options'  => array(
                    '1' => esc_html__( 'Full Screen Search', 'independent' ),
                    '2' => esc_html__( 'Text Box Toggle Search', 'independent' ),
					'3' => esc_html__( 'Full Bar Toggle Search', 'independent' ),
					'4' => esc_html__( 'Bottom Seach Box Toggle', 'independent' ),
                ),
                'default'  => '1'
            ),
		)
    ) );
	
	//Header -> Header Top Bar
	$header_topbar_skin = $acf->themeSkinSettings('header-topbar');
	$header_topbar_ads = $acf->themeAdsList('header-topbar', 'top bar');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Top Bar', 'independent' ),
        'id'         => 'header-topbar',
        'desc'       => esc_html__( 'This is the setting for Header top bar', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'header-topbar-height',
				'type'     => 'dimensions',
				'units'		=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Header Top Bar Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease header topbar height.', 'independent' ),
				'default'  => array(
					'height'  => 40,
					'units'=> 'px'
				),
			),
			array(
				'id'       => 'header-topbar-sticky-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Header Top Bar Sticky Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease header topbar sticky height.', 'independent' ),
				'default'  => array(
					'height'  => '',
					'units'=> 'px'
				),
			),
			array(
                'id'       => 'header-topbar-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Topbar Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for header topbar. Here you can set header topbar font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$header_topbar_skin[0],
			$header_topbar_skin[1],
			$header_topbar_skin[2],
			$header_topbar_skin[3],
			$header_topbar_skin[4],
			array(
                'id'     => 'header-topbar-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-topbar-text-1',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 1', 'independent' ),
                'subtitle' => esc_html__( 'Custom text shows header topbar. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'header-topbar-text-2',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 2', 'independent' ),
                'subtitle' => esc_html__( 'One more custom text shows header topbar. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'header-topbar-date',
                'type'     => 'text',
                'title'    => esc_html__( 'Date Format', 'independent' ),
                'desc'     => esc_html__( 'Enter date format like: l, F j, Y', 'independent' ),
                'default'  => 'l, F j, Y',
            ),
			$header_topbar_ads,
			array(
				'id'      => 'header-topbar-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Header Top Bar Items', 'independent' ),
				'desc'    => esc_html__( 'Needed header topbar items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'header-topbar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
						'header-topbar-date' => esc_html__( 'Date', 'independent' ),
						'header-topbar-menu'    => esc_html__( 'Top Menu', 'independent' ),
						'header-topbar-social'	=> esc_html__( 'Social', 'independent' ),
						'header-topbar-search'	=> esc_html__( 'Search', 'independent' ),
						'header-topbar-ads-list'    => esc_html__( 'Ads', 'independent' ),
						'header-phone'   		=> esc_html__( 'Phone Number', 'independent' ),
						'header-address'  		=> esc_html__( 'Address Text', 'independent' ),
						'header-email'   		=> esc_html__( 'Email', 'independent' )
					),
					'Left'  => array(
						'header-topbar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),					
					),
					'Center' => array(
					),
					'Right' => array(
						
					)
				),
			),
		)
    ) );
	
	//Header -> Header Logo Section
	$header_logobar_skin = $acf->themeSkinSettings('header-logobar');
	$sticky_header_logobar_skin = $acf->themeSkinSettings('sticky-header-logobar');
	$header_logobar_ads = $acf->themeAdsList('header-logobar', 'logo bar');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Logo Section', 'independent' ),
        'id'         => 'header-logobar',
        'desc'       => esc_html__( 'This is the setting for header logo section.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'header-logobar-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Header Logo Section Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease header logo section height.', 'independent' ),
				'default'  => array(
					'height'  => 130,
					'units'=> 'px'
				),
			),
			array(
				'id'       => 'header-logobar-sticky-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Header Logo Section Sticky Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease header logo section sticky height.', 'independent' ),
				'default'  => array(
					'height'  => 110,
					'units'=> 'px'
				),
			),
			array(
                'id'       => 'header-logobar-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Logo Section Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for header logo section. Here you can set header logo section font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$header_logobar_skin[0],
			$header_logobar_skin[1],
			$header_logobar_skin[2],
			$header_logobar_skin[3],
			$header_logobar_skin[4],
			array(
                'id'     => 'header-logobar-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-logobar-text-1',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 1', 'independent' ),
                'subtitle' => esc_html__( 'Custom text shows header logo section. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'header-sticky-logobar-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Sticky Header Logo Section Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for sticky header logo section. Here you can set sticky header logo section font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			array(
				'id'       => 'sticky-header-logobar-color',
				'type'     => 'color',
				'title'    => __('Font Color', 'independent'), 
				'subtitle' => __('Pick a font color for sticky header logo section.', 'independent'),
				'validate' => 'color',
			),
			$sticky_header_logobar_skin[1],
			$sticky_header_logobar_skin[2],
			$sticky_header_logobar_skin[3],
			$sticky_header_logobar_skin[4],
			array(
                'id'     => 'header-sticky-logobar-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-logobar-text-1',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 1', 'independent' ),
                'subtitle' => esc_html__( 'Custom text shows header logo section. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'header-logobar-text-2',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 2', 'independent' ),
                'subtitle' => esc_html__( 'One more custom text shows header logo section. Here, you can place shortcode.', 'independent' )
            ),
			$header_logobar_ads,
			array(
				'id'      => 'header-logobar-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Header Logo Section Items', 'independent' ),
				'desc'    => esc_html__( 'Needed header logo section items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'header-logobar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
						'header-logobar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
						'header-logobar-menu'    => esc_html__( 'Main Menu', 'independent' ),
						'header-logobar-ads-list'    => esc_html__( 'Ads', 'independent' ),
						'header-logobar-social'	=> esc_html__( 'Social', 'independent' ),
						'header-logobar-search'	=> esc_html__( 'Search', 'independent' ),
						'header-logobar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'independent' ),
						'header-logobar-search-toggle'	=> esc_html__( 'Search Toggle', 'independent' ),
						'header-phone'   		=> esc_html__( 'Phone Number', 'independent' ),
						'header-address'  		=> esc_html__( 'Address Text', 'independent' ),
						'header-email'   		=> esc_html__( 'Email', 'independent' ),
						'header-cart'   		=> esc_html__( 'Cart', 'independent' ),
						'header-logobar-sticky-logo'	=> esc_html__( 'Sticky Logo', 'independent' )
					),
					'Left'  => array(	
						'header-logobar-logo'	=> esc_html__( 'Logo', 'independent' ),										
					),
					'Center' => array(
					),
					'Right' => array(						
					)
				),
			),
		)
    ) );
	
	//Header -> Header Navbar
	$header_navbar_skin = $acf->themeSkinSettings('header-navbar');
	$sticky_header_navbar_skin = $acf->themeSkinSettings('sticky-header-navbar');
	$header_navbar_ads = $acf->themeAdsList('header-navbar', 'navbar');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Navbar', 'independent' ),
        'id'         => 'header-navbar',
        'desc'       => esc_html__( 'This is the setting for header navbar section.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'header-navbar-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Header Navbar Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease header Navbar height.', 'independent' ),
				'default'  => array(
					'height'  => 50,
					'units'=> 'px'
				),
			),
			array(
				'id'       => 'header-navbar-sticky-height',
				'type'     => 'dimensions',
				'units'			=> array( 'px' ),
				'width'    => false,
				'title'    => esc_html__( 'Header Navbar Sticky Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease header navbar sticky height.', 'independent' ),
				'default'  => array(
					'height'  => 50,
					'units'=> 'px'
				),
			),
			array(
                'id'       => 'header-navbar-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Header Navbar Section Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for header navbar section. Here you can set header navbar font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$header_navbar_skin[0],
			$header_navbar_skin[1],
			$header_navbar_skin[2],
			$header_navbar_skin[3],
			$header_navbar_skin[4],
			array(
                'id'     => 'header-navbar-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-sticky-navbar-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Sticky Header Navbar Section Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for sticky header navbar section. Here you can set sticky header navbar section font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			array(
				'id'       => 'sticky-header-navbar-color',
				'type'     => 'color',
				'title'    => __('Font Color', 'independent'), 
				'subtitle' => __('Pick a font color for sticky header navbar section.', 'independent'),
				'validate' => 'color',
			),
			$sticky_header_navbar_skin[1],
			$sticky_header_navbar_skin[2],
			$sticky_header_navbar_skin[3],
			$sticky_header_navbar_skin[4],
			array(
                'id'     => 'header-sticky-navbar-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-navbar-text-1',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 1', 'independent' ),
                'subtitle' => esc_html__( 'Custom text shows header navbar section. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'header-navbar-text-2',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 2', 'independent' ),
                'subtitle' => esc_html__( 'One more custom text shows header navbar section. Here, you can place shortcode.', 'independent' )
            ),
			$header_navbar_ads,
			array(
				'id'      => 'header-navbar-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Header Navbar Section Items', 'independent' ),
				'desc'    => esc_html__( 'Needed header navbar section items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'header-navbar-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
						'header-navbar-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
						'header-navbar-logo'	=> esc_html__( 'Logo', 'independent' ),
						'header-navbar-social'	=> esc_html__( 'Social', 'independent' ),
						'header-navbar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'independent' ),
						'header-navbar-sticky-logo'	=> esc_html__( 'Stikcy Logo', 'independent' ),
						'header-navbar-search'	=> esc_html__( 'Search', 'independent' ),
						'header-navbar-ads-list'    => esc_html__( 'Ads', 'independent' ),
						'header-phone'   		=> esc_html__( 'Phone Number', 'independent' ),
						'header-address'  		=> esc_html__( 'Address Text', 'independent' ),
						'header-email'   		=> esc_html__( 'Email', 'independent' ),
						'header-cart'   		=> esc_html__( 'Cart', 'independent' )
					),
					'Left'  => array(											
						'header-navbar-menu'    => esc_html__( 'Main Menu', 'independent' ),
					),
					'Center' => array(
					),
					'Right' => array(
						'header-navbar-search-toggle'	=> esc_html__( 'Search Toggle', 'independent' ),
					)
				),
			),
		)
    ) );
	
	//Header -> Header Sticky/Fixed
	$header_left_skin = $acf->themeSkinSettings('header-fixed');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Sticky/Fixed', 'independent' ),
        'id'         => 'header-fixed',
        'desc'       => esc_html__( 'This is the setting for fixed header left/right in body.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'header-fixed-width',
				'type'     => 'dimensions',
				'units'		=> array( 'px' ),
				'units_extended'=> 'false',
				'height'    => false,
				'title'    => esc_html__( 'Sticky Header Width', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease left sticky header width.', 'independent' ),
				'default'  => array(
					'width'  => 350,
					'units'=> 'px'
				)
			),
			array(
                'id'       => 'header-fixed-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Sticky Header Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for sticky header. Here you can set sticky header font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$header_left_skin[0],
			$header_left_skin[2],
			$header_left_skin[3],
			$header_left_skin[4],
			array(
                'id'       => 'header-fixed-background',
                'type'     => 'background',
                'title'    => esc_html__( 'Background', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for sticky header background.', 'independent' ),
                'default'   => '',
            ),
			array(
                'id'     => 'header-fixed-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'header-fixed-text-1',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 1', 'independent' ),
                'subtitle' => esc_html__( 'Custom text shows on sticky header. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'header-fixed-text-2',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 2', 'independent' ),
                'subtitle' => esc_html__( 'One more custom text shows on sticky header. Here, you can place shortcode.', 'independent' )
            ),
			array(
				'id'      => 'header-fixed-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Sticky/Fixed Header Items', 'independent' ),
				'desc'    => esc_html__( 'Needed stciky header items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'header-fixed-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
						'header-fixed-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
						'header-fixed-search'	=> esc_html__( 'Search Form', 'independent' )
					),
					'Top'  => array(
						'header-fixed-logo' => esc_html__( 'Logo', 'independent' )
					),
					'Middle'  => array(
						'header-fixed-menu'	=> esc_html__( 'Menu', 'independent' )					
					),
					'Bottom'  => array(
						'header-fixed-social'	=> esc_html__( 'Social', 'independent' )					
					)
				),
			),
		)
    ) );
	
	//Header -> Mobile Menu Space
	$mobile_menu_skin = $acf->themeSkinSettings('mobile-menu');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Mobile Menu', 'independent' ),
        'id'         => 'mobile-menu',
        'desc'       => esc_html__( 'This is the setting for mobile menu', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'mobile-header-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Mobile Header Settings', 'independent' ),
                'indent'   => true, 
            ),
			array(
                'id'       => 'mobile-header-from',
                'type'     => 'select',
                'title'    => esc_html__( 'Mobile Header From', 'independent' ),
				'desc' => esc_html__( 'Choose your mobile header shows from tablet, tablet landscape or mobile', 'independent' ),
                'options'  => array(
                    'mobile' => esc_html__( 'Mobile', 'independent' ),
					'tab-port' => esc_html__( 'Tablet (portrait)', 'independent' ),
                    'tab-land' => esc_html__( 'Tablet (landscape)', 'independent' ),
                ),
                'default'  => 'tab-land'
            ),
			array(
				'id'       => 'mobile-header-height',
				'type'     => 'dimensions',
				'units'		=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Mobile Header Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease mobile header width.', 'independent' ),
				'default'  => array(
					'height'  => 80,
					'units'=> 'px'
				)
			),
			array(
                'id'       => 'mobile-header-background',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Background', 'independent' ),
                'subtitle' => esc_html__( 'Choose mobile header background color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
            ),
			array(
                'id'       => 'mobile-header-link-color',
                'type'     => 'link_color',
                'title'    => esc_html__( 'Links Color', 'independent' ),
                'subtitle' => esc_html__( 'Choose mobile header link color options.', 'independent' ),
                'default'  => array(
                    'regular' => '',
                    'hover'   => '',
                    'active'  => '',
                )
            ),
			array(
                'id'       => 'mobile-header-sticky',
                'type'     => 'switch',
                'title'    => esc_html__( 'Mobile Header Sticky', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable this option to sticky mobile header.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'mobile-header-sticky-scrollup',
                'type'     => 'switch',
                'title'    => esc_html__( 'Sticky Scroll Up', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable this option to sticky mobile header only scroll up.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
				'required' 		=> array('mobile-header-sticky', '!=', 0)
            ),
			array(
				'id'       => 'mobile-header-sticky-height',
				'type'     => 'dimensions',
				'units'		=> array( 'px' ),
				'units_extended'=> 'false',
				'width'    => false,
				'title'    => esc_html__( 'Mobile Header Sticky Height', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease mobile header sticky height.', 'independent' ),
				'default'  => array(
					'height'  => 75,
					'units'=> 'px'
				),
				'required' 		=> array('mobile-header-sticky', '!=', 0)
			),
			array(
                'id'       => 'mobile-header-sticky-background',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Sticky Background', 'independent' ),
                'subtitle' => esc_html__( 'Choose mobile header sticky background color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
				'required' 		=> array('mobile-header-sticky', '!=', 0)
            ),
			array(
                'id'       => 'mobile-header-sticky-link-color',
                'type'     => 'link_color',
                'title'    => esc_html__( 'Sticky Links Color', 'independent' ),
                'subtitle' => esc_html__( 'Choose mobile header sticky link color options.', 'independent' ),
                'default'  => array(
                    'regular' => '',
                    'hover'   => '',
                    'active'  => '',
                ),
				'required' 		=> array('mobile-header-sticky', '!=', 0)
            ),
			array(
				'id'      => 'mobile-header-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Mobile Header Items', 'independent' ),
				'desc'    => esc_html__( 'Needed mobile header items drag from disabled and put enabled parts like left, center or right.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'mobile-header-cart'	=> esc_html__( 'Cart Icon', 'independent' )
					),
					'Left'  => array(
						'mobile-header-menu'	=> esc_html__( 'Menu Icon', 'independent' )		
					),
					'Center'  => array(
						'mobile-header-logo' => esc_html__( 'Logo', 'independent' )
					),
					'Right'  => array(
						'mobile-header-search'	=> esc_html__( 'Search Icon', 'independent' )
					)
				),
			),
			array(
                'id'     => 'mobile-header-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'mobile-menu-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Mobile Menu Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for mobile menu area. Here you can set mobile menu space font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			array(
				'id'       => 'mobile-menu-max-width',
				'type'     => 'dimensions',
				'units'		=> array( 'px' ),
				'units_extended'=> 'false',
				'height'    => false,
				'title'    => esc_html__( 'Mobile Menu Max Width', 'independent' ),
				'desc'     => esc_html__( 'Increase or decrease mobile menu maximum width. If you need full width means just leave this empty.', 'independent' ),
				'default'  => array(
					'width'  => '',
					'units'=> 'px'
				)
			),
			$mobile_menu_skin[0],
			$mobile_menu_skin[2],
			$mobile_menu_skin[3],
			$mobile_menu_skin[4],
			array(
                'id'       => 'mobile-menu-background',
                'type'     => 'background',
                'title'    => esc_html__( 'Background', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for mobile menu background.', 'independent' ),
                'default'   => '',
            ),
			array(
                'id'       => 'mobile-menu-animate-from',
                'type'     => 'select',
                'title'    => esc_html__( 'Mobile Header Animate From', 'independent' ),
				'desc' => esc_html__( 'Choose your mobile header animate from left, right, top or bottom.', 'independent' ),
                'options'  => array(
                    'left' => esc_html__( 'Left', 'independent' ),
					'right' => esc_html__( 'Right', 'independent' ),
                    'top' => esc_html__( 'Top', 'independent' ),
					'bottom' => esc_html__( 'Bottom', 'independent' ),
                ),
                'default'  => 'left'
            ),
			array(
                'id'     => 'mobile-menu-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'mobile-menu-text-1',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 1', 'independent' ),
                'subtitle' => esc_html__( 'Custom text shows on mobile menu space. Here, you can place shortcode.', 'independent' )
            ),
			array(
                'id'       => 'mobile-menu-text-2',
                'type'     => 'text',
                'title'    => esc_html__( 'Custom Text 2', 'independent' ),
                'subtitle' => esc_html__( 'One more custom text shows on mobile menu space. Here, you can place shortcode.', 'independent' )
            ),
			array(
				'id'      => 'mobile-menu-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Mobile Menu Items', 'independent' ),
				'desc'    => esc_html__( 'Needed mobile menu items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'mobile-menu-text-1'	=> esc_html__( 'Custom Text 1', 'independent' ),
						'mobile-menu-text-2'	=> esc_html__( 'Custom Text 2', 'independent' ),
						'mobile-menu-social'	=> esc_html__( 'Social', 'independent' )
					),
					'Top'  => array(
						'mobile-menu-logo' => esc_html__( 'Logo', 'independent' )
					),
					'Middle'  => array(
						'mobile-menu-mainmenu'	=> esc_html__( 'Menu', 'independent' )
					),
					'Bottom'  => array(
						'mobile-menu-search'	=> esc_html__( 'Search Form', 'independent' )
					)
				),
			),
		)
    ) );
	
	//Footer Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Footer', 'independent' ),
        'id'               => 'footer',
        'desc'             => esc_html__( 'These are footer general settings of independent theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-credit-card'
    ) );
	
	//Footer -> Footer General
	$footer_skin = $acf->themeSkinSettings('footer');
	$footer_ads = $acf->themeAdsList('footer', 'footer');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer General', 'independent' ),
        'id'         => 'footer-general',
        'desc'       => esc_html__( 'This is the setting for Footer General', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'footer-layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Footer Layout', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer layout boxed or wide.', 'independent' ),
				'options' => array(
					'boxed' => esc_html__( 'Boxed', 'independent' ),
					'wide'  => esc_html__( 'Wide', 'independent' ),
				),
				'default'  => 'wide',
				'required' 		=> array('page-layout', '=', 'wide')
			),
			array(
                'id'       => 'back-to-top',
                'type'     => 'switch',
                'title'    => esc_html__( 'Back To Top', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable back to top icon.', 'independent' ),
                'default'  => 1,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'hidden-footer',
                'type'     => 'switch',
                'title'    => esc_html__( 'Hidden Footer', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable hidden footer.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'footer-settings-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for footer. Here you can set footer font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$footer_skin[0],
			$footer_skin[2],
			$footer_skin[3],
			$footer_skin[4],
			array(
                'id'       => 'footer-background',
                'type'     => 'background',
                'title'    => esc_html__( 'Background', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for footer background.', 'independent' ),
                'default'   => '',
            ),
			array(
                'id'       => 'footer-background-overlay',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Background Overlay', 'independent' ),
                'subtitle' => esc_html__( 'Choose background overlay color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
            ),
			array(
                'id'     => 'footer-settings-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			$footer_ads,
			array(
				'id'      => 'footer-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Footer Items', 'independent' ),
				'desc'    => esc_html__( 'Needed footer items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'Enabled'  => array(
						'footer-bottom'	=> esc_html__( 'Footer Bottom', 'independent' )
					),
					'disabled' => array(
						'footer-top' => esc_html__( 'Footer Top', 'independent' ),
						'footer-middle'	=> esc_html__( 'Footer Middle', 'independent' )
					)
				),
			),
		)
    ) );
	
	//Footer -> Footer Top
	$footer_skin = $acf->themeSkinSettings('footer-top');
	$footer_top_sidebars = $acf->themeSidebarsList( 'footer-top', array( 'title' => esc_html__( 'Footer Top Columns', 'independent' ), 'default' => '4' ) );
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Top', 'independent' ),
        'id'         => 'footer-top',
        'desc'       => esc_html__( 'This is the setting for footer top.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'footer-top-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Top Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for footer top. Here you can set footer top font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			array(
				'id'       => 'footer-top-container',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Footer Top Inner Layout', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer top layout boxed or wide.', 'independent' ),
				'options' => array(
					'boxed' => esc_html__( 'Boxed', 'independent' ),
					'wide'  => esc_html__( 'Wide', 'independent' ),
				),
				'default'  => 'boxed'
			),
			$footer_skin[0],
			$footer_skin[1],
			$footer_skin[2],
			$footer_skin[3],
			$footer_skin[4],
			$footer_skin[6],
			array(
                'id'       => 'footer-top-title-color',
                'type'     => 'color',
                'title'    => esc_html__( 'Widget Title Color', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer top widgets title color.', 'independent' ),
				'validate' => 'color'
            ),
			array(
                'id'     => 'footer-top-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'footer-sidebars-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Top Columns and Sidebars Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for footer top columns and sidebars. Choose number of columns and set needed widgets to selected sidebars.', 'independent' ),
                'indent'   => true, 
            ),
			$footer_top_sidebars[0],
			$footer_top_sidebars[1],
			$footer_top_sidebars[2],
			$footer_top_sidebars[3],
			$footer_top_sidebars[4],
			array(
                'id'     => 'footer-sidebars-end',
                'type'   => 'section',
                'indent' => false, 
            ),
		)
    ) );
	
	//Footer -> Footer Middle
	$footer_skin = $acf->themeSkinSettings('footer-middle');
	$footer_top_sidebars = $acf->themeSidebarsList( 'footer-middle', array( 'title' => esc_html__( 'Footer Middle Columns', 'independent' ), 'default' => '12' ) );
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Middle', 'independent' ),
        'id'         => 'footer-middle',
        'desc'       => esc_html__( 'This is the setting for footer middle.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'footer-middle-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Middle Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for footer middle. Here you can set footer middle font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			array(
				'id'       => 'footer-middle-container',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Footer Middle Inner Layout', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer middle layout boxed or wide.', 'independent' ),
				'options' => array(
					'boxed' => esc_html__( 'Boxed', 'independent' ),
					'wide'  => esc_html__( 'Wide', 'independent' ),
				),
				'default'  => 'boxed'
			),
			$footer_skin[0],
			$footer_skin[1],
			$footer_skin[2],
			$footer_skin[3],
			$footer_skin[4],
			$footer_skin[6],
			array(
                'id'       => 'footer-middle-title-color',
                'type'     => 'color',
                'title'    => esc_html__( 'Widget Title Color', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer middle widgets title color.', 'independent' ),
				'validate' => 'color'
            ),
			array(
                'id'     => 'footer-middle-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'footer-middle-sidebars-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Middle Columns and Sidebars Settings', 'independent' ),
                'subtitle' => esc_html__( 'This is settings for footer middle columns and sidebars. Choose number of columns and set needed widgets to selected sidebars.', 'independent' ),
                'indent'   => true, 
            ),
			$footer_top_sidebars[0],
			$footer_top_sidebars[1],
			$footer_top_sidebars[2],
			$footer_top_sidebars[3],
			$footer_top_sidebars[4],
			array(
                'id'     => 'footer-middle-sidebars-end',
                'type'   => 'section',
                'indent' => false, 
            ),
		)
    ) );
	
	//Footer -> Footer Bottom
	$footer_skin = $acf->themeSkinSettings('footer-bottom');
	$footer_top_sidebars = $acf->themeSidebarsList( 'footer-bottom', array( 'title' => esc_html__( 'Footer Bottom Columns', 'independent' ), 'default' => '12' ) );
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Bottom', 'independent' ),
        'id'         => 'footer-bottom',
        'desc'       => esc_html__( 'This is the setting for footer bottom.', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'footer-bottom-container',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Footer Bottom Inner Layout', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer bottom layout boxed or wide.', 'independent' ),
				'options' => array(
					'boxed' => esc_html__( 'Boxed', 'independent' ),
					'wide'  => esc_html__( 'Wide', 'independent' ),
				),
				'default'  => 'boxed'
			),
			array(
				'id'	=>'copyright-text',
				'type'	=> 'textarea',
				'title'	=> esc_html__( 'Copyright Text', 'independent' ), 
				'desc'	=> esc_html__( 'This is the copyright text. Shown on footer bottom if enable footer bottom in footer items', 'independent' ),
				'default'	=> '&copy; Copyright 2018. All Rights Reserved. Designed by <a href="http://zozothemes.com/">Zozo Themes</a>',
			),
			array(
                'id'       => 'footer-bottom-fixed',
                'type'     => 'switch',
                'title'    => esc_html__( 'Footer Bottom Fixed', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable footer bottom to fixed at bottom of page.', 'independent' ),
                'default'  => 0,
                'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
            ),
			array(
                'id'       => 'footer-bottom-start',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Bottom Skin', 'independent' ),
                'subtitle' => esc_html__( 'This is individual skin settings for footer bottom. Here you can set footer bottom font color, link color, etc..', 'independent' ),
                'indent'   => true, 
            ),
			$footer_skin[0],
			$footer_skin[1],
			$footer_skin[2],
			$footer_skin[3],
			$footer_skin[4],
			$footer_skin[6],
			array(
                'id'       => 'footer-bottom-title-color',
                'type'     => 'color',
                'title'    => esc_html__( 'Widget Title Color', 'independent' ),
				'subtitle' => esc_html__( 'Choose footer bottom widgets title color.', 'independent' ),
				'validate' => 'color'
            ),
			array(
                'id'     => 'footer-bottom-end',
                'type'   => 'section',
                'indent' => false, 
            ),
			array(
                'id'       => 'footer-bottom-widget',
                'type'     => 'select',
                'title'    => esc_html__( 'Footer Bottom Widget', 'independent' ),
                'desc'     => esc_html__( 'Select widget area for showing on footer copyright section.', 'independent' ),
                'data'     => 'sidebars',
            ),
			array(
				'id'      => 'footer-bottom-items',
				'type'    => 'sorter',
				'title'   => esc_html__( 'Footer Bottom Items', 'independent' ),
				'desc'    => esc_html__( 'Needed footer bottom items drag from disabled and put enabled.', 'independent' ),
				'options' => array(
					'disabled' => array(
						'social'	=> esc_html__( 'Footer Social Links', 'independent' ),
						'widget'	=> esc_html__( 'Custom Widget', 'independent' ),
						'menu'	=> esc_html__( 'Footer Menu', 'independent' )
					),
					'Left'  => array(
						
					),
					'Center'  => array(
						'copyright' => esc_html__( 'Copyright Text', 'independent' )
					),
					'Right'  => array(
						
					)
				),
			),
		)
    ) );
	
	//Widget Templates Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Widget Templates', 'independent' ),
        'id'               => 'widget-templates',
        'desc'             => esc_html__( 'These are the widget template settings for theme.', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-th-large'
    ) );
	
	//Widget Templates -> General
	$categories_array = $acf->themeCategories();	
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Widget General', 'independent' ),
        'id'         => 'widget-templates-general',
        'desc'       => esc_html__( 'This is the setting for widget template general', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'widget-title-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Widget Title Style', 'independent' ),
				'desc'	   => esc_html__( 'Choose this option for change widget title style.', 'independent' ),
				'options'  => array(
					''	=> esc_html__( 'Default', 'independent' ),
					'1'	=> esc_html__( 'Style 1', 'independent' ),
					'2'	=> esc_html__( 'Style 2', 'independent' )
				),
				'default'  => ''
			),
		)
    ) );
	
	//Page Template
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Page Template', 'independent' ),
        'id'               => 'template',
        'desc'             => esc_html__( 'These is the template settings for page.', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-newspaper-o'
    ) );
	
	//Templates -> Page
	$template = 'page'; $template_cname = 'Page'; $template_sname = 'page';
	$template_t = $acf->themeSkinSettings('template-'.$template);
	$page_title_items = $acf->themePageTitleItems('template-'.$template);
	$color = $acf->themeFontColor('template-'.$template);
	
	Redux::setSection( $opt_name, array(
		'title'      => esc_html__( 'Page Template', 'independent' ),
		'id'         => 'template-page',
		'desc'       => esc_html__( 'This is the setting for page template', 'independent' ),
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => $template.'-page-title-opt',
				'type'     => 'switch',
				'title'    => sprintf( esc_html__( '%1$s Title', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title.', 'independent' ), $template_sname ),
				'default'  => 1,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-pagetitle-settings-start',
				'type'     => 'section',
				'title'    => esc_html__( 'Page Title Settings', 'independent' ),
				'subtitle' => esc_html__( 'This is page title style settings for this template', 'independent' ),
				'indent'   => true, 
				'required' 		=> array($template.'-page-title-opt', '=', 1)
			),
			$color[0],
			$template_t[2],
			$template_t[3],
			$template_t[4],
			$template_t[5],
			array(
				'id'       => $template.'-page-title-parallax',
				'type'     => 'switch',
				'title'    => esc_html__( 'Background Parallax', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title background parallax.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-page-title-bg',
				'type'     => 'switch',
				'title'    => esc_html__( 'Background Video', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title background video.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-page-title-video',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Page Title Background Video', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Set page title background video for %1$s page. Only allowed youtube video id. Example: UWF7dZTLW4c', 'independent' ), $template_sname ),
				'required' => array($template.'-page-title-bg', '=', 1),
				'default'  => ''
			),
			array(
                'id'       => $template.'-page-title-overlay',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Page Title Overlay', 'independent' ),
                'subtitle' => esc_html__( 'Choose page title overlay rgba color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
            ),
			$page_title_items[0],
			array(
				'id'     => $template.'-pagetitle-settings-end',
				'type'   => 'section',
				'indent' => false, 
			),
			array(
				'id'       => $template.'-settings-start',
				'type'     => 'section',
				'title'    => sprintf( esc_html__( '%1$s Settings', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'This is settings for %1$s', 'independent' ), $template_sname ),
				'indent'   => true
			),
			array(
				'id'       => $template.'-page-template',
				'type'     => 'image_select',
				'title'    => sprintf( esc_html__( '%1$s Template', 'independent' ), $template_cname ),
				'desc'     => sprintf( esc_html__( 'Choose your current %1$s page template.', 'independent' ), $template_sname ),
				'options'  => array(
					'no-sidebar' => array(
						'alt' => esc_html__( 'No Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/1.png'
					),
					'right-sidebar' => array(
						'alt' => esc_html__( 'Right Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/2.png'
					),
					'left-sidebar' => array(
						'alt' => esc_html__( 'Left Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/3.png'
					),
					'both-sidebar' => array(
						'alt' => esc_html__( 'Both Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/4.png'
					)
				),
				'default'  => 'no-sidebar'
			),
			array(
				'id'       => $template.'-left-sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Left Sidebar', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Select widget area for showing %1$s on left sidebar.', 'independent' ), $template_sname ),
				'data'     => 'sidebars',
				'required' 		=> array($template.'-page-template', '=', array( 'left-sidebar', 'both-sidebar' ))
			),
			array(
				'id'       => $template.'-right-sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Right Sidebar', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Select widget area for showing %1$s on right sidebar.', 'independent' ), $template_sname ),
				'data'     => 'sidebars',
				'default'  => 'sidebar-1',
				'required' 		=> array($template.'-page-template', '=', array( 'right-sidebar', 'both-sidebar' ))
			),
			array(
				'id'       => $template.'-sidebar-sticky',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sidebar Sticky', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable sidebar sticky.', 'independent' ),
				'default'  => 1,
				'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
				'required' => array($template.'-page-template', '!=', 'no-sidebar')
			),
			array(
				'id'       => $template.'-page-hide-sidebar',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sidebar on Mobile', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to show or hide sidebar on mobile.', 'independent' ),
				'default'  => 1,
				'on'       => esc_html__( 'Show', 'independent' ),
				'off'      => esc_html__( 'Hide', 'independent' ),
				'required' => array($template.'-page-template', '!=', 'no-sidebar')
			),
			array(
				'id'     => $template.'-settings-end',
				'type'   => 'section',
				'indent' => false, 
			)
		)
    ) );

	
	//Templates Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Theme Templates', 'independent' ),
        'id'               => 'templates',
        'desc'             => esc_html__( 'These are the template settings for theme like blog, archive, etc..', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-th-large'
    ) );
	
	//Templates -> General
	$categories_array = $acf->themeCategories();	
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Template General', 'independent' ),
        'id'         => 'templates-general',
        'desc'       => esc_html__( 'This is the setting for template general', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'theme-templates',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Theme Templates', 'independent' ),
				'desc'     => esc_html__( 'Active needed theme templates. Actived theme templates are show once save and refresh theme option page. Unselected templates choosing archive template if enabled archive otherwise choosing blog template for default layout.', 'independent' ),
				'multi'    => true,
				'options' => array(
					'archive'	=> esc_html__( 'Archive', 'independent' ),
					'category'	=> esc_html__( 'Category', 'independent' ),
					'tag'		=> esc_html__( 'Tag', 'independent' ),
					'search'	=> esc_html__( 'Search', 'independent' ),
					'author'	=> esc_html__( 'Author', 'independent' )
				),
				'default' => array('archive'),
			),
			array(
				'id'       => 'theme-categories',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Theme Categories Template', 'independent' ),
				'desc'     => esc_html__( 'Active needed category templates. Actived category templates are show once save and refresh theme option page. Unselected templates choosing order category/archive/blog template for default layout.', 'independent' ),
				'multi'    => true,
				'options' => $categories_array
			),
			array(
				'id'       => 'search-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Search Content', 'independent' ),
				'desc'	   => esc_html__( 'Choose this option for search content from site.', 'independent' ),
				'options'  => array(
					'all'	=> esc_html__( 'All', 'independent' ),
					'post'	=> esc_html__( 'Post Content Only', 'independent' ),
					'page'	=> esc_html__( 'Page Content Only', 'independent' )
				),
				'default'  => 'post'
			),
		)
    ) );
	
	//Templates -> Single Post
	$template = 'single-post'; $template_cname = 'Single Post'; $template_sname = 'single post';
	$template_t = $acf->themeSkinSettings('template-'.$template);
	$template_article = $acf->themeSkinSettings($template.'-article');
	$template_article_overlay = $acf->themeSkinSettings($template.'-article-overlay');
	$page_title_items = $acf->themePageTitleItems('template-'.$template);
	$color = $acf->themeFontColor('template-'.$template);
	$template_article_color = $acf->themeFontColor($template.'-article');
	$template_article_overlay_color = $acf->themeFontColor($template.'-article-overlay');
	$overlay_margin = $acf->themeMarginFields( $template.'-article-overlay' );
	
	$article_top_ads = $acf->themeAdsList('article-top', 'article top', 'Article Top');
	$article_inline_ads = $acf->themeAdsList('article-inline', 'article inline', 'Article Inline');
	$article_bottom_ads = $acf->themeAdsList('article-bottom', 'article bottom', 'Article Bottom');

	
	Redux::setSection( $opt_name, array(
		'title'      => esc_html__( 'Single Post Template', 'independent' ),
		'id'         => 'templates-single-post',
		'desc'       => esc_html__( 'This is the setting for single post template', 'independent' ),
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => $template.'-page-title-opt',
				'type'     => 'switch',
				'title'    => sprintf( esc_html__( '%1$s Page Title', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-pagetitle-settings-start',
				'type'     => 'section',
				'title'    => esc_html__( 'Page Title Settings', 'independent' ),
				'subtitle' => esc_html__( 'This is page title style settings for this template', 'independent' ),
				'indent'   => true, 
				'required' 		=> array($template.'-page-title-opt', '=', 1)
			),
			$color[0],
			$template_t[2],
			$template_t[3],
			$template_t[4],
			$template_t[5],
			array(
				'id'       => $template.'-page-title-parallax',
				'type'     => 'switch',
				'title'    => esc_html__( 'Background Parallax', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title background parallax.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-page-title-bg',
				'type'     => 'switch',
				'title'    => esc_html__( 'Background Video', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s page title background video.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => $template.'-page-title-video',
				'type'     => 'text',
				'title'    => sprintf( esc_html__( '%1$s Page Title Background Video', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Set page title background video for %1$s page. Only allowed youtube video id. Example: UWF7dZTLW4c', 'independent' ), $template_sname ),
				'required' => array($template.'-page-title-bg', '=', 1),
				'default'  => ''
			),
			array(
                'id'       => $template.'-page-title-overlay',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Page Title Overlay', 'independent' ),
                'subtitle' => esc_html__( 'Choose page title overlay rgba color.', 'independent' ),
                'default'  => array(
                    'color' => '',
                    'alpha' => ''
                ),
                'mode'     => 'background',
            ),
			$page_title_items[0],
			array(
				'id'     => $template.'-pagetitle-settings-end',
				'type'   => 'section',
				'indent' => false, 
			),
			array(
				'id'       => $template.'-featured-slider',
				'type'     => 'switch',
				'title'    => sprintf( esc_html__( '%1$s Featured Slider', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s featured slider.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
			),
			array(
				'id'       => $template.'-article-settings-start',
				'type'     => 'section',
				'title'    => esc_html__( 'Article Skin', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'This is skin settings for each %1$s article', 'independent' ), $template_sname ),
				'indent'   => true
			),
			$template_article_color[0],
			$template_article[2],
			$template_article[3],
			$template_article[4],
			$template_article[1],
			array(
				'id'     => $template.'-article-settings-end',
				'type'   => 'section',
				'indent' => false, 
			),
			array(
				'id'       => $template.'-article-overlay-settings-start',
				'type'     => 'section',
				'title'    => esc_html__( 'Article Overlay Skin', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'This is skin settings for each %1$s article overlay.', 'independent' ), $template_sname ),
				'indent'   => true,
			),
			$template_article_overlay_color[0],
			$template_article_overlay[2],
			$template_article_overlay[3],
			$template_article_overlay[4],
			$overlay_margin,
			$template_article_overlay[1],
			array(
				'id'     => $template.'-article-overlay-settings-end',
				'type'   => 'section',
				'indent' => false, 
			),
			array(
				'id'       => $template.'-post-formats-start',
				'type'     => 'section',
				'title'    => esc_html__( 'Post Format Settings', 'independent' ),
				'subtitle' => sprintf( esc_html__( 'This is post format settings for %1$s', 'independent' ), $template_sname ),
				'indent'   => true
			),
			array(
				'id'       => $template.'-video-format',
				'type'     => 'select',
				'title'    => esc_html__( 'Video Format', 'independent' ),
				'desc'	   => sprintf( esc_html__( 'Choose %1$s page video post format settings.', 'independent' ), $template_sname ),
				'options'  => array(
					'onclick' => esc_html__( 'On Click Run Video', 'independent' ),
					'overlay' => esc_html__( 'Modal Box Video', 'independent' ),
					'direct' => esc_html__( 'Direct Video', 'independent' )
				),
				'default'  => 'onclick'
			),
			array(
				'id'       => $template.'-quote-format',
				'type'     => 'select',
				'title'    => esc_html__( 'Quote Format', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Choose %1$s page quote post format settings.', 'independent' ), $template_sname ),
				'options'  => array(
					'featured' => esc_html__( 'Dark Overlay', 'independent' ),
					'theme-overlay' => esc_html__( 'Theme Overlay', 'independent' ),
					'theme' => esc_html__( 'Theme Color Background', 'independent' ),
					'none' => esc_html__( 'None', 'independent' )
				),
				'default'  => 'featured'
			),
			array(
				'id'       => $template.'-link-format',
				'type'     => 'select',
				'title'    => esc_html__( 'Link Format', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Choose %1$s page link post format settings.', 'independent' ), $template_sname ),
				'options'  => array(
					'featured' => esc_html__( 'Dark Overlay', 'independent' ),
					'theme-overlay' => esc_html__( 'Theme Overlay', 'independent' ),
					'theme' => esc_html__( 'Theme Color Background', 'independent' ),
					'none' => esc_html__( 'None', 'independent' )
				),
				'default'  => 'featured'
			),
			array(
				'id'       => $template.'-gallery-format',
				'type'     => 'select',
				'title'    => esc_html__( 'Gallery Format', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Choose %1$s page gallery post format settings.', 'independent' ), $template_sname ),
				'options'  => array(
					'default' => esc_html__( 'Default Gallery', 'independent' ),
					'popup' => esc_html__( 'Popup Gallery', 'independent' ),
					'grid' => esc_html__( 'Grid Popup Gallery', 'independent' )
				),
				'default'  => 'default'
			),
			array(
				'id'     => $template.'-post-formats-end',
				'type'   => 'section',
				'indent' => false, 
			),
			array(
				'id'       => $template.'-settings-start',
				'type'     => 'section',
				'title'    => sprintf( esc_html__( '%1$s Settings', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'This is settings for %1$s', 'independent' ), $template_sname ),
				'indent'   => true
			),
			array(
				'id'       => $template.'-page-template',
				'type'     => 'image_select',
				'title'    => sprintf( esc_html__( '%1$s Template', 'independent' ), $template_cname ),
				'desc'     => sprintf( esc_html__( 'Choose your current %1$s page template.', 'independent' ), $template_sname ),
				'options'  => array(
					'no-sidebar' => array(
						'alt' => esc_html__( 'No Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/1.png'
					),
					'right-sidebar' => array(
						'alt' => esc_html__( 'Right Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/2.png'
					),
					'left-sidebar' => array(
						'alt' => esc_html__( 'Left Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/3.png'
					),
					'both-sidebar' => array(
						'alt' => esc_html__( 'Both Sidebar', 'independent' ),
						'img' => get_template_directory_uri() . '/assets/images/page-layouts/4.png'
					)
				),
				'default'  => 'right-sidebar'
			),
			array(
				'id'       => $template.'-left-sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Left Sidebar', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Select widget area for showing %1$s page on left sidebar.', 'independent' ), $template_sname ),
				'data'     => 'sidebars',
				'required' 		=> array($template.'-page-template', '=', array( 'left-sidebar', 'both-sidebar' ))
			),
			array(
				'id'       => $template.'-right-sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Right Sidebar', 'independent' ),
				'desc'     => sprintf( esc_html__( 'Select widget area for showing %1$s page on right sidebar.', 'independent' ), $template_sname ),
				'data'     => 'sidebars',
				'default'  => 'sidebar-1',
				'required' 		=> array($template.'-page-template', '=', array( 'right-sidebar', 'both-sidebar' ))
			),
			array(
				'id'       => $template.'-sidebar-sticky',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sidebar Sticky', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable sidebar sticky.', 'independent' ),
				'default'  => 1,
				'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
				'required' => array($template.'-page-template', '!=', 'no-sidebar')
			),
			array(
				'id'       => $template.'-page-hide-sidebar',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sidebar on Mobile', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to show or hide sidebar on mobile.', 'independent' ),
				'default'  => 1,
				'on'       => esc_html__( 'Show', 'independent' ),
				'off'      => esc_html__( 'Hide', 'independent' ),
				'required' => array($template.'-page-template', '!=', 'no-sidebar')
			),
			array(
				'id'       => $template.'-full-wrap',
				'type'     => 'switch',
				'title'    => esc_html__( 'Full Width Wrap', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable to show or hide full width post wrapper.', 'independent' ),
				'default'  => 0,
				'on'       => esc_html__( 'Show', 'independent' ),
				'off'      => esc_html__( 'Hide', 'independent' )
			),
			$article_top_ads,
			$article_inline_ads,
			$article_bottom_ads,
			array(
				'id'      => $template.'-topmeta-items',
				'type'    => 'sorter',
				'title'   => sprintf( esc_html__( '%1$s Top Meta Items', 'independent' ), $template_cname ),
				'desc'    => sprintf( esc_html__( 'Needed %1$s top meta items drag from disabled and put enabled part. ie: Left or Right.', 'independent' ), $template_sname ),
				'options' => array(
					'Left'  => array(
						'date'	=> esc_html__( 'Date', 'independent' ),
						'category'	=> esc_html__( 'Category', 'independent' )
					),
					'Right'  => array(
					),
					'disabled' => array(
						'social'	=> esc_html__( 'Social Share', 'independent' ),
						'comments'	=> esc_html__( 'Comments', 'independent' ),
						'likes'	=> esc_html__( 'Likes', 'independent' ),
						'author'	=> esc_html__( 'Author', 'independent' ),
						'views'	=> esc_html__( 'Views', 'independent' ),
						'tag'	=> esc_html__( 'Tags', 'independent' ),
						'favourite'	=> esc_html__( 'Favourite', 'independent' ),
						'author-name'	=> esc_html__( 'Author Name', 'independent' )
					)
				),
			),
			array(
				'id'      => $template.'-bottommeta-items',
				'type'    => 'sorter',
				'title'   => sprintf( esc_html__( '%1$s Bottom Meta Items', 'independent' ), $template_cname ),
				'desc'    => sprintf( esc_html__( 'Needed %1$s bottom meta items drag from disabled and put enabled part. ie: Left or Right.', 'independent' ), $template_sname ),
				'options' => array(
					'Left'  => array(
						'tag'	=> esc_html__( 'Tags', 'independent' ),
					),
					'Right'  => array(
					),
					'disabled' => array(
						'date'	=> esc_html__( 'Date', 'independent' ),
						'social'	=> esc_html__( 'Social Share', 'independent' ),
						'category'	=> esc_html__( 'Category', 'independent' ),
						'social'	=> esc_html__( 'Social Share', 'independent' ),
						'comments'	=> esc_html__( 'Comments', 'independent' ),
						'likes'	=> esc_html__( 'Likes', 'independent' ),
						'author'	=> esc_html__( 'Author', 'independent' ),
						'views'	=> esc_html__( 'Views', 'independent' ),
						'tag'	=> esc_html__( 'Tags', 'independent' ),
						'favourite'	=> esc_html__( 'Favourite', 'independent' ),
						'author-name'	=> esc_html__( 'Author Name', 'independent' )
					)
				),
			),
			array(
				'id'      => $template.'-items',
				'type'    => 'sorter',
				'title'   => sprintf( esc_html__( '%1$s Items', 'independent' ), $template_cname ),
				'desc'    => sprintf( esc_html__( 'Needed %1$s items drag from disabled and put enabled part.', 'independent' ), $template_sname ),
				'options' => array(
					'Enabled'  => array(
						'thumb'	=> esc_html__( 'Thumbnail', 'independent' ),
						'top-meta'	=> esc_html__( 'Top Meta', 'independent' ),
						'title'	=> esc_html__( 'Title', 'independent' ),
						'content'	=> esc_html__( 'Content', 'independent' ),
						'bottom-meta'	=> esc_html__( 'Bottom Meta', 'independent' ),
					),
					'disabled' => array(
						'breadcrumb'	=> esc_html__( 'Breadcrumbs', 'independent' )
					)
				),
			),
			array(
				'id'       => $template.'-overlay-opt',
				'type'     => 'switch',
				'title'    => sprintf( esc_html__( '%1$s Overlay', 'independent' ), $template_cname ),
				'subtitle' => sprintf( esc_html__( 'Enable/Disable %1$s post overlay.', 'independent' ), $template_sname ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'      => $template.'-overlay-items',
				'type'    => 'sorter',
				'title'   => sprintf( esc_html__( '%1$s Overlay Items', 'independent' ), $template_cname ),
				'desc'    => sprintf( esc_html__( 'Needed %1$s overlay items drag from disabled and put enabled part.', 'independent' ), $template_sname ),
				'options' => array(
					'Enabled'  => array(
						'title'	=> esc_html__( 'Title', 'independent' ),
					),
					'disabled' => array(
						'top-meta'	=> esc_html__( 'Top Meta', 'independent' ),
						'bottom-meta'	=> esc_html__( 'Bottom Meta', 'independent' )
					)
				),
				'required' 		=> array($template.'-overlay-opt', '=', 1)
			),
			array(
				'id'      => $template.'-page-items',
				'type'    => 'sorter',
				'title'   => sprintf( esc_html__( '%1$s Page Items', 'independent' ), $template_cname ),
				'desc'    => sprintf( esc_html__( 'Needed %1$s items drag from disabled and put enabled part.', 'independent' ), $template_sname ),
				'options' => array(
					'Enabled'  => array(
						'post-items'	=> esc_html__( 'Post Items', 'independent' ),
						'author-info'	=> esc_html__( 'Author Info', 'independent' ),
						'review'	=> esc_html__( 'Review Info', 'independent' ),
						'post-nav'	=> esc_html__( 'Post Navigation', 'independent' ),
						'comment'	=> esc_html__( 'Comment', 'independent' )
					),
					'disabled' => array(
						'related-articles'	=> esc_html__( 'Related Articles', 'independent' ),
						'author-articles'	=> esc_html__( 'Author Articles', 'independent' ),
						'article-inline-ads-list'	=> esc_html__( 'Article Inline Ads', 'independent' )
					)
				),
			),
			array(
				'id'       => 'related-posts-filter',
				'type'     => 'select',
				'title'    => esc_html__( 'Related Posts From', 'independent' ),
				'desc'     => esc_html__( 'Choose related posts from category or tag.', 'independent' ),
				'options'  => array(
					'category'	=> esc_html__( 'Category', 'independent' ),
					'tag'		=> esc_html__( 'Tag', 'independent' )
				),
				'default'  => 'category'
			),
			array(
				'id'       => 'related-max-posts',
				'type'     => 'text',
				'title'    => esc_html__('Related Post Columns', 'independent'),
				'desc'     => esc_html__('Enter related post maximum limit for get from posts query. Example 5.', 'independent'),
				'default'  => '3'
			),
			array(
				'id'       => 'author-max-posts',
				'type'     => 'text',
				'title'    => esc_html__('Author Related Post Columns', 'independent'),
				'desc'     => esc_html__('Enter author related post maximum limit for get from posts query. Example 5.', 'independent'),
				'default'  => '3'
			),
			array(
				'id'     => $template.'-settings-end',
				'type'   => 'section',
				'indent' => false, 
			)
		)
	) );
	
	//Templates -> Blog
	$blog_array = $acf->independentThemeOptTemplate( 'blog', esc_html__( 'Blog', 'independent' ), esc_html__( 'blog', 'independent' ) );
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Blog Template', 'independent' ),
        'id'         => 'templates-blog',
        'desc'       => esc_html__( 'This is the setting for blog template', 'independent' ),
        'subsection' => true,
        'fields'     => $blog_array
    ) );
	
	$theme_templates = $acf->independentGetThemeTemplatesKey();
	if( !empty( $theme_templates ) && in_array( "archive", $theme_templates ) ):
		//Templates -> Archive
		$archive_array = $acf->independentThemeOptTemplate( 'archive', esc_html__( 'Archive', 'independent' ), esc_html__( 'archive', 'independent' ) );
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Archive Template', 'independent' ),
			'id'         => 'templates-archive',
			'desc'       => esc_html__( 'This is the setting for archive template', 'independent' ),
			'subsection' => true,
			'fields'     => $archive_array
		) );
	endif;
	
	if( !empty( $theme_templates ) && in_array( "category", $theme_templates ) ):
		//Templates -> Category
		$category_array = $acf->independentThemeOptTemplate( 'category', esc_html__( 'Category', 'independent' ), esc_html__( 'category', 'independent' ) );
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Category Template', 'independent' ),
			'id'         => 'templates-category',
			'desc'       => esc_html__( 'This is the setting for category template', 'independent' ),
			'subsection' => true,
			'fields'     => $category_array
		) );
	endif;
	
	if( !empty( $theme_templates ) && in_array( "tag", $theme_templates ) ):
		//Templates -> Tag
		$tag_array = $acf->independentThemeOptTemplate( 'tag', esc_html__( 'Tag', 'independent' ), esc_html__( 'tag', 'independent' ) );
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Tag Template', 'independent' ),
			'id'         => 'templates-tag',
			'desc'       => esc_html__( 'This is the setting for tag template', 'independent' ),
			'subsection' => true,
			'fields'     => $tag_array
		) );
	endif;
	
	if( !empty( $theme_templates ) && in_array( "author", $theme_templates ) ):
		//Templates -> Author
		$author_array = $acf->independentThemeOptTemplate( 'author', esc_html__( 'Author', 'independent' ), esc_html__( 'author', 'independent' ) );
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Author Template', 'independent' ),
			'id'         => 'templates-author',
			'desc'       => esc_html__( 'This is the setting for author template', 'independent' ),
			'subsection' => true,
			'fields'     => $author_array
		) );
	endif;
	
	if( !empty( $theme_templates ) && in_array( "search", $theme_templates ) ):
		//Templates -> Search
		$search_array = $acf->independentThemeOptTemplate( 'search', esc_html__( 'Search', 'independent' ), esc_html__( 'search', 'independent' ) );
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Search Template', 'independent' ),
			'id'         => 'templates-search',
			'desc'       => esc_html__( 'This is the setting for search template', 'independent' ),
			'subsection' => true,
			'fields'     => $search_array
		) );
	endif;
	
	//Templates -> All Categories
	$cat_templates = $acf->independentGetAdminThemeOpt( 'theme-categories' );

	if( !empty( $cat_templates ) ){
		
		Redux::setSection( $opt_name, array(
			'title'            => esc_html__( 'Categories Templates', 'independent' ),
			'id'               => 'templates-categories',
			'desc'             => esc_html__( 'This is the template setting for all theme categories.', 'independent' ),
			'customizer_width' => '400px',
			'icon'             => 'fa fa-newspaper-o'
		) );
		
		// Show only enabled category templates
		foreach( $cat_templates as $cat_name ){
			
			$cat_key = str_replace( "category-", "", $cat_name );
			$cat_name = get_cat_name( absint( $cat_key ) );
			
			$cat_key = "category-" . $cat_key;
			$cat_sname = strtolower( $cat_name );
			//Templates -> Dynamic Categories
			$cat_array = $acf->independentThemeOptTemplate( $cat_key, $cat_name, $cat_sname );
			Redux::setSection( $opt_name, array(
				'title'      => sprintf( esc_html__( '%1$s Template', 'independent' ), $cat_name ),
				'id'         => 'templates-' . $cat_key,
				'desc'       => sprintf( esc_html__( 'This is the setting for %1$s category template', 'independent' ), $cat_name ),
				'subsection' => true,
				'fields'     => $cat_array
			) );
			
		} // Categories foreach
		
	} // All categories template if condition
		
	//Sliders Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Sliders', 'independent' ),
        'id'               => 'sliders',
        'desc'             => esc_html__( 'These are the sliders settings of independent Theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-film'
    ) );
	
	// Featured Slider
	$featured_slider = $acf->themeSliders('featured');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Featured Slider', 'independent' ),
        'id'         => 'sliders-featured',
        'desc'       => esc_html__( 'This is the setting for featured slider', 'independent' ),
        'subsection' => true,
        'fields'     => $featured_slider
    ) );
	
	// Related Slider
	$related_slider = $acf->themeSliders('related');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Related Slider', 'independent' ),
        'id'         => 'sliders-related',
        'desc'       => esc_html__( 'This is the setting for related slider', 'independent' ),
        'subsection' => true,
        'fields'     => $related_slider
    ) );
	
	// Blog Post Slider
	$blog_slider = $acf->themeSliders('blog');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Blog Post Slider', 'independent' ),
        'id'         => 'sliders-blog',
        'desc'       => esc_html__( 'This is the setting for blog post slider', 'independent' ),
        'subsection' => true,
        'fields'     => $blog_slider
    ) );
	
	// Single Post Slider
	$single_slider = $acf->themeSliders('single');
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Single Post Slider', 'independent' ),
        'id'         => 'sliders-single',
        'desc'       => esc_html__( 'This is the setting for single post slider', 'independent' ),
        'subsection' => true,
        'fields'     => $single_slider
    ) );
	
	//Social Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Social', 'independent' ),
        'id'               => 'social',
        'desc'             => esc_html__( 'These are the Social settings of independent Theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-users'
    ) );
	
	//Social -> Links
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Social Settings', 'independent' ),
        'id'         => 'social-links',
        'desc'       => esc_html__( 'This is the setting for social links', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
                'id'       => 'social-icons-type',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Social Iocns Type', 'independent' ),
                'desc'     => esc_html__( 'Choose your social icons type.', 'independent' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    'squared' => array(
                        'alt' => esc_html__( 'Squared', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/1.png'
                    ),
                    'rounded' => array(
                        'alt' => esc_html__( 'Rounded', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/2.png'
                    ),
                    'circled' => array(
                        'alt' => esc_html__( 'Circled', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/3.png'
                    ),
					'transparent' => array(
                        'alt' => esc_html__( 'Nothing', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/4.png'
                    )
                ),
                'default'  => 'transparent'
            ),
			array(
                'id'       => 'social-icons-type-footer',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Footer Bottom Social Iocns Type', 'independent' ),
                'desc'     => esc_html__( 'Choose your social icons type.', 'independent' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    'squared' => array(
                        'alt' => esc_html__( 'Squared', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/1.png'
                    ),
                    'rounded' => array(
                        'alt' => esc_html__( 'Rounded', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/2.png'
                    ),
                    'circled' => array(
                        'alt' => esc_html__( 'Circled', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/3.png'
                    ),
					'transparent' => array(
                        'alt' => esc_html__( 'Nothing', 'independent' ),
                        'img' => ReduxFramework::$_url . 'assets/img/social-icons/4.png'
                    )
                ),
                'default'  => 'transparent'
            ),
			array(
                'id'       => 'social-icons-fore',
                'type'     => 'select',
                'title'    => esc_html__( 'Social Icons Fore', 'independent' ),
				'desc'     => esc_html__( 'Social icons fore color settings.', 'independent' ),
                'options'  => array(
                    'black'		=> esc_html__( 'Black', 'independent' ),
                    'white'		=> esc_html__( 'White', 'independent' ),
					'own'		=> esc_html__( 'Own Color', 'independent' ),
                ),
                'default'  => 'white'
            ),
			array(
                'id'       => 'social-icons-hfore',
                'type'     => 'select',
                'title'    => esc_html__( 'Social Icons Fore Hover', 'independent' ),
				'desc'     => esc_html__( 'Social icons fore hover color settings.', 'independent' ),
                'options'  => array(
                    'h-black'		=> esc_html__( 'Black', 'independent' ),
                    'h-white'		=> esc_html__( 'White', 'independent' ),
					'h-own'		=> esc_html__( 'Own Color', 'independent' ),
                ),
                'default'  => 'h-white'
            ),
			array(
                'id'       => 'social-icons-bg',
                'type'     => 'select',
                'title'    => esc_html__( 'Social Icons Background', 'independent' ),
				'desc'     => esc_html__( 'Social icons background color settings.', 'independent' ),
                'options'  => array(
                    'bg-black'		=> esc_html__( 'Black', 'independent' ),
                    'bg-white'		=> esc_html__( 'White', 'independent' ),
					'bg-light'		=> esc_html__( 'RGBA Light', 'independent' ),
					'bg-dark'		=> esc_html__( 'RGBA Dark', 'independent' ),
					'bg-own'		=> esc_html__( 'Own Color', 'independent' ),
                ),
                'default'  => 'bg-own'
            ),
			array(
                'id'       => 'social-icons-hbg',
                'type'     => 'select',
                'title'    => esc_html__( 'Social Icons Background Hover', 'independent' ),
				'desc'     => esc_html__( 'Social icons background hover color settings.', 'independent' ),
                'options'  => array(
                    'hbg-black'		=> esc_html__( 'Black', 'independent' ),
                    'hbg-white'		=> esc_html__( 'White', 'independent' ),
					'hbg-light'		=> esc_html__( 'RGBA Light', 'independent' ),
					'hbg-dark'		=> esc_html__( 'RGBA Dark', 'independent' ),
					'hbg-own'		=> esc_html__( 'Own Color', 'independent' ),
                ),
                'default'  => 'hbg-dark'
            ),
			array(
                'id'       => 'social-fb',
                'type'     => 'text',
                'title'    => esc_html__( 'Facebook', 'independent' ),
                'desc'     => esc_html__( 'Enter the facebook link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-twitter',
                'type'     => 'text',
                'title'    => esc_html__( 'Twitter', 'independent' ),
                'desc'     => esc_html__( 'Enter the twitter link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-instagram',
                'type'     => 'text',
                'title'    => esc_html__( 'Instagram', 'independent' ),
                'desc'     => esc_html__( 'Enter the instagram link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-pinterest',
                'type'     => 'text',
                'title'    => esc_html__( 'Pinterest', 'independent' ),
                'desc'     => esc_html__( 'Enter the pinterest link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-gplus',
                'type'     => 'text',
                'title'    => esc_html__( 'Google Plus', 'independent' ),
                'desc'     => esc_html__( 'Enter the Google Plus link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-youtube',
                'type'     => 'text',
                'title'    => esc_html__( 'Youtube', 'independent' ),
                'desc'     => esc_html__( 'Enter the Youtube link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-vimeo',
                'type'     => 'text',
                'title'    => esc_html__( 'Vimeo', 'independent' ),
                'desc'     => esc_html__( 'Enter the Vimeo link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-soundcloud',
                'type'     => 'text',
                'title'    => esc_html__( 'Soundcloud', 'independent' ),
                'desc'     => esc_html__( 'Enter the Soundcloud link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-yahoo',
                'type'     => 'text',
                'title'    => esc_html__( 'Yahoo', 'independent' ),
                'desc'     => esc_html__( 'Enter the Yahoo link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-tumblr',
                'type'     => 'text',
                'title'    => esc_html__( 'Tumblr', 'independent' ),
                'desc'     => esc_html__( 'Enter the Tumblr link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-paypal',
                'type'     => 'text',
                'title'    => esc_html__( 'Paypal', 'independent' ),
                'desc'     => esc_html__( 'Enter the Paypal link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-mailto',
                'type'     => 'text',
                'title'    => esc_html__( 'Mailto', 'independent' ),
                'desc'     => esc_html__( 'Enter the Mailto link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-flickr',
                'type'     => 'text',
                'title'    => esc_html__( 'Flickr', 'independent' ),
                'desc'     => esc_html__( 'Enter the Flickr link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-dribbble',
                'type'     => 'text',
                'title'    => esc_html__( 'Dribbble', 'independent' ),
                'desc'     => esc_html__( 'Enter the Dribbble link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-linkedin',
                'type'     => 'text',
                'title'    => esc_html__( 'LinkedIn', 'independent' ),
                'desc'     => esc_html__( 'Enter the linkedin link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
			array(
                'id'       => 'social-rss',
                'type'     => 'text',
                'title'    => esc_html__( 'RSS', 'independent' ),
                'desc'     => esc_html__( 'Enter the rss link. If no link means just leave it blank', 'independent' ),
                'default'  => '',
            ),
		)
    ) );
	
	//Social -> Share
	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Social Share', 'independent' ),
        'id'         => 'social-share',
        'desc'       => esc_html__( 'This is the setting for social share', 'independent' ),
        'subsection' => true,
        'fields'     => array(
			array(
				'id'       => 'post-social-shares',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Post Social Shares', 'independent' ),
				'desc'     => esc_html__( 'Actived social items only showing post share list.', 'independent' ),
				'multi'    => true,
				'options' => array(
					'fb'	=> esc_html__( 'Facebook', 'independent' ),
					'twitter'	=> esc_html__( 'Twitter', 'independent' ),
					'linkedin'		=> esc_html__( 'Linkedin', 'independent' ),
					'gplus'	=> esc_html__( 'Google Plus', 'independent' ),
					'pinterest'	=> esc_html__( 'Pinterest', 'independent' ),
					'whatsapp'	=> esc_html__( 'Whatsapp', 'independent' )
				),
				'default' => array('fb', 'twitter', 'linkedin', 'gplus', 'pinterest', 'whatsapp'),
			),
			array(
				'id'       => 'comments-social-shares',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Comments Social Shares', 'independent' ),
				'desc'     => esc_html__( 'Actived social items only showing comments share list.', 'independent' ),
				'multi'    => true,
				'options' => array(
					'fb'	=> esc_html__( 'Facebook', 'independent' ),
					'twitter'	=> esc_html__( 'Twitter', 'independent' ),
					'linkedin'		=> esc_html__( 'Linkedin', 'independent' ),
					'gplus'	=> esc_html__( 'Google Plus', 'independent' ),
					'pinterest'	=> esc_html__( 'Pinterest', 'independent' )
				),
				'default' => array('fb', 'twitter', 'linkedin', 'gplus', 'pinterest'),
			),
		)
    ) );
	
	//WooCommerce
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		Redux::setSection( $opt_name, array(
			'title'            => esc_html__( 'Woo', 'independent' ),
			'id'               => 'woo',
			'desc'             => esc_html__( 'These are the WooCommerce settings of independent Theme', 'independent' ),
			'customizer_width' => '400px',
			'icon'             => 'fa fa-shopping-cart'
		) );
		
		//WooCommerce -> General
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'General Settings', 'independent' ),
			'id'         => 'woo-general',
			'desc'       => esc_html__( 'This is general WooCommerce setting.', 'independent' ),
			'subsection' => true,
			'fields'     => array(
				array(
					'id'       => 'woo-page-template',
					'type'     => 'image_select',
					'title'    => esc_html__( 'Woocommerce Shop Template', 'independent' ),
					'desc'     => esc_html__( 'Choose your current woocommerce shop page template.', 'independent' ),
					'options'  => array(
						'no-sidebar' => array(
							'alt' => esc_html__( 'No Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/1.png'
						),
						'right-sidebar' => array(
							'alt' => esc_html__( 'Right Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/2.png'
						),
						'left-sidebar' => array(
							'alt' => esc_html__( 'Left Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/3.png'
						),
						'both-sidebar' => array(
							'alt' => esc_html__( 'Both Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/4.png'
						)
					),
					'default'  => 'right-sidebar'
				),
				array(
					'id'       => 'woo-left-sidebar',
					'type'     => 'select',
					'title'    => esc_html__( 'Choose Left Sidebar', 'independent' ),
					'desc'     => esc_html__( 'Select widget area for showing woocommerce shop template on left sidebar.', 'independent' ),
					'data'     => 'sidebars',
					'required' 		=> array('woo-page-template', '=', array( 'left-sidebar', 'both-sidebar' ))
				),
				array(
					'id'       => 'woo-right-sidebar',
					'type'     => 'select',
					'title'    => esc_html__( 'Choose Right Sidebar', 'independent' ),
					'desc'     => esc_html__( 'Select widget area for showing woocommerce shop template on right sidebar.', 'independent' ),
					'data'     => 'sidebars',
					'default'  => 'sidebar-1',
					'required' 		=> array('woo-page-template', '=', array( 'right-sidebar', 'both-sidebar' ))
				),
				array(
					'id'       => 'woo-shop-columns',
					'type'     => 'select',
					'title'    => esc_html__( 'Shop Columns', 'independent' ),
					'desc'     => esc_html__( 'This is column settings woocommerce shop page products.', 'independent' ),
					'options'  => array(
						'2'		=> esc_html__( '2 Columns', 'independent' ),
						'3'		=> esc_html__( '3 Columns', 'independent' ),
						'4'		=> esc_html__( '4 Columns', 'independent' ),
						'5'		=> esc_html__( '5 Columns', 'independent' ),
						'6'		=> esc_html__( '6 Columns', 'independent' ),
					),
					'default'  => '3'
				),
				array(
					'id'       => 'woo-shop-ppp',
					'type'     => 'text',
					'title'    => esc_html__( 'Shop Product Per Page', 'independent' ),
					'desc'     => esc_html__( 'This is column settings woocommerce related products per page.', 'independent' ),
					'default'  => '12'
				),
				array(
					'id'       => 'woo-related-ppp',
					'type'     => 'text',
					'title'    => esc_html__( 'Related Product Per Page', 'independent' ),
					'desc'     => esc_html__( 'This is column settings woocommerce related products per page.', 'independent' ),
					'default'  => '3'
				),
			)
		) );
		
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Archive Template', 'independent' ),
			'id'         => 'woo-archive-page',
			'desc'       => esc_html__( 'This is the setting for woocommerce archive page template', 'independent' ),
			'subsection' => true,
			'fields'     => array(
				array(
					'id'       => 'wooarchive-page-template',
					'type'     => 'image_select',
					'title'    => esc_html__( 'Woocommerce Archive Template', 'independent' ),
					'desc'     => esc_html__( 'Choose your current Woocommerce Archive page template.', 'independent' ),
					'options'  => array(
						'no-sidebar' => array(
							'alt' => esc_html__( 'No Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/1.png'
						),
						'right-sidebar' => array(
							'alt' => esc_html__( 'Right Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/2.png'
						),
						'left-sidebar' => array(
							'alt' => esc_html__( 'Left Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/3.png'
						),
						'both-sidebar' => array(
							'alt' => esc_html__( 'Both Sidebar', 'independent' ),
							'img' => get_template_directory_uri() . '/assets/images/page-layouts/4.png'
						)
					),
					'default'  => 'right-sidebar'
				),
				array(
					'id'       => 'wooarchive-left-sidebar',
					'type'     => 'select',
					'title'    => esc_html__( 'Choose Left Sidebar', 'independent' ),
					'desc'     => esc_html__( 'Select widget area for showing woocommerce archive template on left sidebar.', 'independent' ),
					'data'     => 'sidebars',
					'required' 		=> array('wooarchive-page-template', '=', array( 'left-sidebar', 'both-sidebar' ))
				),
				array(
					'id'       => 'wooarchive-right-sidebar',
					'type'     => 'select',
					'title'    => esc_html__( 'Choose Right Sidebar', 'independent' ),
					'desc'     => esc_html__( 'Select widget area for showing woocommerce archive template on right sidebar.', 'independent' ),
					'data'     => 'sidebars',
					'default'  => 'sidebar-1',
					'required' 		=> array('wooarchive-page-template', '=', array( 'right-sidebar', 'both-sidebar' ))
				),
			)
		) );
		
		// Woo Related Slider
		$woo_related_slider = $acf->themeSliders('woo-related');
		Redux::setSection( $opt_name, array(
			'title'      => esc_html__( 'Woo Related Slider', 'independent' ),
			'id'         => 'woo-related-slider',
			'desc'       => esc_html__( 'This is the setting for woocommerce related slider', 'independent' ),
			'subsection' => true,
			'fields'     => $woo_related_slider
		) );
		
	}
	
	//Minifier Tab
	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Minifier', 'independent' ),
		'id'               => 'minifier',
		'desc'             => esc_html__( 'These are minifier general settings of independent theme', 'independent' ),
		'customizer_width' => '400px',
		'icon'             => 'fa fa-file-archive-o'
	) );
			
	Redux::setSection( $opt_name, array(
		'title'      => esc_html__( 'Minifier General', 'independent' ),
		'id'         => 'minifier-general',
		'desc'       => esc_html__( 'This is the setting for minifier general', 'independent' ),
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'js-minify',
				'type'     => 'switch',
				'title'    => esc_html__( 'JS Minify', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable minify js for website.', 'independent' ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
			array(
				'id'       => 'css-minify',
				'type'     => 'switch',
				'title'    => esc_html__( 'CSS Minify', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable minify css for website.', 'independent' ),
				'default'  => 0,
				'on'       => esc_html__( 'Enabled', 'independent' ),
				'off'      => esc_html__( 'Disabled', 'independent' ),
			),
		)
	) );
	
	//Maintenance Tab
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Maintenance', 'independent' ),
        'id'               => 'maintenance',
        'desc'             => esc_html__( 'These are the maintenance settings of Independent theme', 'independent' ),
        'customizer_width' => '400px',
        'icon'             => 'fa fa-sliders'
    ) );
	
	Redux::setSection( $opt_name, array(
		'title'      => esc_html__( 'Maintenance General', 'independent' ),
		'id'         => 'maintenance-general',
		'desc'       => esc_html__( 'This is the setting for maintenance general', 'independent' ),
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'maintenance-mode',
				'type'     => 'switch',
				'title'    => esc_html__( 'Maintenance Mode Option', 'independent' ),
				'subtitle' => esc_html__( 'Enable/Disable maintenance mode.', 'independent' ),
				'default'  => 0,
				'on'       => esc_html__( 'Enable', 'independent' ),
				'off'      => esc_html__( 'Disable', 'independent' ),
			),
			array(
				'id'       => 'maintenance-type',
				'type'     => 'select',
				'title'    => esc_html__( 'Maintenance Type', 'independent' ),
				'desc'     => esc_html__( 'Select maintenance mode page coming soon or maintenance.', 'independent' ),
				'options'  => array(
					'cs'		=> esc_html__( 'Coming Soon Default', 'independent' ),
					'mn'		=> esc_html__( 'Maintenance Default', 'independent' ),
					'cus'		=> esc_html__( 'Custom', 'independent' )
				),
				'required' 		=> array( 'maintenance-mode', '=', 1)
			),
			array(
				'id'       => 'maintenance-custom',
				'type'     => 'select',
				'title'    => esc_html__( 'Maintenance Custom Page', 'independent' ),
				'desc'     => esc_html__( 'Enter service slug for register custom post type.', 'independent' ),
				'data'  => 'pages',
				'required' 		=> array( 'maintenance-type', '=', "cus")
			),
			array(
				'id'       => 'maintenance-phone',
				'type'     => 'text',
				'title'    => esc_html__( 'Phone Number', 'independent' ),
				'desc'     => esc_html__( 'Enter phone number shown on when maintenance mode actived.', 'independent' ),
				'default'  => ''
			),
			array(
				'id'       => 'maintenance-email',
				'type'     => 'text',
				'title'    => esc_html__( 'Email Id', 'independent' ),
				'desc'     => esc_html__( 'Enter email id shown on when maintenance mode actived.', 'independent' ),
				'default'  => ''
			),
			array(
				'id'		=>'maintenance-address',
				'type'		=> 'textarea',
				'title'		=> esc_html__( 'Address', 'independent' ), 
				'desc'		=> esc_html__( 'Place here your address and info.', 'independent' ),
				'validate'	=> 'html_custom',
				'allowed_html'	=> array(
					'a' => array(
					'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array(),
					'p' => array()
				)
			)
		)
	) );
	
    /*
     * <--- END SECTIONS
     */
	
	/*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    add_action( 'redux/loaded', 'remove_demo' );
	/**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }
	