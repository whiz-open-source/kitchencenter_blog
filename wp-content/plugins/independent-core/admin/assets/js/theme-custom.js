(function( $ ) {
 
    "use strict";

	$( document ).ready(function() {

		//Custom Font Delete Event
		$( ".independent-cus-font-del" ).click(function() {
			var font_id = $(this).data('font');
			font_id = font_id.replace("'", "");
			font_id = font_id.replace("'", "");
			var parent_tr = $(this).parents('tr');
			var cur = this;
			
			$( cur ).replaceWith( "<p>" + independent_core_admin_ajax_var.process + "</p>" );
			
			$.ajax({
				type: "post",
				dataType : "json",
				url: independent_core_admin_ajax_var.admin_ajax_url,
				data : {action: "independent_custom_font_del", font_id : font_id, f_nounce: independent_core_admin_ajax_var.font_nonce},
				error: function(data){
					alert(independent_core_admin_ajax_var.font_del_pbm);
				},
				success: function(data){
					if( data.type == "success" ) {
						$( parent_tr ).remove();
					}else{
						alert(data.res);
					}
				}
			});
			return false;
		});
		
		// Star Rating
		if( $("ul.star-rating").length ){

			$("ul.star-rating > li").dblclick(function() {
				var index = $( this ).index();
				var parent = $( this ).parent( "ul.star-rating" );
				var i;
				
				//Reset
				$( parent ).find( "li > span" ).removeClass( "fa-star fa-star-half-o" ).addClass( "fa-star-o" );
				
				if( index != 0 ){
					for( i = 1; i <= index; i++ ){
						$( parent ).find( "li:eq("+ i +") > span" ).removeClass( "fa-star-o fa-star-half-o" ).addClass( "fa-star" );
					}
				}
				
				$( parent ).next( ".independent-meta-rating-value" ).val( index );
				
			});
			
		} // Star rating exists
		
		// Star Rating
		if( $("ul.star-rating").length ){

			$("ul.star-rating > li").click(function() {
				var index = $( this ).index();
				var parent = $( this ).parent( "ul.star-rating" );
				var i;
				
				//Reset
				$( parent ).find( "li > span" ).removeClass( "fa-star fa-star-half-o" ).addClass( "fa-star-o" );
				
				var rat_val = 0;
				if( index != 0 ){
					for( i = 1; i <= index; i++ ){
						if( i == index ) 
							$( parent ).find( "li:eq("+ i +") > span" ).removeClass( "fa-star-o fa-star" ).addClass( "fa-star-half-o" );
						else
							$( parent ).find( "li:eq("+ i +") > span" ).removeClass( "fa-star-o fa-star-half-o" ).addClass( "fa-star" );
					}
					rat_val = index - 0.5;
				}
				
				$( parent ).next( ".independent-meta-rating-value" ).val( rat_val );
				
			});
			
		} // Star rating exists
		
		// Meta Star Rating
		if( $("ul.independent-meta-rating").length ){
		
			$( "ul.independent-meta-rating" ).each(function( index ) {
				var meta_val = $( this ).next( ".independent-meta-rating-value" ).val();
				if( meta_val ){
					//Reset
					$( this ).find( "li > span" ).removeClass( "fa-star fa-star-half-o" ).addClass( "fa-star-o" );
					var index = meta_val;
					
					if( index != 0 ){
						var i;
						for( i = 0.5; i <= index; i+=0.5 ){
							if( i == index && i % 1 == 0.5 ){
								$( this ).find( "li:eq("+ ( i + 0.5 ) +") > span" ).removeClass( "fa-star-o fa-star" ).addClass( "fa-star-half-o" );
							}else{
								$( this ).find( "li:eq("+ i +") > span" ).removeClass( "fa-star-o fa-star-half-o" ).addClass( "fa-star" );
							}
						}
					}
				}
			});

		} // Meta star rating exists
		
		/*Color Picker*/
		var parent = jQuery('body');
		if (jQuery('body').hasClass('widgets-php')){
			parent = jQuery('.widget-liquid-right');
		}
		jQuery(document).ready(function() {
			parent.find('.independent-color-picker').wpColorPicker();
		});
		
		jQuery(document).on('widget-added', function(e, widget){
			widget.find('.independent-color-picker').wpColorPicker();
		});
		
		jQuery(document).on('widget-updated', function(e, widget){
			widget.find('.independent-color-picker').wpColorPicker();
		});
		
		/*Meta Drag and Drop Multi Field*/
		$( document ).on( "click", "ul.img-select > li > img", function() {
			var cur_items = this;
			var parents = $( cur_items ).parents('ul.img-select');
			$( parents ).find('li').removeClass('selected');
			$( cur_items ).parent('li').addClass('selected');
			$( parents ).next('input').val( $( cur_items ).parent('li').data('id') );
		});
		
	});
	
})(jQuery);